package com.flytxt.vision.cloudfunctions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.flytxt.vision.processflow.ProcessFlow;
import com.flytxt.vision.processruntime.exception.ProcessException;

@SpringBootApplication
//@FunctionScan
@ComponentScan(basePackages = "com.flytxt.vision")
public class CloudFunctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudFunctionApplication.class, args);
	}

	

	@Autowired
	ProcessFlow processFlow;

	@Bean
	public Function<Message<Map>, Message<String>> members()
			throws JsonParseException, JsonMappingException, IOException, ProcessException {

		String processId = null;
		try {
			HashMap map = new HashMap<>();
			map.put("tweet", "India has high call drops");
			//processId = processFlow.getProcessByPageId("184531795488548");
			processFlow.startProcess(processId, null, map);
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		return event -> {

			String processIdentifier =null;
			Map payload = event.getPayload();

			try {
				processFlow.startProcess(processIdentifier, null, null);
				// String processId = processflow.getProcessByPageId((String)map.get("pageId"));
//				router.route(payload);

			} catch ( ProcessException e) {
				e.printStackTrace();
			}
			return new GenericMessage<>("");

		};

	}

}
