package com.flytxt.vision.cloudfunctions;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.flytxt.vision.core.exception.VisionException;


@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/lamda")
public class DummyController {
	
	
	


	@RequestMapping(path="/facebook",method=RequestMethod.GET)
	public ResponseEntity<Integer> get (@RequestParam("hub.challenge") Integer  challenge) throws VisionException{		
		
		return new ResponseEntity<>(challenge,HttpStatus.OK);	
		
	}
	
	@RequestMapping(path="/facebook",method=RequestMethod.POST)
	public ResponseEntity<String> post (Map  mapBody) throws VisionException{	
		
		return new ResponseEntity<>("",HttpStatus.OK);		
	}

}
