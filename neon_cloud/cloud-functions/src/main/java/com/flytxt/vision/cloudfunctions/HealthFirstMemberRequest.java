package com.flytxt.vision.cloudfunctions;

import java.io.Serializable;

public class HealthFirstMemberRequest implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String memberId;

	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "memberId " + memberId;
	}

}
