package com.flytxt.vision.cloudfunctions;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.function.adapter.aws.SpringBootApiGatewayRequestHandler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;

public class AwsLambdaHandler extends SpringBootApiGatewayRequestHandler {

	@Override
	public Object handleRequest(APIGatewayProxyRequestEvent event, Context context) {
		super.initialize();

		if ("GET".equalsIgnoreCase(event.getHttpMethod())) {
			Map<String, List<String>> multiValueQueryStringParameters = event.getMultiValueQueryStringParameters();
			String challenge = multiValueQueryStringParameters.get("hub.challenge").get(0);
			return convertOutput(Integer.parseInt(challenge));
		} else {
			return super.handleRequest(event, context);
		}

	}

}
