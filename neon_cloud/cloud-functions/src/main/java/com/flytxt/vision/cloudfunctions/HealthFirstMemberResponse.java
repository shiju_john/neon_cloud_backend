package com.flytxt.vision.cloudfunctions;

public class HealthFirstMemberResponse {
	
	private String memberId;
    private String  coverage="TEST";

   
	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	
}
