package com.flytxt.vision.processruntime.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flytxt.vision.processruntime.exception.ProcessException;

/**
 * @author shiju.john
 */
public class ProcessDefinition implements Process {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String processType;
	private boolean isExecutable ;
	private String processId; 
	private String processName;
	private String processVersion;
	private String inputConnectorId;
	private String tenantId;
	
	
	private List<ItemDefinition> processVariable;
	private List<Task> tasks ;
	private List<SequenceFlow> sequenceFlows;
	
	public ProcessDefinition() {
		setProcessVariable(new ArrayList<ItemDefinition>());
		setTasks(new ArrayList<Task>());
		this.isExecutable =true;
		this.processType ="Private";
	}
	
	/**
	 * @return the processType
	 */
	public String getProcessType() {
		return processType;
	}
	
	
	/**
	 * @param processType the processType to set
	 */
	public void setProcessType(String processType) {
		this.processType = processType;
	}


	/**
	 * @return the isExecutable
	 */
	public boolean isExecutable() {
		return isExecutable;
	}


	/**
	 * @param isExecutable the isExecutable to set
	 */
	public void setExecutable(boolean isExecutable) {
		this.isExecutable = isExecutable;
	}


	

	/**
	 * @return the tasks
	 */
	public List<Task> getTasks() {
		return tasks;
	}

	/**
	 * @param tasks the tasks to set
	 */
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	/**
	 * @return the sequenceFlows
	 */
	public List<SequenceFlow> getSequenceFlows() {
		return sequenceFlows;
	}

	/**
	 * @param sequenceFlows the sequenceFlows to set
	 */
	public void setSequenceFlows(List<SequenceFlow> sequenceFlows) {
		this.sequenceFlows = sequenceFlows;
	}

	

	/**
	 * @param processVariables the processVariables to set
	 */
	public void setProcessVariable(List<ItemDefinition> processVariable) {
		this.processVariable = processVariable;
	}

	
	
	/**
	 * 
	 */
	@Override
	public Map<String,Object> getProcessVariables(){
		Map<String,Object> variables =  new HashMap<>();		
		for (ItemDefinition itemDefinition : this.processVariable) {
			variables.put(itemDefinition.getId(), itemDefinition.getVariableValue());
			
		}
		return variables ;
	}
	
	/**
	 * 
	 * @return
	 * @throws ProcessException
	 */
	public String getStartEvent() throws ProcessException   {
		for(Task task:tasks) {
			if( task.isStartEvent()){
				return task.getTaskId();
			}
		}
		throw new ProcessException("No start event found");
	}

	
	
	
	
	
	/**
	 * 
	 * @param taskId
	 * @return
	 * @throws ProcessException
	 */
	
	@Override
	public Task getTask(String taskId) throws ProcessException {
		for (Task task : tasks) {
			if(task.getTaskId().equalsIgnoreCase(taskId)) {
				return task;
			}			
		}
		throw new  ProcessException("No Task found with Id " + taskId );
	}

	@Override
	public String getId() {
		return getProcessId();
	}

	@Override
	public String getVersion() {
		return this.getProcessVersion();
	}

	/**
	 * @return the processId
	 */
	public String getProcessId() {
		return processId;
	}

	/**
	 * @param processId the processId to set
	 */
	public void setProcessId(String processId) {
		this.processId = processId;
	}

	/**
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * @param processName the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * @return the processVersion
	 */
	public String getProcessVersion() {
		return processVersion;
	}

	/**
	 * @param processVersion the processVersion to set
	 */
	public void setProcessVersion(String processVersion) {
		this.processVersion = processVersion;
	}

	/**
	 * @return the inputConnectorId
	 */
	public String getInputConnectorId() {
		return inputConnectorId;
	}

	/**
	 * @param inputConnectorId the inputConnectorId to set
	 */
	public void setInputConnectorId(String inputConnectorId) {
		this.inputConnectorId = inputConnectorId;
	}

	/**
	 * @return the tenantId
	 */
	@Override
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public List<Task> getAllTask() {	
		return tasks;
	}

	
}
