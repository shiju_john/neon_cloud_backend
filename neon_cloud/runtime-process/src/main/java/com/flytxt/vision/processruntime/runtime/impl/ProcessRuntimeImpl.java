package com.flytxt.vision.processruntime.runtime.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.processruntime.KnowledgeBase;
import com.flytxt.vision.processruntime.entity.ItemDefinition;
import com.flytxt.vision.processruntime.entity.Process;
import com.flytxt.vision.processruntime.entity.ProcessInstance;
import com.flytxt.vision.processruntime.entity.ProcessInstanceImpl;
import com.flytxt.vision.processruntime.entity.SequenceFlow;
import com.flytxt.vision.processruntime.entity.Task;
import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.entity.TaskInstanceImpl;
import com.flytxt.vision.processruntime.exception.ProcessCacheException;
import com.flytxt.vision.processruntime.exception.ProcessException;
import com.flytxt.vision.processruntime.exception.TaskException;
import com.flytxt.vision.processruntime.runtime.KnowledgeRuntime;
import com.flytxt.vision.processruntime.runtime.ProcessRuntime;
import com.flytxt.vision.processruntime.runtime.RuleEvalator;
import com.flytxt.vision.processruntime.runtime.TaskExecutor;
import com.flytxt.vision.processruntime.runtime.event.ProcessEventListener;
import com.flytxt.vision.processruntime.runtime.event.ScriptTask;
import com.flytxt.vision.processruntime.runtime.event.TaskEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author shiju.john
 *
 */

@Component
@Scope(value="singleton")
public class ProcessRuntimeImpl implements ProcessRuntime{
	
	private KnowledgeRuntime knowledgeRuntime;
	private KnowledgeBase knowledgeBase;
	
	private Gson gson = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRuntimeImpl.class);
	
	@Autowired
	public ProcessRuntimeImpl(KnowledgeRuntime knowledgeRuntime,KnowledgeBase knowledgeBase){
		
		this.knowledgeRuntime =  knowledgeRuntime;
		this.knowledgeBase = knowledgeBase;
		gson = new Gson();
	}

	@Override
	public ProcessInstance getProcessInstance(long processInstanceId) {		
		return null;
	} 

	@Override
	public long startProcess(String processId,String version, Map<String, Object> processVariables) throws ProcessException {
		Process process = knowledgeBase.getProcess(processId, version);
		ProcessInstance processInstance = getProcess(process,processVariables); 
		try {	
			
			executeProcessEventListener(processInstance,null,true,false); 							
			String startEvent =  process.getStartEvent();			
			start(startEvent , process, processInstance);				
			executeProcessEventListener(processInstance,null,false,true);
			
		} catch (ProcessException e) {
			LOGGER.error("Error while running the process. "+e.getMessage(),e);			
			executeProcessEventListener(processInstance,e,false,false);
		}catch (Exception e) { 
			LOGGER.error("Error while running the process. "+e.getMessage(),e);			
			executeProcessEventListener(processInstance,e,false,false);
		}
		
		return 0;
		
	}
	
	private void start(String taskId ,Process process,ProcessInstance processInstance) throws ProcessException{
		
		List<Task> nextTasks = this.nextTask(taskId,process,processInstance.getProcessVariables()); 
		if (null!=nextTasks && nextTasks.size()>0) {
			for(Task task : nextTasks) {
				String taskCategory = (String)((Map)task.getTaskProperties()).get("category");
				if(!taskCategory.equalsIgnoreCase("GateWay")) {					
					executeTask(processInstance,task);
				}					
				start(task.getTaskId(),process,processInstance);
			}
		}
	}
	
	private List<Task> nextTask(String sourceId,Process process,Map<String,Object> processVariables) throws ProcessException {
		List<Task> tasks = new ArrayList<>();
		Task sourceTask = process.getTask(sourceId);
		String taskCategory = (String)((Map)sourceTask.getTaskProperties()).get("category");
		if(taskCategory.equalsIgnoreCase("GateWay")) {			
			for (SequenceFlow sequenceFlow :process.getSequenceFlows()) {
				if(sourceId.equalsIgnoreCase(sequenceFlow.getFrom())){
					RuleEvalator ruleEvalator  = knowledgeRuntime.getRuleEvaluator(sequenceFlow.getRuleEvaluvatorName());
					if(ruleEvalator.evaluateRule(sequenceFlow.getFilter(), processVariables)) {
						tasks.add(process.getTask(sequenceFlow.getTo()));		
					}								
				}			
			}
			
		}else {
			for (SequenceFlow sequenceFlow :process.getSequenceFlows()) {
				if(sourceId.equalsIgnoreCase(sequenceFlow.getFrom())){
					tasks.add(process.getTask(sequenceFlow.getTo()));
					break;
				}			
			}
		}
		return tasks;
	
		
	}	
	
	
	/**
	 * 
	 * @param processInstance
	 * @param nextTask
	 * @throws TaskException
	 */
	
	private void executeTask(ProcessInstance processInstance ,Task nextTask) throws TaskException{		
				
		TaskInstance taskInstance = getTaskInstance(nextTask,processInstance.getProcessVariables());
		executeTaskEventListener(taskInstance,processInstance.getProcessVariables(),null,true,false);
		
		this.executeScript(nextTask.getOnEntry(),taskInstance,processInstance.getProcessVariables(),true);
		//Task Executor Start
		TaskExecutor taskExecutor = knowledgeRuntime.getTaskExecutor(nextTask.getTaskType());					
		taskInstance.setTaskOutput(taskExecutor.execute(taskInstance, processInstance.getProcessVariables()));
		this.setTaskOutput(taskInstance,nextTask,processInstance.getProcessVariables());
		////Task Executor  End
		this.executeScript(nextTask.getOnExit(),taskInstance,processInstance.getProcessVariables(),false);
		//onExit start
		executeTaskEventListener(taskInstance,processInstance.getProcessVariables(),null,false,true);	
		
	}
	

	/**
	 * 
	 * @param process
	 * @return
	 */
	private ProcessInstance getProcess(Process process,Map<String,Object> runTimeProcessVariables) {
		if(null==process) {
			LOGGER.error("unable to load the process");
		}
		ProcessInstanceImpl processInstance = new ProcessInstanceImpl(process.getId(),process.getVersion());
		processInstance.setInstanceId(UUID.randomUUID().toString());
		processInstance.setTenantId(process.getTenantId());
		processInstance.setProcessVariables(process.getProcessVariables());
		processInstance.getProcessVariables().put("tenant_id", process.getTenantId());
		if (null!=runTimeProcessVariables) {
			processInstance.getProcessVariables().putAll(runTimeProcessVariables);
		}
		
		return processInstance;
	}

	/**
	 * 
	 * @param taskInstance
	 * @param processVariables
	 * @param exception
	 * @param taskStarted
	 * @param taskCompleted
	 */
	private void executeTaskEventListener(TaskInstance taskInstance,Map<String, Object> processVariables,
			Exception exception, boolean taskStarted,boolean taskCompleted) {
		
		List<TaskEventListener> taskEventListeners = knowledgeRuntime.getTaskEventListeners();
		for(TaskEventListener eventListener :taskEventListeners ) {
			if(taskStarted) {
				eventListener.onStart(taskInstance,processVariables);
			}else if(taskCompleted){
				eventListener.onEnd(taskInstance,processVariables);
			}else {
				eventListener.onException(taskInstance, processVariables, exception);
			}
		}
		
	}
	
	/**
	 * 
	 * @param processInstance
	 * @param exception
	 * @param processStarted
	 * @param processCompletd
	 */
	private void executeProcessEventListener(ProcessInstance processInstance,Exception exception, boolean processStarted,boolean processCompletd) {
		List<ProcessEventListener> processEventListeners = knowledgeRuntime.getProcessEventListeners();
		for(ProcessEventListener eventListener :processEventListeners ) {
			if(processStarted) {
				eventListener.processStarted(processInstance);
			}else if(processCompletd){
				eventListener.processCompleted(processInstance);
			}else {
				eventListener.hasException(processInstance,exception);
			}
		}
	}
	
	/**
	 * 
	 * @param scriptClassName
	 * @param taskInstance
	 * @param processVariables
	 * @param isEntry
	 */
	private void executeScript(String scriptClassName,TaskInstance taskInstance,
			Map<String,Object>processVariables , boolean isEntry) {
		if(null!=scriptClassName) {
			try {
				ScriptTask scriptTask = (ScriptTask) Class.forName(scriptClassName).newInstance();
				if(isEntry) {
					scriptTask.onEntry(taskInstance, processVariables);
				}else {
					scriptTask.onExit(taskInstance, processVariables);
				}
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				
			}
		}
	}

	/**
	 * 
	 * @param taskInstance
	 * @param nextTask
	 * @param processVariables
	 */
	private void setTaskOutput(TaskInstance taskInstance, Task task,
			Map<String, Object> processVariables) {
		if( task.getDataOutput() !=null && taskInstance.getTaskOutput() !=null) {
			Object result =  null!=taskInstance.getTaskOutput() ? taskInstance.getTaskOutput().get("result"):null;
			for (ItemDefinition itemDefinition : task.getDataOutput()) {
				
				
				if (result instanceof Map) {
					Map outPut = (Map) result;
					if(outPut.containsKey(itemDefinition.getId().intern())) {
						processVariables.put(itemDefinition.getId().intern(),
								outPut.get(itemDefinition.getId().intern()));
					}
					
				}else {
					processVariables.put(itemDefinition.getId().intern(), result);
				}
				/*if(taskInstance.getTaskOutput().containsKey(itemDefinition.getId().intern())) {
					processVariables.put(itemDefinition.getId(),
							taskInstance.getTaskOutput().get(itemDefinition.getId().intern()));
				}*/
			}
		}
	}

	/**
	 * 
	 * @param task
	 * @param processVariables
	 * @return
	 */
	private TaskInstance getTaskInstance(Task task,Map<String,Object> processVariables) {
		TaskInstanceImpl instanceImpl = new  TaskInstanceImpl();
		instanceImpl.setTaskId(task.getTaskId());
		instanceImpl.setTaskInstanceId(UUID.randomUUID().toString());
		instanceImpl.setTaskInput(task.getTaskInput(processVariables));		
		String jsonString = gson.toJson(task.getTaskProperties());		
		Map<String, Object> mapCopy  = gson.fromJson(jsonString,new TypeToken<Map<String, Object>>() {}.getType());		
		instanceImpl.setTaskProperties(mapCopy);
		return instanceImpl;
	}

	@Override
	public boolean stopProcess(long processInstanceId) {
		return false;
	}

	@Override
	public boolean hasProcessLoaded(String processId, String version) {
		return knowledgeBase.hasProcessLoaded(processId, version);
	}

	@Override
	public void addProcess(Process process) throws ProcessCacheException {
		knowledgeBase.addProcess(process);	
	}

	

}
