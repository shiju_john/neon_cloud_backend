package com.flytxt.vision.processruntime.entity;

import java.io.Serializable;
/**
 * 
 * @author shiju.john
 *
 */
public class SequenceFlow implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String from;	
	private String to;
	private String fromPosition;
	private String toPosition;
	private Object filter;
	private String ruleEvaluvatorName;
	
	SequenceFlow(){
		ruleEvaluvatorName ="filter";
	}
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}




	/**
	 * @return the fromPosition
	 */
	public String getFromPosition() {
		return fromPosition;
	}


	/**
	 * @param fromPosition the fromPosition to set
	 */
	public void setFromPosition(String fromPosition) {
		this.fromPosition = fromPosition;
	}


	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}


	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}


	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}


	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}


	/**
	 * @return the toPosition
	 */
	public String getToPosition() {
		return toPosition;
	}


	/**
	 * @param toPosition the toPosition to set
	 */
	public void setToPosition(String toPosition) {
		this.toPosition = toPosition;
	}


	/**
	 * @return the filter
	 */
	public Object getFilter() {
		return filter;
	}


	/**
	 * @param filter the filter to set
	 */
	public void setFilter(Object filter) {
		this.filter = filter;
	}


	/**
	 * @return the ruleEvaluvatorName
	 */
	public String getRuleEvaluvatorName() {
		return ruleEvaluvatorName;
	}


	/**
	 * @param ruleEvaluvatorName the ruleEvaluvatorName to set
	 */
	public void setRuleEvaluvatorName(String ruleEvaluvatorName) {
		this.ruleEvaluvatorName = ruleEvaluvatorName;
	}

}
