package com.flytxt.vision.processruntime.entity;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 
 * @author shiju.john
 *
 */
public class ItemDefinition implements Serializable{
	
	
			
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String label;
	private String valueType;    //processVariable  // 
	private Object value;
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Serializable value) {
		this.value = value;
	}
	
	
	public Object getVariableValue(){

		if (!"PV".equalsIgnoreCase(this.getValueType())) {
	
			switch (this.getValueType()) {			
				case "Object":
					return value;
				
				case "String":
					return value;
					
				case "Integer":
					return null!=value?Integer.valueOf(""+value): null;				
				
				case "Double":
					return null!=value? Double.valueOf(""+value):null;
				case "Long":
					return null!=value? Long.valueOf(""+value): null;
				
				case "Float":
					return null!=value? Float.valueOf(""+value):null;
					
				case "BigDecimal":
					return null!=value? new BigDecimal(""+value):null;
					
				default:
					return value;
					
			}			
		}
		return null;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the valueType
	 */
	public String getValueType() {
		return valueType;
	}
	/**
	 * @param valueType the valueType to set
	 */
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

}
