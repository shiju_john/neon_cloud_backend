package com.flytxt.vision.processruntime.entity;

import java.util.Map;
import java.util.Set;

/**
 * 
 * @author shiju.john
 *
 */
@SuppressWarnings("rawtypes")
public class TaskInstanceImpl implements TaskInstance {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String taskInstanceId;
	private String taskId;
	private Map<String,Object> taskInput;	
	private Map taskProperties;	
	private Map<String,Object> taskOutput;
	
	
	
	/**
	 * @return the taskInput
	 */
	public Map<String,Object> getTaskInput() {
		return taskInput;
	}
	
	
	/**
	 * @param taskInput the taskInput to set
	 */
	public void setTaskInput(Map<String,Object> taskInput) {
		this.taskInput = taskInput;
	}
	
	
	/**
	 * @return the taskInstanceId
	 */
	public String getTaskInstanceId() {
		return taskInstanceId;
	}
	
	/**
	 * @param taskInstanceId the taskInstanceId to set
	 */
	public void setTaskInstanceId(String taskInstanceId) {
		this.taskInstanceId = taskInstanceId;
	}
	
	
	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}
	
	
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	/**
	 * @return the taskInput
	 */
	public Object getTaskInputValue(String inputVariable) {		
		return taskInput.get(inputVariable);
	}
	
	public Set<String> getTaskInputKeys() {		
		return taskInput.keySet();
	}
	
	/**
	 * 
	 * @param inputVariable
	 * @param value
	 * @return
	 */
	public Object setTaskInputValue(String inputVariable,Object value) {		
		return taskInput.put(inputVariable,value);
	}


	/**
	 * @return the taskOutput
	 */
	public Map<String,Object> getTaskOutput() {
		return taskOutput;
	}


	/**
	 * @param taskOutput the taskOutput to set
	 */
	public void setTaskOutput(Map<String,Object> taskOutput) {
		this.taskOutput = taskOutput;
	}


	/**
	 * @return the taskProperties
	 */
	public Map getTaskProperties() {
		return taskProperties;
	}


	/**
	 * @param taskProperties the taskProperties to set
	 */
	public void setTaskProperties(Map taskProperties) {
		this.taskProperties = taskProperties;
	}


	@Override
	public Object getTaskProperty(String popertyName) {
		return this.getTaskProperties().get(popertyName);
	}
	

}
