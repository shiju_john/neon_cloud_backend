package com.flytxt.vision.processruntime.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.flytxt.vision.processruntime.exception.ProcessException;

/**
 * 
 * @author shiju.john
 *
 */
public interface Process  extends Serializable{
	
	/**
	 * 
	 * @return
	 */
	public String getId() ;
	
	/**
	 * 
	 * @return
	 */
	public String getVersion() ;
	
	/**
	 * 
	 * @return
	 */
	public String getTenantId();
	
	/**
	 * 
	 * @return
	 */
	public Map<String,Object> getProcessVariables();

	
	/**
	 * 
	 * @return
	 */
	public String getStartEvent() throws ProcessException;
	
	
	/**
	 * 
	 * @param startEvent
	 * @throws ProcessException 
	 */
	public Task getTask(String taskId) throws ProcessException;
	
	
	/**
	 * 
	 * @return
	 */
	public List<Task> getAllTask();
	
	
	public List<SequenceFlow> getSequenceFlows();

}
