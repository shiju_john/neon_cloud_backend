package com.flytxt.vision.processruntime.exception;

public class TaskException extends ProcessException {

	public TaskException(String message) {
		super(message);
	}
	
	public TaskException(String message,Exception e) {
		super(message,e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
