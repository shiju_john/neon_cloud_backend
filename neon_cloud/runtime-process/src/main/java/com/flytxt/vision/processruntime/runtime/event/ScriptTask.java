package com.flytxt.vision.processruntime.runtime.event;

import java.io.Serializable;
import java.util.Map;

import com.flytxt.vision.processruntime.entity.TaskInstance;

/**
 * 
 * @author shiju.john
 *
 */
public interface ScriptTask extends Serializable{
	
	
	/**
	 * 
	 * @param taskInstance
	 * @param processVariables
	 */
	public void onEntry(TaskInstance taskInstance,Map<String,Object> processVariables);
	
	
	/**
	 * 
	 * @param taskInstance
	 * @param processVariables
	 */
	public void onExit(TaskInstance taskInstance,Map<String,Object> processVariables);

}
