package com.flytxt.vision.processruntime;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.processruntime.entity.Process;
import com.flytxt.vision.processruntime.exception.ProcessCacheException;
import com.flytxt.vision.processruntime.exception.ProcessNotFoundException;
/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("singleton")
public class KnowledgeBase {
	
	private static Map<String , Map<String,Process>> processDefinitions;
	
	
	public KnowledgeBase(){
		processDefinitions = new HashMap<>();
	}
	
	/**
	 * 
	 * @param process
	 * @throws ProcessCacheException 
	 */
	public void addProcess(Process process) throws ProcessCacheException {
		Map<String,Process> processMap =  new  HashMap<>();
		if (processDefinitions.containsKey(process.getId().intern())) {
			processMap = processDefinitions.get(process.getId());
		}
		
		if (processMap.containsKey(process.getVersion().intern())) {
			throw new ProcessCacheException("Process version already exist");
		}
		processMap.put(process.getVersion().intern(), process);
		processDefinitions.put(process.getId().intern(), processMap);
	}
	
	/**
	 * 
	 * @param processId
	 * @return
	 */
	public Process getProcess(String processId,String version) throws ProcessNotFoundException{
		Map<String, Process> process = processDefinitions.get(processId);
		if (version==null || version.length()<1) {
			double versionValue,higherVersion=-1; 
			Process latestProcess = null;
			for(String processVersion : process.keySet()) {
				versionValue =  Double.valueOf(processVersion);
				if(versionValue>=higherVersion) {
					latestProcess = process.get(processVersion);
					higherVersion= versionValue;
				}
				
			}
			return latestProcess;
		}
		return process.get(version);
	}
	
		
	public void addProcess(String processJson) {
		
	}
	
	/**
	 * 
	 * @param processId
	 * @param version
	 * @return
	 */
	public boolean hasProcessLoaded(String processId,String version){	
		if( null!=version) {
			return  processDefinitions.containsKey(processId.intern()) ? 
					processDefinitions.get(processId.intern()).containsKey(version.intern()):false;			
		}else {
			return  processDefinitions.containsKey(processId.intern());
			
		}
		
	}

}
