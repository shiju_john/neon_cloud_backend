package com.flytxt.vision.processruntime.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author shiju.john
 *
 */
public class ProcessInstanceImpl implements ProcessInstance{
	
	
	private String processId;
	private String processVersion;
	private Map<String,Object> processVariables ;
	private String instanceId;
	private String status;
	private String tenantId;
	
	public ProcessInstanceImpl(String processId, String version){
		setProcessVariables(new HashMap<>());
		this.processId = processId;
		this.processVersion = version;
	}
	

	
	@Override
	public String getProcessInstanceId() {		
		return instanceId;
	}


	/**
	 * @return the processId
	 */
	public String getProcessId() {
		return processId;
	}


	/**
	 * @param processId the processId to set
	 */
	public void setProcessId(String processId) {
		this.processId = processId;
	}


	/**
	 * @return the processVersion
	 */
	public String getProcessVersion() {
		return processVersion;
	}


	/**
	 * @param processVersion the processVersion to set
	 */
	public void setProcessVersion(String processVersion) {
		this.processVersion = processVersion;
	}


	/**
	 * @return the processVariables
	 */
	@Override
	public Map<String,Object> getProcessVariables() {
		return processVariables;
	}


	/**
	 * @param processVariables the processVariables to set
	 */
	public void setProcessVariables(Map<String,Object> processVariables) {
		this.processVariables = processVariables;
	}


	public void setInstanceId(byte[] bytes) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}


	/**
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}



	@Override
	public String getProcessStatus() {
		return status;
	}



	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}



	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
