package com.flytxt.vision.processruntime.runtime.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flytxt.vision.processruntime.runtime.KnowledgeRuntime;
import com.flytxt.vision.processruntime.runtime.RuleEvalator;
import com.flytxt.vision.processruntime.runtime.TaskExecutor;
import com.flytxt.vision.processruntime.runtime.event.ProcessEventListener;
import com.flytxt.vision.processruntime.runtime.event.TaskEventListener;
import com.flytxt.vision.processruntime.runtime.log.ProcessInstanceLog;
import com.flytxt.vision.processruntime.runtime.log.TaskInstanceLog;
/**
 * 
 * @author shiju.john
 *
 */


public class KnowledgeRuntimeImpl implements KnowledgeRuntime{
	
	private static List<ProcessEventListener>  processEventListener = new ArrayList<ProcessEventListener>();
	private static List<TaskEventListener>  taskEventListener = new ArrayList<TaskEventListener>();
	private static List<ProcessInstanceLog>  processInstanceLogs = new ArrayList<ProcessInstanceLog>();
	private static List<TaskInstanceLog>  taskInstanceLogs = new ArrayList<TaskInstanceLog>();
	
	private static Map<String,RuleEvalator>  ruleEvalators = new HashMap<>();
	
	private static Map<String,TaskExecutor> taskExecutors = new HashMap<String, TaskExecutor>(8);


	
	@Override
	public void addProcessEventListener(ProcessEventListener eventListener) {
		processEventListener.add(eventListener);		
	}

	@Override
	public void addTaskEventListener(TaskEventListener eventListener) {
		taskEventListener.add(eventListener);		
	}

	@Override
	public void addProcessLog(ProcessInstanceLog instanceLog) {
		processInstanceLogs.add(instanceLog);
		
	}

	@Override
	public void addTaskLog(TaskInstanceLog instanceLog) {
		taskInstanceLogs.add(instanceLog);
		
	}

	@Override
	public void addTaskExcecutor(String type, TaskExecutor taskExecutor) {
		taskExecutors.put(type, taskExecutor);
	}

	@Override
	public TaskExecutor getTaskExecutor(String type) {
		return taskExecutors.get(type);
	}

	@Override
	public List<ProcessEventListener> getProcessEventListeners() {
		return processEventListener;
	}

	@Override
	public List<TaskEventListener> getTaskEventListeners() {
		return taskEventListener;
	}


	@Override
	public RuleEvalator getRuleEvaluator(String name) {
			return ruleEvalators.get(name);
	}


	@Override
	public void addRuleEvaluator(String name, RuleEvalator evaluator) {
		ruleEvalators.put(name, evaluator);
		
	}

}
