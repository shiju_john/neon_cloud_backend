package com.flytxt.vision.processruntime.runtime;

import java.util.List;

import com.flytxt.vision.processruntime.runtime.event.ProcessEventListener;
import com.flytxt.vision.processruntime.runtime.event.TaskEventListener;
import com.flytxt.vision.processruntime.runtime.log.ProcessInstanceLog;
import com.flytxt.vision.processruntime.runtime.log.TaskInstanceLog;

/**
 * 
 * @author shiju.john
 *
 */
public interface KnowledgeRuntime {
	
	/**
	 * 
	 * @param eventListener
	 */
	public void addProcessEventListener(ProcessEventListener eventListener);
	
	
	/**
	 * 
	 * @param eventListener
	 */
	public void addTaskEventListener(TaskEventListener eventListener);
	
	
	/**
	 * 
	 * @param instanceLog
	 */
	public void addProcessLog(ProcessInstanceLog instanceLog);
	
	
	/**
	 * 
	 * @param instanceLog
	 */
	public void addTaskLog(TaskInstanceLog instanceLog);
	
	
	/**
	 * 
	 * @param type
	 * @param taskExecutor
	 */
	public void addTaskExcecutor(String type,TaskExecutor taskExecutor);
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public TaskExecutor getTaskExecutor(String type) ;
	
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public RuleEvalator getRuleEvaluator(String name);
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public void addRuleEvaluator(String name,RuleEvalator evaluator);
	
	/***
	 * 
	 * @param eventListener
	 */
	public List<ProcessEventListener> getProcessEventListeners();
	
	/**
	 * 
	 * @return
	 */
	public List<TaskEventListener> getTaskEventListeners();

}
