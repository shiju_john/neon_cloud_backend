package com.flytxt.vision.processruntime.exception;

/**
 * 
 * @author shiju.john
 *
 */
public class RuleException  extends ProcessException{

	public RuleException(String message) {
		super(message);
	}

	public RuleException(String localizedMessage, Exception e) {
		super(localizedMessage,e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
