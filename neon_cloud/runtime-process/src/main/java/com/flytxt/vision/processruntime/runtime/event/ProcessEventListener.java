package com.flytxt.vision.processruntime.runtime.event;

import java.util.Map;

import com.flytxt.vision.processruntime.entity.ProcessInstance;
/**
 * 
 * @author shiju.john
 *
 */
public interface ProcessEventListener {
	
	/**
	 * 
	 * @param processInstance
	 */
	void processStarted(ProcessInstance processInstance);
	
	/**
	 * 
	 * @param processInstance
	 */
	void processCompleted(ProcessInstance processInstance);
	
	/**
	 * 
	 * @param processId
	 * @param processInstanceId
	 * @param processVariables
	 */
	void afterProcessExited(String processId,long processInstanceId,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param instance
	 * @param exception
	 */
	void hasException(ProcessInstance instance,Exception exception);	

}
