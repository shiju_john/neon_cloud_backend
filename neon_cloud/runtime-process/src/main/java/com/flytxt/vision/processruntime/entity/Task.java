package com.flytxt.vision.processruntime.entity;

import java.io.Serializable;
import java.util.HashMap;
/**
 * 
 * @author fly
 *
 */
import java.util.List;
import java.util.Map;

/**
 * 
 * @author shiju.john
 *
 */
public class Task implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String taskType;
	private String taskName;
	private String taskId;
	
	private Object taskProperties;
	
	private String onEntry;
	private String onExit;
	
	private boolean startEvent;
	
	private List<ItemDefinition> dataInput;
	private List<ItemDefinition> dataOutput;
	/**
	 * @return the taskType
	 */
	public String getTaskType() {
		return taskType;
	}
	/**
	 * @param taskType the taskType to set
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	/**
	 * @return the startEvent
	 */
	public boolean isStartEvent() {
		return startEvent;
	}
	/**
	 * @param startEvent the startEvent to set
	 */
	public void setStartEvent(boolean startEvent) {
		this.startEvent = startEvent;
	}
	/**
	 * @return the dataInput
	 */
	public List<ItemDefinition> getDataInput() {
		return dataInput;
	}
	/**
	 * @param dataInput the dataInput to set
	 */
	public void setDataInput(List<ItemDefinition> dataInput) {
		this.dataInput = dataInput;
	}
	/**
	 * @return the dataOutput
	 */
	public List<ItemDefinition> getDataOutput() {
		return dataOutput;
	}
	/**
	 * @param dataOutput the dataOutput to set
	 */
	public void setDataOutput(List<ItemDefinition> dataOutput) {
		this.dataOutput = dataOutput;
	}
	
	/**
	 * 
	 * @param processVariables
	 * @return
	 */
	public Map<String,Object> getTaskInput(Map<String ,Object> processVariables){		
		Map<String,Object> variables =  new HashMap<>();
		if (null!=this.dataInput) {
			for (ItemDefinition itemDefinition : this.dataInput) {
				if("PV".equalsIgnoreCase(itemDefinition.getValueType())) {
					variables.put(itemDefinition.getId(), processVariables.get(itemDefinition.getValue()));
				}else {
					variables.put(itemDefinition.getId(), itemDefinition.getVariableValue());
				}				
			}
		}
		return variables ;	
	}
	/**
	 * @return the onEntry
	 */
	public String getOnEntry() {
		return onEntry;
	}
	/**
	 * @param onEntry the onEntry to set
	 */
	public void setOnEntry(String onEntry) {
		this.onEntry = onEntry;
	}
	/**
	 * @return the onExit
	 */
	public String getOnExit() {
		return onExit;
	}
	/**
	 * @param onExit the onExit to set
	 */
	public void setOnExit(String onExit) {
		this.onExit = onExit;
	}
	/**
	 * @return the taskProperties
	 */
	public Object getTaskProperties() {
		return taskProperties;
	}
	/**
	 * @param taskProperties the taskProperties to set
	 */
	public void setTaskProperties(Object taskProperties) {
		this.taskProperties = taskProperties;
	}
	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	

	
	

}
