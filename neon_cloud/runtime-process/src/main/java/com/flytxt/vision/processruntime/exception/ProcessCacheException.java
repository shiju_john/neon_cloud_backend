package com.flytxt.vision.processruntime.exception;

/**
 * 
 * @author shiju.john
 *
 */
public class ProcessCacheException extends ProcessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProcessCacheException(String message){
		super(message);
		
	}

	

}
