package com.flytxt.vision.processruntime.runtime;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.flytxt.vision.processruntime.entity.ProcessInstance;
import com.flytxt.vision.processruntime.exception.ProcessCacheException;
import com.flytxt.vision.processruntime.exception.ProcessException;

/**
 * 
 * @author shiju.john
 *
 */
@Component
public interface ProcessRuntime {
	
	/**
	 * 
	 * @param processInstanceId
	 * @return
	 */
	ProcessInstance getProcessInstance(long processInstanceId);
	
	/**
	 * 
	 * @param processId
	 * @param processVariables
	 * @return
	 */
	long startProcess(String processId,String version ,Map<String,Object> processVariables)  throws ProcessException;
	
	/**
	 * 
	 * @param processInstanceId
	 * @return
	 */
	boolean stopProcess(long processInstanceId);
	
	
	/**
	 * 
	 * @param processId
	 * @param version
	 * @return
	 */
	public boolean hasProcessLoaded(String processId,String version);

	/**
	 * 
	 * @param convertToProcess
	 * @throws ProcessCacheException 
	 */
	void addProcess(com.flytxt.vision.processruntime.entity.Process process) throws ProcessCacheException;
		

}
