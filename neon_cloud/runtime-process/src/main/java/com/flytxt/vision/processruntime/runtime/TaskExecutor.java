package com.flytxt.vision.processruntime.runtime;

import java.util.Map;

import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.exception.TaskException;
/**
 * 
 * @author shiju.john
 *
 */
public interface TaskExecutor {
	
	/**
	 * 
	 * @param instance
	 * @param processVariables
	 * @throws TaskException 
	 */
	public Map<String,Object> execute(TaskInstance instance,Map<String,Object> processVariables) throws TaskException;

}
