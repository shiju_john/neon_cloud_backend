package com.flytxt.vision.processruntime.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author shiju.john
 *
 */
public interface TaskInstance extends Serializable{
	
	
	/**
	 * 
	 * @return
	 */
	public String getTaskInstanceId();
	
	
	/**
	 * 
	 * @return
	 */
	public String getTaskId() ;
	
	/**
	 * 
	 * @param inputVariable
	 * @return
	 */
	public Object getTaskInputValue(String inputVariable) ;
	
	
	public Object getTaskProperty(String popertyName) ;
	
	/**
	 * 
	 * @param taskOutput
	 */
	public void setTaskOutput(Map<String,Object> taskOutput);
	
	
	/**
	 * 
	 * @return
	 */
	
	public Map<String,Object> getTaskOutput();
	
	/***
	 * 
	 * @return
	 */
	public Set<String> getTaskInputKeys() ;

}
