package com.flytxt.vision.processruntime.entity;

import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;
/**
 * 
 * @author shiju.john
 *
 */
public interface ProcessInstance {
	
	
	/**
	 * 
	 * @return
	 */
	@XmlTransient
	public String getProcessStatus();
	
	
	public String getTenantId();
	
	/**
	 * 
	 * @return
	 */
	public String getProcessInstanceId();
	
	
	/**
	 * 
	 * @return
	 */
	public String getProcessId();
	
	/**
	 * 
	 * @return
	 */
	public Map<String,Object> getProcessVariables();

}
