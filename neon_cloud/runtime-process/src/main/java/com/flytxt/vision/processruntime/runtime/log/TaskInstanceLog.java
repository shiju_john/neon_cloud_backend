package com.flytxt.vision.processruntime.runtime.log;

import java.util.Map;

import com.flytxt.vision.processruntime.entity.TaskInstance;

public interface TaskInstanceLog {
	
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 */
	void onStart(long processInstanceId,String processId,TaskInstance instance,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 */
	void onStop(long processInstanceId,String processId,TaskInstance instance,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 * @param exception
	 */
	void onException(long processInstanceId,String processId,TaskInstance instance,Map<String, Object> processVariables,Exception exception);


}
