package com.flytxt.vision.processruntime.exception;


/**
 * 
 * @author shiju.john
 *
 */
public class ProcessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProcessException(String message){
		super(message);
		
	}

	public ProcessException(String message, Exception e) {
		super(message,e);
	}
	
	

}
