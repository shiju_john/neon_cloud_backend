package com.flytxt.vision.processruntime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.flytxt.vision.processruntime.runtime.KnowledgeRuntime;
import com.flytxt.vision.processruntime.runtime.impl.KnowledgeRuntimeImpl;
/**
 * 
 * @author shiju.john
 *
 */
@Configuration
public class BeanConfiguration {
	
	@Bean
	@Scope("singleton")
	public KnowledgeBase  getKnowledgeBase(){
		return new KnowledgeBase();
	}
	
	
	@Bean
	@Scope("singleton")
	public KnowledgeRuntime getKnowledgeRuntime() {
		return new KnowledgeRuntimeImpl();
	} 
	
	
//	@Bean
//	@Scope("singleton")
//	public ProcessRuntime getProcessRuntime() {
//		return new ProcessRuntimeImpl();
//	} 


}
