package com.flytxt.vision.processruntime.exception;
/**
 * 
 * @author shiju.john
 *
 */
public class ProcessNotFoundException extends ProcessException {

	public ProcessNotFoundException(String message) {
		super(message);
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
