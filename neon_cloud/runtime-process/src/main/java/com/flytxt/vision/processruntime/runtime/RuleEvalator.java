package com.flytxt.vision.processruntime.runtime;

import java.util.Map;

import com.flytxt.vision.processruntime.exception.RuleException;

/**
 * 
 * @author shiju.john
 *
 */
public interface RuleEvalator {
	
	/**
	 * 
	 * @param filterObject
	 * @param processVariables
	 * @return
	 */
	
	public boolean evaluateRule(Object ruleObject,Map<String,Object> processVariables) throws RuleException;

}
