package com.flytxt.vision.processruntime.runtime.event;

import java.util.Map;

import com.flytxt.vision.processruntime.entity.TaskInstance;

public interface TaskEventListener {
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 */
	void onStart(TaskInstance instance,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 */
	void onEnd(TaskInstance instance,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processInstanceId
	 * @param processId
	 * @param instance
	 * @param processVariables
	 * @param exception
	 */
	void onException(TaskInstance instance,Map<String, Object> processVariables,Exception exception);


}
