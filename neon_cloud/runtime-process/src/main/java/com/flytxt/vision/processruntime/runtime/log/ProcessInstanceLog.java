package com.flytxt.vision.processruntime.runtime.log;

import java.util.Map;

public interface ProcessInstanceLog {
	
	/***
	 * 
	 * @param processId
	 * @param processInstanceId
	 * @param processVariables
	 */
	void processStarted(String processId,long processInstanceId,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processId
	 * @param processInstanceId
	 * @param processVariables
	 */
	void processCompleted(String processId,long processInstanceId,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processId
	 * @param processInstanceId
	 * @param processVariables
	 */
	void afterProcessExited(String processId,long processInstanceId,Map<String, Object> processVariables);
	
	/**
	 * 
	 * @param processId
	 * @param processInstanceId
	 * @param processVariables
	 */
	void hasException(String processId,long processInstanceId,Map<String, Object> processVariables);	

}
