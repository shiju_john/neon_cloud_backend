package com.flytxt.aws.entity;

import java.io.Serializable;
/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public class AwsDataFilter<T> implements Serializable{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Filter filterObj;
	private Page pages;
	private String mapping;
	private String tenantId;
	private String [] fields; // scan on particular fields
	
	private T entity;
	
	
	
	/**
	 * @return the filterObj
	 */
	public Filter getFilterObj() {
		return filterObj;
	}
	
	
	/**
	 * @param filterObj the filterObj to set
	 */
	public void setFilterObj(Filter filterObj) {
		this.filterObj = filterObj;
	}
	
	
	/**
	 * @return the pages
	 */
	public Page getPages() {
		return pages;
	}
	
	
	/**
	 * @param pages the pages to set
	 */
	public void setPages(Page pages) {
		this.pages = pages;
	}
	
	
	/**
	 * @return the mapping
	 */
	public String getMapping() {
		return mapping;
	}
	
	
	/**
	 * @param mapping the mapping to set
	 */
	public void setMapping(String mapping) {
		this.mapping = mapping;
	}


	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
		
	}
	
	public String  getTenantId() {
		return this.tenantId;
		
	}


	/**
	 * @return the entity
	 */
	public T getEntity() {
		return entity;
	}


	/**
	 * @param entity the entity to set
	 */
	public void setEntity(T entity) {
		this.entity = entity;
	}
	
	public AwsDataFilter<T>  withPages(Page pages){
		this.pages= pages;
		return this;
	}
	
	public AwsDataFilter<T>  withFilter(Filter filter){
		this.filterObj= filter;
		return this;
	}


	/**
	 * @return the fields
	 */
	public String [] getFields() {
		return fields;
	}


	/**
	 * @param fields the fields to set
	 */
	public void setFields(String [] fields) {
		this.fields = fields;
	}


	public AwsDataFilter<T> withFields(String[] fields) {
		this.fields=fields;
		return this;
	}	

}
