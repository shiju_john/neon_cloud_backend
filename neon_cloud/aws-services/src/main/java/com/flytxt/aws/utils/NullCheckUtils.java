package com.flytxt.aws.utils;

public class NullCheckUtils {
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNotNull(String value) {
		return value!=null && value!="" && value.length()>0;
	}
	

}
