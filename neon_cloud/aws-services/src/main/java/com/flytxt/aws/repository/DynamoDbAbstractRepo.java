package com.flytxt.aws.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.flytxt.aws.entity.Filter;
import com.flytxt.aws.entity.FilterRules;
import com.flytxt.aws.entity.Page;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.utils.NullCheckUtils;

/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public abstract class DynamoDbAbstractRepo<T> implements DynamoDbRepository<T> {

	protected static final String HASH_KEY = "hashKey";
	protected static final String VERSION = "version";

	protected UpdateItemRequest getUpdateItemRequest(Map<String, Object> attributes, String tableName) {

		Map<String, AttributeValue> key = new HashMap<>();
		key.put(HASH_KEY, new AttributeValue().withS((String) attributes.get(HASH_KEY)));
		UpdateItemRequest updateItemRequest = new UpdateItemRequest().withTableName(tableName).withKey(key);

		for (Entry<String, Object> item : attributes.entrySet()) {
			if (VERSION.equals(item.getKey())) {
				updateItemRequest.addAttributeUpdatesEntry(item.getKey(),
						new AttributeValueUpdate().withValue(new AttributeValue().withN((String) item.getValue()))
								.withAction(AttributeAction.ADD));
			} else if (!HASH_KEY.equalsIgnoreCase(item.getKey())) {
				updateItemRequest.addAttributeUpdatesEntry(item.getKey(), getupdateAttribute(item.getValue()));
			}
		}

		if (null == (String) attributes.get(VERSION)) {
			updateItemRequest.addAttributeUpdatesEntry(VERSION, new AttributeValueUpdate()
					.withValue(new AttributeValue().withN("1")).withAction(AttributeAction.ADD));
		}

		return updateItemRequest;
		// .addExpectedEntry("fullname",new ExpectedAttributeValue().withValue(new
		// AttributeValue().withS(prefix)).withComparisonOperator(ComparisonOperator.BEGINS_WITH));

	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	protected AttributeValueUpdate getupdateAttribute(Object value) {
		AttributeValueUpdate attribute = new AttributeValueUpdate().withAction(AttributeAction.PUT);
		attribute.withValue(new AttributeValue().withS((String) value));
		return attribute;

	}
	
	/**
	 * @param data
	 * @param metaMapping
	 * @return
	 */
	protected Map<String, AttributeValue> getPutItems(Map<String, Object> data, List<Map> mappings) {
		Map<String, AttributeValue> attributeValues = new HashMap<>();
		AttributeValue attributeValue = new AttributeValue();
		attributeValue.setS(getHashKey());
		attributeValues.put(HASH_KEY, attributeValue);
		for (Entry<String, Object> item : data.entrySet()) {
			attributeValues.put(item.getKey(), getAttrValue(item.getValue(),getDataType(mappings, item.getKey())));
		}
		attributeValues.put(VERSION, new AttributeValue().withN("1"));
		return attributeValues;
	
	}
	
	/**
	 * 
	 * @param mappings
	 * @param key
	 * @return
	 */
	private String getDataType(List<Map> mappings,String key){		
		for (Map entry : mappings) {
			if (entry.get("destCol").equals(key)) {
				return (String) entry.get("datatype");
			}
		}
		return null;		
	}
	
	
	/**
	 * 
	 * @param item
	 * @return
	 */
	protected Map<String, Object> getTableEntityColumnValue(Map<String, AttributeValue> item) {
		Map<String, Object> columnValues = new HashMap<>(item.size());
		for (Entry<String, AttributeValue> entry : item.entrySet()) {
			columnValues.put(entry.getKey(), getValueFromAttribute(entry.getValue()));
		}
		return columnValues;
	}
	
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	protected Object getValueFromAttribute(AttributeValue value) {
		if(null!=value ) {
			if (value.getS() != null)
				return value.getS();
			if (value.getN() != null)
				return value.getN();
			if (value.getB() != null)
				return value.getB();
			if (value.getSS() != null)
				return value.getSS();
			if (value.getNS() != null)
				return value.getNS();
			if (value.getBS() != null)
				return value.getBS();
			if (value.getM() != null)
				return value.getM();
			if (value.getL() != null)
				return value.getL();
			if (value.getBOOL() != null)
				return value.getBOOL();
			if (value.getNULL() != null)
				return value.getNULL();
			}
		return null;

	}

	protected AttributeValue getAttributeValue(Object value) {

		AttributeValue attributeValue = new AttributeValue();
		if (null == value) {
			attributeValue.setNULL(true);
		} else {
			attributeValue.setS("" + value);
		}
		return attributeValue;
	}

	
	

	protected TableEntity getTableEntity(List<Map<String, AttributeValue>> items, String tableName) {
		final TableEntity result = new TableEntity();
		result.setTableName(tableName);
		if (null != items && items.size()>0) {
			for (Map<String, AttributeValue> item : items) {
				result.addRow(getTableEntityColumnValue(item));
			}
		}
		return result;
	}

	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	protected Map<String, AttributeValue> getMapAttributeValue(String key, String value) {
		Map<String, AttributeValue> attrMapValue = new HashMap<>(2);
		attrMapValue.put(key, new AttributeValue().withS(value));
		return attrMapValue;
	}

	/**
	 * 
	 * @param page
	 * @param scanRequest
	 */
	public ScanRequest setPagination(Page page, ScanRequest scanRequest) {
		if (null != page && validatePageRequest(page)) {
			scanRequest.withLimit(page.getPageSize());
			 if(NullCheckUtils.isNotNull(page.getLastKey())) {
				scanRequest.withExclusiveStartKey(getMapAttributeValue(HASH_KEY, page.getLastKey()));				
			}
		}
		return scanRequest;

	}

	/**
	 * 
	 * @param page
	 * @return
	 */
	private boolean validatePageRequest(Page page) {
		if (page.getPageSize() < 1 || page.getPageSize() >= Integer.MAX_VALUE) {
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param page
	 * @param scanResult
	 * @return
	 */
	protected Page setPageDetailsFromResult(Page page, ScanResult scanResult) {
		if(null!= page){			
			if(null!=scanResult.getLastEvaluatedKey()) {
				page.setLastKey((String)getValueFromAttribute(scanResult.getLastEvaluatedKey().get(HASH_KEY)));
				page.setHasNext(true);
				page.addToIndexKey(page.getLastKey());
			}else {
				page.setLastKey(null);
				page.setHasNext(false);
			}
		}
		return page;
		
	}
	
	/**
	 * 
	 * @param filter
	 * @return
	 */
	protected  Map<String, Object> getFilterCondition(Filter filter) {
		
		return getFilterMap(filter,1);
		
			
			
	}
	/**
	 * 
	 * @param filter
	 * @param count  : count is used for adding an index in the attribute vale identifier. 
	 * 		
	 * @return
	 */
	private Map<String, Object> getFilterMap(Filter filter, int count) {
		if(filter!=null && NullCheckUtils.isNotNull(filter.getCondition()) && null!=filter.getRules()){
			Map<String, AttributeValue> attrValues = new HashMap<String, AttributeValue>();
			StringBuilder  condition = new StringBuilder();
			int index=1;
			for(FilterRules filterRules : filter.getRules()) {			
				attrValues.put(":"+filterRules.getKey()+"_"+count,getAttrValue(filterRules.getValue(),filterRules.getType()));
				String conditionString = DynamoDbOperators.get(filterRules.getOperator()).getCondition(filterRules, count);				
				condition.append(" "+ (filter.getRules().size()==index++? conditionString :conditionString + " "+filter.getCondition().toLowerCase()));
				count++;					
			}
			if(null!=filter.getInnergroups()) {
				for(Filter innerFilter : filter.getInnergroups()) {
					Map<String, Object> innerFilterCondition = this.getFilterMap(innerFilter,count);
					if(null!=innerFilterCondition.get("condition") && NullCheckUtils.isNotNull(((String)innerFilterCondition.get("condition")).trim())) {
						condition.append( " " + filter.getCondition() + "( "+innerFilterCondition.get("condition") +")");
						
					}
					if(null!=innerFilterCondition.get("attributeValues") && ((Map<String, AttributeValue>)innerFilterCondition.get("attributeValues")).size()>0) {
						attrValues.putAll((Map<String, AttributeValue>)innerFilterCondition.get("attributeValues"));
						count =(Integer) innerFilterCondition.get("count");
					}					
				}				
			}
			Map<String, Object> filters =  new HashMap<>(4);
			filters.put("condition", condition.toString());
			filters.put("attributeValues", attrValues);
			filters.put("count", count);
			return filters;	
			
		}
		return null;	
		
	}

	/**
	 * Convert the value to corresponding scalar attribute 
	 * @param value
	 * @param type
	 * @return
	 */
	private static AttributeValue getAttrValue(Object value,String type) {		
		ScalarAttributes attribute = ScalarAttributes.get(type);
		return attribute.getAttributeValue(null==value?null:""+value);	
	}

	/**
	 * 
	 * @param scanRequest
	 * @param filter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ScanRequest setFilterToScanRequest(ScanRequest scanRequest, Filter filter) {
		@SuppressWarnings("rawtypes")
		Map parsedFilter = getFilterCondition(filter);
		if(null!=parsedFilter) {
			scanRequest.withFilterExpression((String) parsedFilter.get("condition"))
			.withExpressionAttributeValues((Map<String, AttributeValue>) parsedFilter.get("attributeValues"));
//			/.withExpressionAttributeNames((Map<String, String>) filters.get("attributeNames"));
			
		}
		return scanRequest;
		
		
	}

}
