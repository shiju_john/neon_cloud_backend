package com.flytxt.aws.api;

import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

import javax.jms.ConnectionFactory;

import com.flytxt.aws.entity.AwsTransferUser;
import com.flytxt.aws.exception.BucketExistException;
import com.flytxt.aws.exception.BucketPathExistException;

/**
 * 
 * @author shiju.john
 *
 */
public interface AWSService {
	
	
	/**
	 * 
	 * @param amazonAWSAccessKey
	 * @param amazonAWSSecretKey
	 */
	public void setAccountId(String amazonAWSAccessKey, String amazonAWSSecretKey);
	
	
	/**
	 * 
	 * @param bucketName
	 * @throws BucketExistException
	 */
	public void createBucket(String bucketName) throws BucketExistException;
	
	/**
	 * 
	 * @param queueName
	 * @param policy
	 */
	public void addSqsPolicy(String queueName, String policy);
	
	/**
	 * 
	 * @param roleName
	 * @param policy
	 * @return
	 */
	public String createRole(String roleName,String policy);
	
	/**
	 * 
	 * @param queueName
	 * @param policy
	 * @return
	 */
	public String createQueue(String queueName, String policy);
	
	/**
	 * 
	 * @return
	 */
	public String getRegionKey();
	
	
	/**
	 * 
	 * @return
	 */
	public String getAccountId();
	
	
	/**
	 * 
	 * @param bucketName
	 * @param aqsArn
	 * @param s3SqsName
	 */
	public void addQueueNotification(String bucketName, String aqsArn, String s3SqsName);
	
	/**
	 * 
	 * @return
	 */
	public String createServer();

	
	/**
	 * 
	 * @param bucketName
	 * @param path
	 * @throws BucketPathExistException
	 */
	void createFolder(String bucketName, String path) throws BucketPathExistException;

	
	/**
	 * 
	 * @param serverId
	 * @param userName
	 * @return
	 */
	public AwsTransferUser getSFTPUser(String serverId, String userName);

	/**
	 * 
	 * @param userName
	 * @param serverId
	 */
	public void deleteSFTPUser(String userName, String serverId);
	
	/**
	 * 
	 * @param withSshPublicKey
	 */
	public void createSFTPUser(AwsTransferUser withSshPublicKey);

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getSSHKey();
	
	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @return
	 */
	public BufferedReader getS3File(String bucketName, String fileKey);
	
	
	/**
	 * 
	 * @param queueName
	 * @param message
	 */
	public void sendMessage(String queueName, String message);
	
	/**
	 * 
	 * @return
	 */
	public ConnectionFactory getConnectionFactory();
	
	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @param string
	 */
	public void moveFile(String bucketName, String fileKey, String string);

	/**
	 * 
	 * @param roleName
	 * @param policyArn
	 */
	public void attachPolicyToRole(String roleName, String policyArn);
	
	/**
	 * 
	 * @param policyName
	 * @param policyJson
	 * @return
	 */
	public String createPolicy(String policyName, String policyJson);
	
	/**
	 * 
	 * @param queueName
	 * @return
	 */
	public String getQueueUrl(String queueName);
	
	/**
	 * 
	 * @param bucketName
	 * @param fileName
	 * @param text
	 * @return
	 */
	public void writeFile(String bucketName,String fileName, String text);
	
	
	/**
	 * 
	 * @param bucketName
	 * @param fileName
	 * @param start
	 * @param end
	 * @return
	 */
	public BufferedReader getS3FileRange(String bucketName, String fileName, int start, int end);
	
	/**
	 * 
	 * @param bucketName
	 * @param domainName
	 * @param type
	 */
	public List<String> getFiles(String bucketName,String domainName,String type);
	
	/**
	 * 
	 * @param bucketName
	 * @param path
	 * @return
	 */
	public String getPresignedUrl(String bucketName,String path);

	
	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 */
	public long getFileInfo(String bucketName, String fileKey);
	

}
