package com.flytxt.aws.repository;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
/**
 * 
 * @author shiju.john
 *
 */
public enum ScalarAttributes {

	string {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	}, 
	
	integer {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withN(value);
		}
	},

	number {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withN(value);
		}
	},

	currency {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	time {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	date {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	mobile {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	fixed_line {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	marital_status {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	location {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	gender {
		@Override
		public AttributeValue getAttributeValue(String value) {
			// TODO Auto-generated method stub
			return null;
		}
	},

	email {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	unknown_data_type {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	},

	unique_id {
		@Override
		public AttributeValue getAttributeValue(String value) {
			if(null==value)
				return new AttributeValue().withNULL(true);
			return new AttributeValue().withS(value);
		}
	};
	
	public abstract AttributeValue getAttributeValue(String value);
	
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public static ScalarAttributes get(String type) {
		for(ScalarAttributes attributes: ScalarAttributes.values()){
			if(attributes.name().equalsIgnoreCase(type)) {
				return attributes;
			}			
		}
		return ScalarAttributes.unknown_data_type;
	}

}
