package com.flytxt.aws.repository;

import java.util.UUID;

import com.flytxt.aws.entity.AwsDataFilter;
import com.flytxt.aws.entity.Filter;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.exception.AwsServiceException;




/**
 * 
 * @author shiju.john
 *
 */
public interface DynamoDbRepository<T> {

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws AwsServiceException
	 */
	public T createTable(T entity) throws AwsServiceException;

	/**
	 * 
	 * @param T
	 * @return
	 * @throws AwsServiceException
	 */
	public T save(T T) throws AwsServiceException;

	/**
	 * 
	 * @param tableName
	 * @param keyFieldValue
	 * @param keyField
	 * @return
	 */
	public T findById(final String tableName, final String keyFieldValue, String keyField) throws AwsServiceException;

	/**
	 * 
	 * @param filter
	 * @param tableName
	 * @return
	 */
	public T findByExpresion(Filter filter, String tableName);

	/**
	 * 
	 * @param tableName
	 * @return
	 * @throws AwsServiceException
	 */
	public T findAll(final String tableName) throws AwsServiceException;

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws AwsServiceException
	 */
	public T update(T entity) throws AwsServiceException;

	/**
	 * 
	 * @return
	 */
	default public String getHashKey() {
		synchronized (DynamoDbRepository.class) {
			return UUID.randomUUID().toString();
		}
	}

	public AwsDataFilter<TableEntity> findByDataFilter(AwsDataFilter<TableEntity> awsDataFilter) throws AwsServiceException;

}
