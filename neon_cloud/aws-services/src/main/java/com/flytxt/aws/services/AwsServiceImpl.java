package com.flytxt.aws.services;

import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

import javax.jms.ConnectionFactory;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.identitymanagement.model.EntityAlreadyExistsException;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import com.amazonaws.services.transfer.model.DescribedUser;
import com.flytxt.aws.api.AWSService;
import com.flytxt.aws.entity.AwsTransferUser;
import com.flytxt.aws.exception.BucketExistException;
import com.flytxt.aws.exception.BucketPathExistException;

/**
 * 
 * @author shiju.john
 *
 */
public class AwsServiceImpl  implements AWSService{

	private S3BucketService bucketService;

	private AwsSFTPService awsSFTPService;

	private AwsSqsService sqsService;
	
	private AwsPolicyService policyService;

	private AWSStaticCredentialsProvider awsStaticCredentialsProvider;

	private String accountId;
	private String regionKey;
	
	private static final String policyPattern = "arn:aws:iam::{account_id}:policy/{policy_name}";

	private AwsServiceImpl(String amazonAWSAccessKey, String amazonAWSSecretKey, String regionKey) {

		this.awsStaticCredentialsProvider = new AWSStaticCredentialsProvider(
				new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey));
		Regions region = Regions.fromName(regionKey);
		this.bucketService = new S3BucketService(awsStaticCredentialsProvider, region);
		this.awsSFTPService = new AwsSFTPService(awsStaticCredentialsProvider, region);
		this.sqsService = new AwsSqsService(awsStaticCredentialsProvider, region);
		this.policyService = new AwsPolicyService(awsStaticCredentialsProvider,region);
		
		this.setRegionKey(regionKey);
		//setAccountId(amazonAWSAccessKey,amazonAWSSecretKey);
		 
	}

	/**
	 * 
	 * @param amazonAWSAccessKey
	 * @param amazonAWSSecretKey
	 */
	public void setAccountId(String amazonAWSAccessKey, String amazonAWSSecretKey) {
		
		AWSSecurityTokenService stsService = AWSSecurityTokenServiceClientBuilder.standard().withCredentials(
	            new AWSStaticCredentialsProvider(new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey))).build();
	    GetCallerIdentityResult callerIdentity = stsService.getCallerIdentity(new GetCallerIdentityRequest());
	    setAccountId(callerIdentity.getAccount());
		
	}

	/**
	 * 
	 * @param amazonAWSAccessKey
	 * @param amazonAWSSecretKey
	 * @return
	 */
	public static AWSService getInstance(String amazonAWSAccessKey, String amazonAWSSecretKey, String region) {		
			return new AwsServiceImpl(amazonAWSAccessKey, amazonAWSSecretKey, region);		

	}

	/**
	 * 
	 * @param bucketName
	 * @param folderName
	 * @throws BucketPathExistException
	 */
	public void createFolder(String bucketName, String folderName) throws BucketPathExistException {
		List<S3ObjectSummary> buckets = bucketService.getObjet(bucketName, folderName);
		if (null == buckets || buckets.size() == 0) {
			bucketService.createFolder(bucketName, folderName);
			return;
		}
		throw new BucketPathExistException("Bucket path already Exist : " + bucketName + "/" + folderName);
	}

	/**
	 * 
	 * @param bucketName
	 * @throws BucketExistException
	 */
	public void createBucket(String bucketName) throws BucketExistException {
		if (!bucketService.hasBucketExist(bucketName)) {
			bucketService.createBucket(bucketName);
			return;
		}
		throw new BucketExistException("Bucket Already Exist");
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getSSHKey() {
		return awsSFTPService.getSSHKey();

	}

	/**
	 * 
	 * @param serverId
	 * @param domainName
	 * @return
	 */
	public AwsTransferUser getSFTPUser(String serverId, String domainName) {
		DescribedUser user = awsSFTPService.getUser(serverId, domainName);
		return user != null ? new AwsTransferUser().withServerId(serverId).withUserName(user.getUserName())
				.withRoleARN(user.getRole()).withPolicyJson(user.getPolicy()).withHomeDir(user.getHomeDirectory())
				: null;
	}

	/**
	 * 
	 * @param transferUser
	 */
	public void createSFTPUser(AwsTransferUser transferUser) {
		awsSFTPService.createUser(transferUser);

	}

	public void deleteSFTPUser(String userName, String serverId) {
		awsSFTPService.deleteUser(serverId, userName);

	}

	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @return
	 */
	public BufferedReader getS3File(String bucketName, String fileKey) {
		return bucketService.getS3File(bucketName, fileKey);
	}

	/**
	 * 
	 * @param queueName
	 * @param policy
	 */
	public String createQueue(String queueName, String policy) {
		return sqsService.createQueue(queueName, policy);
	}

	/**
	 * 
	 * @param queueName
	 * @param message
	 */
	public void sendMessage(String queueName, String message) {
		sqsService.sendMessage(queueName, message);

	}

	

	/**
	 * 
	 * @return
	 */
	public ConnectionFactory getConnectionFactory() {
		return sqsService.getConnectionFactory();
	}

	public void moveFile(String bucketName, String sourceFileKey, String targetFileKey) {
		bucketService.moveFile(bucketName, sourceFileKey, targetFileKey);

	}

	/**
	 * 
	 * @param bucketName
	 * @param queueArn
	 */
	public void addQueueNotification(String bucketName, String queueArn, String configName) {
		bucketService.addQueueNotification(bucketName, queueArn, configName);
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	private void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public void  addSqsPolicy(String queueName, String policy) {
		sqsService.addPolicy(queueName, policy);
		
	}

	/**
	 * @return the regionKey
	 */
	public String getRegionKey() {
		return regionKey;
	}

	/**
	 * @param regionKey the regionKey to set
	 */
	public void setRegionKey(String regionKey) {
		this.regionKey = regionKey;
	}

	/**
	 * 
	 */
	public String createServer() {
		return awsSFTPService.createSFTPServer();		
	}
	
	/**
	 * 
	 * @param policyName
	 * @param policyJson
	 * @return Policy ARN 
	 */
	@Override
	public String  createPolicy(String policyName,String policyJson) {
		try {
			return policyService.createPolicy(policyName, policyJson);
		}catch (EntityAlreadyExistsException e) {
			String policyArn = policyPattern.replaceAll("\\{account_id\\}", getAccountId());
			policyArn = policyArn.replaceAll("\\{policy_name\\}", policyName);
			policyService.deletePolicy(policyArn);
			return policyService.createPolicy(policyName, policyJson);
			
		}
			
		
		
	}
	
	/**
	 * 
	 * @param roleName
	 * @param policyJson
	 * @return
	 */
	private  String createAccountRole(String roleName,String policyJson) {
		try {
			return policyService.createRole(roleName, policyJson);
		}catch (EntityAlreadyExistsException e) {
			deleteRole(roleName);
			return policyService.createRole(roleName, policyJson);
		}
		
	}
	
	void deleteRole(String roleName) {
		policyService.deleteRole(roleName);
	}

	@Override
	public String createRole(String roleName, String policy) {
		return this.createAccountRole(roleName,policy);
		
	}
	
	@Override
	public void attachPolicyToRole(String roleName,String policyArn) {
		policyService.attachRolePolicy(roleName, policyArn);
	}

	@Override
	public String getQueueUrl(String queueName) {
		return sqsService.getQueueUrl(queueName);
	}
	
	@Override
	public void writeFile(String bucketName,String fileName, String text) {
		 bucketService.writeFile(bucketName, fileName, text);
	}

	@Override
	public BufferedReader getS3FileRange(String bucketName, String fileName, int start, int end) {
		return bucketService.getS3FileRange(bucketName, fileName, start, end);
	}

	@Override
	public List<String> getFiles(String bucketName, String domainName, String type) {
		return bucketService.getFiles(bucketName, domainName, type);
	}

	@Override
	public String getPresignedUrl(String bucketName, String path) {
		return bucketService.getPresignedUrl(bucketName, path);
	}

	@Override
	public long getFileInfo(String bucketName, String fileKey) {
		return bucketService.getFileInfo(bucketName, fileKey);
		
	}
	

}
