package com.flytxt.aws.repository;

import com.flytxt.aws.entity.FilterRules;
/**
 * 
 * @author shiju.john
 *
 */
public enum DynamoDbOperators {
	
	EQ(" = ") {
		

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
	} ,
	NE ("<>") {
		

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
	},
	IN ("in") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
		
	},
	LE ("<=") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
		
	},
	LT ("<") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
		
	},
	GE(">=") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
		
	},
	GT(">") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
		
	},
	BETWEEN("between") {
		

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();
			
		}
	},
	NOT_NULL("not null") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + " <> :"+filterRules.getKey()+"_"+index);		
			return condition.toString();			
		}
		
	},
	NULL("null") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + "= :"+filterRules.getKey()+"_"+index);	
			return condition.toString();			
		}
		
	} ,
	CONTAINS("contains") {
		

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(this.getOperator() +"("+filterRules.getKey()+", :"+filterRules.getKey()+"_"+index +")");	
			return condition.toString();			
		}
	},
	NOT_CONTAINS("not contains") {

		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(this.getOperator() +"("+filterRules.getKey()+", :"+filterRules.getKey()+"_"+index +")");	
			return condition.toString();			
		}
		
	},
	BEGINS_WITH("begin_with") {
		
		@Override
		public String getCondition(FilterRules filterRules, int index) {
			StringBuilder  condition = new StringBuilder();
			condition.append(filterRules.getKey() + this.getOperator() +":"+filterRules.getKey()+"_"+index);	
			return condition.toString();			
		}
	};
	
	private String operator;
	
	
	public abstract String getCondition(FilterRules filterRules,int index);
	
	private DynamoDbOperators(String operator){
		this.setOperator(operator);
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	public static DynamoDbOperators get(String key) {
		return DynamoDbOperators.valueOf(key);		
	}
	
	

}
