


package com.flytxt.aws.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author shiju.john
 *
 */

public class tt implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 
	 */
	private String condition;
	
	
	/**
	 * 
	 */
	private ArrayList rules;
	
	
	/**
	 * 
	 */
	private ArrayList  innergroups;
	
	
	

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the rules
	 */
	public List getRules() {
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(ArrayList rules) {
		this.rules = rules;
	}

	/**
	 * @return the innergroup
	 */
	public List getInnergroups() {
		return innergroups;
	}

	/**
	 * @param innergroup the innergroup to set
	 */
	public void setInnergroups(ArrayList innergroup) {
		this.innergroups = innergroup;
	}

	
	

}
