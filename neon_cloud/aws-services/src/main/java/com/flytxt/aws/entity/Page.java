package com.flytxt.aws.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author shiju.john
 *
 */
public class Page implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean hasNext;
	private boolean hasPrevious;
	
	private int pageNo;
	private int  pageSize;
	private String lastKey;
	private List<String> previousBatchKey;
	
	
	
	/**
	 * @return the hasNext
	 */
	public boolean isHasNext() {
		return hasNext;
	}

	/**
	 * @param hasNext the hasNext to set
	 */
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	/**
	 * @return the hasPrevious
	 */
	public boolean isHasPrevious() {
		return hasPrevious;
	}

	/**
	 * @param hasPrevious the hasPrevious to set
	 */
	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}

	

	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	

	/**
	 * @return the lastKey
	 */
	public String getLastKey() {		
		return lastKey;	
	}
	
	
	public String getPaginationKey(){
		return this.pageNo >0 && getPreviousBatchKey().size() >= pageNo ? getPreviousBatchKey().get(pageNo-1):lastKey;	
	}

	/**
	 * @param lastKey the lastKey to set
	 */
	public void setLastKey(String lastKey) {
		this.lastKey = lastKey;
	}

	/**
	 * @return the previousBatchKey
	 */
	public List<String> getPreviousBatchKey() {
		if(null==previousBatchKey) {
			setPreviousBatchKey(new ArrayList<>());
		}
		return previousBatchKey;
	}

	/**
	 * @param previousBatchKey the previousBatchKey to set
	 */
	public void setPreviousBatchKey(List<String> previousBatchKey) {
		this.previousBatchKey = previousBatchKey;
	}
	
	/**
	 * 
	 * @param key
	 */
	public void addToIndexKey(String key) {
		if(null==previousBatchKey) {
			setPreviousBatchKey(new ArrayList<>());
		}
		if(!previousBatchKey.contains(key)) {
			previousBatchKey.add(key);		
		}		
	}

}
