package com.flytxt.aws.services;

import java.util.HashMap;
import java.util.Map;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
/**
 * 
 * @author shiju.john
 *
 */
public class AwsSqsService {

	private AmazonSQS amazonSQS;
	
	private SQSConnectionFactory connectionFactory;
	
	//private String  queueUrl;
	
	public AwsSqsService(AWSStaticCredentialsProvider credentialsProvider, Regions region) {
		amazonSQS = AmazonSQSClientBuilder.standard().withRegion(region).withCredentials(credentialsProvider).build();		
		setConnectionFactory(new SQSConnectionFactory(new ProviderConfiguration(),amazonSQS));

	}
	
	
	/**
	 * 
	 * @param queueName
	 * @return
	 */
	public String createQueue(String queueName,String policy) {	
	
		CreateQueueRequest createQueueRequest = new CreateQueueRequest();
		createQueueRequest.withQueueName(queueName) 
			.addAttributesEntry("DelaySeconds", "1")
	        .addAttributesEntry("MessageRetentionPeriod", "86400");
	        if(policy!=null && !"".equals(policy)) {
	        	 createQueueRequest.addAttributesEntry("Policy", policy);
	        }
	       
//		createQueueRequest.withAttributes(attributes)
		CreateQueueResult queueResult = amazonSQS.createQueue(createQueueRequest);		
		return queueResult.getQueueUrl();
	}
	
	
	/**
	 * 
	 * @param queueUrl
	 * @param policy
	 */
	public void  addPolicy(String queueName,String policy){
		Map<String ,String> attributes = new HashMap<>();
		attributes.put("Policy", policy);
		amazonSQS.setQueueAttributes(getQueueUrl(queueName),attributes);		
	}
	
	
	
	
	/**
	 * 
	 * @param queueName
	 * @return
	 */
	public String getQueueUrl(String queueName){
		
		return  amazonSQS.getQueueUrl(queueName).getQueueUrl();
		
	}
	
	/**
	 * 
	 * @param queueName
	 */
	public void deleteQueueUrl(String queueName){
		amazonSQS.deleteQueue(getQueueUrl(queueName));		
	}
	
	
	public void sendMessage(String queueName,String body){
		SendMessageRequest send_msg_request = new SendMessageRequest()
		        .withQueueUrl(this.getQueueUrl(queueName))
		        .withMessageBody(body)
		        .withDelaySeconds(5);
		amazonSQS.sendMessage(send_msg_request);
	}


	/**
	 * @return the connectionFactory
	 */
	public SQSConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}


	/**
	 * @param connectionFactory the connectionFactory to set
	 */
	private void setConnectionFactory(SQSConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}
	

}
