package com.flytxt.aws.entity;

import java.io.Serializable;

/**
 * 
 * @author shiju.john
 *
 */
public class AwsTransferUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String serverId;

	private String userName;

	private String roleARN;

	private String policyJson;
	
	private String homeDir;
	
	private String sshPublicKey;
	
	/**
	 * @return the serverId
	 */
	public String getServerId() {
		return serverId;
	}

	/**
	 * @param serverId the serverId to set
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public AwsTransferUser withServerId(String serverId){
		this.serverId = serverId;
		return this;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public AwsTransferUser withUserName(String userName){
		this.userName = userName;
		return this;
	}

	/**
	 * @return the roleARN
	 */
	public String getRoleARN() {
		return roleARN;
	}

	/**
	 * @param roleARN the roleARN to set
	 */
	public void setRoleARN(String roleARN) {
		this.roleARN = roleARN;
	}
	
	public AwsTransferUser withRoleARN(String roleARN){
		this.roleARN = roleARN;
		return this;
	}

	/**
	 * @return the policyJson
	 */
	public String getPolicyJson() {
		return policyJson;
	}

	/**
	 * @param policyJson the policyJson to set
	 */
	public void setPolicyJson(String policyJson) {
		this.policyJson = policyJson;
	}
	
	/**
	 * 
	 * @param policyJson
	 * @return
	 */
	public AwsTransferUser withPolicyJson(String policyJson){
		this.policyJson = policyJson;
		return this;
	}

	
	/**
	 * 
	 * @param homeDir
	 * @return
	 */
	public AwsTransferUser withHomeDir(String homeDir) {
		this.setHomeDir(homeDir);
		return this;
	}

	/**
	 * @return the homeDir
	 */
	public String getHomeDir() {
		return homeDir;
	}

	/**
	 * @param homeDir the homeDir to set
	 */
	public void setHomeDir(String homeDir) {
		this.homeDir = homeDir;
	}

	public AwsTransferUser withSshPublicKey(String sshPublicKey) {
		this.setSshPublicKey(sshPublicKey);
		return this;
	}

	/**
	 * @return the sshPublicKey
	 */
	public String getSshPublicKey() {
		return sshPublicKey;
	}

	/**
	 * @param sshPublicKey the sshPublicKey to set
	 */
	public void setSshPublicKey(String sshPublicKey) {
		this.sshPublicKey = sshPublicKey;
	}

}
