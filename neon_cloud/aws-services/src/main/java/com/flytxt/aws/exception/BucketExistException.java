package com.flytxt.aws.exception;
/**
 * 
 * @author shiju.john
 *
 */
public class BucketExistException extends AwsServiceException {

	public BucketExistException(String messag) {
		super(messag);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
