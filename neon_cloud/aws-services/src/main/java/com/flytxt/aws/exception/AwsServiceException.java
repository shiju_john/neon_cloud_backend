package com.flytxt.aws.exception;
/**
 * 
 * @author shiju.john
 *
 */
public class AwsServiceException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	AwsServiceException(String message){
		super(message);
	}

}
