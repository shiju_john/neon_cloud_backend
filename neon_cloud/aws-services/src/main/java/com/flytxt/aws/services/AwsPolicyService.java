package com.flytxt.aws.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachedPolicy;
import com.amazonaws.services.identitymanagement.model.CreatePolicyRequest;
import com.amazonaws.services.identitymanagement.model.CreatePolicyResult;
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest;
import com.amazonaws.services.identitymanagement.model.CreateRoleResult;
import com.amazonaws.services.identitymanagement.model.DeletePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest;
import com.amazonaws.services.identitymanagement.model.DetachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesResult;

/**
 * 
 * @author shiju.john
 *
 */

public class AwsPolicyService {

	AmazonIdentityManagement amazonIdentityManagement;

	/**
	 * 
	 * @param credentialsProvider
	 */
	public AwsPolicyService(AWSStaticCredentialsProvider credentialsProvider, Regions region) {
		this.amazonIdentityManagement = AmazonIdentityManagementClientBuilder.standard().withRegion(region)
				.withCredentials(credentialsProvider).build();
	}

	/**
	 * 
	 * @param roleName
	 * @param trustPolicyJson
	 * @return
	 */
	public String createRole(String roleName, String trustPolicyJson) {
		CreateRoleRequest roleRequest = new CreateRoleRequest().withRoleName(roleName)
				.withAssumeRolePolicyDocument(trustPolicyJson);
		
		
				
	
		CreateRoleResult result = amazonIdentityManagement.createRole(roleRequest);
		return result.getRole().getArn();
	}

	/**
	 * 
	 * @param roleName
	 * @param policyArn
	 */
	public void attachRolePolicy(String roleName, String policyArn) {
		AttachRolePolicyRequest attach_request = new AttachRolePolicyRequest().withRoleName(roleName)
				.withPolicyArn(policyArn);
		
		amazonIdentityManagement.attachRolePolicy(attach_request);

	}

	/**
	 * 
	 * @param policyName
	 * @param policyJson
	 * @return
	 */
	public String createPolicy(String policyName, String policyJson) {

		CreatePolicyRequest request = new CreatePolicyRequest().withPolicyName(policyName)
				.withPolicyDocument(policyJson);
		CreatePolicyResult response = amazonIdentityManagement.createPolicy(request);
		return response.getPolicy().getArn();

	}

	public String getPolicy(String policyArn) {

		GetPolicyRequest request = new GetPolicyRequest().withPolicyArn(policyArn);
		GetPolicyResult response = amazonIdentityManagement.getPolicy(request);
		return response.getPolicy().getDescription();
	}
	
	/**
	 * 
	 * @param roleName
	 */
	public void deleteRole(String roleName) {
		ListAttachedRolePoliciesResult result = amazonIdentityManagement.listAttachedRolePolicies(new ListAttachedRolePoliciesRequest().withRoleName(roleName));
		if(result!=null && !result.getAttachedPolicies().isEmpty()) {
			for(AttachedPolicy attachedPolicy : result.getAttachedPolicies()) {
				amazonIdentityManagement.detachRolePolicy(new DetachRolePolicyRequest().withRoleName(roleName).withPolicyArn(attachedPolicy.getPolicyArn()));
			}
		}		
		amazonIdentityManagement.deleteRole(new DeleteRoleRequest().withRoleName(roleName));
		
		
	}
	
	public void deletePolicy(String policyArn) {
		amazonIdentityManagement.deletePolicy(new DeletePolicyRequest().withPolicyArn(policyArn));
		
	}
	
	
	/**
	 * 
	 * @param roleName
	 */
	public void deleteRolePolicy(String roleName) {
		amazonIdentityManagement.deleteRolePolicy(new DeleteRolePolicyRequest().withRoleName(roleName));
	}

}
