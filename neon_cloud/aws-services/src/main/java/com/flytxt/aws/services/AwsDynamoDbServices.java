package com.flytxt.aws.services;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.repository.DynamoDbRepository;
import com.flytxt.aws.repository.DynamoDbRepositoryImpl;
/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public class AwsDynamoDbServices<T> {
	
	private DynamoDbRepository<TableEntity> dbRepository;
	
	public AwsDynamoDbServices(AmazonDynamoDB amazonDynamoDB){
		this.dbRepository = new DynamoDbRepositoryImpl(amazonDynamoDB);
	}
	
		
	public DynamoDbRepository<TableEntity> getRepository(){
		return dbRepository;		
	}

}
