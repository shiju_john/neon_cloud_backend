package com.flytxt.aws.services;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.jclouds.ssh.SshKeys;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.transfer.AWSTransfer;
import com.amazonaws.services.transfer.AWSTransferClientBuilder;
import com.amazonaws.services.transfer.model.CreateServerRequest;
import com.amazonaws.services.transfer.model.CreateServerResult;
import com.amazonaws.services.transfer.model.CreateUserRequest;
import com.amazonaws.services.transfer.model.DeleteServerRequest;
import com.amazonaws.services.transfer.model.DescribeServerRequest;
import com.amazonaws.services.transfer.model.DescribeServerResult;
/**
 * 
 * @author shiju.john
 *
 */
public class CreatePolicy {

	AWSTransfer awsTransfer;

	public CreatePolicy() {
		AWSStaticCredentialsProvider awsStaticCredentialsProvider = new AWSStaticCredentialsProvider(
				new BasicAWSCredentials("AKIAJJIT2SE57SUBSBTA", "qBSt18jctveP7W1v1QtCS18FGSqx8z59zVk8Enjq"));
		AWSTransferClientBuilder awsTransferClientBuilder = AWSTransferClientBuilder.standard()
				.withCredentials(awsStaticCredentialsProvider);
		awsTransfer = awsTransferClientBuilder.build();
	}

	public void createPolicy(AWSStaticCredentialsProvider awsStaticCredentialsProvider) {

		AmazonIdentityManagementClientBuilder builder = AmazonIdentityManagementClientBuilder.standard()
				.withCredentials(awsStaticCredentialsProvider);
		AmazonIdentityManagement identityManagement = builder.build();
//		System.out.println(identityManagement.getAccountSummary());		

		GetRoleRequest getRoleRequest = new GetRoleRequest().withRoleName("SFTP_CONVERSATION");
		System.out.println(identityManagement.getRole(getRoleRequest));

	}

	public String createSFTPServer() {

		/*
		 * ListServersResult servers = awsTransfer.listServers(new
		 * ListServersRequest()); if(servers.getServers().size()>0) {
		 * 
		 * }
		 */

		CreateServerResult server = awsTransfer.createServer(new CreateServerRequest());
		DescribeServerResult describeServerResult = awsTransfer
				.describeServer(new DescribeServerRequest().withServerId(server.getServerId()));

		System.out.println("Server ARN *******  " + server.getServerId());
		return server.getServerId();
	}

	public void createSFTPUser(CreateUserRequest request) {
		awsTransfer.createUser(request);

	}

	public void deleteSFTPServer(String serverId) {
		awsTransfer.deleteServer(new DeleteServerRequest().withServerId(serverId));
	}

	public static void main(String arg[]) throws IOException {
		
//		System.out.println(getSSHKey());
//
		CreatePolicy createPolicy = new CreatePolicy();

		// new CreatePolicy().createPolicy(awsStaticCredentialsProvider);
		//
		String serverId = createPolicy.createSFTPServer();
//		String serverId = "s-57a88291457d4617b";
//		System.out.println( "Server Id ***** "+serverId);
		createPolicy.createSFTPUser(getCreateUserRequest(serverId,"test","/conversation.neon/test"));

//		createPolicy.deleteSFTPServer(serverId);

	}

	static CreateUserRequest getCreateUserRequest(String serverId, String userName, String homeDir) throws IOException {
		CreateUserRequest createUserRequest = new CreateUserRequest();
		createUserRequest.setUserName(userName);
		createUserRequest.setHomeDirectory(homeDir);
		createUserRequest.setRole("arn:aws:iam::493447423170:role/SFTP_CONVERSATION");
		createUserRequest.setPolicy(readPolicyFile());
		createUserRequest.setServerId(serverId);
		/*
		 * createUserRequest.setSshPublicKeyBody(
		 * "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFV8aGqWO7smBQcrp9v0chVkB4kpQ8X3srY5XK2j0bJ4e0cOq7vLr5GQFou/DTmiz43pOI9s/jBYlFrTMnSzhLk7LH7GFYdm/aBhZiIdJ8inVrtlxK2JWg6phUYG+etb4hrGtT+0QkK0tl2arf7siDtYa5cXO1gGIMN/h0VjCoFADunRnrSZyai54U9nbKc2jZ6CSBUB/+W4P3M6ie++ng/aov8J9ATzUCCINB+Jw28LE97tiME5qRx6cg2P+OUdgThKmDYM6h/lCwgCoAG3Amb9ROfGm9D/xVhWAZ3YE+/5uNPwfXTHxY+0dLwp3SZ3puwpPgyIqpL+THVXvNphYt fly@FLY-LP646"
		 * );
		 */

		Map<String, String> keys = getSSHKey();

		createUserRequest.setSshPublicKeyBody(keys.get("public"));
		System.out.println("private Key");
		System.out.println(keys.get("private"));
		System.out.println("private Key End" );
		
		System.out.println(keys.get("public"));
		return createUserRequest;
	}

	static Map<String, String> getSSHKey() throws IOException {

		return SshKeys.generate();

	}

	static String readPolicyFile() throws IOException {

		InputStream is = new FileInputStream(
				"/home/fly/git/neon_cloud_backend/neon_cloud/main-application/src/main/resources/policy.json");
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));

		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();

		while (line != null) {
			sb.append(line);
			line = buf.readLine();
		}
		return sb.toString();
	}

}
