package com.flytxt.aws.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * 
 * @author shiju.john
 *
 */
public class TableEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tableName;
	
	
	@SuppressWarnings("rawtypes")	
	private List<Map> columnMappings;
	
	private List<Map<String,Object>> columnValues;
	
	private String entityType;
	
	public TableEntity(){
		columnValues = new ArrayList<>();
		columnMappings = new ArrayList<>();
	}
	
	
	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}
	
	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	
	
	
	/**
	 * @return the columnValues
	 */
	public List<Map<String,Object>> getColumnValues() {
		if(columnValues==null) {
			columnValues = new ArrayList<>();
		}
		return columnValues;
	}
	
	/**
	 * @param columnValues the columnValues to set
	 */
	public void setColumnValues(List<Map<String,Object>> columnValues) {
		this.columnValues = columnValues;
		
	}

	
	/**
	 * 
	 * @param row
	 */
	public void addRow(Map<String,Object> row){
		if(columnValues==null) {
			columnValues = new ArrayList<>();
		}
		if(row!=null) {
			columnValues.add(row);	
		}
	}


	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}


	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}


	/**
	 * @return the columnMappings
	 */
	@SuppressWarnings("rawtypes")
	public List<Map> getColumnMappings() {
		return columnMappings;
	}


	/**
	 * @param columnMappings the columnMappings to set
	 */
	public void setColumnMappings(@SuppressWarnings("rawtypes") List<Map> columnMappings) {
		this.columnMappings = columnMappings;
	}

}
