package com.flytxt.aws.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutRequest;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemResult;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.flytxt.aws.entity.AwsDataFilter;
import com.flytxt.aws.entity.Filter;
import com.flytxt.aws.entity.FilterRules;
import com.flytxt.aws.entity.Page;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.exception.AwsServiceException;

/**
 * 
 * @author shiju.john
 *
 */

public class DynamoDbRepositoryImpl extends DynamoDbAbstractRepo<TableEntity> {

	AmazonDynamoDB db;
	

	public DynamoDbRepositoryImpl(AmazonDynamoDB db) { 
		this.db = db;
	}

	@Override
	public TableEntity createTable(TableEntity entity) {

		try {
			DynamoDB dynamoDB = new DynamoDB(db);
			Table table = dynamoDB.getTable(entity.getTableName());
			try {
			 table.describe();

			} catch (Exception e) {

				ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
				keySchema.add(new KeySchemaElement().withAttributeName("hashKey").withKeyType(KeyType.HASH));

				ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
				attributeDefinitions.add(new AttributeDefinition().withAttributeName("hashKey")
						.withAttributeType(ScalarAttributeType.S));

				CreateTableRequest request = new CreateTableRequest().withTableName(entity.getTableName())
						.withKeySchema(keySchema).withProvisionedThroughput(
								new ProvisionedThroughput().withReadCapacityUnits(10L).withWriteCapacityUnits(5L));

				request.setAttributeDefinitions(attributeDefinitions);
				table = dynamoDB.createTable(request);
				table.waitForActive();
			}

		} catch (Exception e) {

		}
		return entity;
	}

	/**
	 * 
	 * @param entity
	 * @return
	 */
	@Override
	public TableEntity update(TableEntity entity) throws AwsServiceException {

		UpdateItemResult updateItemResult = db
				.updateItem(getUpdateItemRequest(entity.getColumnValues().get(0), entity.getTableName()));
		List<Map<String, AttributeValue>> data = new ArrayList<>();
		data.add(updateItemResult.getAttributes());
		return getTableEntity(data, entity.getTableName());
	}

//	@Override
//	public TableEntity save(TableEntity entity) throws VisionException {
//
//		PutItemRequest request = getPutItemRequestFromEntity(entity);
//		PutItemResult result = db.putItem(request);
//		return getTableEntity(request.getItem(), entity.getTableName());
//
//	}

	@Override
	public TableEntity save(TableEntity data) throws AwsServiceException {
//		System.out.println("create table time" + System.currentTimeMillis());
//		createTable(data);
//		System.out.println("table credated " + System.currentTimeMillis());
		Map<String, List<WriteRequest>> requestItems = new HashMap<>();
		requestItems.put(data.getTableName(), new ArrayList<WriteRequest>());
		List<Map> maping = data.getColumnMappings();
		data.getColumnValues().forEach((row) -> {
			requestItems.get(data.getTableName()).add(new WriteRequest(new PutRequest(super.getPutItems(row, maping))));
		});
		System.out.println("brfore batch write " + System.currentTimeMillis());
		batchWrite(requestItems);
		System.out.println("batch write completd  " + System.currentTimeMillis());

		/*
		 * Filter filter = new Filter(); filter.setCondition("AND"); filter.addRules(new
		 * FilterRules("fileName", "=", "users.csv")); filter.addRules(new
		 * FilterRules("version", "=", "1", "int")); List<TableEntity> datas =
		 * findByExpresion(filter, data.getTableName()); datas.forEach(entry -> {
		 */

		return data;

	}

	/**
	 * 
	 * @param requestItems
	 */
	void batchWrite(Map<String, List<WriteRequest>> requestItems) {

		for (Entry<String, List<WriteRequest>> entry : requestItems.entrySet()) {

			int count = 0;
			int lastIndex = 25;
			do {
//				System.out.println( "Single batch started "+  System.currentTimeMillis());
				lastIndex = entry.getValue().size() < lastIndex ? lastIndex = entry.getValue().size() : lastIndex;
				List<WriteRequest> subLlist = entry.getValue().subList(count, lastIndex);
				Map<String, List<WriteRequest>> subMapItem = new HashMap<>();
				subMapItem.put(entry.getKey(), subLlist);
				db.batchWriteItem(new BatchWriteItemRequest().withRequestItems(subMapItem));
				count += 25;
				lastIndex += 25;
//				System.out.println( "Single batch completed " +  System.currentTimeMillis());

			} while (count < entry.getValue().size());

		}

	}

	/**
	 * 
	 */
	@Override
	public TableEntity findById(final String tableName, String id, String keyField) {

		Map<String, AttributeValue> keyMap = new HashMap<>();
		List<Map<String, AttributeValue>> data = new ArrayList<>();

		GetItemRequest rq = new GetItemRequest();
		keyMap.put(keyField, new AttributeValue().withS(id));
		rq.withKey(keyMap).withTableName(tableName).withConsistentRead(true);
		GetItemResult item = db.getItem(rq);

		data.add(item.getItem());
		return getTableEntity(data, tableName);

	}

	public static void main(String args[]) {

		
		 AmazonDynamoDB amazonDynamoDB  = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(  new EndpointConfiguration("http://localhost:8000", "us-east-1")).build();
		 DynamoDbRepositoryImpl  repository =  new DynamoDbRepositoryImpl(amazonDynamoDB);
		 
		 AwsDataFilter<TableEntity> awsDataFilter = new AwsDataFilter<>();
		 TableEntity entity = new TableEntity();
		 entity.setTableName("flytxt_customers");
		 Page page =  new Page();
//		 page.setPageSize(10);
		
		 List<Map> mappingList  = new ArrayList<>();
		 Map columnMapping =  new HashMap<>();
		 entity.setColumnMappings(mappingList);
		 
		 Filter filter = new Filter();
		 filter.setCondition("AND");		
		 filter.addRules(new FilterRules("fileName","EQ", "users.csv"));
		 filter.addRules(new FilterRules("age","NOT_NULL", null));//		 
		 filter.addRules(new FilterRules("ID","CONTAINS", null));
		 
		 
		 Filter innerFilter = new Filter();
		 innerFilter.setCondition("OR");		
//		 innerFilter.addRules(new FilterRules("fileName","EQ", "users.csv"));
		 innerFilter.addRules(new FilterRules("age","NOT_NULL", null));
		 
		 innerFilter.addRules(new FilterRules("ID","CONTAINS", null));
		 
//		 filter.addInnerFilter(innerFilter);
		 
		 awsDataFilter.setEntity(entity);
		 awsDataFilter.setPages(page);
		 awsDataFilter.setFilterObj(filter);
		 
//		 DynamoDbRepositoryImpl  repository =  new DynamoDbRepositoryImpl(null);
		 
		 
		 
		 try {
			 do {
				awsDataFilter = repository.findByDataFilter(awsDataFilter);
				
			 }while(awsDataFilter.getPages().getLastKey()!=null);
			 
		} catch (AwsServiceException e) {
			e.printStackTrace();
		}
		 

//			Filter filter = new Filter();
//			filter.setCondition("AND");		
//			filter.addRules(new FilterRules("fileName","=", "users.csv"));
//			filter.addRules(new FilterRules("version","=", "1"));
//			Map<String, Object> filters = FilterUtils.getCondition(filter);
		 
		 
		 
		 
		 

	}

	@Override
	public TableEntity findByExpresion(Filter filter, String tableName) {

		Map<String, Object> filters = super.getFilterCondition(filter);

		@SuppressWarnings("unchecked")
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
				.withFilterExpression((String) filters.get("condition"))
				.withExpressionAttributeValues((Map<String, AttributeValue>) filters.get("attributeValues"))
				.withExpressionAttributeNames((Map<String, String>) filters.get("attributeNames"));

		final ScanRequest scanRequest = new ScanRequest();
		scanRequest.setTableName(tableName);

		scanRequest.setFilterExpression(scanExpression.getFilterExpression());
		scanRequest.setExpressionAttributeNames(scanExpression.getExpressionAttributeNames());
		scanRequest.setExpressionAttributeValues(scanExpression.getExpressionAttributeValues());

		scanRequest.setSelect(scanExpression.getSelect());
		scanRequest.setProjectionExpression(scanExpression.getProjectionExpression());
		scanRequest.setReturnConsumedCapacity(scanExpression.getReturnConsumedCapacity());
		scanRequest.setConsistentRead(scanExpression.isConsistentRead());

		final ScanResult scanResult = db.scan(scanRequest);

		return super.getTableEntity(scanResult.getItems(), tableName);

	}

	/**
	 * 
	 * @param tableName
	 * @return
	 */
	@Override
	public TableEntity findAll(final String tableName) throws AwsServiceException {

		final ScanRequest scanRequest = new ScanRequest();
		scanRequest.setTableName(tableName);
		final ScanResult scanResult = db.scan(scanRequest);
		return getTableEntity(scanResult.getItems(), tableName);

	}

	/**
	 * Use segmented parallel scan if required to improve the scan performance
	 */
	@Override
	public AwsDataFilter<TableEntity> findByDataFilter(AwsDataFilter<TableEntity> awsDataFilter)
			throws AwsServiceException {

		final ScanRequest scanRequest = new ScanRequest();
		scanRequest.setTableName(awsDataFilter.getEntity().getTableName());
		
		if(null !=awsDataFilter.getFields() && awsDataFilter.getFields().length>0) {
			String prokectionExp = String.join(",",awsDataFilter.getFields());	
			prokectionExp = prokectionExp +","+HASH_KEY;
			scanRequest.setProjectionExpression(prokectionExp);
		}
		
		
		super.setPagination(awsDataFilter.getPages(), scanRequest);
		super.setFilterToScanRequest(scanRequest, awsDataFilter.getFilterObj());
		TableEntity tableEntity = new TableEntity();
		tableEntity.setTableName(scanRequest.getTableName());
		ScanResult scanResult =null;
		
		do {
			if(scanResult!=null) {
				scanRequest.setExclusiveStartKey(scanResult.getLastEvaluatedKey());
			}
			scanResult = db.scan(scanRequest);	
			
			int diff = null!=scanRequest.getLimit()?scanRequest.getLimit()-tableEntity.getColumnValues().size():scanResult.getCount();
			List<java.util.Map<String, AttributeValue>> items = scanResult.getCount()>diff ? scanResult.getItems().subList(0, diff):scanResult.getItems() ;			
			scanResult.setLastEvaluatedKey(scanResult.getCount()>diff?scanResult.getItems().get(diff-1):scanResult.getLastEvaluatedKey());
			tableEntity.getColumnValues().addAll(super.getTableEntity(items, awsDataFilter.getEntity().getTableName()).getColumnValues());			
			
		}while(scanResult.getLastEvaluatedKey()!=null &&tableEntity.getColumnValues().size()< scanRequest.getLimit());

		awsDataFilter.setEntity(tableEntity);
		super.setPageDetailsFromResult(awsDataFilter.getPages(), scanResult);

		return awsDataFilter;
	}
	


}
