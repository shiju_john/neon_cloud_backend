package com.flytxt.aws.exception;
/**
 * 
 * @author shiju.john
 *
 */
public class BucketPathExistException extends AwsServiceException {
	
	public BucketPathExistException(String messag) {
		super(messag);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
