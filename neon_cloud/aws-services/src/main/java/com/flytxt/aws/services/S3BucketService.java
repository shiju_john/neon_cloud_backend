package com.flytxt.aws.services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketNotificationConfiguration;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.Filter;
import com.amazonaws.services.s3.model.FilterRule;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.QueueConfiguration;
import com.amazonaws.services.s3.model.S3Event;
import com.amazonaws.services.s3.model.S3KeyFilter;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectId;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.SetBucketNotificationConfigurationRequest;
import com.amazonaws.services.s3.model.VersionListing;

/**
 * 
 * @author shiju.john
 *
 */
public class S3BucketService {

	private AmazonS3 s3Client;
	private static final String SUFFIX = "/";

	/**
	 * 
	 * @param awsStaticCredentialsProvider
	 */
	public S3BucketService(AWSStaticCredentialsProvider awsStaticCredentialsProvider, Regions region) {

		this.s3Client = AmazonS3ClientBuilder.standard().withRegion(region)
				.withCredentials(awsStaticCredentialsProvider).build();
	}

	/**
	 * 
	 * @param bucketName
	 */
	public void deleteBucket(String bucketName) {

		try {

			ObjectListing objectListing = s3Client.listObjects(bucketName);
			while (true) {
				Iterator<S3ObjectSummary> objIter = objectListing.getObjectSummaries().iterator();
				while (objIter.hasNext()) {
					s3Client.deleteObject(bucketName, objIter.next().getKey());
				}

				if (objectListing.isTruncated()) {
					objectListing = s3Client.listNextBatchOfObjects(objectListing);
				} else {
					break;
				}
			}

			VersionListing versionList = s3Client.listVersions(new ListVersionsRequest().withBucketName(bucketName));
			while (true) {
				Iterator<S3VersionSummary> versionIter = versionList.getVersionSummaries().iterator();
				while (versionIter.hasNext()) {
					S3VersionSummary vs = versionIter.next();
					s3Client.deleteVersion(bucketName, vs.getKey(), vs.getVersionId());
				}

				if (versionList.isTruncated()) {
					versionList = s3Client.listNextBatchOfVersions(versionList);
				} else {
					break;
				}
			}

			s3Client.deleteBucket(bucketName);
		} catch (AmazonServiceException e) {
		}

	}

	/**
	 * 
	 * @param bucketName
	 * @return
	 */
	public String createBucket(String bucketName) {

		if (!s3Client.doesBucketExistV2(bucketName)) {
			s3Client.createBucket(new CreateBucketRequest(bucketName));

			String bucketLocation = s3Client.getBucketLocation(new GetBucketLocationRequest(bucketName));
			return bucketLocation;
		}
		throw new AmazonServiceException("Bucket already exist : " + bucketName);

	}

	public boolean hasBucketExist(String bucketName) {
		return s3Client.doesBucketExistV2(bucketName);

	}

	/**
	 * 
	 * @param bucketName
	 * @param folderName
	 */
	public void createFolder(String bucketName, String folderName) {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + SUFFIX, emptyContent,
				metadata);
		s3Client.putObject(putObjectRequest);
	}

	/**
	 * This method first deletes all the files in given folder and than the folder
	 * itself
	 */
	public void deleteFolder(String bucketName, String folderName) {
		List<S3ObjectSummary> fileList = s3Client.listObjects(bucketName, folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			s3Client.deleteObject(bucketName, file.getKey());
		}
		s3Client.deleteObject(bucketName, folderName);
	}

	/**
	 * 
	 * @param bucketName
	 * @param folderName
	 * @return
	 */
	public List<S3ObjectSummary> getObjet(String bucketName, String folderName) {
		
		return s3Client.listObjects(bucketName, folderName).getObjectSummaries();

	}

	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @return
	 */
	public BufferedReader getS3File(String bucketName, String fileKey) {
		return getFileReader(s3Client.getObject(new GetObjectRequest(new S3ObjectId(bucketName, fileKey))));
	}

	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @param start
	 * @param end
	 * @return
	 */
	public BufferedReader getS3FileRange(String bucketName, String fileKey, int start, int end) {
		return getFileReader(
				s3Client.getObject(new GetObjectRequest(new S3ObjectId(bucketName, fileKey)).withRange(start, end)));

	}

	/**
	 * 
	 * @param s3Object
	 * @return
	 */
	private BufferedReader getFileReader(S3Object s3Object) {

		/*
		 * java.util.Date expiration = new java.util.Date(); long expTimeMillis =
		 * expiration.getTime(); expTimeMillis += 1000 * 60 * 60;
		 * expiration.setTime(expTimeMillis); GeneratePresignedUrlRequest
		 * generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName,
		 * objectKey) .withMethod(HttpMethod.POST) .withExpiration(expiration); URL url
		 * = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		 */
		return new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));

	}

	/**
	 * 
	 * @param bucketName
	 * @param sourceFileKey
	 * @param targetFileKey
	 */
	public void moveFile(String bucketName, String sourceFileKey, String targetFileKey) {

		s3Client.copyObject(new CopyObjectRequest(bucketName, sourceFileKey, bucketName, targetFileKey));
		s3Client.deleteObject(new DeleteObjectRequest(bucketName, sourceFileKey));

	}

	/**
	 * 
	 * @return
	 */
	public String getAccountOwner() {
		return s3Client.getS3AccountOwner().getId();
	}

	/**
	 * Add S3 event notification to SQS queue
	 * 
	 * @param bucketName
	 * @param queueArn
	 */
	public void addQueueNotification(String bucketName, String queueArn, String configName) {
		BucketNotificationConfiguration notificationConfiguration = new BucketNotificationConfiguration();
		QueueConfiguration configuration = new QueueConfiguration(queueArn, EnumSet.of(S3Event.ObjectCreatedByPost,S3Event.ObjectCreatedByPut));
		
		S3KeyFilter filter = new S3KeyFilter();
		
		filter.addFilterRule(new FilterRule().withName("prefix").withValue("input/"));
		filter.addFilterRule(new FilterRule().withName("suffix").withValue(".csv"));
		configuration.setFilter( new  Filter().withS3KeyFilter(filter));		
		notificationConfiguration.addConfiguration(configName,configuration);
		
		SetBucketNotificationConfigurationRequest request = new SetBucketNotificationConfigurationRequest(bucketName,
				notificationConfiguration);
		
		s3Client.setBucketNotificationConfiguration(request);
	}
	
	
	/**
	 * 
	 * @param bucketName
	 * @param fileName
	 * @param text
	 */
	void writeFile(String bucketName,String fileName,String text) {
		 s3Client.putObject(bucketName, fileName, text);         
//         // Upload a file as a new object with ContentType and title specified.
//         PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, new File(fileName));
//         ObjectMetadata metadata = new ObjectMetadata();
//         metadata.setContentType("plain/text");
//         metadata.addUserMetadata("x-amz-meta-title", "someTitle");
//         request.setMetadata(metadata);
//         s3Client.putObject(request);
	}
	
	
	/**
	 * 
	 * @param bucketName
	 * @param domainName
	 * @param type
	 * @return
	 */
	public List<String>  getFiles(String bucketName,String domainName,String type){
		List<String> fileNames = new ArrayList<>();
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
                .withPrefix("configuration/"+domainName+"/"+type );
		ObjectListing listing = s3Client.listObjects(listObjectsRequest);
		if(listing!=null && listing.getObjectSummaries()!=null ) {
			for(S3ObjectSummary objectSummaries : listing.getObjectSummaries()) {
				if(!objectSummaries.getKey().endsWith("/")) {
					String fileName = objectSummaries.getKey().substring(objectSummaries.getKey().lastIndexOf("/")+1);					
					String configPath =  objectSummaries.getKey().substring(0,objectSummaries.getKey().lastIndexOf("/"));
					String configName = configPath.substring(configPath.lastIndexOf("/")+1);
					fileNames.add(configName+"/"+fileName);
					
					
				}
			}
		}
		
		return fileNames;
		
	}
	
	/**
	 * 
	 */
	
	public String getPresignedUrl(String bucketName,String path) {
		Date expiration = new Date(System.currentTimeMillis()+1000*60*30);		
		return s3Client.generatePresignedUrl(bucketName, path, expiration).toString();
	}

	public long getFileInfo(String bucketName, String fileKey) {
		return s3Client.getObjectMetadata(bucketName, fileKey).getInstanceLength();
	}

}
