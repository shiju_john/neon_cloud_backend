package com.flytxt.aws.services;

import java.util.Map;

import org.jclouds.ssh.SshKeys;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.transfer.AWSTransfer;
import com.amazonaws.services.transfer.AWSTransferClientBuilder;
import com.amazonaws.services.transfer.model.CreateServerRequest;
import com.amazonaws.services.transfer.model.CreateServerResult;
import com.amazonaws.services.transfer.model.CreateUserRequest;
import com.amazonaws.services.transfer.model.DeleteServerRequest;
import com.amazonaws.services.transfer.model.DeleteUserRequest;
import com.amazonaws.services.transfer.model.DescribeUserRequest;
import com.amazonaws.services.transfer.model.DescribeUserResult;
import com.amazonaws.services.transfer.model.DescribedUser;
import com.amazonaws.services.transfer.model.ResourceNotFoundException;
import com.flytxt.aws.entity.AwsTransferUser;

/**
 * 
 * @author shiju.john
 *
 */
public class AwsSFTPService {

	private AWSTransfer awsTransfer;

	/**
	 * 
	 * @param awsStaticCredentialsProvider
	 */
	public AwsSFTPService(AWSStaticCredentialsProvider awsStaticCredentialsProvider,Regions region) {
		this.awsTransfer = AWSTransferClientBuilder.standard().withRegion(region).withCredentials(awsStaticCredentialsProvider).build();
	}

	/**
	 * 
	 * @return
	 */
	public String createSFTPServer() {
		CreateServerResult server = awsTransfer.createServer(new CreateServerRequest());
		return server.getServerId();
	}

	/**
	 * 
	 * @param serverId
	 */
	public void deleteSFTPServer(String serverId) {
		awsTransfer.deleteServer(new DeleteServerRequest().withServerId(serverId));
	}

	/**
	 * 
	 * @param serverId
	 * @param userName
	 * @return
	 */
	public DescribedUser getUser(String serverId, String userName) {
		try {
			DescribeUserResult result = awsTransfer
					.describeUser(new DescribeUserRequest().withServerId(serverId).withUserName(userName));
			return result.getUser();
		} catch (ResourceNotFoundException e) {
			return null;
		}

	}

	/**
	 * 
	 * @param serverId
	 * @param userName
	 */
	public void deleteUser(String serverId, String userName) {
		awsTransfer.deleteUser(new DeleteUserRequest().withServerId(serverId).withUserName(userName));
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getSSHKey() {
		return SshKeys.generate();
	}

	/**
	 * 
	 * @param transferUser
	 */
	public void createUser(AwsTransferUser transferUser) {
		CreateUserRequest createUserRequest = new CreateUserRequest();
		createUserRequest.setUserName(transferUser.getUserName());
		createUserRequest.setHomeDirectory(transferUser.getHomeDir());
		createUserRequest.setRole(transferUser.getRoleARN());
		createUserRequest.setPolicy(transferUser.getPolicyJson());
		createUserRequest.setServerId(transferUser.getServerId());
		createUserRequest.setSshPublicKeyBody(transferUser.getSshPublicKey());
		awsTransfer.createUser(createUserRequest);

	}

}
