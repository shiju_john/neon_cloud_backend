package com.flytxt.vision.core.exception;

/**
 * 
 * @author shiju.john
 *
 */
public class ZeppelinException extends VisionException {

	public ZeppelinException(String message, Exception e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
