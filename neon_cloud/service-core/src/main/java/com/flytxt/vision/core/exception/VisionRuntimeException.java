package com.flytxt.vision.core.exception;


/**
 * 
 * @author shiju.john
 *
 */
public class VisionRuntimeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VisionRuntimeException(String message) {
        super(message);
    }

}
