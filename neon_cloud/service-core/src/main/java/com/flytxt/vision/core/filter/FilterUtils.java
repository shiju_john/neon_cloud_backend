package com.flytxt.vision.core.filter;

import java.util.ArrayList;
import java.util.List;

import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.filter.entity.FilterRules;

/**
 * 
 * @author shiju.john
 *
 */
public abstract class FilterUtils {
	
	/**
	 * 
	 * @param fieldName
	 * @param operator
	 * @param value
	 * @return
	 */
	public static final Filter getFilterCriteriaList(final String fieldName, final String operator,
			final String value) { 
		
		FilterRules criteria =  new FilterRules();
		criteria.setKey(fieldName);
		criteria.setOperator(operator);
		criteria.setValue(value);
		List<FilterRules> criterias = new ArrayList<>();
		criterias.add(criteria);
		return new Filter(criterias);
		

	}
	
	

}
