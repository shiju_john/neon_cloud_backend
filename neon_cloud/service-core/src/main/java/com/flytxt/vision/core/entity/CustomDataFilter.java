package com.flytxt.vision.core.entity;

import java.util.Map;

import com.flytxt.vision.core.entity.NonPersistableVisionEntity;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.filter.entity.Filter;
/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public class CustomDataFilter<T> implements NonPersistableVisionEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Filter filterObj;
	private Pages<T> pages;
	private Map mapping;
	private T entity;
	private String [] fields;
		
	
	
	/**
	 * @return the filterObj
	 */
	public Filter getFilterObj() {
		return filterObj;
	}
	
	
	/**
	 * @param filterObj the filterObj to set
	 */
	public void setFilterObj(Filter filterObj) {
		this.filterObj = filterObj;
	}
	
	
	/**
	 * @return the pages
	 */
	public Pages<T> getPages() {
		return pages;
	}
	
	
	/**
	 * @param pages the pages to set
	 */
	public void setPages(Pages<T> pages) {
		this.pages = pages;
	}
	
	
	


	/**
	 * @return the entity
	 */
	public T getEntity() {
		return entity;
	}


	/**
	 * @param entity the entity to set
	 */
	public void setEntity(T entity) {
		this.entity = entity;
	}


	/**
	 * @return the mapping
	 */
	public Map getMapping() {
		return mapping;
	}


	/**
	 * @param mapping the mapping to set
	 */
	public void setMapping(Map mapping) {
		this.mapping = mapping;
	}


	/**
	 * @return the fields
	 */
	public String [] getFields() {
		return fields;
	}


	/**
	 * @param fields the fields to set
	 */
	public void setFields(String [] fields) {
		this.fields = fields;
	}

}
