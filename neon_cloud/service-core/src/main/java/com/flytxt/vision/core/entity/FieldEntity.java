package com.flytxt.vision.core.entity;

/**
 * 
 * @author shiju.john
 *
 */

public class FieldEntity implements NonPersistableVisionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fieldName;	
	private String operations;
	private String type;
	
	public FieldEntity(String fieldName, String type,String operations){
		this.fieldName=fieldName;
		this.operations=operations;
		this.type=type;		
	}
	
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}
	
	
	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	/**
	 * @return the operations
	 */
	public String getOperations() {
		return operations;
	}


	/**
	 * @param operations the operations to set
	 */
	public void setOperations(String operations) {
		this.operations = operations;
	}
	
	

}
