package com.flytxt.vision.core.exception;
/**
 * 
 * @author shiju.john
 *
 */
public enum MessageType {
	  SUCCESS, INFO, WARNING, ERROR
}
