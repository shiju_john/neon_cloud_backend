package com.flytxt.vision.utils;

import java.util.Base64;
import java.util.Map;
import java.util.UUID;

import com.flytxt.vision.core.filter.entity.Filter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author shiju.john
 *
 */
public class TenantUtils {
	
	public static Gson gson = new Gson();
	
	
	
	/**
	 * 
	 * @param userName
	 * @param userRole
	 * @param domainName
	 * @return
	 */
	public static String getTenantId(String userName,String userRole,String domainName) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("userName", userName);
		jsonObject.addProperty("role", userRole);
		jsonObject.addProperty("domain", domainName);		
		jsonObject.addProperty("UID", UUID.randomUUID().toString());			
		return Base64.getEncoder().encodeToString(jsonObject.toString().getBytes());		
	}
	
	/**
	 * 
	 * @param tennatId
	 * @return
	 */
	public static String getDomainName(String tenantId) {
		if(HelperUtils.isNotNull(tenantId)) {
			Map<String,String> data = gson.fromJson(new String(Base64.getDecoder().decode(tenantId)), 
					new TypeToken<Map<String, Object>>() {
								}.getType());
				
				return data.get("domain");	
			
		}
		return null;			
	}
	
	public static String awsDomainName(String tennatId) {
		return TenantUtils.getDomainName(tennatId).replaceAll("\\.", "_");
	}
	
	public static String replaceToAwsName(String name) {
		return name.replaceAll("\\.", "_");
	}
	
	/**
	 * 
	 * @param tennatId
	 * @param entityType
	 * @return
	 */
	public static String getTableName(String tennatId,String entityType) {
		return getDomainName(tennatId) + "_" +entityType;		
		
	}
	
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static String mapToJsonString(Map json){		
		return  gson.toJson(json); 
	}
	
	
	
	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Map<String,Object> parseJson(String jsonString) {
		return gson.fromJson(jsonString, 
				new TypeToken<Map<String, Object>>() {
		}.getType());		
		
	}
	
	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Map<String,String> parseJsonToMapString(String jsonString) {
		return gson.fromJson(jsonString, 
				new TypeToken<Map<String, String>>() {
		}.getType());		
		
	}
	
	public static Filter convertMapToFilter(Map filterMap) {
		return gson.fromJson(convertToJson(filterMap),Filter.class);
		
	}
	
	
	
	
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static String convertToJson(Object json){		
		return  gson.toJson(json); 
	}
	
	public static Gson getJsonConvertor() {
		return gson;
		
	}

	
	
	
	

}
