package com.flytxt.vision.core.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author shiju.john
 *
 */
public interface NonPersistableVisionEntity extends VisionEntity {
	
	@XmlTransient
	@com.fasterxml.jackson.annotation.JsonIgnore
	default public Serializable getId() {
		return null;
	}
	
	
	@XmlTransient
	@com.fasterxml.jackson.annotation.JsonIgnore
	default public String getTenantId() {
		return null;
	}
	
	
	@XmlTransient
	@com.fasterxml.jackson.annotation.JsonIgnore
	default public void setTenantId(String tenantId) {
		
	}
	
	default public Long getVersion() {
		return 0L;
	}

	/**
	 * @param version the version to set
	 */
	default public void setVersion(Long version) {
		
	}

}
