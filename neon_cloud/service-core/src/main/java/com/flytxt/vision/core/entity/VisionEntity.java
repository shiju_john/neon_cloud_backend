package com.flytxt.vision.core.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;
/**
 * 
 * @author shiju.john
 *
 */
public interface VisionEntity extends Serializable{
	
	/**
	 * 
	 * @return
	 */
	

	@XmlTransient
	@com.fasterxml.jackson.annotation.JsonIgnore
	public Serializable getId();
	
	
	
	public String getTenantId();
	
	
	
	public void setTenantId(String tenantId);
	
	/**
	 * 
	 * @return
	 */
	public Long getVersion();

	/**	
	 * 
	 * @param version
	 */
	public void setVersion(Long version);

	
	
	

}
