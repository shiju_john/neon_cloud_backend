package com.flytxt.vision.core.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.flytxt.vision.core.entity.VisionEntity;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.HelperUtils;
/**
 * 
 * @author shiju.john
 *
 */
@ControllerAdvice
public class ResponseFilterAdvice implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		String tenantId = null;
		
	    if(request.getURI().getPath().contains("getTenants")) {
	    	return body;	    	
	    }
	    
		List<String> tenantList = request.getHeaders().get(Constants.TENANT_ID);
		if (null != tenantList && !tenantList.isEmpty() && HelperUtils.isNotNull(tenantList.get(0)) && !"null".equalsIgnoreCase(tenantList.get(0))) {
			tenantId = tenantList.get(0);
			if (body instanceof Iterable) {
				return filter(((Iterable) body).iterator(), tenantId);
			} else if (body instanceof List) {
				return filter(((List) body).iterator(), tenantId);
			} else if (body instanceof VisionEntity) {
				return  isvalid((VisionEntity) body,tenantId)?body:null;
			}
		}

		return body;
	}

	/**
	 * 
	 * @param iterator
	 * @param tenantId
	 * @return
	 */
	private List<Object> filter(Iterator<Object> iterator, String tenantId) {

		List<Object> result = new ArrayList<>();
		while (iterator.hasNext()) {
			Object value = iterator.next();
			if (value instanceof VisionEntity) {
				if (isvalid((VisionEntity) value, tenantId)) {
					result.add(value);
				}				
			}else {
				result.add(value);
			}
		}
		return result;

	}
	
	/**
	 * 
	 * @param entity
	 * @param tenantId
	 * @return
	 */
	private boolean isvalid(VisionEntity entity,String tenantId) {
		return tenantId.equalsIgnoreCase(entity.getTenantId()) || !HelperUtils.isNotNull(entity.getTenantId());
		
	}

}
