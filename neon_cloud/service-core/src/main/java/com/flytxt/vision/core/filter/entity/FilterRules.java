package com.flytxt.vision.core.filter.entity;

import javax.validation.constraints.NotNull;

import com.flytxt.vision.core.entity.NonPersistableVisionEntity;

/**
 * 
 * @author shiju.john
 *
 */
public class FilterRules implements NonPersistableVisionEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private String key;
	
	@NotNull
	private String operator;
	
	@NotNull
	private String value;
	

	private String type;	
	

	public FilterRules(){
		
	}
	
	public FilterRules(String key, String operator,String value){
		this.key=  key;
		this.setOperator(operator);
		this.value = value;		
	}
	
	public FilterRules(String key, String operator,String value,String type){
		this.key=  key;
		this.setOperator(operator);
		this.value = value;		
		this.type=type;
	}
	
	public FilterRules(String key,String type){
		this.key=  key;
		this.type = type;		
	}
	
	


	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}


	
}
