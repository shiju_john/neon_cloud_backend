package com.flytxt.vision.utils;

/**
 * 
 * @author shiju.john
 *
 */
public class HelperUtils {
	
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNotNull(String value) {
		return value!=null && value!="" && value.length()>0;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNumeric(String value) {
		return value!=null?value.matches("-?\\d+(\\.\\d+)?")?true:false:false; 
	}
	
	public static boolean checkNull(String value) {
		return value==null || value=="" || value.length()>0 || "null".equalsIgnoreCase(value);
	}
	
	

}
