package com.flytxt.vision.core.filter;

import com.flytxt.vision.core.filter.entity.Filter;
/**
 * 
 * @author shiju.john
 *
 * @param <T> 
 */
public class PredicatesBuilder<T> {
	  
	private Filter params;
	  
    public PredicatesBuilder() {
        
    }
	
    /**
     * 
     * @param criteria
     * @return
     */
    public PredicatesBuilder<T> with(Filter criteria) {	   
        params = criteria;
        return this;
    }
	
	/***
	 * 
	 * @param entity
	 * @return
	 */
    /* @SuppressWarnings("rawtypes")
	public BooleanExpression build(Class entity,String aliasName) {
    	BooleanExpression expression=null ;
        if (params != null) {
        	expression = processFilters(params, entity, aliasName);
        }         
       return expression;          
    }	*/
    
     /*private BooleanExpression processFilters(Filter criteria,Class entity,String aliasName){	    
	   BooleanExpression mainExpression  = processRules( criteria.getRules(),criteria.getCondition(),entity,aliasName);	   
	   for (Filter innerFilter: criteria.getInnergroups()) {
		   if( mainExpression!=null) {
			   mainExpression = innerFilter.getCondition().equalsIgnoreCase("and") ? mainExpression.
					   and(processFilters(innerFilter,entity, aliasName)):
						   mainExpression.or(processFilters(innerFilter,entity, aliasName));
		   }else {
			   mainExpression=processFilters(innerFilter,entity, aliasName);
		   }		   
	   }	  
	   return mainExpression;
    }
    
    private BooleanExpression processRules(List<FilterRules> rules,String condition,Class entity,String aliasName) {
    	FilterPredicate<T> predicate;
    	BooleanExpression exp = null;  
    	if(rules!=null && rules.size()>0) {
        	for (FilterRules fields :rules) {
        		predicate = new FilterPredicate<T>(fields);
        		 BooleanExpression expression = predicate.getPredicate(entity,aliasName);
                 if (exp != null) {                	 
                	 exp = condition.equalsIgnoreCase("and")?exp.and(expression): exp.or(expression);           	     		 
                	
                 }else{
                	 exp= expression;
                 }
        	}      		
        	
        }
    	return exp;
    	
    }*/

}
