package com.flytxt.vision.core.service;

import java.io.Serializable;

import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public interface VisionService<T, E> {

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public T save(T entity) throws VisionException;

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public T get(T entity) throws VisionException;

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public T update(T entity) throws VisionException;

	/**
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @return
	 * @throws Exception
	 */
	public Pages<T> findPage(Pages<T> pages) throws VisionException;

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public T delete(T entity) throws VisionException;

	/**
	 * 
	 * @return
	 */
	public Iterable<T> findAll() throws VisionException;

	/**
	 * 
	 * @return
	 */
	public Pages<T> search(E criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException;

	/** 
	 * 
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	public Iterable<T> search(E criteria) throws VisionException;
	

	public T deleteById(Serializable id) throws VisionException;
	
	/**
	 * 
	 * @param entities
	 * @return
	 * @throws VisionException
	 */

	public Iterable<T> saveAll(Iterable<T> entities) throws VisionException;
	
	
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	default public boolean validate(T entity) throws VisionException{
		return true;
	}
	
	
	
	


}
