package com.flytxt.vision.utils;

public class Constants {
	
	public static final String TENANT_ID = "Tenant-Id";
	public static final String USER_ROLE_HEADER = "user-role";
	public static final String APP_ID_HEADER ="appId";
	public static final String ENTITY_TYPE ="entityType";
	public static final String MODE_NEW = "new";
	public static final String MODE = "mode";
	public static final String FILE_NAME = "fileName";
	public static final String MAPPING = "mapping";
	public static final String META_TEXT = "text";
	public static final String META_DEST = "destCol";
	public static final String SFTP_KEY = "sftpSettings";
	public static final String SSH_KEY = "sshKey";
	
	
	
	public static final String AWS_REGION_KEY = "awsRegion";
	public static final String CONFIG_KEY = "configuration";
	public static final String AWS_BUCKET_NAME_KEY ="awsBucketName";
	public static final String AWS_BUCKET_ARN_KEY = "awsBucketArn";
	public static final String AWS_S3_QUEUE_NAME_KEY = "awsS3QueueName";
	public static final String AWS_SFTP_ROLE_ARN_KEY = "awsSftpRoleArn";
	public static final String AWS_SFTP_SERVER_END_POINTS_KEY = "awsSftpServerEndPoint";
	public static final String AWS_SFTP_SERVER_ID_KEY = "awsSftpServerId";
	public static final String AWS_SFTP_ROLE_POLICY_NAME_KEY = "awsSftpRolePolicyName";
	public static final String AWS_SFTP_USER_POLICY_KEY = "awsSftpUserPolicy";
	public static final String AWS_ASFTP_ROLE_NAME_KEY = "awsSftpRoleName";
	public static final String AWS_SFTP_ROLE_POLICY_ARN_KEY ="awsSftpRolePolicyArn";
	public static final String AWS_TIME = "fileCreatedTime";
	public static final String FILE_KEY = "file";
	public static final String FILTER_KEY = "filter";
	public static final String META_TYPE = "type";
	public static final String AUDIENCE_GROUP_KEY = "audienceGroup";

}
