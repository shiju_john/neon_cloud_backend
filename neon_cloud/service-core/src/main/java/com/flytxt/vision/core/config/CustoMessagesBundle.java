package com.flytxt.vision.core.config;

import java.util.Locale;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
/**
 * 
 * @author shiju.john
 *
 */
public class CustoMessagesBundle  extends ReloadableResourceBundleMessageSource{
	
	//private CustomPropertiesPersistor propertiesPersister;
	
	public void setCustomPersister(CustomPropertiesPersistor propertiesPersister) {	
		super.setPropertiesPersister(propertiesPersister);
		//this.propertiesPersister = propertiesPersister;
	}

	
	
	public  Properties getProperties(Locale locale) {
		PropertiesHolder obj =  super.getMergedProperties(locale);
		return obj.getProperties();
		
	}


}
