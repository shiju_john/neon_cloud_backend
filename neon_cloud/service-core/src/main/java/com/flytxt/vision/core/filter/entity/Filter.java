package com.flytxt.vision.core.filter.entity;

import java.util.ArrayList;
import java.util.List;

import com.flytxt.vision.core.entity.NonPersistableVisionEntity;


/**
 * 
 * @author shiju.john
 *
 */
public class Filter implements NonPersistableVisionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 
	 */
	private String condition;
	
	
	/**
	 * 
	 */
	private List<FilterRules> rules;
	
	
	/**
	 * 
	 */
	private List<Filter>  innergroups;
	
	
	public Filter(){
		this(null,"AND");	
		
	}
	
	public Filter(List<FilterRules> rules){
		this(rules,"AND");		
	}
	
	public Filter(List<FilterRules> rules,String condition){
		this.rules = rules;
		this.condition =condition;
		innergroups= new ArrayList<>();
		
	}

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the rules
	 */
	public List<FilterRules> getRules() {
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(List<FilterRules> rules) {
		this.rules = rules;
	}

	/**
	 * @return the innergroup
	 */
	public List<Filter> getInnergroups() {
		return innergroups;
	}

	/**
	 * @param innergroup the innergroup to set
	 */
	public void setInnergroups(List<Filter> innergroup) {
		this.innergroups = innergroup;
	}

	public void addRules(FilterRules filterRules) {
		if(rules==null) {
			rules = new  ArrayList<>();
		}
		rules.add(filterRules);
	}
	
	


}
