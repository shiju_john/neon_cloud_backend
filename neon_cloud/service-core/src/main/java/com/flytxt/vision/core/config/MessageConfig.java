package com.flytxt.vision.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author shiju.john
 *			
 */
@Component
public class MessageConfig {
	
	@Bean(name = "messageSource")
	public ReloadableResourceBundleMessageSource messageSource() {		
		
		CustoMessagesBundle messageSource = new CustoMessagesBundle();
		CustomPropertiesPersistor propertiesPersistor = new CustomPropertiesPersistor();	
		messageSource.setCustomPersister(propertiesPersistor);
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	
	}

}
