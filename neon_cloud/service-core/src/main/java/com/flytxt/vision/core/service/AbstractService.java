package com.flytxt.vision.core.service;

import java.util.function.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;

import ch.qos.logback.core.filter.Filter;
/**
 * 
 * @author shiju.john
 *
 * @param <T>
 * @param <E> 
 */
public  abstract class AbstractService<T,E> implements VisionService<T,E>{

	protected VisionDao<T> dao;
	
	public AbstractService(VisionDao<T> dao){
		this.dao = dao;
	}
	
	
	@SuppressWarnings("rawtypes")	
	public Predicate getPredicate(Class classType, String aliasName,Filter criterias) {
		/*PredicatesBuilder builder = new PredicatesBuilder();
		if (criterias!=null) {
			builder.with(criterias);
		}	
		
		return  builder.build(classType,aliasName);*/
		return null;
	}	
	
	/**
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @return 
	 */
	protected PageRequest getPageRequest(Pages<T> pages){
		Direction direction =  null!=pages.getSortOrder() ? "ASC".equalsIgnoreCase(pages.getSortOrder())?Direction.ASC:Direction.DESC:null;
		return  null!=direction ?PageRequest.of(pages.getPageNo(), pages.getPageSize(),direction,pages.getSortFields()): 
			PageRequest.of(pages.getPageNo(), pages.getPageSize());
		
	}
	
	/**
	 * 
	 */
	@Override
	public T save(T entity)  throws VisionException{
		validateEntity(entity);
		return dao.save(entity);
	}
	
	
	
	
	/**
	 * 
	 * @param entity
	 * @throws VisionException
	 */
	protected void validateEntity(T entity)  throws VisionException {
		try{
			validate(entity);			
		}catch(VisionException e){
			throw e;
		}catch(Exception e){
			throw new VisionException(e.getMessage(), e);
		}
	}
	
	
	@Override
	public boolean validate(T entity) throws VisionException {
		
//		for (Field f: entity.getClass().getDeclaredFields()) {
//				Unique unique = f.getAnnotation(Unique.class);
//				if(unique!=null) {
//					Iterable<T> records = dao.findBy(entity, null, "by"+f.getName());
//					if(records.iterator().hasNext()) {
//						throw new VisionException("error."+f.getName()+".exist",null);
//					}
//				}
//			   
//			}
		
		return true;
	}
	
	@Override
	public T delete(T entity) throws VisionException {
		if(dao.isExists(((com.flytxt.vision.core.entity.VisionEntity)entity).getId())){
			dao.delete(entity);
			return entity;
		}
		throw new VisionException("error.common.notfound", null);
	}
	
	/**
	 * 
	 * @param jobDetails
	 * @return
	 * @throws VisionException
	 */
	@Override
	public T update(T entity) throws VisionException {
		if(dao.isExists(((com.flytxt.vision.core.entity.VisionEntity)entity).getId())){
			validateEntity(entity);
			return dao.save(entity);
		}
		throw new VisionException("error.common.notfound", null);
	}
	

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterable<T> findAll() {
		return dao.findAll();
	}
	
	@Override 
	public T get(T entity) throws VisionException {		
		return dao.get(entity); 
	}


	@Override
	public Pages<T> findPage(Pages<T> pages) throws VisionException{
		Page<T> page =  dao.findPage(getPageRequest(pages));
		return getPaginationResult(page);
	}
	
	@Override
	public Iterable<T> saveAll(Iterable<T> entities) throws VisionException{
		return dao.saveAll(entities);
	}
	
	/**
	 * 
	 * @param page
	 * @return
	 */
	protected Pages<T> getPaginationResult(Page<T> page) {
		Pages<T> result =  new Pages<>();
		result.setContent(page.getContent());
		result.setTotalElement(page.getTotalElements());
		result.setTotalPage(page.getTotalPages());
		result.setHasPrevious(page.hasPrevious());
		result.setHasNext(page.hasNext());		
		return result;		
	}


	
	
	
}
