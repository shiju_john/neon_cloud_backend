package com.flytxt.vision.core.dao;

import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
/**
 * 
 * @author shiju.john
 *
 * @param <T>
 */
public interface DDLDao<T> {

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */
	public T createTable(T entity) throws VisionException;

	/**
	 * 
	 * @param T
	 * @return
	 * @throws VisionException
	 */
	public T save(T entity) throws VisionException;

	/**
	 * 
	 * @param tableName
	 * @param keyFieldValue
	 * @param keyField
	 * @return
	 */
	public T findById(final String tableName, final String keyFieldValue, String keyField) throws VisionException;

	/**
	 * 
	 * @param filter
	 * @param tableName
	 * @return
	 */
	public T findByExpresion(Filter filter, String tableName);

	/**
	 * 
	 * @param tableName
	 * @return
	 * @throws VisionException
	 */
	public T findAll(final String tableName) throws VisionException;

	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */
	public T update(T entity) throws VisionException;
	
	/**
	 * 
	 * @param dataFilter
	 * @param tenantId
	 * @return
	 */
	public CustomDataFilter<T> findByFilterExpression(CustomDataFilter<T> dataFilter, String tenantId) throws VisionException;
	
	
	
	

	

}
