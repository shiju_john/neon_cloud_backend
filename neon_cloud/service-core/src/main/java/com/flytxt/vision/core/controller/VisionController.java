package com.flytxt.vision.core.controller;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.ApiError;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.VisionService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
/**
 * 
 * @author shiju.john
 *
 * @param <FlyEntity>
 */
@Controller
public abstract class VisionController<T,E> {
	
	
	private Gson gson; 
	
	public VisionService<T, E> service;
	
	public VisionController(VisionService<T,E> service){
		this.service =service ;
		GsonBuilder gsonBuilder  = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(List.class, deserializer);
		this.gson = gsonBuilder.create(); 
	}
	
	/**
	 * 
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResponseEntity<T> getErrorResponse(VisionException e) {
		ApiError  error = new ApiError(HttpStatus.BAD_REQUEST,e.getLocalizedMessage());
		return new ResponseEntity<T>((T)error,HttpStatus.BAD_REQUEST);
		
	}
	
	/*@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void logout(HttpSession session) {
	    session.invalidate();
	}*/
	
//	@RequestMapping(value = "/dashboad/filter",method=RequestMethod.POST)
//	public List<VisionEntity> filterData(@RequestBody  String entityName,@RequestBody  String filters){				 
//		List<Filter> filtersList = gson.fromJson(filters, List.class);
//		return null;
//		
//	}
	
	
	@RequestMapping(path="/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<T> delete (@PathVariable("id") Serializable id) throws VisionException{		
		return new ResponseEntity<>(service.deleteById(id),HttpStatus.OK);	
		
	}
	
	private JsonDeserializer<List<Filter>> deserializer = new JsonDeserializer<List<Filter>>() {  
	    @Override
	    public List<Filter> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
	       List<Filter> filters = new ArrayList<>();
	    	if (json.isJsonArray()) {	        	
	        	for (JsonElement data : (JsonArray)json ) {
	        		if (null!=data.getAsJsonObject().get("conditionalOperator")) {
//	        			filters.add(new ConditionOperator(data.getAsJsonObject().get("conditionalOperator").getAsString()));
	        			
	        		}else {
	        			String key = data.getAsJsonObject().get("key").getAsString();
	        			String value = data.getAsJsonObject().get("value").getAsString();
	        			
	        			String operation = data.getAsJsonObject().get("operation").getAsString();
//	        			filters.add(new FilterFields(key,operation,value));
	        		}	        		
	        	}				
				
			}    

	       return filters;
	    }
	};
	

	@RequestMapping( method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<T> save(@RequestBody @Valid T config)
			throws VisionException {		
		T configData = service.save(config);
		return new ResponseEntity<>(configData, HttpStatus.OK);
	}
	
	@RequestMapping( method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<T> update(@RequestBody @Valid T entity)
			throws VisionException {
		
		entity = service.update(entity);
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}
	
	
	
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Iterable<T>> finadAll () throws VisionException{	
		Iterable<T> configdetails = service.findAll();
		return new ResponseEntity<>(configdetails,HttpStatus.OK);			
	}
	
	
	@RequestMapping(path="/filter", method = RequestMethod.POST)
	public ResponseEntity<Iterable<T>> search(@RequestBody @Valid E filterCriteria)
			throws VisionException {
		Iterable<T> configdetails = service.search(filterCriteria);
		return new ResponseEntity<>(configdetails, HttpStatus.OK);
	}
	
	
	
	/*@RequestMapping(value = "/page", params = { "pageNo", "pageSize", "sortField","sortOrder" }, method = RequestMethod.GET)
	public ResponseEntity<Pages<T>> findPage(@RequestParam("pageNo") Integer pageNo,
		@RequestParam("pageSize") Integer pageSize, @RequestParam("sortField") String sortField,
		@RequestParam("sortOrder") String sortOrder) throws VisionException {
	Pages<T> serviceDetails = service.findPage(pageNo, pageSize, sortField, sortOrder);
	return new ResponseEntity<>(serviceDetails, HttpStatus.OK);
	
	}*/
	
	@RequestMapping(value = "/page" , method = RequestMethod.POST)
	public ResponseEntity<Pages<T>> findPage(@RequestBody Pages<T> pages) throws VisionException {
		Pages<T> serviceDetails = service.findPage(pages);
		return new ResponseEntity<>(serviceDetails, HttpStatus.OK);
	
	}
	
	
		

}
