package com.flytxt.vision.core.entity;

import java.util.List;
/**
 * 
 * @author shiju.john
 *
 */
public class Pages<T> implements NonPersistableVisionEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<T> content;	
	private int totalPage;
	private long totalElement;
	private boolean hasNext;
	private boolean hasPrevious;
	
	private String sortOrder;
	private int pageNo;
	private int  pageSize;
	private String [] sortFields ;
	private String lastKey;
	private List<String> previousBatchKey;
	

	/**
	 * @return the content
	 */
	public List<T> getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(List<T> content) {
		this.content = content;
	}

	
	/**
	 * @return the totalElement
	 */
	public long getTotalElement() {
		return totalElement;
	}

	/**
	 * @param totalElement the totalElement to set
	 */
	public void setTotalElement(long totalElement) {
		this.totalElement = totalElement;
	}

	/**
	 * @return the totalPage
	 */
	public int getTotalPage() {
		return totalPage;
	}

	/**
	 * @param totalPage the totalPage to set
	 */
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	/**
	 * @return the hasNext
	 */
	public boolean isHasNext() {
		return hasNext;
	}

	/**
	 * @param hasNext the hasNext to set
	 */
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	/**
	 * @return the hasPrevious
	 */
	public boolean isHasPrevious() {
		return hasPrevious;
	}

	/**
	 * @param hasPrevious the hasPrevious to set
	 */
	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the sortFields
	 */
	public String [] getSortFields() {
		return sortFields;
	}

	/**
	 * @param sortFields the sortFields to set
	 */
	public void setSortFields(String [] sortFields) {
		this.sortFields = sortFields;
	}

	/**
	 * @return the lastKey
	 */
	public String getLastKey() {
		return lastKey;
	}

	/**
	 * @param lastKey the lastKey to set
	 */
	public void setLastKey(String lastKey) {
		this.lastKey = lastKey;
	}

	/**
	 * @return the previousBatchKey
	 */
	public List<String> getPreviousBatchKey() {
		return previousBatchKey;
	}

	/**
	 * @param previousBatchKey the previousBatchKey to set
	 */
	public void setPreviousBatchKey(List<String> previousBatchKey) {
		this.previousBatchKey = previousBatchKey;
	}

}
