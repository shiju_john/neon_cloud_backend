package com.flytxt.vision.core.dao;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.core.exception.VisionException;
/**
 * 
 * @author shiju.john
 *
 * @param <VisionEntity>
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class VisionDao <T> {
	
	private static final Logger logger = LoggerFactory.getLogger(VisionDao.class);	
	
	private CrudRepository crudRepository;
	
		
	public VisionDao(CrudRepository crudRepository){
		this.crudRepository = crudRepository;
	}
	
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */

	public T save(T entity)  throws VisionException{
		try {
			return (T)crudRepository.save(entity);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new VisionException(e.getMessage(),e);
		}
	}
	
	/**
	 * 
	 * @param entities
	 * @return
	 * @throws VisionException
	 */
	public Iterable<T >saveAll(Iterable<T> entities)  throws VisionException{
		try {
			return crudRepository.saveAll(entities);
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new VisionException(e.getMessage(),e);
		}
	}
	
	/**
	 * 
	 * @param entities
	 * @throws VisionException
	 */	
	public void deleteAll(Iterable<T> entities)  throws VisionException{
		try {
			 crudRepository.deleteAll(entities);
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new VisionException(e.getMessage(),e);
		}
	}
	
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */
	public T update(T entity)  throws VisionException{
		try {
			
			return (T)crudRepository.save(entity);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new VisionException(e.getMessage(),e);
		}
	}
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */
	public T get(T entity) throws VisionException{ 
		try {
			com.flytxt.vision.core.entity.VisionEntity input =  (com.flytxt.vision.core.entity.VisionEntity) entity;
			Optional<T> result = crudRepository.findById(input.getId());//.findOne(entity);
					
			if(result.isPresent())
				return result.get();
			throw new VisionException("error.common.notfound", null);			
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new VisionException(e.getMessage(),e);
		}		
	} 
	
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws VisionException
	 */
	public boolean isExists(Object id) throws VisionException {
		return crudRepository.existsById(id);		
	}
	
	/**
	 * 
	 * @param jobDetails
	 * @return
	 */
	public void delete(T entity) throws VisionException{	
			com.flytxt.vision.core.entity.VisionEntity input =  (com.flytxt.vision.core.entity.VisionEntity) entity;
			crudRepository.deleteById(input.getId());	
	}
	
	/**
	 * 
	 * @return
	 */
	
	public Iterable<T> findAll() {
		return crudRepository.findAll();
	}
	
	
	/**
	 * 
	 * @param exp
	 * @param pageRequest
	 * @return
	 */	
/*	public Page<VisionEntity> findAll(Predicate exp, PageRequest pageRequest)  throws VisionException {
		Page<VisionEntity> result= ((QueryDslPredicateExecutor)crudRepository).findAll(exp,pageRequest);
		return result;
	}*/
	
	/**
	 * 
	 * @param predicate
	 * @return
	 */
/*	public Iterable<VisionEntity> findAll(Predicate predicate) {
		Iterable<VisionEntity> result= ((QueryDslPredicateExecutor)crudRepository).findAll(predicate);	
		return result;
	}
	*/
	/**
	 * 
	 * @param pageRequest
	 * @return
	 */
	public Page<T> findPage(PageRequest pageRequest) {		
		return ((PagingAndSortingRepository)crudRepository).findAll(pageRequest);
		
	}
	
	public Iterable<T>  findBy(T entity,PageRequest pageRequest,String ...type) throws VisionException{
		return null;
	}
	
	


}
