package com.flytxt.vision.configuration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.configuration.entity.PasswordChangeEntity;
import com.flytxt.vision.configuration.service.UserManagementService;
import com.flytxt.vision.configuration.service.gclod.GDriveService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;

/**
 * 
 * @author shiju.john
 *
 */

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/user")
public class UserManagementController extends VisionController<UserEntity, Filter>{

	UserManagementService service;
	
	@Autowired
	GDriveService driveService ;
	
	@Autowired	
	public UserManagementController(UserManagementService service) throws Exception {
		super(service);
		this.service = service;
		//driveService = new GDriveService();
		
	}
	
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public ResponseEntity<UserEntity> login(@RequestBody UserEntity userEntity) throws VisionException {
		return new ResponseEntity<>(service.login(userEntity), HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/passwordChange" , method = RequestMethod.POST)
	public ResponseEntity<Boolean> passwordChange(@RequestBody PasswordChangeEntity passwordChangeEntity) throws VisionException {
		return new ResponseEntity<>(service.passwordChange(passwordChangeEntity), HttpStatus.OK);
	
	}
	
	@RequestMapping(value = "/updatePayment" , method = RequestMethod.PUT)
	public ResponseEntity<UserEntity> updatePayment(@RequestBody UserEntity userEntity) throws VisionException {
		return new ResponseEntity<>(service.updatePayment(userEntity), HttpStatus.OK);
	
	}
	
	@RequestMapping(value = "/hasUserExist" , method = RequestMethod.GET)
	public ResponseEntity<Boolean> hasUserExist(@RequestParam("userName") String userName) throws VisionException {		
		return new ResponseEntity<>( service.hasUserExist(userName), HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/getTenantId" , method = RequestMethod.GET)
	public ResponseEntity<String> getTenantId(@RequestParam("userName") String userName) throws VisionException {		
		return new ResponseEntity<>( service.getTenantId(userName), HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/hasDomainExist" , method = RequestMethod.GET)
	public ResponseEntity<Boolean> hasDomainExist(@RequestParam("domainName") String domainName) throws VisionException {		
		return new ResponseEntity<>( service.hasDomainExist(domainName), HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/getUserByName" , method = RequestMethod.GET)
	public ResponseEntity<UserEntity> getUserByName(@RequestParam("userName") String userName) throws VisionException {	
		UserEntity entity = new UserEntity();
		entity.setUserName(userName);
		entity = service.getUserByName(entity);
		entity.setUserCredential(null);		
		entity.setImgUrl(driveService.getImageId(entity.getDomainName()));
		return new ResponseEntity<>(entity , HttpStatus.OK);	
	}
	
	
	@RequestMapping(value = "/getTenants" , method = RequestMethod.GET)
	public ResponseEntity<Iterable<UserEntity>> getTenants() throws VisionException {				
		return new ResponseEntity<>(service.getTenants() , HttpStatus.OK);			
	}
	
	@PostMapping("/uploadFile")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file, @RequestHeader(Constants.TENANT_ID) String tenantId) throws VisionException {		 
		try {			
			return new ResponseEntity<>(driveService.uploadImage(TenantUtils.getDomainName(tenantId), file),
					HttpStatus.OK);
		} catch (Exception e) {
			throw new VisionException(e.getLocalizedMessage(), null);
		}

    }	
	

	
	
}
