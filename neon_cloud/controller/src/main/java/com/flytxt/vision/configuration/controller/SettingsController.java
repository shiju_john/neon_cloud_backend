package com.flytxt.vision.configuration.controller;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.configuration.service.InitService;
import com.flytxt.vision.configuration.service.LabelPool;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;

/**
 * 
 * @author shiju.john
 *
 */
@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/settings")
public class SettingsController extends VisionController<Settings, Filter> {

	InitService service;

	@Autowired
	LabelPool labelpool;

	@Autowired
	public SettingsController(InitService service) {
		super(service);
		this.service = service;
	}

	@RequestMapping(path = "/labelpool", method = RequestMethod.GET)
	public ResponseEntity<Properties> getLabelpool() throws VisionException {
		return new ResponseEntity<>(labelpool.getMessages(), HttpStatus.OK);
	}

	@RequestMapping(path = "/sentiments", method = RequestMethod.GET)
	public ResponseEntity<String> getConnectorByType() throws VisionException {
		Settings config = new Settings();
		config.setKey("sentiments");
		Iterable<Settings> result = service.findByKey(config, null);
		if (result.iterator() != null && result.iterator().hasNext()) {
			return new ResponseEntity<>(result.iterator().next().getValue(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);

	}

	@RequestMapping(path = "/kafka", method = RequestMethod.GET)
	public ResponseEntity<String> getKafkaConfig() throws VisionException {
		Settings config = new Settings();
		config.setKey("KAFKA_SETTINGS");
		Iterable<Settings> result = service.findByKey(config, null);
		if (result.iterator() != null && result.iterator().hasNext()) {
			return new ResponseEntity<>(result.iterator().next().getValue(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);

	}
	
	@RequestMapping(path = "/get", method = RequestMethod.GET)
	public ResponseEntity<String> getByKey(@RequestParam("key") String key) throws VisionException {
		Settings config = new Settings();
		config.setKey(key);
		Iterable<Settings> result = service.findByKey(config, null);
		if (result.iterator() != null && result.iterator().hasNext()) {
			return new ResponseEntity<>(result.iterator().next().getValue(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(path = "/intent", method = RequestMethod.GET)
	public ResponseEntity<String> getDefaultIntend() throws VisionException {
		Settings config = new Settings();
		config.setKey("defaultIntent");
		Iterable<Settings> result = service.findByKey(config, null);
		if (result.iterator() != null && result.iterator().hasNext()) {
			return new ResponseEntity<>(result.iterator().next().getValue(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);

	}


}
