/**
 * 
 */
package com.flytxt.vision.configuration.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author athul
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sf")
public class SalesforceAuthController {

//	@Autowired
//	private SalesforceAuthService salesforceAuthService;
//	
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String salesForceLogin(@RequestParam("clientId") String clientId, @RequestParam("clientSecret") String clientSecret) {
//    	String redirectUrl = salesforceAuthService.login(clientId,clientSecret);
//    	return new ModelAndView("redirect:" + redirectUrl);
//    	return redirectUrl;
//    }
//    
//    @RequestMapping(value = "/callback", method = RequestMethod.GET)
//    public String authenticate(@RequestParam("code") String code) {
//    	salesforceAuthService.requestTokenAndGetApi(code);
//    	throw new RuntimeException();
//    }
}
