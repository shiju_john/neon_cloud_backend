package com.flytxt.vision.configuration.config;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import com.flytxt.vision.configuration.userrole.ApiAuthorizer;
import com.flytxt.vision.core.entity.VisionEntity;
import com.flytxt.vision.utils.Constants;
/**
 * 
 * @author shiju.john
 *
 */
@ControllerAdvice
public class RequestFilterAdvice  implements RequestBodyAdvice {
	
	
	@Autowired
	ApiAuthorizer apiRoleMapping;

	@Override
	public boolean supports(MethodParameter methodParameter, Type targetType,
			Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	} 

	@Override
	public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
			Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
		return inputMessage;
	}

	@Override
	public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
			Class<? extends HttpMessageConverter<?>> converterType) {
		
		List<String> tenantList = inputMessage.getHeaders().get(Constants.TENANT_ID);		
		if (body instanceof VisionEntity) {
			VisionEntity entity = (VisionEntity) body;			
			entity.setTenantId(tenantList!=null&& !tenantList.isEmpty()? tenantList.get(0):null);
		}
		
		/**
		if(body instanceof ConnectorInstance) {
			String tenantId = tenantList!=null&& !tenantList.isEmpty()? tenantList.get(0):null;
			List<String> roleList = inputMessage.getHeaders().get(Constants.USER_ROLE_HEADER);			
			List<String> appIdList = inputMessage.getHeaders().get(Constants.APP_ID_HEADER);
			String roleId= roleList!=null&& !roleList.isEmpty()? roleList.get(0):null;
			String appId = appIdList!=null&& !appIdList.isEmpty()? appIdList.get(0):null;
			ConnectorInstance entity = (ConnectorInstance) body;		
			try {
				if(!apiRoleMapping.hasWriteEnabled(roleId, appId, tenantId, "configuration/"+entity.getConnectorType())) {
					throw new VisionRuntimeException("Invalid access");
				}
			} catch (VisionException e) {				
				throw new VisionRuntimeException(e.getLocalizedMessage());
			}
			
		} **/
		return body; 
	}

	@Override
	public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
			Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
	
		return body;
	}

}
