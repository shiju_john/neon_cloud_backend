
/**
 * © Copyright 2015 Flytxt BV. ALL RIGHTS RESERVED.
 *
 * All rights, title and interest (including all intellectual property rights) in this software and any derivative works based upon or derived from this software
 * belongs exclusively to Flytxt BV. Access to this software is forbidden to anyone except current employees of Flytxt BV and its affiliated companies who have
 * executed non-disclosure agreements explicitly covering such access. While in the employment of Flytxt BV or its affiliate companies as the case may be,
 * employees may use this software internally, solely in the course of employment, for the sole purpose of developing new functionalities, features, procedures,
 * routines, customizations or derivative works, or for the purpose of providing maintenance or support for the software. Save as expressly permitted above,
 * no license or right thereto is hereby granted to anyone, either directly, by implication or otherwise. On the termination of employment, the license granted
 * to employee to access the software shall terminate and the software should be returned to the employer, without retaining any copies.
 *
 * This software is (i) proprietary to Flytxt BV; (ii) is of significant value to it; (iii) contains trade secrets of Flytxt BV; (iv) is not publicly available;
 * and (v) constitutes the confidential information of Flytxt BV. Any use, reproduction, modification, distribution, public performance or display of this software
 * or through the use of this software without the prior, express written consent of Flytxt BV is strictly prohibited and may be in violation of applicable laws.
 */
package com.flytxt.vision.configuration.controller;

import java.io.Serializable;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.flytxt.vision.configuration.entity.AuthenticationResponse;

/**
 * The SalesForceLogin class
 *
 * @author kiran
 *
 */
@RestController
@RequestMapping("/salesforce")
public class DirtySalesforceController implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    
    private AuthenticationResponse authResponse = new AuthenticationResponse();

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String salesForceLogin(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("secret") String secret, @RequestParam("key") String key) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

//        params.add("username", "athul.raj@flytxt.com");
//        params.add("password", "ME1@salesforceJ9U3iUcP8NLdVtu5AqyDBhI4d");
//        params.add("client_secret", "1007870128987851376");
//        params.add("client_id", "3MVG9pe2TCoA1Pf4L6GIu6rPYGxF4O3wwhto244_5aBtZpRjXdTlL3VZk0BtiiQouVLfRpaXGU7nfLKcGEzSS");
        
        params.add("username", username);
        params.add("password", password);
        params.add("client_secret", secret);
        params.add("client_id", key);
        
        params.add("grant_type", "password");

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);

        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<AuthenticationResponse> response = restTemplate.postForEntity("https://login.salesforce.com/services/oauth2/token", request, AuthenticationResponse.class);
        authResponse = (AuthenticationResponse) response.getBody();

        String objectList = getObjectDetails();
        return objectList;
    }

    @RequestMapping(value = "/getObjectdata", method = RequestMethod.GET, produces = "application/json")
    public String getObjectContent(@RequestParam("query") String query) {
        // Execute a SOQL Query to fetch object Data
        final HttpEntity<MultiValueMap<String, String>> request = getHttpRequest();
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> salesforceTestData = restTemplate.exchange(authResponse.getInstance_url() + "/services/data/v44.0/query?q=" + query, HttpMethod.GET, request, String.class);

        return salesforceTestData.getBody();

    }

    @RequestMapping(value = "/getObjectList", method = RequestMethod.GET, produces = "application/json")
    public String getObjectDetails() {
        // Get a List of Objects
        final HttpEntity<MultiValueMap<String, String>> request = getHttpRequest();
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> salesforceTestData = restTemplate.exchange(authResponse.getInstance_url() + "/services/data/v44.0/sobjects/", HttpMethod.GET, request, String.class);
        return salesforceTestData.getBody();
    }

    @RequestMapping(value = "/getObjectDescription", method = RequestMethod.GET, produces = "application/json")
    public String getObjectDetails(@RequestParam("object") String object) {
        // Get Field and Other Metadata for an Object
        final HttpEntity<MultiValueMap<String, String>> request = getHttpRequest();
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> salesforceTestData = restTemplate.exchange(authResponse.getInstance_url() + "/services/data/v44.0/sobjects/" + object + "/describe/", HttpMethod.GET, request,
                String.class);

        return salesforceTestData.getBody();
    }


    @RequestMapping(value = "/createRecord", method = RequestMethod.POST, consumes = "application/json")
    public String createRecord(@RequestBody Object payload, @RequestParam("object") String object) {
        // Create a Record
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + authResponse.getAccess_token());

        final RestTemplate restTemplate = new RestTemplate();
        final HttpEntity<Object> requestEntity = new HttpEntity<>(payload, headers);
        final ResponseEntity<String> responseEntity = restTemplate.exchange(authResponse.getInstance_url() + "/services/data/v44.0/sobjects/" + object + "/", HttpMethod.POST, requestEntity,
                String.class);
        return responseEntity.getBody();
    }    

    HttpEntity<MultiValueMap<String, String>> getHttpRequest() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + authResponse.getAccess_token());
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        return new HttpEntity<>(params, headers);
    }   


}
