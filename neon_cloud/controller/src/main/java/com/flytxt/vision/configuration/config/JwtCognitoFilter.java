package com.flytxt.vision.configuration.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flytxt.vision.configuration.userrole.ApiAuthorizer;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 */

@Component
public class JwtCognitoFilter implements Filter {
	
//	@Autowired
//	UserRoleCache roleCache;
	
	
	
	@Autowired
	ApiAuthorizer apiRoleMapping;
//
	public void init(FilterConfig filterConfig) throws ServletException {

	}
//
//
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	chain.doFilter(request, response);	
//		
//		HttpServletRequest httpRequest = (HttpServletRequest) request;
//		Enumeration<String> headerNames = httpRequest.getHeaderNames();
//	
//		String path = httpRequest.getRequestURI();
//
//		if (headerNames != null) {
//			
//			String tenantId = httpRequest.getHeader(Constants.TENANT_ID);
//			String roleId = httpRequest.getHeader(Constants.USER_ROLE_HEADER);
//			String appId = httpRequest.getHeader("appId");
//			
//			try {
//				if(hasAccess(httpRequest.getMethod(), path,roleId,appId,tenantId)) {
//					if(apiRoleMapping.isDefault(path.split("/vision/")[1],httpRequest.getMethod())) {
//						chain.doFilter(httpRequest, response);
//					}else {
//						String authObj = httpRequest.getHeader("authorization");	
//						//|| !HelperUtils.isNotNull(tenantId) || "null".equalsIgnoreCase(tenantId))
//						
//						if (!HelperUtils.isNotNull(authObj))  {
//							throw new ServletException("Authorization header not found. Invalid Request Parameters");
//						}
//
//						String jwtToken = authObj.split(" ")[1].trim(); //"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0Iiwicm9sZXMiOiJST0xFX0FETUlOIiwiaXNzIjoibXlzZWxmIiwiZXhwIjoxNDcxMDg2MzgxfQ.1EI2haSz9aMsHjFUXNVz2Z4mtC0nMdZo6bo3-x-aRpw";
//
//						String[] split_string = jwtToken.split("\\.");
//						String base64EncodedHeader = split_string[0];
//						String base64EncodedBody = split_string[1];
//						String base64EncodedSignature = split_string[2];
//
//						java.util.Base64.Decoder base64Url = java.util.Base64.getUrlDecoder();
//						String header = new String(base64Url.decode(base64EncodedHeader));
//						String body = new String(base64Url.decode(base64EncodedBody));
//					
//						Map oauthMap = TenantUtils.parseJson(body);
//						long exp =( (Double)oauthMap.get("exp")).longValue();
//								
//						Date endDate = new Date(exp*1000);					
//						
//						if(new Date().after(endDate)) {
//							throw new ServletException("Token Expired. Please login aganin",null);						
//						}
//						chain.doFilter(httpRequest, response);					
//					}
//				}
//			} catch (VisionException e) {
//				throw new ServletException(e.getLocalizedMessage(),e);
//			}
//		} else {
//			throw new ServletException("Access denied !.");
		
		
		
		
	}
	
	/**
	 * 
	 * @param methodType
	 * @param path
	 * @param roleId
	 * @param appId
	 * @param tenantId
	 * @return
	 * @throws ServletException
	 * @throws VisionException
	 */
	private boolean hasAccess(String methodType,String path,String roleId,String appId,String tenantId) throws ServletException, VisionException {
		if("get".equalsIgnoreCase(methodType) || path.endsWith("/page")){
			if(!apiRoleMapping.hasReadEnabled(roleId,appId,tenantId,path.split("vision/")[1])) {
				throw new ServletException("Invalid access",null);
			}
			return true;
		}else  if(!path.contains("/configuration")){ 
			if(!apiRoleMapping.hasWriteEnabled(roleId,appId,tenantId,path.split("vision/")[1])) {
				throw new ServletException("Invalid access",null);
			}
			return true;
		}else if(path.contains("/configuration")) {
			return true;
			
		}else {
			return false;
		}
	}

	public void destroy() {

	}

}
