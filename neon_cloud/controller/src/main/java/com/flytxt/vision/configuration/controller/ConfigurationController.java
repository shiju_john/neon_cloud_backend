package com.flytxt.vision.configuration.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.InitService;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;

/**
 * 
 * @author shiju.john
 *
 */
@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/configuration")
public class ConfigurationController extends VisionController<ConnectorInstance, Filter> {

	ConfigurationService service;
	@Autowired
	AwsConnectorService awsConnectorService;

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	InitService initService;

	@Value("${com.flytxt.lamda.uri}")
	private String uri;

	@Value("${com.flytxt.translator.uri}")
	private String translatorUri;

	@Value("${com.flytxt.translator.acceskey}")
	private String transltorKey;

	@Autowired
	public ConfigurationController(ConfigurationService service) {
		super(service);
		this.service = service;

	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ConnectorInstance> getConnectorById(@PathVariable("id") String id) throws VisionException {
		ConnectorInstance config = new ConnectorInstance();
		config.setConnectorId(id);

		return new ResponseEntity<>(service.get(config), HttpStatus.OK);

	}

	@RequestMapping(path = "/{type}/get", method = RequestMethod.GET)
	public ResponseEntity getConnectorByType(@PathVariable("type") String type,
			@RequestHeader(Constants.TENANT_ID) String tenantId) throws VisionException {

		InitializationService serviceEnum = InitializationService.getValue(type);

		if (serviceEnum != null) {
			return new ResponseEntity<>(serviceEnum.getConfig(initService, tenantId), HttpStatus.OK);
		} else {

			ConnectorInstance config = new ConnectorInstance();
			config.setConnectorType(type);
			config.setTenantId(tenantId);
			Iterable<ConnectorInstance> result = service.findByType(config, null);
			return new ResponseEntity<>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/{type}/page", method = RequestMethod.POST)
	public ResponseEntity<Pages<ConnectorInstance>> findPage(@PathVariable("type") String type,
			@RequestBody Pages<ConnectorInstance> pages) throws VisionException {
		Pages<ConnectorInstance> serviceDetails = service.findByTypeandPage(type, pages);
		return new ResponseEntity<>(serviceDetails, HttpStatus.OK);

	}

	@RequestMapping(value = "/getByConnectorTypesIn", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ConnectorInstance>> getByTypeIn(
			@RequestParam("connectorTypes") List<String> connectorTypes,
			@RequestHeader(Constants.TENANT_ID) String tenantId) throws VisionException {

		ConnectorInstance instance = new ConnectorInstance();
		instance.setTypes(connectorTypes);
		instance.setTenantId(tenantId);
		return new ResponseEntity<>(service.findByTypesIn(instance), HttpStatus.OK);

	}

	@RequestMapping(value = "/getConfigFileNames", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getConfigFileNamesOf(@RequestParam("entityType") String configType,
			@RequestHeader(Constants.TENANT_ID) String tenantId) {
		return new ResponseEntity<>(awsConnectorService.getFiles(TenantUtils.getDomainName(tenantId), configType),
				HttpStatus.OK);
	}

//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	@PostMapping("/uploadDataFile")
//	public ResponseEntity<String> submit(@ModelAttribute FormDataWithFile formDataWithFile,
//			@RequestHeader(Constants.TENANT_ID) String tenantId) throws VisionException {
//
//		Map meta = TenantUtils.parseJson(formDataWithFile.getFileMeta());
//		String entityType = (String) meta.get(Constants.ENTITY_TYPE);
//		ConnectorInstance connectorInstance = new ConnectorInstance();
//		connectorInstance.setTenantId(tenantId);
//		connectorInstance.setConnectorType(entityType+"_meta");
//		connectorInstance.setStatus("active");
//		connectorInstance.setConnectorName((String)meta.get("title"));  
//		connectorInstance.setConnectorConfig(formDataWithFile.getFileMeta());
//		this.saveMeta(connectorInstance, tenantId);
//		
////		String entityType = (String) meta.get(Constants.ENTITY_TYPE);
//		String mode = (String) meta.get(Constants.MODE);
//		try (DataInputStream in = new DataInputStream(formDataWithFile.getFile().getInputStream());
//				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//				CSVReader csvReader = new CSVReader(reader);) {
//
//			TableEntity entity = new TableEntity();
//			entity.setEntityType(entityType);
//			entity.setTableName(TenantUtils.getTableName(tenantId, entity.getEntityType()));
//			entity.setColumnMappings((List) meta.get(Constants.MAPPING));
//			String[] nextRecord;
//			String header[] = csvReader.readNext();
//			Map row;
//			while ((nextRecord = csvReader.readNext()) != null) {
//
//				row = new HashMap<>();
//				for (int i = 0; i < nextRecord.length; i++) {
//					String mappainColumn = getMappingName(header[i], meta);
//					if (null != mappainColumn) {
//						row.put(mappainColumn, nextRecord[i]);
//					}
//				}
//				row.put(Constants.FILE_NAME, formDataWithFile.getFile().getOriginalFilename());
//				entity.addRow(row);
//
//			}
//			service.saveCustomData(entity, tenantId, entityType);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		if (Constants.MODE_NEW.equalsIgnoreCase(mode)) {
//			service.saveMeta(formDataWithFile.getFileMeta(), tenantId, entityType);
//		}
//
//		return new ResponseEntity<>("Successfully saved", HttpStatus.OK);
//	}

//	@SuppressWarnings("rawtypes")
//	private String getMappingName(String csvHeaderName, Map mapping) {
//		@SuppressWarnings("unchecked")
//		List<Map> mappings = (List<Map>) mapping.get(Constants.MAPPING);
//		for (Map entry : mappings) {
//			if (entry.get(Constants.META_TEXT).equals(csvHeaderName)) {
//				return (String) entry.get(Constants.META_DEST);
//			}
//		}
//		return null;
//
//	}

	@RequestMapping(path = "/getSalesforceData", method = RequestMethod.POST)
	@SuppressWarnings("rawtypes")
	public ResponseEntity<HashMap> getSalesForceData(@RequestBody HashMap<String, Object> params,
			@RequestHeader(Constants.TENANT_ID) String tenantId) throws VisionException {
		ConnectorInstance connectorInstance = null;
		if (params.get("configId") != null) {
			connectorInstance = new ConnectorInstance();
			connectorInstance.setConnectorId((String) params.get("configId"));
			connectorInstance = service.get(connectorInstance);
		}

		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("object-id", params.get("objectId"));
		requestMap.put("queryString", params.get("queryString"));
		requestMap.put("config", connectorInstance != null ? connectorInstance.getConnectorConfig() : null);

		HashMap result = restTemplate.postForObject(uri, requestMap, HashMap.class);

//		HashMap result = restTemplate
//				.getForObject(uri + "?object-id=" + objectId
//						+ "&queryString=" + queryString + "&tenant-id=" + tenantId , HashMap.class);
		return new ResponseEntity<>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/filterExpression", method = RequestMethod.POST)
	public ResponseEntity<CustomDataFilter<TableEntity>> filterExpression(
			@RequestBody CustomDataFilter<TableEntity> dataFilter, @RequestHeader(Constants.TENANT_ID) String tenantId)
			throws VisionException {
		return new ResponseEntity<>(service.filterExpression(dataFilter, tenantId), HttpStatus.OK);

	}

	@RequestMapping(path = "/downloadsshkey", method = RequestMethod.GET)
	public ResponseEntity<Resource> download(@RequestHeader(Constants.TENANT_ID) String tenantId)
			throws IOException, VisionException {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		Settings settings = new Settings();
		settings.setTenantId(tenantId);
		settings.setKey(Constants.SFTP_KEY);
		settings = initService.findByTenantAndKey(settings);
		if (null != settings) {

			Map<String, String> clientSettings = TenantUtils.parseJsonToMapString(settings.getValue());
			clientSettings.get(Constants.SSH_KEY).getBytes(StandardCharsets.UTF_8);

			InputStreamResource resource = new InputStreamResource(
					new ByteArrayInputStream(clientSettings.get(Constants.SSH_KEY).getBytes(StandardCharsets.UTF_8)));

			return ResponseEntity.ok().headers(headers)
					.contentLength(clientSettings.get(Constants.SSH_KEY).getBytes(StandardCharsets.UTF_8).length)
					.contentType(org.springframework.http.MediaType.parseMediaType("application/octet-stream"))
					.body(resource);
		}
		throw new VisionException("Your SSH configuration not found please contact Flytxt support team ", null);
	}

	@RequestMapping(path = "/translate", method = RequestMethod.POST)
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<HashMap> transulateText(@RequestBody HashMap<String, Object> request) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		for (Entry<String, Object> entry : request.entrySet()) {
			if ("q".equalsIgnoreCase(entry.getKey())) {
				map.addAll(entry.getKey(), (ArrayList<String>) entry.getValue());
			} else {
				map.add(entry.getKey(), entry.getValue());
			}
		}
		map.add("key", transltorKey);
		HttpEntity<MultiValueMap<String, Object>> httpRequest = new HttpEntity<MultiValueMap<String, Object>>(map,
				headers);
		ResponseEntity entity = restTemplate.postForEntity(translatorUri, httpRequest, HashMap.class);
		return entity;

	}

}
