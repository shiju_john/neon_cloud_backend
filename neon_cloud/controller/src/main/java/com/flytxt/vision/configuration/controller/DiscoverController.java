package com.flytxt.vision.configuration.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.flytxt.vision.aws.entity.OfferCodes;
import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;

/**
 * 
 * @author shiju.john
 *
 */
@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/dummy")
public class DiscoverController extends  VisionController<String, Filter>{

	private static final Logger LOGGER = LoggerFactory.getLogger(DiscoverController.class);
	public DiscoverController() {
		super(null);	
	}
	
	@RequestMapping(path="/incidentTracker",method=RequestMethod.GET)
	public ResponseEntity<Integer> incidentTracker (@RequestParam("sentiment") String  sentimnet,
			@RequestParam("text") String  text,@RequestParam("context") String  context) throws VisionException{		
		
		LOGGER.info("Received input sentiment : "+ sentimnet + " text : "+text +" context "+context );		
		return new ResponseEntity<>(100,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(path="/sentimentAnalyser",method=RequestMethod.GET,produces={ MediaType.TEXT_HTML_VALUE})
	public ResponseEntity<String> sentimentAnalyser (@RequestParam("text") String  text) throws VisionException{		
		
		LOGGER.info("Received input text : "+text  );	
		return new ResponseEntity<>("Negative",HttpStatus.OK);
		
	}
	
	@RequestMapping(path="/offer",method=RequestMethod.POST)
	public ResponseEntity<OfferCodes> offer (@RequestBody OfferCodes offerCodes) throws VisionException{		
		
		LOGGER.info("Received input context : "+offerCodes.getAspect() + " sentimnet : "+offerCodes.getSentiment());
		offerCodes.setOfferCode( "PLAN_249");
		offerCodes.setOfferDescription("Unlimited Plan 249");		
		return new ResponseEntity<>(offerCodes,HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(path="/getcustomerid/{phno}",method=RequestMethod.GET)
	public ResponseEntity<Long> getCutomet (@PathVariable("phno") Long id) throws VisionException{		
		//return new ResponseEntity<>("{ \"customerId\":\"TestCustomer100\",\"phoneNo\":\"90126754876\"}",HttpStatus.OK);
		return new ResponseEntity<>(id,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(path="/{phno}",method=RequestMethod.POST)
	public ResponseEntity<ProcessFlowEntity> discoverByPhoneNo (@PathVariable("phno") Integer id,@RequestParam("param1") String name,@RequestBody ProcessFlowEntity entity) throws VisionException{		
		//return new ResponseEntity<>("{ \"customerId\":\"TestCustomer100\",\"phoneNo\":\"90126754876\"}",HttpStatus.OK);
		entity.setHashKey(""+id);
		entity.setProcessName("name");
		return new ResponseEntity<>(entity,HttpStatus.OK);
		
	}
	
	@RequestMapping(path="/test" ,method=RequestMethod.POST)
	public ResponseEntity<ProcessFlowEntity> discoverByPhoneNo (@RequestBody ProcessFlowEntity entity) throws VisionException{		
		//return new ResponseEntity<>("{ \"customerId\":\"TestCustomer100\",\"phoneNo\":\"90126754876\"}",HttpStatus.OK);
		entity.setHashKey("");
		entity.setProcessName("name");
		return new ResponseEntity<>(entity,HttpStatus.OK);
		
	}
	
	
	
	
	@RequestMapping(path="/discover/dummy",method=RequestMethod.GET)
	public ResponseEntity<String> discover () throws VisionException{		
		return new ResponseEntity<>("{ \"customerId\":\"TestCustomer100\",\"phoneNo\":\"90126754876\"}",HttpStatus.OK);	
		
	}
	
/*
	@RequestMapping(path="/discover/getphone",method=RequestMethod.GET)
	public ResponseEntity<String> discover () throws VisionException{		
		return new ResponseEntity<>("{ \"customerId\":\"TestCustomer100\",\"phoneNo\":\"90126754876\"}",HttpStatus.OK);	
		
	}*/
	
	
	

}
