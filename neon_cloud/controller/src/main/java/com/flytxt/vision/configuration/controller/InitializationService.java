package com.flytxt.vision.configuration.controller;

import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.configuration.service.InitService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.HelperUtils;

/**
 * 
 * @author shiju.john
 *
 */
public enum InitializationService {
	
	sftpSettings(){

		@Override
		public ConnectorInstance getConfig(InitService service,String tenantId) throws VisionException {
			Settings settings = new Settings();
			settings.setTenantId(tenantId);
			settings.setKey(Constants.SFTP_KEY);
			settings = service.findByTenantAndKey(settings);
			return getData(settings);
		}
		
	},
	customerConfig(){

		@Override
		public ConnectorInstance getConfig(InitService service,String tenantId)  throws VisionException{
			return getConfigData(customerConfig.toString(),service);
		}		
	},
	
	
	eventConfig(){

		@Override
		public ConnectorInstance getConfig(InitService service,String tenantId) throws VisionException {
			return getConfigData(eventConfig.toString(),service);
		}
		
	},
	
	productConfig(){

		@Override
		public ConnectorInstance getConfig(InitService service,String tenantId)  throws VisionException{
			return getConfigData(productConfig.toString(),service);
			
		}
		
	};
	
	public abstract ConnectorInstance getConfig(InitService service,String tenantId)  throws VisionException;
	
	
	private static ConnectorInstance getConfigData(String key,InitService service) throws VisionException {
		
		Settings settings = new Settings();
		settings.setKey(key);		
		Iterable<Settings> result = service.findByKey(settings, null);
		if(null!=result && result.iterator().hasNext()) {
			for(Settings config : result) {
				if(!HelperUtils.isNotNull(config.getTenantId())){
					return getData(settings);
				}
			}
			
		}
		return null;
		
	}
	
	/**
	 * 
	 * @param settings
	 * @return
	 * @throws VisionException
	 */
	private static ConnectorInstance getData(Settings settings) throws VisionException {
		
		if (null != settings) {
			ConnectorInstance connectorInstance = new ConnectorInstance();
			connectorInstance.setConnectorId(settings.getHashKey());
			connectorInstance.setConnectorType(Constants.SFTP_KEY);
			connectorInstance.setConnectorConfig(settings.getValue());
			connectorInstance.setTenantId(settings.getTenantId());
			return connectorInstance ;
		}
		throw new VisionException("FTP connection settings not Found", null);
	}

	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public static InitializationService getValue(String type) {
		for(InitializationService service : InitializationService.values()) {
			if(service.toString().equalsIgnoreCase(type)) {
				return service;
			}
		}
		return null;
	}
	

}
