package com.flytxt.vision.configuration.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.configuration.service.ProcessService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;

/**
 * 
 * @author shiju.john
 *
 */
//@Controller
@CrossOrigin(origins = "*")
//@RequestMapping(path = "/process")
public class ProcessController extends VisionController<ProcessFlowEntity, Filter>{

	ProcessService service;
	
//	@Autowired	
	public ProcessController(ProcessService service) {
		super(service);
		this.service = service;		
	}
	
	
//	@RequestMapping(path="/{id}",method=RequestMethod.GET) 
	public ResponseEntity<ProcessFlowEntity> getProcessById (@PathVariable("id") String id) throws VisionException{
		
		ProcessFlowEntity entity = new ProcessFlowEntity();
		entity.setHashKey(id);
		return new ResponseEntity<>(service.get(entity),HttpStatus.OK);		
	}
	
	
//	@RequestMapping(path="/get",method=RequestMethod.POST)
	public ResponseEntity<ProcessFlowEntity> getProcessDefinition (@RequestBody ProcessFlowEntity config) throws VisionException{
		return new ResponseEntity<>(service.findByProcessIdAndProcessVersionAndStatus(config),HttpStatus.OK);	
		
	}
	


}
