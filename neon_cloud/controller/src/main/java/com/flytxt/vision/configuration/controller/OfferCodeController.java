package com.flytxt.vision.configuration.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import com.flytxt.vision.aws.entity.OfferCodes;
import com.flytxt.vision.configuration.service.OfferCodeService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;

/**
 * 
 * @author shiju.john
 *
 */
//@Controller
@CrossOrigin(origins = "*")
//@RequestMapping(path = "/offercode")
public class OfferCodeController extends VisionController<OfferCodes, Filter>{

	OfferCodeService service;
	
//	@Autowired	
	public OfferCodeController(OfferCodeService service) {
		super(service);
		this.service = service;
		
	}
	
	
//	@RequestMapping(path="/{id}",method=RequestMethod.GET)
	public ResponseEntity<OfferCodes> getConnectorById (@PathVariable("id") String id) throws VisionException{		
		OfferCodes config = new OfferCodes();
		config.setHashKey(id);		
		return new ResponseEntity<>(service.get(config),HttpStatus.OK);	
		
	}
	
	

}
