/**
 * 
 */
package com.flytxt.vision.configuration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flytxt.vision.configuration.entity.SalesforceUserEntity;
import com.flytxt.vision.configuration.service.SalesforceService;

/**
 * @author athul
 *
 */
public class SalesforceController {
	
	@Autowired
	SalesforceService salesforceService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<SalesforceUserEntity> salesForceLogin(@RequestBody SalesforceUserEntity entity) {
		salesforceService.login(entity);
		throw new RuntimeException();
	}
	
	@RequestMapping(value = "/objects", method = RequestMethod.GET)
    public ResponseEntity<SalesforceUserEntity> getObjects() {
		salesforceService.getObjects();
		throw new RuntimeException();
	}
	
	@RequestMapping(value = "/schema", method = RequestMethod.GET)
    public ResponseEntity<SalesforceUserEntity> getSchema() {
		salesforceService.getSchema();
		throw new RuntimeException();
	}
	
	@RequestMapping(value = "/records", method = RequestMethod.GET)
    public ResponseEntity<SalesforceUserEntity> getRecords() {
		salesforceService.getRecords();
		throw new RuntimeException();
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity<SalesforceUserEntity> logout() {
		salesforceService.logout();
		throw new RuntimeException();
	}
}
