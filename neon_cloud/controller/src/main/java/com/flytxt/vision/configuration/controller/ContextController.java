package com.flytxt.vision.configuration.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.configuration.service.ContextService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.Constants;

/**
 * 
 * @author shiju.john
 *
 */
//@Controller
//@CrossOrigin(origins = "*")
//@RequestMapping(path = "/context")
public class ContextController extends VisionController<ContextEntity, Filter>{

	ContextService service;
	
//	@Autowired	
	public ContextController(ContextService service) {
		super(service);
		this.service = service;		
	}
	
//	@RequestMapping(path="/getAll",method=RequestMethod.GET)
	public ResponseEntity<Iterable<ContextEntity>> getConnectorByType( @RequestHeader(value=Constants.TENANT_ID) String tenantId) throws VisionException{		
		return new ResponseEntity<>(service.findContext(tenantId),HttpStatus.OK);	
		
	}
	

}
