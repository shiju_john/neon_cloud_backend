package com.flytxt.vision.configuration.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.configuration.service.ProcessMappingService;
import com.flytxt.vision.core.controller.VisionController;
import com.flytxt.vision.core.filter.entity.Filter;
/**
 * 
 * @author shiju.john
 *
 */
//@Controller
@CrossOrigin(origins = "*")
//@RequestMapping(path = "/pageMapping")
public class PageMappingController  extends VisionController<PageMapping, Filter>{

//	@Autowired	
	public PageMappingController(ProcessMappingService service) {
		super(service);
	}

}
