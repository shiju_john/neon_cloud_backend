package com.flytxt.vision.distributed;


import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class MainJob {

	public static void main(String[] args) {
		SparkConf sparkConf = new SparkConf().setMaster("spark://FLY-LP646:7077").setAppName("JD Word Counter");
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> inputFile = sparkContext.textFile("src/main/resource/text.txt");

		JavaRDD<String> wordsFromFile = inputFile.flatMap(content -> Arrays.asList(content.split(" ")).listIterator());

		JavaPairRDD countData = wordsFromFile.mapToPair(t -> new Tuple2(t, 1)).reduceByKey((x, y) -> (int) x + (int) y);

		countData.saveAsTextFile("CountData");

	}

}
