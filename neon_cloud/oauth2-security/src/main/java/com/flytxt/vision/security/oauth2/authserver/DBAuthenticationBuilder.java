package com.flytxt.vision.security.oauth2.authserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import com.flytxt.vision.security.oauth2.usermanagement.UserManagementImpl;
/**
 * 
 * @author shiju.john
 *
 */

@Configuration
public class DBAuthenticationBuilder implements AuthenticationType{

	
//	@Autowired
//	DataSource dataSource;
	 
	@Autowired
	UserManagementImpl userDetailsService;
	 
	@Override
	public void setAuthenticationManager(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(userDetailsService);
//		auth.jdbcAuthentication().dataSource(dataSource)
//		.usersByUsernameQuery("select username,password, enabled from users where username=?")
//		.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
		
	}

}