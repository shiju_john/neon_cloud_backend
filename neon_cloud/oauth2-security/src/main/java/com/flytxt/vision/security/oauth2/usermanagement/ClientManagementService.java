package com.flytxt.vision.security.oauth2.usermanagement;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class ClientManagementService implements ClientDetailsService{

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		
		 BaseClientDetails details = new BaseClientDetails(clientId,  
                 null,
                 "ADMIN,MARKETING,MARKETING-IT",
                 "implicit,refresh_token,client_credentials,password,authorization_code",
                 "read,write,execute",
                 "https://conversation.flytxt.com");
		 details.setClientSecret("{noop}"+clientId);
		
		return details;
	}

	

}
