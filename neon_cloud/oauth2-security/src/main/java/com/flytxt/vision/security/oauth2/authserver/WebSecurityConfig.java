package com.flytxt.vision.security.oauth2.authserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.flytxt.vision.security.oauth2.usermanagement.UserManagementImpl;

/**
 * 
 * @author shiju.john
 *
 */
@Configuration
//@EnableWebSecurity
//@Order(4)

@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);

	@Autowired
	private AuthenticationType authType;
	
	@Autowired
	UserManagementImpl userService;
	
	@Override
	@Bean
	@Primary
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManager();
	}
	
	@Override
    public void configure( WebSecurity web ) throws Exception {
		// web.ignoring().antMatchers( HttpMethod.POST, "/oauth/**" );
        web.ignoring().antMatchers( HttpMethod.OPTIONS, "/oauth/token" );
        web.ignoring().antMatchers( HttpMethod.POST,"/user");
        web.ignoring().antMatchers( HttpMethod.POST,"/settings/**");
        web.ignoring().antMatchers( HttpMethod.DELETE,"/user/**");
    }


	@Bean
	public UserDetailsService userDetailsService() {
		return userService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.csrf().disable().exceptionHandling()
//				.authenticationEntryPoint(
//						(request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
//				.and().authorizeRequests().antMatchers("/oauth/**").authenticated() //.and().httpBasic();
//				.and().anyRequest().authenticated().and().anonymous().disable();
//		// .and().authorizeRequests().antMatchers("/grapho/**").permitAll().and().httpBasic();
		
		  http
          .csrf().disable()
          .authorizeRequests()
          .anyRequest().authenticated()
          .antMatchers("/oauth/token").permitAll()
          .antMatchers("/user").permitAll()
          .antMatchers("/settings/**").permitAll()
         // .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
//          .anyRequest().authenticated()
          .and().anonymous().disable();
		  
		 // web.ignoring().antMatchers(HttpMethod.OPTIONS, "/oauth/token");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		authType.setAuthenticationManager(auth);
	}
}