package com.flytxt.vision.security.oauth2.usermanagement;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;

/**
 * 
 * @author shiju.john
 *
 */

@Service
public class UserManagementImpl extends AbstractService<UserEntity,Filter>
		implements UserDetailsService {

	public UserManagementImpl(@Qualifier("UserEntity") VisionDao<UserEntity> dao) {
		super(dao);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserName(username);
		try {
			userEntity = this.getUserByName(userEntity);
		} catch (VisionException e) {
			throw new UsernameNotFoundException("error.user.not_exist");
		}
		UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
		builder.password("{bcrypt}"+new BCryptPasswordEncoder().encode(userEntity.getUserCredential()));
		builder.roles(userEntity.getUserRole());
		return builder.build();
	}

	private UserEntity getUserByName(UserEntity userEntity) throws UsernameNotFoundException, VisionException {
		Iterable<UserEntity> entities = dao.findBy(userEntity, null, "byUserName");
		if (!entities.iterator().hasNext())
			throw new UsernameNotFoundException("error.user.not_exist");
		return entities.iterator().next();
	}

	@Override
	public Pages<UserEntity> search(Filter criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException {
		return null;
	}

	@Override
	public Iterable<UserEntity> search(Filter criteria) throws VisionException {
		return null;
	}

	@Override
	public UserEntity deleteById(Serializable id) throws VisionException {
		return null;
	}

}
