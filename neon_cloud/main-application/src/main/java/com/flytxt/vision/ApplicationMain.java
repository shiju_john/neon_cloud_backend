package com.flytxt.vision;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.flytxt.vision.awssqs.processor.campaign.CampaignExecutorImpl;
import com.flytxt.vision.configuration.service.ConfigurationService;

/**
 * 
 * @author shiju.john
 * 
 */

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@ComponentScan(basePackages = { "com.flytxt.vision" })
public class ApplicationMain {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationMain.class, args);
	}

	@Autowired
	ConfigurationService configurationService;

	@Autowired
	CampaignExecutorImpl executorImpl;

	@PostConstruct
	void executeCam() {
		executorImpl.execute("3cb640aa-b50a-4f08-877d-a2012a0a93eb");
		// executorImpl.execute("a62d60b8-0e61-40b5-8276-ca338d6aebe2");

	}

}
