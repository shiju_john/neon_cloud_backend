package com.flytxt.cloudfunction.dataloader;



import java.util.function.Function;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.function.context.FunctionScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@FunctionScan
@ComponentScan(basePackages = "com.flytxt")
public class CloudFunctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudFunctionApplication.class, args);
	}

	

	/*@Bean
	public Function<Message<Map>, Message<String>> members()
			throws JsonParseException, JsonMappingException, IOException, ProcessException {

		
		return event -> {

			Map payload = event.getPayload();

			try {
				
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new GenericMessage<>("");

		};

	}*/
	
	@Bean
    public Function<String, String> reverseString() {
        return value -> new StringBuilder(value).reverse().toString();
    }

}