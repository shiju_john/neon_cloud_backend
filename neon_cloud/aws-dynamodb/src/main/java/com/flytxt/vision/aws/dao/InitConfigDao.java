package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.aws.repository.SettingsRepository;
import com.flytxt.vision.core.dao.VisionDao;

/**
 * 
 * @author shiju.john
 *
 */
public class InitConfigDao extends VisionDao<Settings> {

	private SettingsRepository crudRepository;

	public InitConfigDao(SettingsRepository crudRepository) {
		super(crudRepository);
		this.crudRepository = crudRepository;
	}

	@Override
	public Iterable<Settings> findBy(Settings entity, PageRequest pageRequest, String... type) {
		String typeVal = null!=type && type.length>0 ?type[0]:"";
		switch (typeVal) {
		case "ByTenantAndKey":
			return crudRepository.findByKeyAndTenantId(entity.getKey(), entity.getTenantId());
		case "findByTenantNULL" :
			return crudRepository.findByTenantIdIsNull();

		default:
			return crudRepository.findByKey(entity.getKey());
		}

	}

}
