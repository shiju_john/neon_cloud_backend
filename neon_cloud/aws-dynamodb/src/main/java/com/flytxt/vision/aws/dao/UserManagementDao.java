package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.aws.repository.UserRepository;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 *
 */
public class UserManagementDao extends VisionDao<UserEntity> {
	
	UserRepository crudRepository ;
	public UserManagementDao(UserRepository crudRepository) {
		super(crudRepository);
		this.crudRepository = crudRepository;
	}
	
	@Override
		public Iterable<UserEntity> findBy(UserEntity entity, PageRequest pageRequest, String... type) {
			switch (type[0]) {
			
			case "byParent":				
				return crudRepository.findByParentUserId(entity.getParentUserId());
			case "byUserName":
				return crudRepository.findByUserName(entity.getUserName());
				
			case "byDomain":
				return crudRepository.findByDomainName(entity.getDomainName());
				
			case "getTenants" :
				return crudRepository.findByUserRole(entity.getUserRole());
				
			
			default:
				break;
			}
			return null;
		}


}
