package com.flytxt.vision.aws.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface ProcessRepository extends CrudRepository<ProcessFlowEntity, String> ,
		PagingAndSortingRepository<ProcessFlowEntity, String>{
	
	@EnableScan
	@EnableScanCount
	Page<ProcessFlowEntity> findAll(Pageable pageable);
	
	/**
	 * 
	 * @param ConnectorType
	 * @return
	 */
	List<ProcessFlowEntity> findByProcessIdAndProcessVersionAndStatus(String processId,String processVersion,String status);
	
	/**
	 * 
	 * @param processId
	 * @param processVersion
	 * @param status
	 * @return
	 */
	List<ProcessFlowEntity> findByProcessIdAndStatus(String processId,String status);
	
	/**
	 * 
	 * @param status
	 * @return
	 */
	Iterable<ProcessFlowEntity> findByStatus(String status);
	
}
