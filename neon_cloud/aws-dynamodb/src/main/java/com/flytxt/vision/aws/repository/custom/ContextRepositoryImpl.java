package com.flytxt.vision.aws.repository.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.utils.HelperUtils;
/**
 * 
 * @author shiju.john
 *
 */
public class ContextRepositoryImpl  implements CustomContextRepository<ContextEntity>{
	
	private DynamoDBMapper mapper;
	
	@Autowired
	public ContextRepositoryImpl(DynamoDBMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public List<ContextEntity> getContextNames(ContextEntity entity) {
		
		List<ContextEntity> result =  new ArrayList<>();
		if(!"null".equalsIgnoreCase(entity.getTenantId() ) || HelperUtils.isNotNull(entity.getTenantId())){
			Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
			eav.put(":v1", new AttributeValue().withNULL(true));
			DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
		            .withFilterExpression("tenantId = :null");
			result = mapper.parallelScan(ContextEntity.class, scanExpression, 2);
		}else {
			Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
			eav.put(":v1", new AttributeValue().withS(null));
			DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
		            .withFilterExpression("tenantId = :v1").withExpressionAttributeValues(eav);
			result.addAll(mapper.parallelScan(ContextEntity.class, scanExpression, 2));
		}
//		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
//		eav.put(":v1", new AttributeValue().withS(entity.getTenantId()));
//		
//		DynamoDBQueryExpression<ContextEntity> queryExpression = new DynamoDBQueryExpression<ContextEntity>() 
//			    .withKeyConditionExpression("tenantId = :v1")
//			    .withExpressionAttributeValues(eav);
		
//		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
//	            .withFilterExpression("tenantId = :v1").withExpressionAttributeValues(eav);
		

	    return result;
		
//		return  mapper.query(ContextEntity.class, queryExpression);

	}


}
