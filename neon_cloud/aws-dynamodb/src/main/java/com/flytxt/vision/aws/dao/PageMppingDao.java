package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.aws.repository.PageMappingRepository;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 *
 */
public class PageMppingDao extends VisionDao <PageMapping>{
	
	
	private PageMappingRepository crudRepository;
	

	public PageMppingDao(PageMappingRepository crudRepository) {
		super(crudRepository);
		this.crudRepository= crudRepository;		
	}
	
	@Override
	public Iterable<PageMapping>  findBy(PageMapping entity,PageRequest pageRequest,String ...type){
		
		switch (type!=null&& type.length>0 ?type[0]:"") {		
			
			case "byPageId" :
				return crudRepository.findByPageId(entity.getPageId());
				
			case "byProcessId" :
				return crudRepository.findByProcessId(entity.getProcessId());
			default:
				return null;
		}	
	}


}
