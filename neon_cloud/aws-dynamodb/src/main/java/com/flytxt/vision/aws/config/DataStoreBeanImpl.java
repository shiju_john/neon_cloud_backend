package com.flytxt.vision.aws.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.services.AwsDynamoDbServices;
import com.flytxt.vision.aws.dao.AwsDDLDao;
import com.flytxt.vision.aws.dao.ConnectorDao;
import com.flytxt.vision.aws.dao.ContextDao;
import com.flytxt.vision.aws.dao.InitConfigDao;
import com.flytxt.vision.aws.dao.OfferCodeDao;
import com.flytxt.vision.aws.dao.PageMppingDao;
import com.flytxt.vision.aws.dao.ProcessDao;
import com.flytxt.vision.aws.dao.UserManagementDao;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.aws.entity.OfferCodes;
import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.aws.repository.ConnectorRepository;
import com.flytxt.vision.aws.repository.ContextRepository;
import com.flytxt.vision.aws.repository.OfferCodeRepository;
import com.flytxt.vision.aws.repository.PageMappingRepository;
import com.flytxt.vision.aws.repository.ProcessRepository;
import com.flytxt.vision.aws.repository.SettingsRepository;
import com.flytxt.vision.aws.repository.UserRepository;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.dao.DataStoreBean;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 */
@Configuration
@Profile("aws")
public class DataStoreBeanImpl implements DataStoreBean {
	
	@Bean
	@Qualifier("ConnectorInstance")
	@Autowired
	VisionDao<ConnectorInstance> getConnectorDao(ConnectorRepository connectorRepository ) {		
		return new ConnectorDao(connectorRepository);
	}
	
	
	@Bean
	@Qualifier("OfferCodes")
	@Autowired
	VisionDao<OfferCodes> getOfferCodes(OfferCodeRepository codeRepository ) {		
		return new OfferCodeDao(codeRepository);
	}
	
	@Bean
	@Qualifier("ProcessFlowEntity")
	@Autowired
	VisionDao<ProcessFlowEntity> getProcessDao(ProcessRepository codeRepository ) {		
		return new ProcessDao(codeRepository);
	}
	
	@Bean
	@Qualifier("UserEntity")
	@Autowired
	VisionDao<UserEntity> getUserDao(UserRepository codeRepository ) {		
		return new UserManagementDao(codeRepository);
	}
	
	@Bean
	@Qualifier("PageMapping")
	@Autowired
	VisionDao<PageMapping> getPageMappingDao(PageMappingRepository codeRepository ) {		
		return new PageMppingDao(codeRepository);
	}
	
	@Bean
	@Qualifier("Settings")
	@Autowired
	VisionDao<Settings> getSettings(SettingsRepository codeRepository ) {		
		return new InitConfigDao(codeRepository);
	}
	
	@Bean
	@Qualifier("ContextEntity")
	@Autowired
	VisionDao<ContextEntity> getContextDao(ContextRepository codeRepository ) {		
		return new ContextDao(codeRepository);
	}
	
	
	
	@Bean
	@Qualifier("TableEntity")
	@Autowired
	DDLDao<TableEntity> getDDLDao(AmazonDynamoDB db) {		 
		return new AwsDDLDao(new AwsDynamoDbServices<>(db).getRepository());
	}
	


}
