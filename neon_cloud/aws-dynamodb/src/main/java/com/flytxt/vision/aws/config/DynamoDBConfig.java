package com.flytxt.vision.aws.config;

import java.util.ArrayList;
import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.utils.HelperUtils;

/**
 * 
 * @author shiju.john
 *
 */
@Configuration
@EnableDynamoDBRepositories (basePackages = "com.flytxt.vision.aws.repository")
public class DynamoDBConfig {

	@Value("${amazon.dynamodb.endpoint}")
    private String amazonDynamoDBEndpoint;
 
    @Value("${amazon.aws.accesskey}")
    private String amazonAWSAccessKey;
 
    @Value("${amazon.aws.secretkey}")
    private String amazonAWSSecretKey;
    
    @Value("${amazon.aws.cloudfunction}")
    private boolean isCloudFunction;
 
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
       
    	 AmazonDynamoDB amazonDynamoDB  = null;
         if(HelperUtils.isNotNull(amazonDynamoDBEndpoint)) {
        	amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(  new EndpointConfiguration(amazonDynamoDBEndpoint, "us-east-1")).build();

        }else {
        	
        //AWSCredentials credentials =;
           //  ClientConfiguration config = new ClientConfiguration();
            // config.setProtocol(Protocol.HTTP);
           return  AmazonDynamoDBClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider( new BasicAWSCredentials( amazonAWSAccessKey, amazonAWSSecretKey)))
                       //.withClientConfiguration(config)
                      // .withRegion(getRegion())
                       .build();
        
               // return  ;      
            
        	 //amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().build();
        	 //amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(awsCredentialsProvider).build();
        }
         
   
        return amazonDynamoDB;
    }

//    @Bean
//    public AWSCredentialsProvider awsCredentialsProvider() {
//        //return new ContainerCredentialsProvider();
//    	return new InstanceProfileCredentialsProvider(true);
//    	//return new EnvironmentVariableCredentialsProvider();
//    }
    
    @Bean
    public DynamoDBMapper getDymanoDbMapper(AmazonDynamoDB amazonDynamoDB){    	
    	 return new DynamoDBMapper(amazonDynamoDB);    	
    }


    @Bean
    public InitializingBean initializeTables(DynamoDBMapper mapper,AmazonDynamoDB amazonDynamoDB) {
        return () -> {
           
        	
            // Alternatively, you can scan your model package for the DynamoDBTable annotation
            List<Class> modelClasses = new ArrayList<>();
            modelClasses.add(ConnectorInstance.class);
            //modelClasses.add(OfferCodes.class);
            modelClasses.add(ProcessFlowEntity.class);
            modelClasses.add(UserEntity.class);
          //  modelClasses.add(PageMapping.class);
            modelClasses.add(Settings.class);
           // modelClasses.add(ContextEntity.class);
            if(!isCloudFunction) {
            	 for (Class cls : modelClasses) {
                     CreateTableRequest tableRequest = mapper.generateCreateTableRequest(cls);
                     tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));                     
                      boolean created = TableUtils.createTableIfNotExists(amazonDynamoDB, tableRequest);
                     if (created) {
//                         log.info("Created DynamoDB table for " + cls.getSimpleName());
                     } else {
//                         log.info("Table already exists for " + cls.getSimpleName());
                     }
                 }

                 ListTablesResult tablesResult = amazonDynamoDB.listTables();

//                 log.info("Current DynamoDB tables are: ");
                 for (String name : tablesResult.getTableNames()) {
//                     log.info("\t" + name);
                 }
            	
            }
           
        };
    }
	

}
