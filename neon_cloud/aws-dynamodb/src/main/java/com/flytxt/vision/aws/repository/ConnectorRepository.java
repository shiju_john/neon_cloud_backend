package com.flytxt.vision.aws.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.ConnectorInstance;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface ConnectorRepository extends CrudRepository<ConnectorInstance, String> ,
		PagingAndSortingRepository<ConnectorInstance, String>{
	
	@EnableScan
	@EnableScanCount
	Page<ConnectorInstance> findAll(Pageable pageable);
	
	/**
	 * 
	 * @param ConnectorType
	 * @return
	 */
	List<ConnectorInstance> findByConnectorType(String connectorType);
	
	
	/**
	 * 
	 * @param tenantId
	 * @return
	 */
	List<ConnectorInstance> findByTenantIdAndConnectorType(String tenantId,String connectorType);
	
	
	/**
	 * 
	 * @param ConnectorType
	 * @param pageable
	 * @return
	 */
	Page<ConnectorInstance> findByConnectorType(String connectorType,Pageable pageable);
	
	
	/**
	 * 
	 * @param connectorType
	 * @return
	 */
	List<ConnectorInstance> findByTenantIdAndConnectorTypeIn(String tenantId , List<String> connectorType);
	
	
	/**
	 * 
	 * @param connectorNam
	 * @param connectorType
	 * @return
	 */
	List<ConnectorInstance> findByConnectorNameAndConnectorType(String connectorNam,String connectorType);

	/**
	 * 
	 * @param tenantId
	 * @param connectorType
	 * @param connectorName
	 * @return
	 */
	Iterable<ConnectorInstance> findByTenantIdAndConnectorTypeAndConnectorName(String tenantId, String connectorType,
			String connectorName);
	
	
	
	
}
