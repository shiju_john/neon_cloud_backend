package com.flytxt.vision.aws.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.UserEntity;
/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface UserRepository extends CrudRepository<UserEntity, String> , 
				PagingAndSortingRepository<UserEntity, String> {
	
	@EnableScan
	@EnableScanCount
	Page<UserEntity> findAll(Pageable pageable);

	/**
	 * 
	 * @param parentUserId
	 * @return
	 */
	Iterable<UserEntity> findByParentUserId(String parentUserId);
	
	/**
	 * 
	 * @param userName
	 * @return
	 */

	Iterable<UserEntity> findByUserName(String userName);
	
	/**
	 * 
	 * @param domainName
	 * @return
	 */
	Iterable<UserEntity> findByDomainName(String domainName);
	
	/**
	 * 
	 * @param string
	 * @return
	 */
	Iterable<UserEntity> findByUserRole(String string);

}
