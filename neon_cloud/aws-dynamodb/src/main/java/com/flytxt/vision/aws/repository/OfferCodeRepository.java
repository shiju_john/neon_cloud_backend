package com.flytxt.vision.aws.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.OfferCodes;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface OfferCodeRepository extends CrudRepository<OfferCodes, String> ,
		PagingAndSortingRepository<OfferCodes, String>{
	
	@EnableScan
	@EnableScanCount
	Page<OfferCodes> findAll(Pageable pageable);
	
	
	/**
	 * 
	 * @param sentimnet
	 * @param aspect
	 * @return
	 */
	List<OfferCodes> findBySentimentAndAspect(String sentimnet,String aspect);
	
	
	
}
