package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.OfferCodes;
import com.flytxt.vision.aws.repository.OfferCodeRepository;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 *
 */
public class OfferCodeDao extends VisionDao<OfferCodes>{

	private OfferCodeRepository crudRepository;
	
	public OfferCodeDao(OfferCodeRepository crudRepository) {
		super(crudRepository);	
		this.crudRepository =  crudRepository;
	}
	
	@Override
	public Iterable<OfferCodes>  findBy(OfferCodes entity,PageRequest pageRequest,String ...type){		
		
		return crudRepository.findBySentimentAndAspect(entity.getSentiment(), entity.getAspect());
	}


	

}
