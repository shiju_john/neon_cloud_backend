package com.flytxt.vision.aws.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.aws.repository.custom.CustomContextRepository;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface ContextRepository extends CrudRepository<ContextEntity, String> ,
		PagingAndSortingRepository<ContextEntity, String>,CustomContextRepository<ContextEntity>{
	
	@EnableScan
	@EnableScanCount
	Page<ContextEntity> findAll(Pageable pageable);
	

	/**
	 * 
	 * @param tenantId
	 * @return
	 */
	List<ContextEntity> findByTenantId(String tenantId);

	
	/**
	 * 
	 * @param name
	 * @return
	 */
	Iterable<ContextEntity> findByName(String name);

	
	
}
