package com.flytxt.vision.aws.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.PageMapping;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface PageMappingRepository extends CrudRepository<PageMapping, String> ,
		PagingAndSortingRepository<PageMapping, String>{
	
	@EnableScan
	@EnableScanCount
	Page<PageMapping> findAll(Pageable pageable);
	
	
	/**
	 * 
	 * @param ConnectorType
	 * @param pageable
	 * @return
	 */
	List<PageMapping> findByPageId(String pageId);	
	
	/**
	 * 
	 * @param ConnectorType
	 * @param pageable
	 * @return
	 */
	List<PageMapping> findByProcessId(String processId);
	
	
}
