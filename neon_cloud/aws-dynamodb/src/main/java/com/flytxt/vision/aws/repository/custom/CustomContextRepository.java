package com.flytxt.vision.aws.repository.custom;

/**
 * 
 * @author shiju.john
 *
 */
public interface CustomContextRepository <T> {
	
	/**
	 * 
	 * @param tenantId
	 * @return
	 */
	
	public Iterable<T> getContextNames( T entity);

}
