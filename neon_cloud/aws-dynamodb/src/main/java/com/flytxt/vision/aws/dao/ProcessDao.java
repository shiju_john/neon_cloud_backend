package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.aws.repository.ProcessRepository;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 *
 */
public class ProcessDao extends VisionDao<ProcessFlowEntity> {

	private ProcessRepository crudRepository;
	
	public ProcessDao(ProcessRepository crudRepository) {
		super(crudRepository);
		this.crudRepository = crudRepository;
		
	}
	
	@Override
	public Iterable<ProcessFlowEntity>  findBy(ProcessFlowEntity entity,PageRequest pageRequest,String ...type){
		
		switch (type!=null&& type.length>0 ?type[0]:"") {
		
			case "status":			
				return crudRepository.findByStatus(entity.getStatus());	

			default:
				if(entity.getProcessVersion()!=null)
				return crudRepository.findByProcessIdAndProcessVersionAndStatus(entity.getProcessId(),
						entity.getProcessVersion(), entity.getStatus());
				
				return crudRepository.findByProcessIdAndStatus(entity.getProcessId(), entity.getStatus());
		}
		
	}


}
