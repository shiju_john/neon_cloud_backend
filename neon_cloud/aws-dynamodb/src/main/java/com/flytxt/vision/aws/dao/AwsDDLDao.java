package com.flytxt.vision.aws.dao;

import java.util.List;
import java.util.Map;

import com.flytxt.aws.entity.AwsDataFilter;
import com.flytxt.aws.entity.Page;
import com.flytxt.aws.entity.TableEntity;
import com.flytxt.aws.exception.AwsServiceException;
import com.flytxt.aws.repository.DynamoDbRepository;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author shiju.john
 *
 */
public class AwsDDLDao implements DDLDao<TableEntity> {

	
	private DynamoDbRepository<TableEntity> ddlRepository;

	public AwsDDLDao(DynamoDbRepository<TableEntity> ddlRepository) {
		this.ddlRepository = ddlRepository;
	}

	@Override
	public TableEntity createTable(TableEntity entity) throws VisionException {

		try {
			ddlRepository.createTable(entity);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getLocalizedMessage(), e);
		}
		return entity;

	}

	@Override
	public TableEntity save(TableEntity entity) throws VisionException {
		try {
			return ddlRepository.save(entity);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public TableEntity findById(String tableName, String keyFieldValue, String keyField) throws VisionException {
		try {
			return ddlRepository.findById(tableName, keyFieldValue, keyField);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public TableEntity findByExpresion(Filter filter, String tableName) {

		return ddlRepository.findByExpresion(new com.flytxt.aws.entity.Filter(), tableName);
	}

	@Override
	public TableEntity findAll(String tableName) throws VisionException {
		try {
			return ddlRepository.findAll(tableName);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public TableEntity update(TableEntity entity) throws VisionException {
		try {
			return ddlRepository.update(entity);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public CustomDataFilter<TableEntity> findByFilterExpression(CustomDataFilter<TableEntity> dataFilter,
			String tenantId) throws VisionException {

		try {
			AwsDataFilter<TableEntity> awsDataFilter = customDataFilterToAwsDataFilter(dataFilter);
			awsDataFilter.setTenantId(tenantId);
			Map meta = dataFilter.getMapping();
			TableEntity entity = new TableEntity();
			entity.setEntityType((String) meta.get(Constants.ENTITY_TYPE));
			entity.setTableName(TenantUtils.getTableName(tenantId, entity.getEntityType()));
			if (meta.get(Constants.MAPPING) instanceof List) {
				@SuppressWarnings("unchecked")
				List<Map> mappings = (List<Map>) meta.get(Constants.MAPPING);
				entity.setColumnMappings(mappings);
			}
			awsDataFilter.setEntity(entity);
			return convertAwsFilterToCustomFilter(ddlRepository.findByDataFilter(awsDataFilter), dataFilter);
		} catch (AwsServiceException e) {
			throw new VisionException(e.getMessage(), e);
		}

	}
	


	/**
	 * 
	 * @param findByDataFilter
	 * @param customDataFilter
	 * @return
	 */
	private CustomDataFilter<TableEntity> convertAwsFilterToCustomFilter(AwsDataFilter<TableEntity> aswDataFileter,
			CustomDataFilter<TableEntity> customDataFilter) {

		customDataFilter.setPages(aswDataFileter.getPages() != null ? TenantUtils.gson
				.fromJson(TenantUtils.convertToJson(aswDataFileter.getPages()), new TypeToken<com.flytxt.vision.core.entity.Pages<TableEntity>>() {
				}.getType()) : null);

		customDataFilter.setEntity(aswDataFileter.getEntity());
		return customDataFilter;
	}

	/**
	 * 
	 * @param dataFilter
	 */
	private AwsDataFilter<TableEntity> customDataFilterToAwsDataFilter(CustomDataFilter<TableEntity> dataFilter) {

		return new AwsDataFilter<TableEntity>().withPages(dataFilter.getPages() != null ? TenantUtils.gson
				.fromJson(TenantUtils.convertToJson(dataFilter.getPages()), new TypeToken<Page>() {
				}.getType()) : null)
				.withFilter(dataFilter.getFilterObj() != null
						? TenantUtils.gson.fromJson(TenantUtils.convertToJson(dataFilter.getFilterObj()),
								new TypeToken<com.flytxt.aws.entity.Filter>() {
								}.getType())
						: null)
				.withFields(dataFilter.getFields());
		}

}
