package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.aws.repository.ContextRepository;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.exception.VisionException;
/**
 * 
 * @author shiju.john
 *
 */
public class ContextDao extends VisionDao <ContextEntity>{
	
	private ContextRepository crudRepository;
	

	public ContextDao(ContextRepository crudRepository) {
		super(crudRepository);
		this.crudRepository= crudRepository;		
	}
	
	@Override
	public Iterable<ContextEntity>  findBy(ContextEntity entity,PageRequest pageRequest,String ...type) throws VisionException{
		switch (type[0]) {
		case "byTenant":			
			return crudRepository.getContextNames(entity);
		case "byName":			
			return  crudRepository.findByName(entity.getName());
		default:
			break;
		}
		return null;
	}

	
}
