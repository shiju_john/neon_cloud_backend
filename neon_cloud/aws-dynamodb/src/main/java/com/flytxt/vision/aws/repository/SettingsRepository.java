package com.flytxt.vision.aws.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.flytxt.vision.aws.entity.Settings;

/**
 * 
 * @author shiju.john
 *
 */
@EnableScan
@EnableScanCount
public interface SettingsRepository extends CrudRepository<Settings, String> ,
		PagingAndSortingRepository<Settings, String>{
	
	@EnableScan
	@EnableScanCount
	Page<Settings> findAll(Pageable pageable);
	
	
	
	
	/**
	 * 
	 * @param ConnectorType
	 * @param pageable
	 * @return
	 */
	Iterable<Settings> findByKey(String key);



	/**
	 * 
	 * @param key
	 * @param tenanatId
	 * @return
	 */
	Iterable<Settings> findByKeyAndTenantId(String key,String tenanatId);




	Iterable<Settings> findByTenantIdIsNull();
	
	
	
	
}
