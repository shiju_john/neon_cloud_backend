package com.flytxt.vision.aws.dao;

import org.springframework.data.domain.PageRequest;

import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.repository.ConnectorRepository;
import com.flytxt.vision.core.dao.VisionDao;
/**
 * 
 * @author shiju.john
 *
 */
public class ConnectorDao extends VisionDao <ConnectorInstance>{
	
	private ConnectorRepository crudRepository;
	

	public ConnectorDao(ConnectorRepository crudRepository) {
		super(crudRepository);
		this.crudRepository= crudRepository;	
		
//		SimpleDynamoDBPagingAndSortingRepository
	}
	
	@Override
	public Iterable<ConnectorInstance>  findBy(ConnectorInstance entity,PageRequest pageRequest,String ...type){
		
		switch(type[0]) {
			case "byconnectorName":				
				return crudRepository.findByConnectorNameAndConnectorType(entity.getConnectorName(), entity.getConnectorType());
			
			case "byTenantAndType":
				return crudRepository.findByTenantIdAndConnectorType(entity.getTenantId(), entity.getConnectorType());
			
			case "byTenantAndTypeAndConnectorName":
				return crudRepository.findByTenantIdAndConnectorTypeAndConnectorName(entity.getTenantId(), entity.getConnectorType(),entity.getConnectorName());
				
			case "findByTypesIn":
				return crudRepository.findByTenantIdAndConnectorTypeIn(entity.getTenantId(),entity.getTypes());
			
				
				
			default:		
				if(pageRequest!=null) {
					return crudRepository.findByConnectorType(entity.getConnectorType(), pageRequest);
				}
				return crudRepository.findByConnectorType(entity.getConnectorType());
			}	
		}

}
