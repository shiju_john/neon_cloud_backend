package com.flytxt.vision.awssqs.processor.campaign.controlgroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
/**
 * 
 * @author shiju.john
 *
 */
@Configuration
public class ControlGroupBuilder {
	
	@Autowired
	ApplicationContext applicationContext;
	
	public  ControlGroup build(){
		return applicationContext.getBean(ControlGroup.class);
		
	}

}
