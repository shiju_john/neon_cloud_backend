package com.flytxt.vision.awssqs.processor.campaign.segment;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.processor.CsvFileProcessor;
import com.flytxt.vision.awssqs.processor.ProcessorUtils;
import com.flytxt.vision.awssqs.processor.campaign.controlgroup.ControlGroup;
import com.flytxt.vision.awssqs.processor.campaign.controlgroup.ControlGroupBuilder;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.opencsv.CSVReader;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class SegmentBatchProcessor implements SeqmentFileProcessor {

	@Autowired
	private AwsConnectorService awsConnectorService;

	@Autowired
	private ControlGroupBuilder builder;

	private static final int batchSize = 419430;
	private static final Logger LOGGER = LoggerFactory.getLogger(SegmentBatchProcessor.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void processFile(S3Object s3Object, String controlGroupId, String offerCode, Map<String, Object> meta)
			throws VisionException {

		ControlGroup controlGroup = builder.build();
		int start = 0;
		int end = batchSize;
		String header[] = null;

		String[] uncompletedRec = null;
		List mapping = ProcessorUtils.getMappingFromMeta(meta);
		TableEntity entity = null;
		Set<String> targetedSet = null;
		String segmentFieldId =  getSegmentFieldId(mapping);
		do {
			BufferedReader br = awsConnectorService.getS3FileRange(s3Object.getBucketName(), s3Object.getFileKey(),
					start, end);

			if (null != br) {
				targetedSet = new HashSet<>();
				entity = getTableEntity(s3Object);
				entity.setColumnMappings(mapping);

				try (CSVReader csvReader = new CSVReader(br);) {
					if (start == 0) {
						header = csvReader.readNext();
					}
					
					uncompletedRec = CsvFileProcessor.readEntityFromCsvReader(csvReader, s3Object, entity, header, meta,
							uncompletedRec);			
										
					start = end + 1;
					end = end + batchSize;
					if (start < s3Object.getFileSize()) {
						long startTime = System.currentTimeMillis();
						System.out.println("Start control group Time "+ startTime);
						targetedSet = ProcessorUtils.moveKeyToSet(entity.getColumnValues(), targetedSet, segmentFieldId);
						Set<String> targetedUsres = controlGroup.filterTargetedUsers(s3Object,
								controlGroupId,segmentFieldId , targetedSet);
						long endTime = System.currentTimeMillis();  
						System.out.println("Target users "+targetedUsres.size());
						System.out.println(" End Time " +endTime);
						System.out.println("Total Time" + (endTime-startTime )/1000);
					}

				} catch (IOException e) {
					LOGGER.error(e.getMessage(), e);
					throw new VisionException(e.getMessage(), e);
				}
			}
		} while (start < s3Object.getFileSize());

		if (uncompletedRec != null) {
			entity.addRow(CsvFileProcessor.getTableRow(uncompletedRec, header, entity.getColumnMappings(), s3Object));
		}
		if (entity.getColumnValues().size() > 0) {
			targetedSet = new HashSet<>();
			targetedSet = ProcessorUtils.moveKeyToSet(entity.getColumnValues(), targetedSet, segmentFieldId);			
			Set<String> targetedUsres = controlGroup.filterTargetedUsers(s3Object, controlGroupId,
					getSegmentFieldId(mapping), targetedSet);
		}

	}

	/**
	 * 
	 * @param mapping
	 * @return
	 */
	private String getSegmentFieldId(List<Map<String, Object>> mapping) {
		for (Map<String, Object> entry : mapping) {
			return (String) entry.get(Constants.META_DEST);
		}
		return null;
	}

	/**
	 * 
	 * @param s3Object
	 * @return
	 */
	private TableEntity getTableEntity(S3Object s3Object) {

		TableEntity entity = new TableEntity();
		entity.setEntityType(s3Object.getEntityType());
		entity.setTableName(s3Object.getTableName());
		return entity;
	}

}
