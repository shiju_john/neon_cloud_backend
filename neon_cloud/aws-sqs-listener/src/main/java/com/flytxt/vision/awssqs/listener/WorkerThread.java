package com.flytxt.vision.awssqs.listener;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.processor.FileProcessor;
import com.flytxt.vision.awssqs.processor.FileProcessorBuilder;
import com.flytxt.vision.awssqs.utils.JsonUtils;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 */

@Component
@Scope("prototype")
public class WorkerThread implements Runnable {

	private Map<String, Object> message;

	@Autowired
	private AwsConnectorService awsConnectorService;

	@Autowired
	private FileProcessorBuilder builder;

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkerThread.class);

	@Override
	public void run() {
		S3Object s3Object = null;
		try {
			s3Object = getS3Object();
			s3Object.initDomain();
			String configpath = null;
			switch (s3Object.getEntityType()) {
			case "segments":
				LOGGER.info("New Seqment File moved to the configuration Directory. Domain :"
						+s3Object.getDomainName() + " fileName "+s3Object.getFileName());
				configpath = AwsConnectorService.CONFIGURATION+AwsConnectorService.SUFFIX+ s3Object.getDomainName() + AwsConnectorService.SUFFIX + s3Object.getEntityType()
				+ AwsConnectorService.SUFFIX + s3Object.getAppConfigId();
				awsConnectorService.moveFile(s3Object.getFileKey(), configpath, s3Object.getFileName());
				break;

			
			case "controlGroup":
				LOGGER.info("New ControlGroups File moved to the configuration Directory. Domain :"
						+s3Object.getDomainName() + " fileName "+s3Object.getFileName());
				configpath = AwsConnectorService.CONFIGURATION+AwsConnectorService.SUFFIX+ s3Object.getDomainName() + AwsConnectorService.SUFFIX + s3Object.getEntityType()
				+ AwsConnectorService.SUFFIX + s3Object.getAppConfigId();
				awsConnectorService.moveFile(s3Object.getFileKey(), configpath, s3Object.getFileName());
				break;

			default:
				LOGGER.info("File Process started " + s3Object.getFileKey());
				FileProcessor fileProcessor = builder.build(s3Object.getFileSize());
				fileProcessor.processFile(s3Object);
				configpath = AwsConnectorService.BACKUP+AwsConnectorService.SUFFIX +s3Object.getDomainName() + AwsConnectorService.SUFFIX + s3Object.getEntityType()
						+ AwsConnectorService.SUFFIX + s3Object.getAppConfigId();

				LOGGER.info("File Process completed " + s3Object.getFileKey());			
				awsConnectorService.moveFile(s3Object.getFileKey(), configpath, s3Object.getFileName());
				break;
			}
			
			
			
		} catch (VisionException   e) {
			LOGGER.error(e.getMessage(), e);
			StringBuffer buffer = new StringBuffer();
			buffer.append(s3Object.getDomainName() + " : " +s3Object.getFileName() + ": ");
			buffer.append(e.getMessage());
			if (s3Object != null) {
				String errFilePath = s3Object.getFileKey().substring(0, s3Object.getFileKey().lastIndexOf("/") + 1)
						+ "err/";
				errFilePath = errFilePath + s3Object.getFileName();
				awsConnectorService.writeFile(errFilePath, buffer.toString());
			}
		}catch (FileRejectExecption e) {
			LOGGER.error(e.getMessage(), e);
		}

	}

	@SuppressWarnings("unchecked")
	private S3Object getS3Object() throws VisionException, FileRejectExecption {
		S3Object sObject = new S3Object();
		List<Map<String, Object>> records = (List<Map<String, Object>>) getMessage().get("Records");
		for (Map<String, Object> event : records) {
			String eventSource = (String) event.get("eventSource");
			sObject.setEventName((String) event.get("eventName"));
			sObject.setDate((String) event.get("eventTime"));
			if ("aws:s3".equalsIgnoreCase(eventSource)) {
				Map<String, Object> s3 = (Map<String, Object>) event.get("s3");
				Map<String, Object> bucket = (Map<String, Object>) s3.get("bucket");
				sObject.setBucketName((String) bucket.get("name"));
				sObject.setBucketArn((String) bucket.get("arn"));

				Map<String, Object> s3Object = (Map<String, Object>) s3.get("object");
				sObject.setFileKey((String) s3Object.get("key"));
				sObject.setFileSize((double) s3Object.get("size"));
			}
		}
		
		if(sObject.getFileKey().contains("/err/")) {
			throw new FileRejectExecption("File rejected :"+sObject.getFileKey(),null);
		}
		
		return sObject;
	}

	/**
	 * @return the message
	 */
	public Map<String, Object> getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = JsonUtils.parseJson(message);
	}

}
