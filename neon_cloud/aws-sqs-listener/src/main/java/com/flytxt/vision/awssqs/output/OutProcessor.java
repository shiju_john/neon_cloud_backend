package com.flytxt.vision.awssqs.output;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class OutProcessor extends RecursiveAction {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	@Autowired
	ApplicationContext applicationContext;

	@Value("${com.flytxt.outprocessor.threshold}")
	private int threshold;

	TableEntity entity;

	@Autowired
	@Qualifier("TableEntity")
	private DDLDao<TableEntity> ddldao;

	@Override
	protected void compute() {

		if (entity.getColumnValues().size() > threshold) {
			List<OutProcessor> processors = getOutProcessor();
			ForkJoinTask.invokeAll(processors);
		} else { 
			
			try {
				ddldao.save(entity);
			} catch (VisionException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 
	 * @return
	 */
	private List<OutProcessor> getOutProcessor() {

		List<OutProcessor> outProcessors = new ArrayList<>();
		int end = (int) entity.getColumnValues().size() / 2;

		OutProcessor processor1 = applicationContext.getBean(OutProcessor.class);
		processor1.setData(getEntity(0, end));

		OutProcessor processor2 = applicationContext.getBean(OutProcessor.class);
		processor2.setData(getEntity(end, entity.getColumnValues().size()));

		outProcessors.add(processor1);
		outProcessors.add(processor2);
		return outProcessors;

	}

	
	/**
	 * 
	 * @param entity
	 */
	public void setData(TableEntity entity) {
		this.entity = entity;

	}

	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	private TableEntity getEntity(int start, int end) {
		TableEntity entity = new TableEntity();

		entity.setTableName(this.entity.getTableName());
		entity.setColumnMappings(this.entity.getColumnMappings());
		entity.setEntityType(this.entity.getEntityType());
		entity.setColumnValues(this.entity.getColumnValues().subList(start, end));

		return entity;

	}

}
