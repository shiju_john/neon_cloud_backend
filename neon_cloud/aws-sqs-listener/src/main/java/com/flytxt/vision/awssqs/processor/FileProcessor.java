package com.flytxt.vision.awssqs.processor;

import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 */
public interface FileProcessor {
	
	/**
	 * 
	 * @param s3Object
	 * @throws VisionException
	 */
	public void processFile(S3Object s3Object) throws VisionException;
	

}
