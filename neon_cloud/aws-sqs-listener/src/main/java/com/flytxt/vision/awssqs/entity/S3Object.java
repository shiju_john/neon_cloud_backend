package com.flytxt.vision.awssqs.entity;

import java.io.Serializable;

import com.flytxt.vision.core.exception.VisionException;
/**
 * 
 * @author shiju.john
 *
 */
public class S3Object implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileKey;
	private double fileSize;
	private String bucketName;
	private String bucketArn;
	private String eventName;
	private String entityType;
	private String domainName;
	private String appConfigId;
	private String tenantId;
	private String fileName;
	private String date;
	
	
	private String tableName;

	/**
	 * @return the fileSize
	 */
	public double getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the bucketName
	 */
	public String getBucketName() {
		return bucketName;
	}

	/**
	 * @param bucketName the bucketName to set
	 */
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	/**
	 * @return the bucketArn
	 */
	public String getBucketArn() {
		return bucketArn;
	}

	/**
	 * @param bucketArn the bucketArn to set
	 */
	public void setBucketArn(String bucketArn) {
		this.bucketArn = bucketArn;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the fileKey
	 */
	public String getFileKey() {
		return fileKey;
	}

	/**
	 * @param fileKey the fileKey to set
	 */
	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	public void initDomain() throws VisionException {

		try{
			String inputPath = fileKey.substring(fileKey.indexOf("/")+1);			
			this.domainName = inputPath.substring(0, inputPath.indexOf("/"));
			String subDomain = inputPath.substring(inputPath.indexOf("/") + 1);
			this.entityType = subDomain.substring(0, subDomain.indexOf("/"));
			String appConfigString = subDomain.substring(subDomain.indexOf("/") + 1);
			this.appConfigId = appConfigString.substring(0, appConfigString.indexOf("/"));
			this.setFileName(appConfigString.substring(appConfigString.lastIndexOf("/") + 1));
		}catch (ArrayIndexOutOfBoundsException  | StringIndexOutOfBoundsException e) {
			throw new VisionException("File uploded in to the wrog location. Upload the file into the appropriate folder for processing "+fileKey,null);
		}


	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return the appConfigId
	 */
	public String getAppConfigId() {
		return appConfigId;
	}

	/**
	 * @param appConfigId the appConfigId to set
	 */
	public void setAppConfigId(String appConfigId) {
		this.appConfigId = appConfigId;
	}

	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
