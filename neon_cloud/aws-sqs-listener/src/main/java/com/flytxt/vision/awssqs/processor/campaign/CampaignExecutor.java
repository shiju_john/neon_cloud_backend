package com.flytxt.vision.awssqs.processor.campaign;
/**
 * 
 * @author shiju.john
 *
 */
public interface CampaignExecutor {
	
	/**
	 * 
	 * @param campainId
	 */
	public void execute(String campainId);

}
