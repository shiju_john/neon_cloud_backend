package com.flytxt.vision.awssqs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.boot.SpringApplication;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import com.flytxt.vision.awssqs.listener.SqsMessageListener;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;

/**
 * 
 * @author shiju.john
 *
 */
//@SpringBootApplication
//@ComponentScan(basePackages = { "com.flytxt.vision" })
public class AwsSqsListenerMain {

	//@Value("${amazon.aws.accesskey}")
	String accessKey;

	//@Value("${amazon.aws.secretkey}")
	String secretkey;

	//@Value("${aws.queue.name}")
	private String queueName;

	public static void main(String[] args) {
		SpringApplication.run(AwsSqsListenerMain.class, args);
	}

	//@Bean
	//@Autowired
	public DefaultMessageListenerContainer jmsListenerContainerFactory(
			SqsMessageListener sqsListener) {
		AwsConnectorService awsService =null;
		try {
			awsService = new AwsConnectorService();				
			awsService.createQueue(queueName, readPolicyFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
		dmlc.setConnectionFactory(awsService.getConnectionFactory());
		dmlc.setDestinationName(queueName);
		dmlc.setMessageListener(sqsListener);
		return dmlc;
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	private String readPolicyFile() throws IOException {
		InputStream keyFileStream = AwsSqsListenerMain.class.getClassLoader().getResourceAsStream("SQSPolicy.json");
		BufferedReader buf = new BufferedReader(new InputStreamReader(keyFileStream));

		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();
		while (line != null) {
			sb.append(line);
			line = buf.readLine();
		}
		return sb.toString();
	}

}
