package com.flytxt.vision.awssqs.utils;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonUtils {
	
	private static Gson gson = new Gson();
	
	
	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Map<String,Object> parseJson(String jsonString) {
		return gson.fromJson(jsonString, 
				new TypeToken<Map<String, Object>>() {
		}.getType());		
	}

}
