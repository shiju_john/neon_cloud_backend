package com.flytxt.vision.awssqs.listener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.amazon.sqs.javamessaging.message.SQSTextMessage;

/**
 * 
 * @author shiju.john
 *
 */

@Component
public class SqsMessageListener implements MessageListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(SqsMessageListener.class);

//	private ExecutorService executor;



	@Value("${com.flytxt.workerthread.count}")
	private int workerThreadCount;
	
	@Autowired
	private ExecutorService taskExecutor;
	
	@Autowired
	private ApplicationContext applicationContext;

	


	@Override
	public void onMessage(Message message) {

		ThreadPoolExecutor tPoolExecutor = (ThreadPoolExecutor) taskExecutor;
		if (message instanceof SQSTextMessage) {
			SQSTextMessage sqsTextMessage = (SQSTextMessage) message;
			try {
				WorkerThread messsageProcessor = applicationContext.getBean(WorkerThread.class);
				messsageProcessor.setMessage(sqsTextMessage.getText());
			    taskExecutor.execute(messsageProcessor);
//				Runnable worker = new WorkerThread(sqsTextMessage.getText(), awsConnectorService, service, userService);
//				executor.execute(worker);
			} catch (JMSException e) {
				e.printStackTrace();
			}

		}

		if (tPoolExecutor.getQueue().size() > workerThreadCount) {
//			try {
//				Thread.sleep(100000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
		}
	}

	@PreDestroy
	public void preDestroy() {
		// To_do executor shutdown
	}

}
