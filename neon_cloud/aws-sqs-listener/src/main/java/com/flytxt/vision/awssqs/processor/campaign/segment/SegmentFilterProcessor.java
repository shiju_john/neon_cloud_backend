package com.flytxt.vision.awssqs.processor.campaign.segment;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.processor.ProcessorUtils;
import com.flytxt.vision.awssqs.processor.campaign.controlgroup.ControlGroup;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.HelperUtils;
import com.flytxt.vision.utils.TenantUtils;
/**
 * 
 * @author shiju.john
 *
 */
@Component
public class SegmentFilterProcessor {
	
	@Autowired
	private ConfigurationService configService;
	
	@Autowired 
	private ControlGroup  controlGroup;
	
	public void processFilter(Map<String,Object> segments,String fieldId,String tenantId,String controlGroupId,String offerCode) throws VisionException{
		
		CustomDataFilter<TableEntity> customDataFilter = getDataFilter(segments, 5000, fieldId);
		CustomDataFilter<TableEntity> segmentData = null;
		S3Object s3object = new S3Object();
		s3object.setTenantId(tenantId);
		do {
			segmentData = configService.filterExpression(customDataFilter, tenantId);			
			Set<String> targetedUsers = ProcessorUtils.moveKeyToSet(segmentData.getEntity().getColumnValues(), new HashSet<>(), fieldId);
			if (HelperUtils.isNotNull(controlGroupId)) {
				targetedUsers = controlGroup.filterTargetedUsers(s3object,controlGroupId,fieldId, targetedUsers);

			}
			sendOffer(targetedUsers, offerCode, tenantId);

		} while (segmentData.getPages().isHasNext());
	}
	
	private void sendOffer(Set<String> targetedUsers, String offerCode, String tenantId) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("rawtypes")
	private CustomDataFilter<TableEntity> getDataFilter(Map<String, Object> config, int pageSize, String... type) {
		Filter filter = TenantUtils.convertMapToFilter((Map) config.get("rules"));
		Pages<TableEntity> pages = new Pages<>();
		pages.setPageSize(pageSize);
		CustomDataFilter<TableEntity> customDataFilter = new CustomDataFilter<>();
		customDataFilter.setPages(pages);
		customDataFilter.setFilterObj(filter);
		customDataFilter.setMapping((Map) config.get("mapping"));
		customDataFilter.setFields(type);
		return customDataFilter;

	}


}
