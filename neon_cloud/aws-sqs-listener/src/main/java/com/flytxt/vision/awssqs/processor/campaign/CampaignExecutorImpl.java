package com.flytxt.vision.awssqs.processor.campaign;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.listener.SqsMessageListener;
import com.flytxt.vision.awssqs.processor.campaign.segment.SegmentFilterProcessor;
import com.flytxt.vision.awssqs.processor.campaign.segment.SegmentProcessorBuilder;
import com.flytxt.vision.awssqs.processor.campaign.segment.SeqmentFileProcessor;
import com.flytxt.vision.configuration.cache.CommonSettings;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class CampaignExecutorImpl implements CampaignExecutor {

	@Autowired
	ConfigurationService configService;

	@Autowired
	CommonSettings commonCache;

	@Autowired
	SegmentProcessorBuilder builder;

	@Autowired
	SegmentFilterProcessor filterProcessor;

	@Autowired
	AwsConnectorService awsConnectorService;

	private static final Logger LOGGER = LoggerFactory.getLogger(SqsMessageListener.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(String campainId) {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setConnectorId(campainId);
		try {
			connectorInstance = configService.get(connectorInstance);
			Map<String, Object> campaign = TenantUtils.parseJson(connectorInstance.getConnectorConfig());
			List<Map> audienceOffers = (List<Map>) campaign.get(Constants.AUDIENCE_GROUP_KEY);
			for (Map audienceOffer : audienceOffers) {
				processAudienceOffer(audienceOffer, connectorInstance.getTenantId());
			}

		} catch (VisionException e) {
			LOGGER.error(e.getMessage(), e);
		}

	}

	/**
	 * 
	 * @param audienceOffer
	 * @param tenantId
	 * @throws VisionException
	 */
	private void processAudienceOffer(Map audienceOffer, String tenantId) throws VisionException {

		String segmentId = (String)((Map) audienceOffer.get("selectedSegment")).get("id");
		String controlGroupId = (String)((Map)audienceOffer.get("controlGroup")).get("id");
		String offer = null;
		 if (audienceOffer.get("selectedOffer") instanceof String) {
			offer = (String) audienceOffer.get("selectedOffer");
			
		}
		
		String fieldId = "customerId";
		processSeqments(segmentId, tenantId, fieldId, controlGroupId, offer);

	}

	
	/**
	 * 
	 * @param segmentId
	 * @param tenantId
	 * @param fieldId
	 * @param controlGroupId
	 * @param offerCode
	 * @throws VisionException
	 */
	void processSeqments(String segmentId, String tenantId, String fieldId, String controlGroupId, String offerCode)
			throws VisionException {

		ConnectorInstance connectorInstance = loadData(segmentId);		
		Map<String, Object> segments = TenantUtils.parseJson(connectorInstance.getConnectorConfig());
		String segmentType = (String) segments.get(Constants.META_TYPE);
		
		if (Constants.FILTER_KEY.equalsIgnoreCase(segmentType)) {
			filterProcessor.processFilter(segments, fieldId, tenantId, controlGroupId, offerCode);

		} else if (Constants.FILE_KEY.equalsIgnoreCase(segmentType)) {
			System.out.println(" Start Time "+ System.currentTimeMillis());
			
			S3Object s3Object = getS3FileObject(tenantId,segments,connectorInstance.getConnectorName());
			SeqmentFileProcessor processor = builder.build(s3Object.getFileSize());
			processor.processFile(s3Object, controlGroupId, offerCode,segments);
			System.out.println(" End Time "+ System.currentTimeMillis());
		}

	}
	
	
	/**
	 * 
	 * @param tenantId
	 * @param segments
	 * @param configName
	 * @return
	 */
	private S3Object getS3FileObject(String tenantId,Map<String, Object> segments,String configName) {
		S3Object s3Object = new S3Object();
		s3Object.setTenantId(tenantId);
		s3Object.setDomainName(commonCache.getDomainName(tenantId));
		s3Object.setBucketName(awsConnectorService.getBucketName());
		String fileKey = AwsConnectorService.CONFIGURATION + AwsConnectorService.SUFFIX
				+ commonCache.getAwsDomainName(tenantId) + AwsConnectorService.SUFFIX + segments.get(Constants.ENTITY_TYPE)
				+ AwsConnectorService.SUFFIX +configName+
				AwsConnectorService.SUFFIX + segments.get(Constants.FILE_NAME);
		
		s3Object.setFileKey(fileKey);
		s3Object.setFileSize((double) awsConnectorService.getFileSize(fileKey));
		return s3Object;
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws VisionException
	 */
	private ConnectorInstance loadData(String id) throws VisionException {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setConnectorId(id);
		connectorInstance = configService.get(connectorInstance);
		return connectorInstance;
	}

}
