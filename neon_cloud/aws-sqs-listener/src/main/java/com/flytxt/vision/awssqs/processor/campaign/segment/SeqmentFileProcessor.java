package com.flytxt.vision.awssqs.processor.campaign.segment;

import java.util.Map;

import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.core.exception.VisionException;
/**
 * 
 * @author shiju.john
 *
 */
public interface SeqmentFileProcessor {
	
	/**
	 * 
	 * @param s3Object
	 * @throws VisionException
	 */
	

	public void processFile(S3Object s3Object, String controlGroupId, String offerCode,Map<String, Object> meta) throws VisionException;

}
