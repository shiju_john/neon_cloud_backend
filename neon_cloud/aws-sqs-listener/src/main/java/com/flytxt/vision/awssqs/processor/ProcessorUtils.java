package com.flytxt.vision.awssqs.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.flytxt.vision.utils.Constants;

/**
 * 
 * @author shiju.john
 *
 */
public class ProcessorUtils {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List getMappingFromMeta(Map<String, Object> meta) {

		List mapping = new ArrayList<>();
		if (meta.get(Constants.MAPPING) instanceof List) {
			mapping = (List) meta.get(Constants.MAPPING);
		} else {
			mapping.add(meta.get(Constants.MAPPING));
		}
		return mapping;
	}
	
	
	/**
	 * 
	 * @param sourceList
	 * @param targetSet
	 * @param key
	 * @return
	 */
	public static Set<String> moveKeyToSet(List<Map<String,Object>> sourceList, final Set<String> targetSet,String key){		
		sourceList.stream().parallel().forEach(data->targetSet.add(""+data.get(key)));			
		return targetSet;		
	}

}
