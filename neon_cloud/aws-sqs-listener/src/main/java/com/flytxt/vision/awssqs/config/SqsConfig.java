package com.flytxt.vision.awssqs.config;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.flytxt.vision.awssqs.listener.SqsMessageListener;
import com.flytxt.vision.configuration.cache.CommonSettings;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.utils.Constants;

/**
 * 
 * @author shiju.john
 *
 */
@Configuration
public class SqsConfig {

	@Autowired
	AwsConnectorService awsService;

	@Autowired
	CommonSettings cache;
	
	@Value("${com.flytxt.workerthread.count}")
	private int workerThreadCount;

	private static final Logger logger = LoggerFactory.getLogger(SqsConfig.class);

	@Bean
	@Autowired
	public DefaultMessageListenerContainer jmsListenerContainerFactory(SqsMessageListener sqsListener) {

		try {
			awsService.getQueueUrl(cache.getConfigValue(Constants.AWS_S3_QUEUE_NAME_KEY));
		} catch (QueueDoesNotExistException e) {
			String error = "Queue is not found . Please check the Configuration "
					+ cache.getConfigValue(Constants.AWS_S3_QUEUE_NAME_KEY);
			logger.error(error);
			throw new RuntimeException(error);
		}

		DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
		dmlc.setConnectionFactory(awsService.getConnectionFactory());
		dmlc.setDestinationName(cache.getConfigValue(Constants.AWS_S3_QUEUE_NAME_KEY));
		dmlc.setMessageListener(sqsListener);
		return dmlc;
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
//	private String readPolicyFile() throws IOException {
//		InputStream keyFileStream = SqsConfig.class.getClassLoader().getResourceAsStream("SQSPolicy.json");
//		BufferedReader buf = new BufferedReader(new InputStreamReader(keyFileStream));
//
//		String line = buf.readLine();
//		StringBuilder sb = new StringBuilder();
//		while (line != null) {
//			sb.append(line);
//			line = buf.readLine();
//		}
//		return sb.toString();
//	}

	@Bean
	public ExecutorService threadPoolTaskExecutor() {
		ThreadPoolExecutor executor =  (ThreadPoolExecutor)Executors.newFixedThreadPool(workerThreadCount);
		executor.setCorePoolSize(workerThreadCount);		
		return executor;
	}

}
