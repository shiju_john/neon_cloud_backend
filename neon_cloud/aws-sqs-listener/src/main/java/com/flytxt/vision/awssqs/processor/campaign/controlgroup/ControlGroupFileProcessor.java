package com.flytxt.vision.awssqs.processor.campaign.controlgroup;

import java.util.Map;
import java.util.Set;

import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.core.exception.VisionException;

public interface ControlGroupFileProcessor {
	
	/**
	 * 
	 * @param s3Object
	 * @param targetedFieldId
	 * @param meta
	 * @param targetedUsers
	 * @throws VisionException
	 */
	Set<String>  processFile(S3Object s3Object, String targetedFieldId, Map<String, Object> meta,
			Set<String> targetedUsers) throws VisionException;

}
