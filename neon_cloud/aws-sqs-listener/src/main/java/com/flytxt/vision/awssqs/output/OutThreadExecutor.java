package com.flytxt.vision.awssqs.output;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
/**
 * 
 * @author shiju.john
 *
 */
@Component
public class OutThreadExecutor {
	
	ForkJoinPool commonPool = ForkJoinPool.commonPool();
	
	@Autowired
	ApplicationContext applicationContext ;
	
	public ForkJoinTask<Void> execute(TableEntity entity) {
		OutProcessor outProcessor= getForkTask(entity);
		commonPool.execute(outProcessor);
		return outProcessor;
	}
	
	public OutProcessor getForkTask(TableEntity entity){
		OutProcessor outProcessor = applicationContext.getBean(OutProcessor.class);
		outProcessor.setData(entity);
		return outProcessor;
	}

}
