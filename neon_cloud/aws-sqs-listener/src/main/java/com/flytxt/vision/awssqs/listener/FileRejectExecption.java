package com.flytxt.vision.awssqs.listener;

/**
 * 
 * @author shiju.john
 *
 */
public class FileRejectExecption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileRejectExecption(String message, Throwable e) {
		super(message, e);
	}

}
