package com.flytxt.vision.awssqs.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.utils.Constants;
import com.opencsv.CSVReader;
/**
 * 
 * @author shiju.john
 *
 */
public class CsvFileProcessor {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static  String[] readEntityFromCsvReader(CSVReader csvReader, S3Object s3Object, TableEntity entity,
			String[] header, Map meta, String[] uncompletedRec) throws IOException {

		String[] currentRec;
		String[] prevRec = null;

		while ((currentRec = csvReader.readNext()) != null) {

			if (uncompletedRec != null
					&& (uncompletedRec.length != header.length || currentRec.length != header.length)) {
				currentRec = mergeUncompletdRows(uncompletedRec, currentRec);

			} else if (uncompletedRec != null && uncompletedRec.length == header.length) {
				entity.addRow(getTableRow(uncompletedRec, header, entity.getColumnMappings(), s3Object));
			}
			uncompletedRec = null;

			if (prevRec != null) {
				entity.addRow(getTableRow(prevRec, header, entity.getColumnMappings(), s3Object));

			}
			prevRec = currentRec;

		}
		return prevRec; // always last record is not processing;

	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String[] mergeUncompletdRows(String[] uncompletedRec, String[] remainingData) {

		uncompletedRec[uncompletedRec.length - 1] = uncompletedRec[uncompletedRec.length - 1] + remainingData[0];
		List<String> mergedData = new ArrayList(Arrays.asList(uncompletedRec));
		mergedData.addAll(Arrays.asList(Arrays.copyOfRange(remainingData, 1, remainingData.length)));
		return mergedData.toArray(new String[0]);

	}
	
	@SuppressWarnings("rawtypes")
	public static Map getTableRow(String[] nextRecord, String[] header, List<Map> mapping, S3Object s3Object) {
		Map<String, String> row = new HashMap<>();
		for (int i = 0; i < nextRecord.length; i++) {
			String mappainColumn = getMappingName(header[i], mapping);
			if (null != mappainColumn) {
				row.put(mappainColumn, nextRecord[i]);
			}
		}
		
		return row;
	}
	
	@SuppressWarnings("rawtypes")
	private static String getMappingName(String csvHeaderName, List<Map> mapping) {	
		for (Map entry : mapping) {
			if (entry.get(Constants.META_TEXT).equals(csvHeaderName)) {
				return (String) entry.get(Constants.META_DEST);
			}
		}
		return null;

	}

}
