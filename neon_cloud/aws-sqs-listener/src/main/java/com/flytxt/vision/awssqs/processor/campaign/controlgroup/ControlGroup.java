package com.flytxt.vision.awssqs.processor.campaign.controlgroup;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.processor.ProcessorUtils;
import com.flytxt.vision.configuration.cache.CommonSettings;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class ControlGroup {

	@Autowired
	ConfigurationService configService;

	@Autowired
	ControlGroupFileProcessor controlGroupFileProcessor;

	@Autowired
	AwsConnectorService awsConnectorService;

	@Autowired
	CommonSettings commonCache;

	Map<String, Object> controlGroupConfig = null;
	
	S3Object s3CGObject = null;

	/**
	 * 
	 * @param controlGroupId
	 * @param tenantId
	 * @param fieldId
	 * @param targetedUsers
	 * @param offerCode
	 * @return
	 * @throws VisionException
	 */
	public Set<String> filterTargetedUsers(S3Object s3Object, String controlGroupId, String fieldId,
			Set<String> targetedUsers) throws VisionException {
		if (null == controlGroupConfig) {
			controlGroupConfig = loadData(controlGroupId);
		}

		String controlGroupType = (String) controlGroupConfig.get(Constants.META_TYPE);

		if (Constants.FILTER_KEY.equalsIgnoreCase(controlGroupType)) {
			CustomDataFilter<TableEntity> customDataFilter = getDataFilter(controlGroupConfig, 500, fieldId);
			CustomDataFilter<TableEntity> controlGroupData = null;

			do {
				controlGroupData = configService.filterExpression(customDataFilter, s3Object.getTenantId());
				Set<String> controlGroupUsers = ProcessorUtils
						.moveKeyToSet(controlGroupData.getEntity().getColumnValues(), new HashSet<>(), fieldId);
				targetedUsers = targetedUsers.stream().filter(e -> !controlGroupUsers.contains(e))
						.collect(Collectors.toSet());

			} while (controlGroupData.getPages().isHasNext() && targetedUsers != null && !targetedUsers.isEmpty());

		} else if (Constants.FILE_KEY.equalsIgnoreCase(controlGroupType)) {
			if(s3CGObject==null) {
				s3CGObject = new S3Object();
				s3CGObject.setDomainName(s3Object.getDomainName());
				s3CGObject.setTenantId(s3Object.getTenantId());
				s3CGObject.setBucketName(s3Object.getBucketName());

				String fileKey = AwsConnectorService.CONFIGURATION + AwsConnectorService.SUFFIX
						+ commonCache.getAwsDomainName(s3Object.getTenantId()) + AwsConnectorService.SUFFIX
						+ controlGroupConfig.get(Constants.ENTITY_TYPE) + AwsConnectorService.SUFFIX
						+ controlGroupConfig.get("title") + AwsConnectorService.SUFFIX
						+ controlGroupConfig.get(Constants.FILE_NAME);
				s3CGObject.setFileKey(fileKey);
				s3CGObject.setFileSize((double) awsConnectorService.getFileSize(fileKey));
			}			
			targetedUsers = controlGroupFileProcessor.processFile(s3CGObject, fieldId, controlGroupConfig,
					targetedUsers);
		}
		return targetedUsers;
	}

	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws VisionException
	 */
	private Map<String, Object> loadData(String id) throws VisionException {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setConnectorId(id);
		connectorInstance = configService.get(connectorInstance);
		return TenantUtils.parseJson(connectorInstance.getConnectorConfig());
	}

	@SuppressWarnings("rawtypes")
	private CustomDataFilter<TableEntity> getDataFilter(Map<String, Object> config, int pageSize, String... type) {
		Filter filter = TenantUtils.convertMapToFilter((Map) config.get("rules"));
		Pages<TableEntity> pages = new Pages<>();
		pages.setPageSize(pageSize);
		CustomDataFilter<TableEntity> customDataFilter = new CustomDataFilter<>();
		customDataFilter.setPages(pages);
		customDataFilter.setFilterObj(filter);
		customDataFilter.setMapping((Map) config.get("mapping"));
		customDataFilter.setFields(type);
		return customDataFilter;

	}

}
