package com.flytxt.vision.awssqs.processor.campaign.controlgroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.processor.CsvFileProcessor;
import com.flytxt.vision.awssqs.processor.ProcessorUtils;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.opencsv.CSVReader;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class ControlGroupFileProcessorImpl implements ControlGroupFileProcessor {

	@Autowired
	private AwsConnectorService awsConnectorService;

	private static final int batchSize = 419430;

	private static final Logger LOGGER = LoggerFactory.getLogger(ControlGroupFileProcessor.class);

	private Set<String> controlGroupUsers = new HashSet<>();

	private boolean cacheable = true;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Set<String> processFile(S3Object s3CGObject, String targetedFieldId, Map<String, Object> meta,
			Set<String> targetedUsers) throws VisionException {

		int start = 0;
		int end = batchSize;
		String header[] = null;

		String[] uncompletedRec = null;
		List mapping = ProcessorUtils.getMappingFromMeta(meta);
		String controlGroupFieldId = getControlGroupFieldId(mapping);
		TableEntity entity = null;
		if (cacheable && !controlGroupUsers.isEmpty()) {
			long startTime = System.currentTimeMillis();
			targetedUsers = filterTargetedUers(targetedUsers, targetedFieldId, controlGroupUsers, controlGroupFieldId);
			System.out.println("Filtering time" + (System.currentTimeMillis() - startTime) / 1000);
		} else {
			do {
				BufferedReader br = awsConnectorService.getS3FileRange(s3CGObject.getBucketName(),
						s3CGObject.getFileKey(), start, end);

				if (null != br) {
					entity = getTableEntity(s3CGObject);
					entity.setColumnMappings(mapping);
					try (CSVReader csvReader = new CSVReader(br);) {
						if (start == 0) {
							header = csvReader.readNext();
						}
						uncompletedRec = CsvFileProcessor.readEntityFromCsvReader(csvReader, s3CGObject, entity, header,
								meta, uncompletedRec);
						start = end + 1;
						end = end + batchSize;

						if (start < s3CGObject.getFileSize()) {
							if (cacheable) {
								controlGroupUsers = ProcessorUtils.moveKeyToSet(entity.getColumnValues(),
										controlGroupUsers, controlGroupFieldId);
							} else {
								Set<String> controlGroupUsers = ProcessorUtils.moveKeyToSet(entity.getColumnValues(),
										new HashSet<>(), controlGroupFieldId);
								targetedUsers = filterTargetedUers(targetedUsers, targetedFieldId, controlGroupUsers,
										controlGroupFieldId);
							}
						}

					} catch (IOException e) {
						LOGGER.error(e.getMessage(), e);
						throw new VisionException(e.getMessage(), e);
					}
				}
			} while (start < s3CGObject.getFileSize());

			if (uncompletedRec != null) {
//				TableEntity entity = getTableEntity(s3CGObject);
//				entity.setColumnMappings(mapping);
				entity.addRow(
						CsvFileProcessor.getTableRow(uncompletedRec, header, entity.getColumnMappings(), s3CGObject));
				
			}
			if(entity.getColumnValues()!=null && !entity.getColumnValues().isEmpty()) {
				if (cacheable) {
					controlGroupUsers = ProcessorUtils.moveKeyToSet(entity.getColumnValues(), controlGroupUsers,
							controlGroupFieldId);
					targetedUsers = filterTargetedUers(targetedUsers, targetedFieldId, controlGroupUsers,
							controlGroupFieldId);
				} else {
					Set<String> controlGroupUsers = ProcessorUtils.moveKeyToSet(entity.getColumnValues(), null,
							controlGroupFieldId);
					targetedUsers = filterTargetedUers(targetedUsers, targetedFieldId, controlGroupUsers,
							controlGroupFieldId);
				}
			}
			
		}
		return targetedUsers;

	}

	/**
	 * 
	 * @param targetedUsers
	 * @param targetedField
	 * @param controlGroupUsers
	 * @param controlGroupField
	 * @return
	 */
	private Set<String> filterTargetedUers(Set<String> targetedUsers, String targetedField,
			Set<String> controlGroupUsers, String controlGroupField) {
		return targetedUsers.stream().parallel().filter(e -> !controlGroupUsers.contains(e))
				.collect(Collectors.toSet());
//		return targetedUsers.stream().parallel()
//				.filter(e ->!( controlGroupUsers.stream().parallel().anyMatch(controlgroup ->
//				controlgroup.get(controlGroupField).equals(e.get(targetedField)))))
//				.collect(Collectors.toList());
//		List<Map<String, Object>> newTarGet =  new ArrayList<>();
//		for(Map<String, Object> user : targetedUsers) {
//			if(!controlGroupUsers.stream().parallel().anyMatch(element ->  element.get(controlGroupField).equals(user.get(targetedField)))) {
//				newTarGet.add(user);
//			}
//			
//		}
//		return newTarGet;

	}

	/**
	 * 
	 * @param s3Object
	 * @return
	 */
	private TableEntity getTableEntity(S3Object s3Object) {

		TableEntity entity = new TableEntity();
		entity.setEntityType(s3Object.getEntityType());
		entity.setTableName(s3Object.getTableName());
		return entity;
	}

	/**
	 * 
	 * @param mapping
	 * @return
	 */
	private String getControlGroupFieldId(List<Map<String, Object>> mapping) {
		for (Map<String, Object> entry : mapping) {
			return (String) entry.get(Constants.META_DEST);

		}
		return null;
	}

}
