package com.flytxt.vision.awssqs.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.awssqs.output.OutThreadExecutor;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.UserManagementService;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;
import com.opencsv.CSVReader;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("prototype")
public class BatchFileProcessor implements FileProcessor {

	@Autowired
	private AwsConnectorService awsConnectorService;

	@Autowired
	private ConfigurationService service;

	@Autowired
	private UserManagementService userService;

	@Autowired 
	private OutThreadExecutor outThreadExecutor;
	
	
	
	private static final int batchSize = 419430;

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchFileProcessor.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void processFile(S3Object s3Object) throws VisionException {

		Map<String, Object> meta = getConfigMeta(s3Object);
		List<ForkJoinTask<Void>> futreTask = new ArrayList<>();
		s3Object = setTableName(s3Object);
		
		int start = 0;
		int end = batchSize;
		String header[] = null;

		String[] uncompletedRec = null;
		do {
			BufferedReader br = awsConnectorService.getS3FileRange(s3Object.getBucketName(), s3Object.getFileKey(),
					start, end);
			
			if (null != br) {
				TableEntity entity = getTableEntity(s3Object);
				entity.setColumnMappings((List) meta.get(Constants.MAPPING));
				
				try (CSVReader csvReader = new CSVReader(br);) {
					if (start == 0) {
						header = csvReader.readNext();
					}
					uncompletedRec = readEntityFromCsvReader(csvReader, s3Object, entity, header, meta,uncompletedRec);
					start = end + 1;
					end = end + batchSize;

				} catch (IOException e) {
					LOGGER.error(e.getMessage(), e);
					throw new VisionException(e.getMessage(), e);
				}
				futreTask.add(outThreadExecutor.execute(entity));
			}
		} while (start < s3Object.getFileSize());

		if (uncompletedRec != null) {
			TableEntity entity = getTableEntity(s3Object);
			entity.setColumnMappings((List) meta.get(Constants.MAPPING));
			entity.addRow(getTableRow(uncompletedRec, header, meta, s3Object));
			futreTask.add(outThreadExecutor.execute(entity));
		}
		
		waitForCompletion(futreTask);
		System.out.println();

	}

	private void waitForCompletion(List<ForkJoinTask<Void>> futreTask) {
		for(ForkJoinTask<Void> task :futreTask) {
			task.join();
		}
	}

	private S3Object setTableName(S3Object s3Object) {
		s3Object.setTableName(TenantUtils.getTableName(s3Object.getTenantId(), s3Object.getEntityType()));
		return s3Object;
	}

	/**
	 * 
	 * @param uncompletedRec
	 * @param remainingData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String[] mergeUncompletdRows(String[] uncompletedRec, String[] remainingData) {

		uncompletedRec[uncompletedRec.length - 1] = uncompletedRec[uncompletedRec.length - 1] + remainingData[0];
		List<String> mergedData = new ArrayList(Arrays.asList(uncompletedRec));
		mergedData.addAll(Arrays.asList(Arrays.copyOfRange(remainingData, 1, remainingData.length)));
		return mergedData.toArray(new String[0]);

	}

	/**
	 * 
	 * @param s3Object
	 * @return
	 */
	private TableEntity getTableEntity(S3Object s3Object) {

		TableEntity entity = new TableEntity();
		entity.setEntityType(s3Object.getEntityType());
		
		entity.setTableName(s3Object.getTableName());
		return entity;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String[] readEntityFromCsvReader(CSVReader csvReader, S3Object s3Object, TableEntity entity,
			String[] header, Map meta, String[] uncompletedRec) throws IOException {

		String[] currentRec;
		String[] prevRec = null;

		while ((currentRec = csvReader.readNext()) != null) {
			
			if (uncompletedRec != null
					&& (uncompletedRec.length != header.length || currentRec.length != header.length)) {
				currentRec = mergeUncompletdRows(uncompletedRec, currentRec);				

			}else if(uncompletedRec!=null && uncompletedRec.length == header.length) {
				entity.addRow(getTableRow(uncompletedRec, header, meta, s3Object));
			}
			uncompletedRec=null;

			if (prevRec != null) {
				entity.addRow(getTableRow(prevRec, header, meta, s3Object));

			}
			prevRec = currentRec;

		}
		return prevRec; // always last record is not processing;

	}

	/**
	 * 
	 * @param nextRecord
	 * @param header
	 * @param meta
	 * @param s3Object
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Map getTableRow(String[] nextRecord, String[] header, Map meta, S3Object s3Object) {
		Map<String, String> row = new HashMap<>();
		for (int i = 0; i < nextRecord.length; i++) {
			String mappainColumn = getMappingName(header[i], meta);
			if (null != mappainColumn) {
				row.put(mappainColumn, nextRecord[i]);
			}
		}
		row.put(Constants.FILE_NAME, s3Object.getFileName());
		row.put(Constants.AWS_TIME, s3Object.getDate());
		return row;
	}
	
	
	/**
	 * 
	 * @param s3Object
	 * @return
	 */
	private Map<String, Object> getConfigMeta(S3Object s3Object) {

		ConnectorInstance connectorInstance = new ConnectorInstance();
		try {
			String tenantId = userService.getTenentByDomain(s3Object.getDomainName().replaceAll("_", "\\."));
			s3Object.setTenantId(tenantId);
			connectorInstance.setConnectorName(s3Object.getAppConfigId());
			connectorInstance.setConnectorType(s3Object.getEntityType() + "_meta");
			connectorInstance.setTenantId(tenantId);
			Iterable<ConnectorInstance> meta = this.service.findByTenantAndTypeAndConnectorName(connectorInstance);
			return TenantUtils.parseJson(meta.iterator().next().getConnectorConfig());
		} catch (VisionException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	/**
	 * 
	 * @param csvHeaderName
	 * @param mapping
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private String getMappingName(String csvHeaderName, Map mapping) {
		@SuppressWarnings("unchecked")
		List<Map> mappings = (List<Map>) mapping.get(Constants.MAPPING);
		for (Map entry : mappings) {
			if (entry.get(Constants.META_TEXT).equals(csvHeaderName)) {
				return (String) entry.get(Constants.META_DEST);
			}
		}
		return null;

	}

}
