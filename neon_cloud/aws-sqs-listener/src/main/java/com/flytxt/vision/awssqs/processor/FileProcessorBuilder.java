package com.flytxt.vision.awssqs.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
/**
 * 
 * @author shiju.john
 *
 */
@Configuration
public class FileProcessorBuilder {

	@Autowired
	ApplicationContext applicationContext;
	
	
	/**
	 * 
	 * @param fileSize
	 * @return
	 */
	public FileProcessor build(double fileSize) {

		//return applicationContext.getBean(SimpleFileProcessor.class);		
		//return applicationContext.getBean(BatchFileProcessor.class);

		if (fileSize < (2 * 1000 * 1000)) {
			return applicationContext.getBean(SimpleFileProcessor.class);
		} else {
			return applicationContext.getBean(BatchFileProcessor.class);
		}

	}

}
