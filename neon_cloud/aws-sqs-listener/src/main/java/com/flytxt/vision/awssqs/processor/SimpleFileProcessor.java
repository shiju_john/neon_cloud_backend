package com.flytxt.vision.awssqs.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.awssqs.entity.S3Object;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.UserManagementService;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;
import com.opencsv.CSVReader;
/**
 * 
 * @author shiju.john
 *
 */

@Component
@Scope("prototype")

public class SimpleFileProcessor implements FileProcessor {
	
	@Autowired
	private AwsConnectorService  awsConnectorService;
	
	@Autowired
	private ConfigurationService service;
	
	@Autowired
	private UserManagementService  userService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFileProcessor.class);
	
	

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void processFile(S3Object s3Object) throws VisionException {
		
		Map meta = getConfigMeta(s3Object);	
		String tableName = TenantUtils.getTableName(s3Object.getTenantId(), s3Object.getEntityType());
		BufferedReader br = awsConnectorService.getS3File(s3Object.getBucketName(), s3Object.getFileKey());
		if (null != br) {
			LOGGER.info("Aws Read Started ");
			try(CSVReader csvReader = new CSVReader(br);) {
				TableEntity entity = new TableEntity();
				entity.setEntityType(s3Object.getEntityType());
				entity.setTableName(tableName);
				entity.setColumnMappings((List) meta.get(Constants.MAPPING));
				String[] nextRecord;				
				String header[] = csvReader.readNext();
				Map row;
				while ((nextRecord = csvReader.readNext()) != null) {

					row = new HashMap<>();
					for (int i = 0; i < nextRecord.length; i++) {
						String mappainColumn = getMappingName(header[i], meta);
						if (null != mappainColumn) {
							row.put(mappainColumn, nextRecord[i]);
						}
					}
					if(row.size()>0) {
						row.put(Constants.FILE_NAME, s3Object.getFileName());
						row.put(Constants.AWS_TIME, s3Object.getDate());
						entity.addRow(row);
					}else {
						throw new VisionException("Invalid Meta for the file "+s3Object.getFileName() +
								" Please check the file is uploaded into the right directory", null);
					}
					
				}
				LOGGER.info("Aws Read Completed. DB write started ");
				service.saveCustomData(entity, s3Object.getTenantId(), s3Object.getEntityType());	
				LOGGER.info("Db write completed ");
				
			} catch (IOException | VisionException e) {
				LOGGER.error(e.getMessage(),e);
				throw new VisionException(e.getMessage(), e);
			}
		}else {
			LOGGER.error("Unable to read the file :"+s3Object.getFileKey());
		}
		LOGGER.info("File Successfully processed : "+s3Object.getFileKey());
	}
	

	private Map<String,Object> getConfigMeta(S3Object s3Object){
	
		ConnectorInstance connectorInstance = new ConnectorInstance();
		try {
			String tenantId = userService.getTenentByDomain(s3Object.getDomainName().replaceAll("_", "\\."));
			s3Object.setTenantId(tenantId);
			connectorInstance.setConnectorName(s3Object.getAppConfigId());
			connectorInstance.setConnectorType(s3Object.getEntityType()+"_meta");
			connectorInstance.setTenantId(tenantId);
			Iterable<ConnectorInstance>  meta = this.service.findByTenantAndTypeAndConnectorName(connectorInstance);
			return TenantUtils.parseJson(meta.iterator().next().getConnectorConfig());
		} catch (VisionException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	@SuppressWarnings("rawtypes")
	private String getMappingName(String csvHeaderName, Map mapping) {
		@SuppressWarnings("unchecked")
		List<Map> mappings = (List<Map>) mapping.get(Constants.MAPPING);
		for (Map entry : mappings) {
			if (entry.get(Constants.META_TEXT).equals(csvHeaderName)) {
				return (String) entry.get(Constants.META_DEST);
			}
		}
		return null;

	}

}
