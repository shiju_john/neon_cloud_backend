package org.aws.sqs.listener;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public static void main(String[] args) {
    	long starttime = System.currentTimeMillis();
    	System.out.println("Start time "+ starttime);
    	Set<String> targetedUsers = new HashSet<>();
    	Set<String> controlGroup = new HashSet<>();
    	for(int i=0;i<1000000;i++) {
    		targetedUsers.add(""+i);
    		if(i%10==0) {
    			controlGroup.add(""+i);
    		}
    	}
    	
    	Set<String> users = targetedUsers.stream().parallel().filter(targetUser -> !controlGroup.contains(targetUser)).collect(Collectors.toSet());
    	long endTime = System.currentTimeMillis();
    	System.out.println("End time "+endTime );
    	System.out.println("Total time "+(endTime -starttime )/1000);
    	
    }
    
}
