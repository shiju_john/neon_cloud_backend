package com.flytxt.vision.configuration.service.gclod;

import java.util.List;
import java.util.stream.Stream;

import com.google.api.services.sheets.v4.Sheets;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.client.spreadsheet.WorksheetQuery;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;

/**
 * 
 * @author shiju.john
 *
 */

//@Service
//@Scope("singleton")
public class GSheetService {
	
	private GDriveService  driveService;
	private SpreadsheetService sheetService ;
	private FeedURLFactory factory;
	private Sheets sheet;
	
	
	
//	@Autowired
	public GSheetService(GDriveService  driveService) {	
		factory = FeedURLFactory.getDefault();
		this.setDriveService(driveService); 
		this.initSpredSheetService();
		
	}
	
	private void initSpredSheetService() {
		sheetService = new SpreadsheetService("neon_cloud");
		sheetService.setOAuth2Credentials(driveService.getCredential());	
		
		sheet =  new Sheets.Builder(driveService.getHttpTransport(), driveService.getJsonFactory(), driveService.getCredential())
		        .setApplicationName("Google-SheetsSample/0.1")
		        .build();
		
	}
	
	
	/**
	 * 
	 * @param spreadsheet
	 * @return
	 * @throws Exception
	 */
	
	public SpreadsheetEntry getSpreadsheet(String spreadsheet) throws Exception {

		SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(factory.getSpreadsheetsFeedUrl());
		spreadsheetQuery.setTitleQuery(spreadsheet);

		SpreadsheetFeed spreadsheetFeed = getSheetService().query(spreadsheetQuery, SpreadsheetFeed.class);
		List<SpreadsheetEntry> spreadsheets = spreadsheetFeed.getEntries();
		if (spreadsheets.isEmpty()) {
			throw new Exception("No spreadsheets with that name");
		}
		return spreadsheets.get(0);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public SpreadsheetEntry getSpreadsheetById(String id) throws Exception {		

		SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(factory.getSpreadsheetsFeedUrl());
		SpreadsheetFeed spreadsheetFeed = getSheetService().query(spreadsheetQuery, SpreadsheetFeed.class);
		List<SpreadsheetEntry> spreadsheets = spreadsheetFeed.getEntries();
		if (spreadsheets.isEmpty()) {
			throw new Exception("No spreadsheets with the id "+ id);
		}		
		Stream<SpreadsheetEntry> result = spreadsheets.stream().filter(entry-> entry.getId().equals(id));		
		return result.iterator().hasNext()?result.iterator().next():null;
	}
	
	/**
	 * 
	 * @param spreadsheet
	 * @param worksheet
	 * @return
	 * @throws Exception
	 */
	public WorksheetEntry getWorksheet(String spreadsheetName, String worksheetName) throws Exception {

		SpreadsheetEntry spreadsheetEntry = getSpreadsheet(spreadsheetName);

		WorksheetQuery worksheetQuery = new WorksheetQuery(spreadsheetEntry.getWorksheetFeedUrl());

		worksheetQuery.setTitleQuery(worksheetName);
		WorksheetFeed worksheetFeed = getSheetService().query(worksheetQuery, WorksheetFeed.class);
		List<WorksheetEntry> worksheets = worksheetFeed.getEntries();
		if (worksheets.isEmpty()) {
			throw new Exception(
					"No worksheets with that name in spreadhsheet " + spreadsheetEntry.getTitle().getPlainText());
		}

		return worksheets.get(0);
	}
	
	

	/**
	 * @return the driveService
	 */
	public GDriveService getDriveService() {
		return driveService;
	}

	/**
	 * @param driveService the driveService to set
	 */
	public void setDriveService(GDriveService driveService) {
		this.driveService = driveService;
	}

	/**
	 * @return the sheetService
	 */
	public SpreadsheetService getSheetService() {
		return sheetService;
	}

	/**
	 * @param sheetService the sheetService to set
	 */
	public void setSheetService(SpreadsheetService sheetService) {
		this.sheetService = sheetService;
	}	

}
