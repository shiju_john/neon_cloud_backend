package com.flytxt.vision.configuration.exception;

import com.flytxt.vision.core.exception.VisionException;
/**
 * 
 * @author shiju.john
 *
 */
public class UserManagementException extends VisionException{

	public UserManagementException(String message, Exception e) {
		super(message, e);
	}
	
	public UserManagementException(String message) {
		super(message,null);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
