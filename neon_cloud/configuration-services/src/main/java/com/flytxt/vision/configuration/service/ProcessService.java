package com.flytxt.vision.configuration.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author shiju.john
 *
 */

@Service
public class ProcessService extends AbstractService<ProcessFlowEntity, Filter>{
	
	@Autowired
	private ProcessMappingService service;
	
	private Gson  gson = new Gson();
	
	@Autowired	
	public ProcessService(@Qualifier("ProcessFlowEntity") VisionDao<ProcessFlowEntity> dao) {
		super(dao);
		
	}

	
	@Override
	public Pages<ProcessFlowEntity> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		return null;
	}

	@Override
	public Iterable<ProcessFlowEntity> search(Filter criteria) throws VisionException {
		return null;
	}
	
	
	@Override
	public ProcessFlowEntity save(ProcessFlowEntity entity) throws VisionException {
		/*List<PageMapping> mappings = getPageMapping(entity.getProcessConfig());
		if ("active".equalsIgnoreCase(entity.getStatus()) && mappings.size()<=0) {
			throw new VisionException("error.workflow.page_not_subscribed", null);
		}
		hasPageExist(mappings);*/
		
		ProcessFlowEntity flowEntity =  super.save(entity);
		//this.updateProcessMapping(flowEntity,mappings);		
		return flowEntity;
	}
	
	@Override
	public ProcessFlowEntity update(ProcessFlowEntity entity) throws VisionException {
		List<PageMapping> mappings = getPageMapping(entity.getProcessConfig());
		if ("active".equalsIgnoreCase(entity.getStatus()) && mappings.size()<=0) {
			throw new VisionException("error.workflow.page_not_subscribed", null);
		}
		ProcessFlowEntity flowEntity =  super.update(entity);
		this.updateProcessMapping(flowEntity,mappings);		
		return flowEntity;
	}
	
	/**
	 * 
	 * @param flowEntity
	 */
	private void updateProcessMapping(ProcessFlowEntity flowEntity, List<PageMapping> mappings) throws VisionException {

		if ("active".equalsIgnoreCase(flowEntity.getStatus())) {
			PageMapping pageMapping = new PageMapping();
			pageMapping.setProcessId(flowEntity.getProcessId());			
			deleteExistingMapping(pageMapping);
			for (PageMapping mapping : mappings) {
				mapping.setProcessId(flowEntity.getProcessId());
				mapping.setProcessVersion(flowEntity.getProcessVersion());
				mapping.setTenantId(flowEntity.getTenantId());
			}
			service.saveAll(mappings);
		}
	}

	/**
	 * 
	 * @param pageMapping
	 * @throws VisionException
	 */
	void deleteExistingMapping(PageMapping pageMapping) throws VisionException {
		List<PageMapping> savedMappings =(List) service.findByProcessId(pageMapping);
		if (savedMappings != null) {
			service.deleteAll(savedMappings);
		}
		
	}
	
	void hasPageExist(List<PageMapping> pageMapping) throws VisionException {
		for(PageMapping mapping : pageMapping) {			
			List<PageMapping> savedMappings =(List<PageMapping>) service.findByPageId(mapping);
			if (savedMappings != null && savedMappings.size()>0) {
				//service.deleteById(savedMappings.get(0).getHashKey());
				throw new VisionException("error.user.page_already_subscribed", null);
			}
		}
			
		
	}

	@SuppressWarnings("rawtypes")
	private List<PageMapping> getPageMapping(String jsonString) {

		List<PageMapping> pageMappings = new ArrayList<>();
		Map<String, Object> mapCopy = gson.fromJson(jsonString, new TypeToken<Map<String, Object>>() {
		}.getType());
		Object tasks = mapCopy.get("tasks");
		if (tasks instanceof List) {		
			LinkedTreeMap connector = getInputConnector((List) tasks);
			Object pages = ((LinkedTreeMap) connector.get("connectorConfig")).get("pages");
			if (pages instanceof List) {
				List<LinkedTreeMap> pageList = (List) pages;
				for (LinkedTreeMap map : pageList) {
					setPageMapping(map,pageMappings);				
				}
			}
			if (pages instanceof Map) {
				setPageMapping((LinkedTreeMap)pages,pageMappings);				
			}
		}
		return pageMappings;
	}
	
	/**
	 * 
	 * @param map
	 * @param pageMappings
	 */
	void  setPageMapping(@SuppressWarnings("rawtypes") LinkedTreeMap map,List<PageMapping> pageMappings) {
		
		if ("true".equalsIgnoreCase("" + map.get("status"))) {
			PageMapping mapping  = new PageMapping();
			mapping.setPageId("" + map.get("id"));
			pageMappings.add(mapping);			
		}
		
	}

	@SuppressWarnings("rawtypes")
	private LinkedTreeMap getInputConnector(List<LinkedTreeMap<Object, Object>> pages) {
		for (LinkedTreeMap map : pages) {
			if ("inputConnector".equalsIgnoreCase("" + map.get("taskType"))) {
				return (LinkedTreeMap) map.get("taskProperties");
			}
		}
		return null;

	}

	/**
	 * 
	 * @param config
	 * @return
	 * @throws VisionException 
	 */
	public ProcessFlowEntity findByProcessIdAndProcessVersionAndStatus(ProcessFlowEntity config) throws VisionException {
		Iterable<ProcessFlowEntity> result = dao.findBy(config, null, new String[0]);
		if (null != result && result.iterator().hasNext()) {
			return result.iterator().next();
		}
		return null;				
	}
	
	@Override
	public ProcessFlowEntity deleteById(Serializable id) throws VisionException {
		ProcessFlowEntity flowEntity = new ProcessFlowEntity();
		flowEntity.setHashKey((String)id);
		
		ProcessFlowEntity entity =   super.get(flowEntity);
		if (entity!=null) {
			super.delete(flowEntity );	
			PageMapping pageMapping = new PageMapping();
			pageMapping.setProcessId(entity.getProcessId());
			deleteExistingMapping(pageMapping);
			return flowEntity;
		}
		throw new VisionException("error.common.notfound", null);
		
	}


}
