/**
 * 
 */
package com.flytxt.vision.configuration.entity;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author athul
 *
 */

@Data
public class SalesforceUserEntity {
	
	@NotNull 
	public final String username;
	@NotNull
	public final String password;
	@NotNull
	public final String clientId;
	@NotNull
	public final String clientSecret;
}
