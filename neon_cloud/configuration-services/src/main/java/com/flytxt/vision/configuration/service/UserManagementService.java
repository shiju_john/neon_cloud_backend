package com.flytxt.vision.configuration.service;

import java.io.Serializable;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.aws.exception.BucketPathExistException;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.configuration.entity.PasswordChangeEntity;
import com.flytxt.vision.configuration.exception.UserManagementException;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.HelperUtils;
import com.flytxt.vision.utils.TenantUtils;
import com.google.gson.JsonObject;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class UserManagementService extends AbstractService<UserEntity, Filter> {
	
	@Autowired
	AwsConnectorService awsConnectorService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementService.class);
	
	@Autowired
	InitService service;

	public UserManagementService(@Qualifier("UserEntity") VisionDao<UserEntity> dao) {
		super(dao);
	}

	@Override
	public Pages<UserEntity> search(Filter criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException {
		return null;
	}

	@Override
	public Iterable<UserEntity> search(Filter criteria) throws VisionException {
		return null;
	}

	@Override
	public UserEntity deleteById(Serializable id) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setHashKey((String) id);
		return super.delete(userEntity);
	}

	public Iterable<UserEntity> findByParentId(String parentUserId) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setParentUserId(parentUserId);
		return dao.findBy(userEntity, null, "byParent");
	}

	/**
	 * 
	 * @param userEntity
	 * @return
	 * @throws VisionException 
	 */
	public UserEntity login(UserEntity userEntity) throws VisionException {

		UserEntity entity = getUserByName(userEntity);
		if (!entity.getUserCredential().equals(userEntity.getUserCredential())) {
			throw new UserManagementException("error.user.login_error");
		}
		entity.setUserCredential("");
		return entity;
	}
	
	
	@Override
	public UserEntity save(UserEntity entity) throws VisionException {

		if(hasUserExist(entity.getUserName())){
			throw new UserManagementException("error.user.exist");
		}
		if("TENANT".equalsIgnoreCase(entity.getUserRole()) && !HelperUtils.isNotNull(entity.getParentUserId())) {
			
			if(HelperUtils.isNotNull(entity.getDomainName())){
				if(hasDomainExist(entity.getDomainName())) {
					throw new VisionException("error.domian_exist", null);
				}
				entity.setTenantId(TenantUtils.getTenantId( entity.getUserName(), entity.getUserRole(), entity.getDomainName()));
				createAwsDirectoryForDomain(entity.getDomainName(),entity.getTenantId());
				
				
			}else {
				throw new VisionException("error.domian_mandatory", null);
			}
			
		}
		return super.save(entity);
	}

	
	public Map<String,String> createAwsDirectoryForDomain(String domainName, String tenantId) throws VisionException {
		domainName = domainName.replaceAll("\\.","_");
		Map<String,String> bucketJson = new HashMap<>(2);
		try {
			LOGGER.info("Start to create bucket folder for the domain "+domainName);
			bucketJson = awsConnectorService.createDomainDirectory(domainName);
		
			
		} catch (BucketPathExistException e) {
			LOGGER.info("Bucket folder already Exist for the domain "+domainName);			
		}
		
		LOGGER.info("creating SFTP user for the domain "+domainName);
		try{
			bucketJson.putAll(awsConnectorService.createAwsTransferUser(domainName));
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		LOGGER.info("SFTP user created for the domain "+domainName);		
		
		Settings settings = new  Settings();
		settings.setKey(Constants.SFTP_KEY);			
		settings.setValue(TenantUtils.mapToJsonString(bucketJson));
		settings.setTenantId(tenantId);
		settings = service.save(settings);
		LOGGER.info("Save the user SFTP configuration for the domain "+domainName);
		return bucketJson;
		
	}

	@Override
	public UserEntity update(UserEntity entity) throws VisionException {
		
		if("TENANT".equalsIgnoreCase(entity.getUserRole()) && !HelperUtils.isNotNull(entity.getTenantId())){
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("userName", entity.getUserName());
				jsonObject.addProperty("role", entity.getUserRole());
				jsonObject.addProperty("UID", UUID.randomUUID().toString());			
				entity.setTenantId(Base64.getEncoder().encodeToString(jsonObject.toString().getBytes()));			
		}
		return super.update(entity);
	}
	
	
	/**
	 * 
	 * @param userEntity
	 * @return
	 * @throws VisionException 
	 */
	public UserEntity getUserByName(UserEntity userEntity) throws VisionException {
		Iterable<UserEntity> entities = dao.findBy(userEntity, null, "byUserName");
		if (!entities.iterator().hasNext())
			throw new UserManagementException("error.user.not_exist");
		return entities.iterator().next();
	}
	
	

	/**
	 * 
	 * @param userName
	 * @return
	 * @throws VisionException 
	 */
	public boolean hasUserExist(String userName) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserName(userName);
		Iterable<UserEntity> entity = dao.findBy(userEntity, null, "byUserName");
		return entity.iterator().hasNext();
	}
	
	
	
	public String getTenantId(String userName) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserName(userName);
		Iterable<UserEntity> entity = dao.findBy(userEntity, null, "byUserName");
		if(entity.iterator().hasNext()) {
			return entity.iterator().next().getTenantId();
		}
		throw new UserManagementException("error.user.not_exist");
	}

	 /**
	  * 
	  * @param passwordChangeEntity
	  * @return
	  * @throws VisionException
	  */
	public Boolean passwordChange(PasswordChangeEntity passwordChangeEntity) throws VisionException {
		UserEntity entity = new UserEntity();
		entity.setUserName(passwordChangeEntity.getUserName());
		entity = getUserByName(entity);		
		if(passwordChangeEntity.getConfirmPassword().equals(passwordChangeEntity.getPassword()) 
				&& entity.getUserCredential().equals(passwordChangeEntity.getOldPassword())) {
			entity.setUserCredential(passwordChangeEntity.getPassword());
			super.update(entity);
			return true;			
		}
		throw new UserManagementException("error.user.password_change");		
	}

	/**
	 * 
	 * @param userEntity
	 * @return
	 * @throws VisionException
	 */
	public UserEntity updatePayment(UserEntity userEntity) throws VisionException {
		UserEntity entity = getUserByName(userEntity);
		entity.setPaymentYn(userEntity.getPaymentYn());
		entity.setPaymentMode(entity.getPaymentMode());		
		return update(entity);
	}
	
	/**
	 * 
	 * @param domainName
	 * @return
	 * @throws VisionException
	 */
	public Boolean  hasDomainExist(String domainName) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setDomainName(domainName);
		Iterable<UserEntity> data;		
		data = dao.findBy(userEntity, null, "byDomain");
		return data!=null && data.iterator().hasNext();			
		
	}
	
	
	/**
	 * 
	 * @param domainName
	 * @return
	 * @throws VisionException
	 */
	public String getTenentByDomain(String domainName) throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setDomainName(domainName);
		Iterable<UserEntity> entity = dao.findBy(userEntity, null, "byDomain");
		return entity!=null && entity.iterator().hasNext()?entity.iterator().next().getTenantId()	:null;
	}

	
	/**
	 * 
	 * @return
	 * @throws VisionException
	 */
	public Iterable<UserEntity> getTenants() throws VisionException {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserRole("TENANT");
		return dao.findBy(userEntity, null, "getTenants");
	}

}
