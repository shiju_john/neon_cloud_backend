/**
 * 
 */
package com.flytxt.vision.configuration.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.flytxt.vision.configuration.entity.SalesforceUserEntity;
import com.force.api.ForceApi;

/**
 * @author athul
 *
 */

@Service
public class SalesforceAuthService {
	
	@Value("salesforce.redirect.url")
	private String salesforceRedirectUrl;

	private ForceApi api;
	private SalesforceUserEntity entity;
	
//	public String login(String clientId, String clientSecret) {
//		entity = new SalesforceUserEntity(clientId, clientSecret, salesforceRedirectUrl);
//		
//		String url = Auth.startOAuthWebServerFlow(new AuthorizationRequest()
//				.apiConfig(new ApiConfig()
//					.setClientId(entity.getClientId())
//					.setRedirectURI(entity.getRedirectUrl())));
//		return url;
//	}
//
//	/**
//	 * @param code
//	 */
//	public ForceApi requestTokenAndGetApi(String code) {
//		entity.setCode(code);
//		ApiSession s = Auth.completeOAuthWebServerFlow(new AuthorizationResponse()
//				.apiConfig(new ApiConfig()
//					.setClientId(entity.getClientId())
//					.setClientSecret(entity.getClientSecret())
//					.setRedirectURI(entity.getRedirectUrl()))
//				.code(entity.getCode()));
//		api = new ForceApi(s);
//		return api;
//	}

}
