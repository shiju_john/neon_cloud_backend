package com.flytxt.vision.configuration.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;

/**
 * 
 * @author shiju.john
 *
 */

@Service
public class ContextService extends AbstractService<ContextEntity, Filter>{
	
	
	
	@Autowired	
	public ContextService(@Qualifier("ContextEntity") VisionDao<ContextEntity> dao) {
		super(dao);
		
	}

	
	@Override
	public Pages<ContextEntity> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		return null;
	}

	@Override
	public Iterable<ContextEntity> search(Filter criteria) throws VisionException {
		return null;
	}

		
	@Override
	public ContextEntity deleteById(Serializable id) throws VisionException {
		ContextEntity contextEntity = new ContextEntity();
		contextEntity.setHashKey((String)id);
		return super.delete(contextEntity );
	}
	
	public Iterable<ContextEntity> findContext(String tenantId) throws VisionException {
		ContextEntity entity = new ContextEntity();
		entity.setTenantId(tenantId);
		return dao.findBy(entity, null,  "byTenant");
	}
	
}
