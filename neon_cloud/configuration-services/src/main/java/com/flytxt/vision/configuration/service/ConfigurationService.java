package com.flytxt.vision.configuration.service;

import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.configuration.service.aws.AwsConnectorService;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.CustomDataFilter;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
import com.flytxt.vision.utils.Constants;
import com.flytxt.vision.utils.TenantUtils;

/**
 * 
 * @author shiju.john
 *
 */

@Service
public class ConfigurationService extends AbstractService<ConnectorInstance, Filter> {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationService.class);
	
	

	@Autowired
	AwsConnectorService awsConnectorService;

	@Autowired
	@Qualifier("TableEntity")
	private DDLDao<TableEntity> ddldao;

	@Autowired
	private InitService inintService;

	@Autowired
	UserManagementService userService;

	@Autowired
	public ConfigurationService(@Qualifier("ConnectorInstance") VisionDao<ConnectorInstance> dao) {
		super(dao);

	}

	@Override
	public Pages<ConnectorInstance> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		return null;
	}

	@Override
	public Iterable<ConnectorInstance> search(Filter criteria) throws VisionException {
		return null;
	}

	/**
	 * 
	 * @param instance
	 * @return
	 * @throws VisionException
	 */
	public Iterable<ConnectorInstance> findByTenantAndType(ConnectorInstance instance) throws VisionException {
		return dao.findBy(instance, null, "byTenantAndType");
	}

	/**
	 * 
	 * @param config
	 * @param pages
	 * @return
	 * @throws VisionException
	 */
	public Iterable<ConnectorInstance> findByType(ConnectorInstance config, Pages<ConnectorInstance> pages)
			throws VisionException {
		CustomData customData = CustomData.getTenantData(config.getConnectorType());
		if (customData != null) {
			return customData.findAll(config, dao, ddldao);
		}

		ConfigData configData = ConfigData.getTenantData(config.getConnectorType());
		if (null != configData) {
			return configData.findAll(config, dao, ddldao);
		}

		return dao.findBy(config, null, "");
	}

	/**
	 * 
	 * @param type
	 * @param pages
	 * @return
	 * @throws VisionException
	 */
	public Pages<ConnectorInstance> findByTypeandPage(String type, Pages<ConnectorInstance> pages)
			throws VisionException {
		ConnectorInstance config = new ConnectorInstance();
		config.setConnectorType(type);
		Page<ConnectorInstance> page = (Page<ConnectorInstance>) dao.findBy(config, super.getPageRequest(pages), "");
		return super.getPaginationResult(page);

	}

	@Override
	public ConnectorInstance update(ConnectorInstance entity) throws VisionException {
		return super.update(entity);
	}

	@Override
	public ConnectorInstance save(ConnectorInstance entity) throws VisionException {
		EntityMetaData customData = EntityMetaData.getTenantData(entity.getConnectorType());
		if (null != customData) {
			Map<String, Object> meta = TenantUtils.parseJson(entity.getConnectorConfig());
			String entityType = (String) meta.get(Constants.ENTITY_TYPE);
			String mode = (String) meta.get(Constants.MODE);
			if (Constants.MODE_NEW.equalsIgnoreCase(mode)) {
				entity = customData.save(entity, dao, ddldao);
			}
			createAwsConfiguration(entity, entityType, customData.getGroup());
			if("config".equalsIgnoreCase(customData.getGroup())) {
				meta.put("url", awsConnectorService.getPresignedUrl(entity.getConnectorName(), entityType));
				entity.setConnectorConfig(TenantUtils.mapToJsonString(meta));
			}
			
			return entity;
		}
		ConfigData configData = ConfigData.getTenantData(entity.getConnectorType());
		if (null != configData) {
			return configData.save(entity, dao, ddldao);
		}
		return super.save(entity);
	}

	@Override
	public ConnectorInstance deleteById(Serializable id) throws VisionException {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setConnectorId((String) id);
		return super.delete(connectorInstance);
	}

	/**
	 * 
	 * @param records
	 * @param tenantId
	 * @param fileType
	 * @throws VisionException
	 */
	public void saveCustomData(TableEntity records, String tenantId, String fileType) throws VisionException {
		CustomData customData = CustomData.getTenantData(fileType);
		if (null != customData) {
			customData.save(records, dao, ddldao);
		}

	}

	/**
	 * 
	 * @param connectorInstance
	 * @param metaType
	 * @return
	 * @throws VisionException
	 */
	private Map<String, String> createAwsConfiguration(ConnectorInstance connectorInstance, String metaType,
			String group) throws VisionException {
//		UserEntity  entity = new UserEntity();
//		entity.setUserName("conversation");
//		
//		entity = userService.getUserByName(entity);
//		
//		entity.setTenantId(TenantUtils.getTenantId(entity.getUserName(), entity.getUserRole(), entity.getDomainName()));
//		userService.update(entity);
		String domainName = TenantUtils.awsDomainName(connectorInstance.getTenantId());
		Settings config = new Settings();
		config.setKey(Constants.SFTP_KEY);
		config.setTenantId(connectorInstance.getTenantId());
		Settings settings = inintService.findByTenantAndKey(config);
		Map<String, String> configData = settings != null ? TenantUtils.parseJsonToMapString(settings.getValue())
				: null;
		if (null == settings) {
			configData = userService.createAwsDirectoryForDomain(domainName, connectorInstance.getTenantId());

		}

		if ("config".equalsIgnoreCase(group)) {
			awsConnectorService.createConfigBucket(domainName,
					metaType + AwsConnectorService.SUFFIX + connectorInstance.getConnectorName());
		} else {
			awsConnectorService.createInputBucket(domainName,
					metaType + AwsConnectorService.SUFFIX + connectorInstance.getConnectorName());
		}

		return configData;
	}
	
	
	

	/**
	 * 
	 * @param instance
	 * @return
	 * @throws VisionException
	 */
	public Iterable<ConnectorInstance> findByTypesIn(ConnectorInstance instance) throws VisionException {
		return dao.findBy(instance, null, "findByTypesIn");
	}

	/**
	 * 
	 * @param dataFilter
	 * @param tenantId
	 * @return
	 * @throws VisionException
	 */
	public CustomDataFilter<TableEntity> filterExpression(CustomDataFilter<TableEntity> dataFilter, String tenantId)
			throws VisionException {
		return ddldao.findByFilterExpression(dataFilter, tenantId);
	}

	/**
	 * 
	 * @param connectorInstance
	 * @return
	 * @throws VisionException
	 */
	public Iterable<ConnectorInstance> findByTenantAndTypeAndConnectorName(ConnectorInstance connectorInstance)
			throws VisionException {
		return dao.findBy(connectorInstance, null, "byTenantAndTypeAndConnectorName");
	}

}
