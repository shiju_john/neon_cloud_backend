package com.flytxt.vision.configuration.service.gclod;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.flytxt.vision.configuration.service.ConfigurationService;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.SecurityUtils;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
/**
 * 
 * @author shiju.john
 *
 */

@Service
@Scope("singleton")
public class GDriveService {
	
		
	private HttpTransport httpTransport;
	private JacksonFactory jsonFactory;
	private GoogleCredential credential;
	private Drive drive;
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationService.class);
	
	
	public GDriveService() throws Exception {		
		 setHttpTransport(GoogleNetHttpTransport.newTrustedTransport());
		 setJsonFactory(new JacksonFactory());
		 this.initCredentials();
		 drive =  getDriveService();		
	}
	
	
	/**
	 * 
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void initCredentials() throws GeneralSecurityException, IOException, URISyntaxException{	
	  InputStream keyFile =  GDriveService.class.getClassLoader().getResourceAsStream("testapp-232908-9650364d79d7.p12");
	  PrivateKey serviceAccountPrivateKey = SecurityUtils.loadPrivateKeyFromKeyStore(
		          SecurityUtils.getPkcs12KeyStore(),keyFile , "notasecret",
		          "privatekey", "notasecret");
	  
	 // .setServiceAccountPrivateKeyFromP12File(new java.io.File
	    		// (GDriveService.class.getClassLoader().getResourceAsStream("testapp-232908-9650364d79d7.p12").))
		
	  setCredential(new GoogleCredential.Builder()
	     .setTransport(getHttpTransport())
	     .setJsonFactory(getJsonFactory()) 
	     .setServiceAccountId("neon-service@testapp-232908.iam.gserviceaccount.com")
	     .setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE))
	     .setServiceAccountPrivateKey(serviceAccountPrivateKey)	   
	     .build());
		
	}
	
	/**
	 * 
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private Drive getDriveService() throws GeneralSecurityException, IOException, URISyntaxException{    
		return  new Drive.Builder(getHttpTransport(), getJsonFactory(),getCredential())
         .setApplicationName("neon_cloud").build();
	}
	
	
	/**
	 * 
	 * @param domainName
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public String  createDomain(String domainName) throws IOException, URISyntaxException{		
	String dirId = getDirectoryId(domainName);
	  if (null==dirId) {			  
		  com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		  fileMetadata.setName(domainName);
		  fileMetadata.setMimeType("application/vnd.google-apps.folder");
		  
		  com.google.api.services.drive.model.File file = drive.files().create(fileMetadata)
		    .setFields("id")
		    .execute();	
		  dirId = file.getId();
	  }
	  return dirId;
		  
	}
	
	/**
	 * 
	 * @param domainName
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void  defaultSettings(String domainName) throws IOException, URISyntaxException{		
		uploadTemplates(domainName,createDomain(domainName));
		
	}
	
	public String uploadImage(String domainName,MultipartFile fileContent) throws IOException, URISyntaxException{
		String dirId = createDomain(domainName);		
		File fileMetadata = new File();
		fileMetadata.setName(domainName+"_photo");
		fileMetadata.setParents(Collections.singletonList(dirId));
		fileMetadata.setOriginalFilename(domainName+"_photo");
		InputStreamContent mediaContent = new InputStreamContent("image/jpeg",fileContent.getInputStream());
		File file = drive.files().create(fileMetadata, mediaContent)
		    .setFields("id")
		    .execute();
		
		createAnyoneReadPermission(file.getId());
		return file.getId();
	}
	
	public String  getImageId(String domainName) {
		try {
			String domainDir = getDirectoryId(domainName);
			Files.List request = drive.files().list().setQ(
				       "trashed=false and name='"+domainName+"_photo' and '"+domainDir+"' in parents");
			FileList files = request.execute();
		  	for (File directory : files.getFiles()) {
		  		return directory.getId();		  		
		  	}
		} catch (IOException e) {
			logger.error("unable to load the image. Domain "+domainName);
		}
		return null;
		
	}
	
	/**
	 * 
	 * @param domainName
	 * @return
	 */
	private String getDirectoryId(String domainName) {
		try {
			boolean exist=false;
			Files.List request = drive.files().list().setQ(
				       "mimeType='application/vnd.google-apps.folder' and trashed=false and name='"+domainName+"'");
			File domainDir = null;
			  do {
				  	FileList files = request.execute();
				  	for (File directory : files.getFiles()) {
				  		if(domainName.equals(directory.getName()) || domainName.equals(directory.getOriginalFilename())) {
				  			domainDir = directory;
				  			exist=true;
				  			break;
				  		}			  		
				  	}		      
			        request.setPageToken(files.getNextPageToken());
			    } while (request.getPageToken() != null &&
			             request.getPageToken().length() > 0 && !exist);
			
			
			return null!=domainDir ? domainDir.getId() : null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws IOException
	 */
	private List<File> filesBasedOnCriteria(String criteria) throws IOException {
		List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
		Files.List request = drive.files().list();
		if (criteria != null) {
			request = request.setQ(criteria);
		}
		do {
			FileList files = request.execute();
			result.addAll(files.getFiles());
			request.setPageToken(files.getNextPageToken());
		} while (request.getPageToken() != null && request.getPageToken().length() > 0);
		return result;
	}
	
	
	/**
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void uploadTemplates(String domainName, String parent) throws IOException, URISyntaxException {
		
		String cmFileName = domainName+"_campaign_constructs";
		List<File> exitsing_files = filesBasedOnCriteria("name='"+cmFileName+"'");
		if(exitsing_files==null||exitsing_files.isEmpty() || exitsing_files.size()==0) {
			File fileMetadata = new File();
			fileMetadata.setName(cmFileName);
			fileMetadata.setOriginalFilename(cmFileName);
			fileMetadata.setParents(Collections.singletonList(parent));
			fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");

			java.io.File filePath = new java.io.File(
					this.getClass().getClassLoader().getResource("templates/campaign_constructs.csv").toURI());
			FileContent mediaContent = new FileContent("text/csv", filePath);

			File file = drive.files().create(fileMetadata, mediaContent).setFields("id").execute();
			createPermissionForEmail(file.getId(), "shiju.john@flytxt.com");			
			
		}
		
	}
	
	/**
	 * 
	 * @param googleFileId
	 * @param googleEmail
	 * @return
	 * @throws IOException
	 */
	private Permission createPermissionForEmail(String googleFileId, String googleEmail) throws IOException {
		// All values: user - group - domain - anyone
		String permissionType = "user"; // Valid: user, group
		// organizer - owner - writer - commenter - reader
		String permissionRole = "writer";

		Permission newPermission = new Permission();
		newPermission.setType(permissionType);
		newPermission.setRole(permissionRole);
		newPermission.setEmailAddress(googleEmail);
		return drive.permissions().create(googleFileId, newPermission).execute();
	}
	
	/**
	 * 
	 * @param googleFileId
	 * @return
	 * @throws IOException
	 */
	private Permission createAnyoneReadPermission(String googleFileId) throws IOException {
		// All values: user - group - domain - anyone
		String permissionType = "anyone"; // Valid: user, group
		// organizer - owner - writer - commenter - reader
		String permissionRole = "reader";
		Permission newPermission = new Permission();
		newPermission.setType(permissionType);
		newPermission.setRole(permissionRole);
		
		return drive.permissions().create(googleFileId, newPermission).execute();
	}
	
	
	/**
	 * 
	 * @throws IOException
	 */
	public void deleteAll() throws IOException {		
		List<File> files = filesBasedOnCriteria(null);		
		for (File  file : files) {			
			drive.files().delete(file.getId()).execute();			
		}		
	}
	
	

	/**
	 * @param credential the credential to set
	 */
	public void setCredential(GoogleCredential credential) {
		this.credential = credential;
	}

	/**
	 * @param credential the credential to set
	 */
	public GoogleCredential getCredential() {
		return this.credential;
	}


	/**
	 * @return the jsonFactory
	 */
	public JacksonFactory getJsonFactory() {
		return jsonFactory;
	}


	/**
	 * @param jsonFactory the jsonFactory to set
	 */
	public void setJsonFactory(JacksonFactory jsonFactory) {
		this.jsonFactory = jsonFactory;
	}


	/**
	 * @return the httpTransport
	 */
	public HttpTransport getHttpTransport() {
		return httpTransport;
	}


	/**
	 * @param httpTransport the httpTransport to set
	 */
	public void setHttpTransport(HttpTransport httpTransport) {
		this.httpTransport = httpTransport;
	}


	
	

}
