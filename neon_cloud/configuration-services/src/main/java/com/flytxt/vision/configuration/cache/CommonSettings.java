package com.flytxt.vision.configuration.cache;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.configuration.service.InitService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.HelperUtils;
import com.flytxt.vision.utils.TenantUtils;

@Component
@Scope("singleton")
public class CommonSettings {

	@Autowired
	private InitService initService;

	private Map<String, Settings> configurationCache;
	
	private Map<String, String> tenantMap = new HashMap<>();
	
	
	private static final Logger logger = LoggerFactory.getLogger(UserRoleCache.class);

	@PostConstruct
	public void loadAll() {
		try {
			configurationCache = new HashMap<>();
			Iterable<Settings> configurations = initService.findByTenantIsNull();
			for (Settings config : configurations) {
				if (HelperUtils.isNotNull(config.getType()) && config.getType().equalsIgnoreCase("configuration")) {
					configurationCache.put(config.getKey(), config);
				}
			}
		} catch (VisionException e) {
			 logger.error(e.getLocalizedMessage(),e);
		}
	}
	
	private String loadKey(String key) {
		Settings settings = new Settings();
		settings.setKey(key);
		try {
			Iterable<Settings> data = initService.findByKey(settings, null);
			for(Settings config:data) {
				if(!HelperUtils.isNotNull(config.getTenantId())){
					configurationCache.put(key, config);
					return config.getValue();
				}
				
			}
		} catch (VisionException e) {
			 logger.error(e.getLocalizedMessage(),e);
			 logger.error("Critical error :  Unable to load system configuration Key :" + key);
		}
		return null;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getConfigValue(String key) {
		return configurationCache.containsKey(key)? 
			configurationCache.get(key).getValue():loadKey(key);		

	}

	/**
	 * @return the initService
	 */
	public InitService getInitService() {
		return initService;
	}

	/**
	 * @param initService the initService to set
	 */
	public void setInitService(InitService initService) {
		this.initService = initService;
	}
	
	/**
	 * 
	 * @param tenantId
	 * @return
	 */
	public String getDomainName(String tenantId){
		if(!tenantMap.containsKey(tenantId)) {
			tenantMap.put(tenantId, TenantUtils.getDomainName(tenantId));
		}
		return tenantMap.get(tenantId);
		
	}
	
	public String getAwsDomainName(String tenantId){
		if(!tenantMap.containsKey(tenantId)) {
			tenantMap.put(tenantId, TenantUtils.getDomainName(tenantId));
		}
		return TenantUtils.replaceToAwsName(tenantMap.get(tenantId));
		
	}
//
//	/**
//	 * @return the tenantMap
//	 */
//	public Map<String, String> getTenantMap() {
//		return tenantMap;
//	}
//
//	/**
//	 * @param tenantMap the tenantMap to set
//	 */
//	public void setTenantMap(Map<String, String> tenantMap) {
//		this.tenantMap = tenantMap;
//	}

}
