package com.flytxt.vision.configuration.cache;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.HelperUtils;
import com.flytxt.vision.utils.TenantUtils;

@Component
public class UserRoleCache {
	
	private static final Logger logger = LoggerFactory.getLogger(UserRoleCache.class);
	
	
	private Map<String,Map> userRoleMap;
	
	@Autowired
	ConfigurationService configurationService;
	
	
	
	public UserRoleCache() {
		userRoleMap = new HashMap<String, Map>();
	}
	
	public Map getUserRole(String tenantId,String userRole,String userAppId){	
		if(HelperUtils.isNotNull(userRole)) {
			boolean loadded =false;
			Map <String , Map> roleMap = userRoleMap.get(tenantId);
			if(null==roleMap) {
				loadRoles(tenantId);
				loadded=true;
			}
			roleMap = userRoleMap.get(tenantId);
			if(roleMap==null) {
				return null;
			}
			Map appMap = roleMap.get(userRole);
			if (appMap==null && !loadded) {
				loadArole(userRole,tenantId);
			}
			appMap = roleMap.get(userRole);
			return appMap!=null?(Map)appMap.get(userAppId):null;	
		}
		return null;
		
	}
	
	public void loadArole(String roleName,String tenantId) {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setTenantId(tenantId);
		connectorInstance.setConnectorType("userRole");
		Iterable<ConnectorInstance> userRoles;
		try {
			userRoles = configurationService.findByTenantAndType(connectorInstance);
			userRoles.forEach(role->{	
				Map userRole = TenantUtils.parseJson(role.getConnectorConfig());
				if(((String)userRole.get("userRole")).equalsIgnoreCase(roleName)) {
					
					Map roleMap = userRoleMap.get(tenantId);
					Map<String, Map> appMap = null;
					
					if(null==roleMap) {
						roleMap = new HashMap<>();
					}else {
						appMap =(Map) roleMap.get((String)userRole.get("userRole"));
					}
					if(appMap==null) {
						appMap = new HashMap<>();
					}				
					appMap.put((String)userRole.get("appId"), userRole);				
					roleMap.put((String)userRole.get("userRole"), appMap);
					userRoleMap.put(tenantId, roleMap);
					
				}
			});
		} catch (VisionException e) {
			e.printStackTrace();
		}		
		
	}

	private void loadRoles(String tenantId) {
		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setTenantId(tenantId);
		connectorInstance.setConnectorType("userRole");
		try {
			Iterable<ConnectorInstance> userRoles = configurationService.findByTenantAndType(connectorInstance);
			userRoles.forEach(role->{				
				
				Map userRole = TenantUtils.parseJson(role.getConnectorConfig());
				
				Map roleMap = userRoleMap.get(tenantId);
				Map<String, Map> appMap = null;
				
				if(null==roleMap) {
					roleMap = new HashMap<>();
				}else {
					appMap =(Map) roleMap.get((String)userRole.get("userRole"));
				}
				if(appMap==null) {
					appMap = new HashMap<>();
				}				
				appMap.put((String)userRole.get("appId"), userRole);				
				roleMap.put((String)userRole.get("userRole"), appMap);
				userRoleMap.put(tenantId, roleMap);
				
			});
		} catch (VisionException e) {
			logger.error(e.getLocalizedMessage(),e);
		}	
	}
	

}
