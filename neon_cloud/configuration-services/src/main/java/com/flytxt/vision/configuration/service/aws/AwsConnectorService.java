package com.flytxt.vision.configuration.service.aws;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.flytxt.aws.api.AWSService;
import com.flytxt.aws.entity.AwsTransferUser;
import com.flytxt.aws.exception.BucketPathExistException;
import com.flytxt.aws.services.AwsServiceImpl;
import com.flytxt.vision.configuration.cache.CommonSettings;
import com.flytxt.vision.utils.Constants;

/**
 * 
 * @author shiju.john
 *
 */
@Component
public class AwsConnectorService {



	@Value("${amazon.aws.accesskey}")
	private String amazonAWSAccessKey;

	@Value("${amazon.aws.secretkey}")
	private String amazonAWSSecretKey;

	private AWSService awsServices = null;

	public static final String SUFFIX = "/";

	public static final String BACKUP = "backup";
	private static final String INPUT_FOLDER = "input";
	public static final String CONFIGURATION = "configuration";

	private static final Logger LOGGER = LoggerFactory.getLogger(AwsConnectorService.class);

	@Autowired
	private CommonSettings configCache;

	@PostConstruct
	private void postConstruct() {
		awsServices = AwsServiceImpl.getInstance(amazonAWSAccessKey, amazonAWSSecretKey,
				configCache.getConfigValue(Constants.AWS_REGION_KEY));
	}

	public AwsConnectorService() throws IOException {

	}

	/**
	 * 
	 * @param domainName
	 * @return
	 * @throws BucketPathExistException
	 */
	public Map<String, String> createDomainDirectory(String domainName) throws BucketPathExistException {

		createDirIfNotExist(INPUT_FOLDER + SUFFIX + domainName); // create input folder

		LOGGER.info("Input Bucket created. Domain : " + domainName);

		LOGGER.info("Start to create bucket backup folder for the domain " + domainName);

		createDirIfNotExist(BACKUP + SUFFIX + domainName);

		LOGGER.info("Backup folder created . Domain : " + domainName);

		LOGGER.info("Configuration folder creation started : Domain " + domainName);

		createDirIfNotExist(CONFIGURATION + SUFFIX + domainName);

		LOGGER.info("Configuration folder created : Domain " + domainName);

		Map<String, String> jsonObject = new HashMap<>();
		jsonObject.put("input_path", SUFFIX + getBucketName() + SUFFIX + INPUT_FOLDER + SUFFIX + domainName);
		jsonObject.put("host", getServerEndPoint());
		return jsonObject;

	}

	/**
	 * 
	 * @param domainName
	 * @param configName
	 * @throws Exception
	 */
	public void createInputBucket(String domainName, String configName) {

		String folderPath = domainName + SUFFIX + configName;
		createDirIfNotExist(INPUT_FOLDER + SUFFIX + folderPath);
		createDirIfNotExist(INPUT_FOLDER + SUFFIX + folderPath + SUFFIX + "err");
		createDirIfNotExist(BACKUP + SUFFIX + folderPath);

	}

	private void createDirIfNotExist(String path) {
		try {
			awsServices.createFolder(getBucketName(), path);
		} catch (BucketPathExistException e) {
			LOGGER.warn(e.getLocalizedMessage());
		}
	}

	/**
	 * 
	 * @param domainName
	 * @param configName
	 */
	public void createConfigBucket(String domainName, String configName) {
		createInputBucket(domainName, configName);

		String folderPath = domainName + SUFFIX + configName;
		try {
			awsServices.createFolder(getBucketName(), CONFIGURATION + SUFFIX + folderPath);
		} catch (BucketPathExistException e) {
			LOGGER.warn(e.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public Map<String, String> createAwsTransferUser(String userName) {

		AwsTransferUser user = awsServices.getSFTPUser(getSftpServerId(), userName);
		Map<String, String> jsonObject = new HashMap<>();
		if (null != user) {
			awsServices.deleteSFTPUser(userName, getSftpServerId());
		}

		Map<String, String> sshKeys = awsServices.getSSHKey();
		awsServices.createSFTPUser(new AwsTransferUser().withUserName(userName)
				.withHomeDir(SUFFIX + getBucketName() + SUFFIX + INPUT_FOLDER + SUFFIX + userName)
				.withRoleARN(configCache.getConfigValue(Constants.AWS_SFTP_ROLE_ARN_KEY))
				.withServerId(getSftpServerId())
				.withPolicyJson(configCache.getConfigValue(Constants.AWS_SFTP_USER_POLICY_KEY))
				.withSshPublicKey(sshKeys.get("public")));

		jsonObject.put("sshKey", sshKeys.get("private"));
		jsonObject.put("sshPublicKey", sshKeys.get("public"));
		jsonObject.put("input_path",
				AwsConnectorService.SUFFIX + getBucketName() + SUFFIX + INPUT_FOLDER + SUFFIX + userName);
		jsonObject.put("host", configCache.getConfigValue(Constants.AWS_SFTP_SERVER_END_POINTS_KEY));
		jsonObject.put("user_name", userName);
		return jsonObject;

	}


	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @return
	 */
	public BufferedReader getS3File(String bucketName, String fileKey) {
		return awsServices.getS3File(bucketName, fileKey);
	}

	/**
	 * 
	 * @param queueName
	 * @param message
	 */
	public void sendMessage(String queueName, String message) {
		awsServices.sendMessage(queueName, message);

	}

	/**
	 * 
	 * @return
	 */
	public ConnectionFactory getConnectionFactory() {
		return awsServices.getConnectionFactory();
	}

	/**
	 * 
	 * @param queueName
	 * @param policy
	 */
	public void createQueue(String queueName, String policy) {
		awsServices.createQueue(queueName, policy);

	}

	/**
	 * @return the serverEndPoint
	 */
	private String getServerEndPoint() {
		return configCache.getConfigValue(Constants.AWS_SFTP_SERVER_END_POINTS_KEY);
	}

	/**
	 * 
	 * @param fileKey
	 * @param configpath
	 * @param fileName
	 */
	public void moveFile(String fileKey, String configpath, String fileName) {
		awsServices.moveFile(getBucketName(), fileKey,  configpath +SUFFIX + fileName);

	}

	/**
	 * 
	 * @param bucketName
	 * @param queueArn
	 * @param configName
	 */
	public void addQueueNotification(String bucketName, String queueArn, String configName) {
		awsServices.addQueueNotification(bucketName, queueArn, configName);
	}

	public  String getBucketName() {
		return configCache.getConfigValue(Constants.AWS_BUCKET_NAME_KEY);
	}

	private String getSftpServerId() {
		return configCache.getConfigValue(Constants.AWS_SFTP_SERVER_ID_KEY);
	}

	public String getQueueUrl(String queueName) {
		return awsServices.getQueueUrl(queueName);
		
	}
	
	public void writeFile(String fileName,String text) {
		 awsServices.writeFile(getBucketName(),fileName,text);
	}
	
	/**
	 * 
	 * @param bucketName
	 * @param fileKey
	 * @return
	 */
	public BufferedReader getS3FileRange(String bucketName, String fileName,int start,int end) {
		return awsServices.getS3FileRange(getBucketName(), fileName,start,end);
	}
	
	/**
	 * 
	 * @param bucketName
	 * @param domainName
	 * @param type
	 */
	public List<String> getFiles( String domainName, String type) {
		return awsServices.getFiles(getBucketName(), domainName, type);
	}
	
	
	public String getPresignedUrl(String configName,String entityType) {
		return awsServices.getPresignedUrl(getBucketName(), INPUT_FOLDER +SUFFIX + entityType + SUFFIX + configName +SUFFIX );
	}

	public long getFileSize(String fileKey) {
		return awsServices.getFileInfo(getBucketName(), fileKey);
		
	}

}
