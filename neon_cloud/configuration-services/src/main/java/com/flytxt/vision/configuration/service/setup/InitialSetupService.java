package com.flytxt.vision.configuration.service.setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.aws.entity.UserEntity;
import com.flytxt.vision.configuration.service.ConfigurationService;
import com.flytxt.vision.configuration.service.UserManagementService;
import com.flytxt.vision.core.exception.VisionException;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
/**
 * 
 * @author shiju.john
 *
 */
@Component
public class InitialSetupService {
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private UserManagementService userManagementService;
	
	private JsonParser jsonParser = new JsonParser();	
	private Gson gson = new Gson();
	
	public void execute(String path) {
		
		File currentDir = new File(path); 
		File[] files = currentDir.listFiles();
		if (files.length>0) {
			for (File file : files) {
				if (file.isDirectory()) {
					process(file.getName(),file);
				}
			}
			
		}		
	}
	
	void process(String serviceName,File dir){
		if(dir!=null) {
			switch(serviceName) {			
				case "insert":					
					for(File file : getAllSubDir(dir)) {											
						executeInsertService(file.getName(),file);
					}					
					break;
					
				case "update":
					break;
				case "delete":
					break;
				default:
					break;
			
			}
		}
		
	}
	
	private void executeInsertService(String serviceName,File currentDir) {
		if(currentDir!=null) {
			switch(serviceName) {			
				case "config":			
					for(File file : getAllFiles(currentDir)) {					
						try {
							ConnectorInstance connectorInstance = (ConnectorInstance)this.getObject(file, ConnectorInstance.class);
							configurationService.save(connectorInstance);
						} catch (VisionException | FileNotFoundException e) {
							e.printStackTrace();
						}						
					}					
					break;
					
				case "user":
					for(File file : getAllFiles(currentDir)) {					
						try {
							UserEntity userEntity = (UserEntity)this.getObject(file, UserEntity.class);
							userManagementService.save(userEntity);
						} catch (VisionException | FileNotFoundException e) {
							e.printStackTrace();
						}						
					}	
					break;
			}
		}		
	}
	
	private Object getObject(File file,Class type) throws FileNotFoundException {
		FileReader reader = new FileReader(file.getPath());
		return gson.fromJson(jsonParser.parse(reader).toString(),type);
	}
	
	private File[] getAllFiles(File dir){		
		File [] subDir =  dir.listFiles();		
		return Arrays.stream(subDir).filter(s -> !s.isDirectory()).toArray(File[]::new);
	}
	

	private File[] getAllSubDir(File dir){		
		File [] subDir =  dir.listFiles();		
		return Arrays.stream(subDir).filter(s -> s.isDirectory()).toArray(File[]::new);
	}
	
	
	public static void main(String[] args) {
		new InitialSetupService().execute("/home/fly/git/neon_cloud_backend/neon_cloud/initialsetup");		
	}

}
