package com.flytxt.vision.configuration.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.OfferCodes;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;

/**
 * 
 * @author shiju.john
 *
 */

@Service
public class OfferCodeService extends AbstractService<OfferCodes, Filter>{
	
	
	@Autowired	
	public OfferCodeService(@Qualifier("OfferCodes") VisionDao<OfferCodes> dao) {
		super(dao);
		
	}

	
	@Override
	public Pages<OfferCodes> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<OfferCodes> search(Filter criteria) throws VisionException {
		// TODO Auto-generated method stub		
		return null;
	}
	
	@Override
	public OfferCodes deleteById(Serializable id) throws VisionException {
		OfferCodes connectorInstance = new OfferCodes();
		connectorInstance.setHashKey((String)id);
		return super.delete(connectorInstance );
	}


}
