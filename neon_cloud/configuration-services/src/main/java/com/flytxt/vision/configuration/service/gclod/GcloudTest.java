package com.flytxt.vision.configuration.service.gclod;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.flytxt.vision.core.entity.Pages;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;

//@Component
public class GcloudTest {

	GDriveService driveService;
	ListService listService;
	GSheetService sheetService;
	
	static final String campaign_filename="_campaign_constructs";
//
//	@Autowired
	GcloudTest(ListService listService, GDriveService driveService, GSheetService gSheetService) throws Exception {
		this.driveService = driveService;
		this.sheetService = gSheetService;
		this.listService = listService;

		//delete();
		//createDomain("test_domain");
//		addCampaignConstaructsEntry("test_domain", preapareData());
		getCampaignEntry("test_domain");
		//cokwr
		//update("test_domain","cpzh4");
	}
	
	
	void delete() throws IOException {
		driveService.deleteAll();
	}

	void createDomain(String domainName) {

		try {
			driveService.createDomain(domainName);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}

	}
	
	private Map<String, String> preapareUpdateData() {
		Map<String, String> data = new HashMap<>();
		data.put("campaignName", "updated Name");
		data.put("description", "updated Description");
		data.put("id","cpzh4");
		return data;
		
	}
	
	private Map<String ,String> preapareData( ){		
		Map<String, String> data = new HashMap<>();
		data.put("campaignName", "Test Campaign5");
		data.put("description", "Test Campaign desc5");
		return data;
		
		
	}

	/**
	 * 
	 * @param domainName
	 * @param data
	 * @throws Exception
	 */
	public void addCampaignConstaructsEntry(String domainName,Map<String, String> data) throws Exception {		
		SpreadsheetEntry entry = sheetService.getSpreadsheet(domainName +campaign_filename);
		URL listFeedUrl = entry.getWorksheets().get(0).getListFeedUrl();		
		listService.addNewEntry(listFeedUrl, data);
		
	}
	
	public void getCampaignEntry(String domainName) throws Exception {		
		SpreadsheetEntry entry = sheetService.getSpreadsheet(domainName +campaign_filename);
		URL listFeedUrl = entry.getWorksheets().get(0).getListFeedUrl();	
		Pages<Map<String, String>> pages = new Pages<>();
		pages.setPageNo(3);
		pages.setPageSize(2);
		
		//List<Map<String,String>> result = listService.query(listFeedUrl, "campaignname=\"Test Campaign2\" or campaignname=\"Test Campaign1\"");
		pages = listService.query(listFeedUrl, null,pages);		
		for (Map<String, String> map : pages.getContent()) {
		}
	}
	
	void update(String domainName,String id) throws Exception{
		SpreadsheetEntry entry = sheetService.getSpreadsheet(domainName +campaign_filename);
		URL listFeedUrl = entry.getWorksheets().get(0).getListFeedUrl();	
		Map<String,String> reMap = listService.update(listFeedUrl, "cpzh4",preapareUpdateData());
	}

}
