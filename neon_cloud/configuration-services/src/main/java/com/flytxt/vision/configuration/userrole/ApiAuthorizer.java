package com.flytxt.vision.configuration.userrole;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.flytxt.vision.configuration.cache.UserRoleCache;
import com.flytxt.vision.core.exception.VisionException;

/**
 * 
 * @author shiju.john
 *
 */
@Component
@ConfigurationProperties(prefix = "flytxt")
public class ApiAuthorizer {

	private Map<String, Permission> commonAccessService;

	private Map<String, String> featuremapping;

	private UserRoleCache userRoleCache;
	
	private static final String METHOD_TYPE_GET = "get";
	private static final String METHOD_TYPE_POST = "post";

	@Autowired
	public ApiAuthorizer(UserRoleCache cache) {
		
		commonAccessService = new HashMap<>(4);
		userRoleCache = cache;
		initCommonAccess();
	}

	private void initCommonAccess() {
		commonAccessService.put("user/getUserByName", new Permission(METHOD_TYPE_GET, "ALL"));
		commonAccessService.put("user/hasDomainExist", new Permission(METHOD_TYPE_GET, "ALL"));
		commonAccessService.put("configuration/app_config/get", new Permission(METHOD_TYPE_GET, "ALL"));
		commonAccessService.put("configuration/subscription/get", new Permission(METHOD_TYPE_GET, "OAUTH"));
		commonAccessService.put("configuration/userRole/get", new Permission(METHOD_TYPE_GET, "OAUTH"));

	}

	
	/**
	 * 
	 * @param userRole
	 * @param appId
	 * @param tenantId
	 * @param service
	 * @return
	 * @throws VisionException
	 */
	public boolean hasReadEnabled(String userRole, String appId, String tenantId, String service)
			throws VisionException {
		return "TENANT".equalsIgnoreCase(userRole) ? true
				: hasDeafultAccess(service, METHOD_TYPE_GET) ? true
						: hasServiceAccess(userRole, appId, tenantId, service, METHOD_TYPE_GET);
	}

	/**
	 * 
	 * @param userRole
	 * @param appId
	 * @param tenantId
	 * @param service
	 * @return
	 * @throws VisionException
	 */
	public boolean hasWriteEnabled(String userRole, String appId, String tenantId, String service)
			throws VisionException {
		return "TENANT".equalsIgnoreCase(userRole) ? true
				: hasDeafultAccess(service, METHOD_TYPE_POST) ? true
						: hasServiceAccess(userRole, appId, tenantId, service, METHOD_TYPE_POST);
	}

	@SuppressWarnings("rawtypes")
	private boolean hasServiceAccess(String userRole, String appId, String tenantId, String service, String methodType)
			throws VisionException {

		Map userRoleMap = userRoleCache.getUserRole(tenantId, userRole, appId);
		if (userRoleMap != null) {
			String feature = featuremapping.get(getServiceName(service));
			Map permission = (Map) userRoleMap.get("permission");
			if (permission != null) {
				@SuppressWarnings("unchecked")
				List<String> featurePermission = (List) permission.get(feature);
				if (featurePermission != null && !featurePermission.isEmpty()) {
					return featurePermission.contains("manage") ? true
							: METHOD_TYPE_GET.equalsIgnoreCase(methodType) && featurePermission.contains("read") ? true: false;
				}
			}
			return false;
		}

		throw new VisionException("User Role is not defined " + userRole, null);

	}
	
	/**
	 * 
	 * @param serviceName
	 * @return
	 */
	private String getServiceName(String serviceName){
		return serviceName.replaceAll("/", "_");
	}



	/**
	 * 
	 * @param userRole
	 * @param appId
	 * @param tenantId
	 * @return
	 */
	private boolean hasDeafultAccess(String service, String methodType) {
		Permission permission = commonAccessService.get(service);
		return permission != null ? permission.getMethodType().equalsIgnoreCase(methodType) : false;

	}

	static class Permission {

		private String methodType;
		private String role;

		public Permission(String methodType, String role) {
			this.setMethodType(methodType);
			this.setRole(role);
		}

		/**
		 * @return the methodType
		 */
		public String getMethodType() {
			return methodType;
		}

		/**
		 * @param methodType the methodType to set
		 */
		public void setMethodType(String methodType) {
			this.methodType = methodType;
		}

		/**
		 * @return the role
		 */
		public String getRole() {
			return role;
		}

		/**
		 * @param role the role to set
		 */
		public void setRole(String role) {
			this.role = role;
		}

	}

	public boolean isDefault(String serviceName, String methodType) {
		Permission permission = commonAccessService.get(serviceName);
		return permission != null
				? "ALL".equalsIgnoreCase(permission.getRole()) && hasDeafultAccess(serviceName, methodType) ? true
						: false
				: false;
	}

	/**
	 * @return the featuremapping
	 */
	public Map<String, String> getFeaturemapping() {
		return featuremapping;
	}

	/**
	 * @param featuremapping the featuremapping to set
	 */
	public void setFeaturemapping(Map<String, String> featuremapping) {
		this.featuremapping = featuremapping;
	}

}
