package com.flytxt.vision.configuration.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.TenantUtils;
import com.google.gson.Gson;
/**
 * 
 * @author shiju.john
 *
 */
public enum EntityMetaData {

	products_meta("products_meta","input") {
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {					  
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao, ddlDao);
			return connectorInstance;
		}
				
		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}


	},
	customers_meta("customers_meta","input") {
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao, ddlDao);
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}


	},
	events_meta("events_meta","input") {
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao, ddlDao);
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}

		

	},
	creatives_meta("creatives_meta","input"){
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao, ddlDao);
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}

		
	},
	
	control_group_meta("control_group_meta","config"){
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao);
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}

		
	},
	segments_meta("segments_meta","config"){
		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			validateMeta(connectorInstance,dao);
			saveMeta(connectorInstance, dao);
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}

		
	};
		




	private String key;
	private String group ;
	
	static Gson gson = new Gson();


	/**
	 * 
	 */
	private static final Logger logger = LoggerFactory.getLogger(EntityMetaData.class);

	private EntityMetaData(String key, String group) {
		this.setKey(key);
		this.setGroup(group);
	}
	
	

	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,DDLDao<TableEntity> ddlDao) throws VisionException;
	
	
	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
			VisionDao<ConnectorInstance> dao) throws VisionException;

	public static EntityMetaData getTenantData(String type) {
		try {
			return EntityMetaData.valueOf(type);
		} catch (Exception e) {
			return null;
		}
	}


	private static void saveMeta(ConnectorInstance entity, VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao)
			throws VisionException {	
			ddlDao.createTable(getTableEntityFromMeta(entity.getConnectorConfig(), entity.getTenantId()));
			dao.save(entity);
			
	}
	
	private static void saveMeta(ConnectorInstance entity, VisionDao<ConnectorInstance> dao)
			throws VisionException {	
			dao.save(entity);
			
	}
	
	private static void  validateMeta(ConnectorInstance entity,VisionDao<ConnectorInstance> dao) throws VisionException {
		
		Iterable<ConnectorInstance> productMeta = dao.findBy(entity, null, "byTenantAndTypeAndConnectorName");
		if (productMeta.iterator().hasNext()) {		
			logger.error(
					"Meta Already saved against the Tenant " + TenantUtils.getDomainName(entity.getTenantId()));
			throw new  VisionException("Meta Already Exist",null);
		}		
	}
	
	protected void validateMetaName(ConnectorInstance entity, VisionDao<ConnectorInstance> dao) throws VisionException {
		Iterable<ConnectorInstance> productMeta = dao.findBy(entity, null, "byTenantAndConnectorName");
		if (!productMeta.iterator().hasNext()) {		
			logger.error(
					"Meta Already saved against the Tenant " + TenantUtils.getDomainName(entity.getTenantId()));
			throw new  VisionException("Meta Already Exist",null);
		}	
		
	}

	/**
	 * 
	 * @param metaJson
	 * @param tenantId
	 * @return
	 */
	private static TableEntity getTableEntityFromMeta(String metaJson, String tenantId) {
		TableEntity tableEntity = new TableEntity(); 
		Map<String, Object> map = TenantUtils.parseJson(metaJson);
		tableEntity.setTableName(TenantUtils.getTableName(tenantId, (String) map.get("entityType")));  
		tableEntity.setColumnMappings((List<Map>) map.get("mapping"));
		return tableEntity;

	}



	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}



	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}



	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}



	/**
	 * @param group the group to set
	 */
	private void setGroup(String group) {
		this.group = group;
	}
	
	

	
	
	

}
