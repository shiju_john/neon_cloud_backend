package com.flytxt.vision.configuration.service;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.HelperUtils;
/**
 * 
 * @author shiju.john
 *
 */
public enum ConfigData {

	app_config("app_config") {

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {

			Iterable<ConnectorInstance> data = dao.findBy(connectorInstance, null, "");
			for (Iterator<ConnectorInstance> it = data.iterator(); it.hasNext();) {
				ConnectorInstance instance = it.next();
				if ((HelperUtils.checkNull(instance.getTenantId())
						&& HelperUtils.checkNull(connectorInstance.getTenantId()))
						&& connectorInstance.getConnectorType().equals(instance.getConnectorType())) {
					throw new VisionException("error.appconfig_exist", null);
				}
			}

			return dao.save(connectorInstance);
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			return dao.findBy(connectorInstance, null, "");
		}

	},
	
	subscribed_app("subscribed_app"){

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {

			Iterable<ConnectorInstance> data = dao.findBy(connectorInstance, null, "byTenantAndType");
			for (Iterator<ConnectorInstance> it = data.iterator(); it.hasNext();) {
				ConnectorInstance instance = it.next();
				if(instance.getTenantId().equals(connectorInstance.getTenantId()) && connectorInstance.getConnectorType().equals(instance.getConnectorType())) {
					logger.error("Apps are alreay subscribed . For subscribe more apps please update the existing configuration insed of create new");
					throw new VisionException("error.appconfig_exist", null);
				}
			}
			return dao.save(connectorInstance);
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			return dao.findBy(connectorInstance, null,"byTenantAndType");
		}
		
	},
	
	salesforce("salesforce"){

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {

			Iterable<ConnectorInstance> data = dao.findBy(connectorInstance, null, "byTenantAndTypeAndConnectorName");
			for (Iterator<ConnectorInstance> it = data.iterator(); it.hasNext();) {
				ConnectorInstance instance = it.next();
				if(instance.getTenantId().equals(connectorInstance.getTenantId()) && connectorInstance.getConnectorType().equals(instance.getConnectorType())) {
					logger.error("Configuration name alreay Existing. Please choose different name to save  configutation");
					throw new VisionException("error.config_exist", null);
				}
			}
			return dao.save(connectorInstance);
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			return dao.findBy(connectorInstance, null,"byTenantAndType");
		}
		
	};
	
	

	private String type;

	private static final Logger logger = LoggerFactory.getLogger(ConfigData.class);
	
	/**
	 * 
	 * @param type
	 */
	private ConfigData(String type) {
		this.setType(type);
	}

	
	
	public static ConfigData getTenantData(String type) {
		try {
			return ConfigData.valueOf(type);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
			DDLDao<TableEntity> ddlDao) throws VisionException;

	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
			VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException;
	

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
