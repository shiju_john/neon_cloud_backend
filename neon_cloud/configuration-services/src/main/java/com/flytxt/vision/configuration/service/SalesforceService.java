/**
 * 
 */
package com.flytxt.vision.configuration.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.flytxt.vision.configuration.entity.AuthenticationResponse;
import com.flytxt.vision.configuration.entity.SalesforceUserEntity;

/**
 * @author athul
 *
 */
public class SalesforceService {
	

    private AuthenticationResponse authResponse = new AuthenticationResponse();

	public AuthenticationResponse login(SalesforceUserEntity entity) {
		final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

//        params.add("username", "athul.raj@flytxt.com");
//        params.add("password", "ME1@salesforceJ9U3iUcP8NLdVtu5AqyDBhI4d");
//        params.add("client_id", "3MVG9pe2TCoA1Pf4L6GIu6rPYGxF4O3wwhto244_5aBtZpRjXdTlL3VZk0BtiiQouVLfRpaXGU7nfLKcGEzSS");
//        params.add("client_secret", "1007870128987851376");
        
        params.add("username", entity.getUsername());
        params.add("password", entity.getPassword());
        params.add("client_id", entity.getClientId());
        params.add("client_secret", entity.getClientSecret());
        params.add("grant_type", "password");

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);

        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<AuthenticationResponse> response = restTemplate.postForEntity("https://login.salesforce.com/services/oauth2/token", request, AuthenticationResponse.class);
        authResponse = (AuthenticationResponse) response.getBody();
        return authResponse;
	}
	
	public void getObjects() {
		throw new RuntimeException();
	}
	
	public void getRecords() {
		throw new RuntimeException();
	}
	
	public void getSchema() {
		throw new RuntimeException();
	}
	
	public void logout() {
		throw new RuntimeException();
	}
}
