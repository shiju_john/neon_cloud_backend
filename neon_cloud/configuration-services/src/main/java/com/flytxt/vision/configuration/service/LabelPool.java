package com.flytxt.vision.configuration.service;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;

import com.flytxt.vision.core.config.CustoMessagesBundle;

@Configuration

/**
 * 
 * @author shiju.john
 *
 */

public class LabelPool {
	
	@Autowired
	private MessageSource msgSource;
	
	LabelPool() {

	}
	public Properties getMessages() {
		
		if ( msgSource instanceof CustoMessagesBundle) {
			CustoMessagesBundle bundle = (CustoMessagesBundle) msgSource;
			//return bundle.getPropertiesPersister().getData();
		 return bundle.getProperties(LocaleContextHolder.getLocale());
			
		}
		/*Locale currentLocale = LocaleContextHolder.getLocale();
		Object result = msgSource.getMessage("ui", null, currentLocale);*/
		return null;

	}

	

}
