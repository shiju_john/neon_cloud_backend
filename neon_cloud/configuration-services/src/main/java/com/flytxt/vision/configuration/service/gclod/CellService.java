package com.flytxt.vision.configuration.service.gclod;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.data.spreadsheet.Cell;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;

//@Service
public class CellService {
	
	GSheetService sheetService;
	
//	@Autowired
	public CellService(GSheetService sheetService) {
		this.sheetService = sheetService;
	}
	
	/**
	 * 
	 * @param cellFeedUrl
	 * @param row
	 * @param col
	 * @param input
	 * @throws Exception
	 */
	public void insertCellEntry(URL cellFeedUrl, int row, int col, String input) throws Exception {
		CellEntry newEntry = new CellEntry(row, col, input);
		sheetService.getSheetService().insert(cellFeedUrl, newEntry);

	}
	
	/**
	 * 
	 * @param cellFeedUrl
	 * @return
	 * @throws Exception
	 */
	public List<String> getColumnHeaders(URL cellFeedUrl) throws Exception {

		List<String> headers = new ArrayList<String>();
		// Create a query for the top row of cells only (1-based)
		CellQuery cellQuery = new CellQuery(cellFeedUrl);
		cellQuery.setMaximumRow(1);

		// Get the cell feed matching the query
		CellFeed topRowCellFeed = sheetService.getSheetService().query(cellQuery, CellFeed.class);
		// Get the cell entries fromt he feed
		List<CellEntry> cellEntries = topRowCellFeed.getEntries();
		for (CellEntry entry : cellEntries) {
			// Get the cell element from the entry
			Cell cell = entry.getCell();
			headers.add(cell.getValue());
		}

		return headers;
	}

}
