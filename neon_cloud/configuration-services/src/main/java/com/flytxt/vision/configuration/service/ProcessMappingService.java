package com.flytxt.vision.configuration.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;

@Service
public class ProcessMappingService extends AbstractService<PageMapping, Filter>{
	
	
	@Autowired	
	public ProcessMappingService(@Qualifier("PageMapping") VisionDao<PageMapping> dao) {
		super(dao);
		
	}

	
	@Override
	public Pages<PageMapping> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		return null;
	}

	@Override
	public Iterable<PageMapping> search(Filter criteria) throws VisionException {
		return null;
	}
	
	public Iterable<PageMapping> save(Iterable<PageMapping> mappings) throws VisionException {
		return dao.saveAll(mappings);
	}


	@Override
	public PageMapping deleteById(Serializable id) throws VisionException {
		PageMapping pageMapping = new PageMapping();
		pageMapping.setHashKey((String)id);
		dao.delete(pageMapping);
		return pageMapping;
	}

	public void deleteAll(List<PageMapping> pageMappings) throws VisionException {
		dao.deleteAll(pageMappings);		
	}

	/**
	 * 
	 * @param pageMapping
	 * @return
	 * @throws VisionException 
	 */
	public Iterable<PageMapping> findByPageId(PageMapping pageMapping) throws VisionException {
		return dao.findBy(pageMapping,null,"byPageId");		
	}


	public Iterable<PageMapping> findByProcessId(PageMapping pageMapping) throws VisionException {
		 return dao.findBy(pageMapping,null,"byProcessId");	
	}
	
}
