package com.flytxt.vision.configuration.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flytxt.aws.entity.TableEntity;
import com.flytxt.vision.aws.entity.ConnectorInstance;
import com.flytxt.vision.core.dao.DDLDao;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.utils.TenantUtils;
import com.google.gson.Gson;
/**
 * 
 * @author shiju.john
 *
 */
public enum CustomData {

	
	products("products") {

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			saveData(connectorInstance, dao, ddlDao, EntityMetaData.products_meta.getKey());
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			
			TableEntity entities = ddlDao
					.findAll(TenantUtils.getTableName(connectorInstance.getTenantId(), products.getType()));
			return convertoConnectorInstance(entities,connectorInstance,products.getType());

		}

		@Override
		public void save(TableEntity entities,  VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddldao) throws VisionException {
			ddldao.save(entities);
			
		}		

	},
	
	events("events") {

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			saveData(connectorInstance, dao, ddlDao, EntityMetaData.events_meta.getKey());
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			
			TableEntity entities = ddlDao
					.findAll(TenantUtils.getTableName(connectorInstance.getTenantId(), products.getType()));
			return convertoConnectorInstance(entities,connectorInstance,products.getType());

		}

		@Override
		public void save(TableEntity entities,  VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddldao) throws VisionException {
			ddldao.save(entities);
			
		}		

	},
	
	creatives("creatives") {


		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			saveData(connectorInstance, dao, ddlDao, EntityMetaData.creatives_meta.getKey());
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			
			TableEntity entities = ddlDao
					.findAll(TenantUtils.getTableName(connectorInstance.getTenantId(), products.getType()));
			return convertoConnectorInstance(entities,connectorInstance,products.getType());

		}

		@Override
		public void save(TableEntity entities,  VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddldao) throws VisionException {
			ddldao.save(entities);
			
		}		

	},
	
	customers("customers") {

		@Override
		public ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddlDao) throws VisionException {
			saveData(connectorInstance, dao, ddlDao, EntityMetaData.customers_meta.getKey());
			return connectorInstance;
		}

		@Override
		public Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
				VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException {
			TableEntity entities = ddlDao
					.findAll(TenantUtils.getTableName(connectorInstance.getTenantId(),customers.getType() ));
			return convertoConnectorInstance(entities,connectorInstance,customers.getType());
		}

		@Override
		public void save(TableEntity entities,  VisionDao<ConnectorInstance> dao,
				DDLDao<TableEntity> ddldao) throws VisionException {			
			ddldao.save(entities);			
			
		}

		
	};



	private String type;
	
	static Gson gson = new Gson();


	/**
	 * 
	 */
	private static final Logger logger = LoggerFactory.getLogger(CustomData.class);

	private CustomData(String type) {
		this.setType(type);
	}
	
	

	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract ConnectorInstance save(ConnectorInstance connectorInstance, VisionDao<ConnectorInstance> dao,
			DDLDao<TableEntity> ddlDao) throws VisionException;
	
	public abstract void save(TableEntity records, VisionDao<ConnectorInstance> dao,
			DDLDao<TableEntity> ddldao) throws VisionException ;
	/**
	 * 
	 * @param connectorInstance
	 * @param dao
	 * @param ddlDao
	 * @return
	 * @throws VisionException
	 */
	public abstract Iterable<ConnectorInstance> findAll(ConnectorInstance connectorInstance,
			VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao) throws VisionException;

	public static CustomData getTenantData(String type) {
		try {
			return CustomData.valueOf(type);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param entity
	 * @param dao
	 * @param ddlDao
	 * @param meta_Type
	 * @throws VisionException
	 */
	private static void saveData(ConnectorInstance entity, VisionDao<ConnectorInstance> dao, DDLDao<TableEntity> ddlDao,
			String meta_Type) throws VisionException {

		ConnectorInstance connectorInstance = new ConnectorInstance();
		connectorInstance.setTenantId(entity.getTenantId());
		connectorInstance.setConnectorType(meta_Type);
		Iterable<ConnectorInstance> productMeta = dao.findBy(connectorInstance, null, "byTenantAndType");
		if (productMeta.iterator().hasNext()) {		
			TableEntity tableEntity = getTableEntityFromMeta(productMeta.iterator().next().getConnectorConfig(),
					entity.getTenantId());
			Map<String, Object> map = TenantUtils.parseJson(entity.getConnectorConfig());
			String entityType = (String) map.get("entityType");
			tableEntity.setTableName(TenantUtils.getTableName(entity.getTenantId(), entityType));
			for (Map data : (List<Map>) map.get(entityType)) {				
				tableEntity.addRow(data);
			}			
			execute((String)map.get("updateMethod"),ddlDao,tableEntity);

		} else {

			logger.warn("Product Meta configuration not found against the Tenant "
					+ TenantUtils.getDomainName(entity.getTenantId()));
			throw new VisionException("error.product_meta_not_found", null);
		}

	}

	
	protected void validateMetaName(ConnectorInstance entity, VisionDao<ConnectorInstance> dao) throws VisionException {
		Iterable<ConnectorInstance> productMeta = dao.findBy(entity, null, "byTenantAndConnectorName");
		if (!productMeta.iterator().hasNext()) {		
			logger.error(
					"Meta Already saved against the Tenant " + TenantUtils.getDomainName(entity.getTenantId()));
			throw new  VisionException("Meta Already Exist",null);
		}	
		
	}

	/**
	 * 
	 * @param metaJson
	 * @param tenantId
	 * @return
	 */
	private static TableEntity getTableEntityFromMeta(String metaJson, String tenantId) {
		TableEntity tableEntity = new TableEntity(); 
		Map<String, Object> map = TenantUtils.parseJson(metaJson);
		tableEntity.setTableName(TenantUtils.getTableName(tenantId, (String) map.get("entityType")));  
		tableEntity.setColumnMappings((List<Map>) map.get("mapping"));
		return tableEntity;

	}
	
	/**
	 * 
	 * @param entities
	 * @param connectorInstance
	 * @return
	 */
	private static Iterable<ConnectorInstance> convertoConnectorInstance(TableEntity entity,
			ConnectorInstance connectorInstance,String type) {
		List<ConnectorInstance> result = new ArrayList<>();
		ConnectorInstance dataInstance = new ConnectorInstance();
		dataInstance.setTenantId(connectorInstance.getTenantId());
		dataInstance.setConnectorType(connectorInstance.getConnectorType());		
		dataInstance.setConnectorConfig(gson.toJson(entity.getColumnValues()));	
		result.add(dataInstance);		
		return result;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 
	 * @param type
	 * @param ddlDao
	 * @param listEntity
	 * @throws VisionException
	 */
	private static void execute(String type, DDLDao<TableEntity> ddlDao, TableEntity listEntity) throws VisionException {
		switch(type) {
			case "add":
				ddlDao.save(listEntity);	
				break;
			case "update":
				ddlDao.update(listEntity);
			default : 
				logger.error(type + " not implemented");
		}
	}

	

}
