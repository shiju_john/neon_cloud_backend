package com.flytxt.vision.configuration.service.gclod;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.flytxt.vision.core.entity.Pages;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.util.ServiceException;

//@Service
//@Scope("singleton")
public class ListService {

	private GSheetService sheetService;

//	@Autowired
	public ListService(GSheetService sheetService) {
		this.sheetService = sheetService;
	}

	/**
	 * 
	 * @param listFeedUrl
	 * @param entry
	 * @throws ServiceException
	 * @throws IOException
	 */
	public ListEntry addNewEntry(URL listFeedUrl, Map<String, String> entry) throws ServiceException, IOException {
		ListEntry newEntry = new ListEntry();
		newEntry.setVersionId("0");
		setEntryContentsFromMap(newEntry, entry);
		return sheetService.getSheetService().insert(listFeedUrl, newEntry);
	}

	/**
	 * 
	 * @param listFeedUrl
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	private ListEntry getEntryById(URL listFeedUrl, String id) throws ServiceException {
		try {
			String baseurl = listFeedUrl.toString() + "/" + id;
			return sheetService.getSheetService().getEntry(new URL(baseurl), ListEntry.class);
		} catch (IOException | ServiceException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @param nameValuePairs
	 * @throws IOException
	 * @throws ServiceException
	 */
	public Map<String, String> update(URL listFeedUrl, String id, Map<String, String> nameValuePairs)
			throws IOException, ServiceException {

		ListEntry entry = this.getEntryById(listFeedUrl, id);
		if (entry != null) {
			setEntryContentsFromMap(entry, nameValuePairs);
			entry = entry.update();
		} else {
			entry = addNewEntry(listFeedUrl, nameValuePairs);
		}
		return getNamePair(entry);

	}

	/**
	 * Performs a full database-like query on the rows.
	 * 
	 * @param structuredQuery a query like: name = "Bob" and phone != "555-1212"
	 * @throws ServiceException when the request causes an error in the Google
	 *                          Spreadsheets service.
	 * @throws IOException      when an error occurs in communication with the
	 *                          Google Spreadsheets service.
	 */
	public List<Map<String, String>> query(URL listFeedUrl, String structuredQuery)
			throws IOException, ServiceException {
		ListQuery query = new ListQuery(listFeedUrl);
		query.setSpreadsheetQuery(structuredQuery);

		ListFeed feed = sheetService.getSheetService().query(query, ListFeed.class);
		List<Map<String, String>> result = new ArrayList<>();

		for (ListEntry entry : feed.getEntries()) {

			result.add(getNamePair(entry));
		}

		return result;
	}

	/**
	 * 
	 * @param listFeedUrl
	 * @param structuredQuery
	 * @param page
	 * @return
	 * @throws IOException
	 * @throws ServiceException
	 */
	public Pages<Map<String, String>> query(URL listFeedUrl, String structuredQuery, Pages<Map<String, String>> page)
			throws IOException, ServiceException {
		ListQuery query = new ListQuery(listFeedUrl);
		query.setSpreadsheetQuery(structuredQuery);
		query.setStartIndex(page.getPageNo() != 1 ? ((page.getPageNo() - 1) * page.getPageSize()) + 1 : 1);
		query.setMaxResults(page.getPageSize());
		ListFeed feed = sheetService.getSheetService().query(query, ListFeed.class);
		List<Map<String, String>> result = new ArrayList<>();
		page.setHasNext(feed.getNextLink() != null ? true : false);
		page.setHasPrevious(feed.getPreviousLink() != null ? true : false);
		for (ListEntry entry : feed.getEntries()) {
			result.add(getNamePair(entry));
		}
		page.setContent(result);
		return page;
	}

	/**
	 * 
	 * @param entry
	 * @return
	 */

	private Map<String, String> getNamePair(ListEntry entry) {
		Map<String, String> namePair = new HashMap<>();
		if (entry != null) {
			String id = entry.getId().substring(entry.getId().lastIndexOf('/') + 1);
			namePair.put("id", id);
			for (String tag : entry.getCustomElements().getTags()) {
				namePair.put(tag, entry.getCustomElements().getValue(tag));

			}
		}
		return namePair;
	}

	/**
	 * 
	 * @param entryToChange
	 * @param nameValuePairs
	 */
	private void setEntryContentsFromMap(ListEntry entryToChange, Map<String, String> nameValuePairs) {
		for (Entry<String, String> entry : nameValuePairs.entrySet()) {
			entryToChange.getCustomElements().setValueLocal(entry.getKey(), entry.getValue());
		}
	}

}
