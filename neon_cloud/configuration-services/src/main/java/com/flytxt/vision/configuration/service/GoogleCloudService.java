package com.flytxt.vision.configuration.service;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.Drive.Files.Create;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gdata.client.Query.CustomParameter;
import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.client.spreadsheet.WorksheetQuery;
import com.google.gdata.data.spreadsheet.Cell;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.Field;
import com.google.gdata.data.spreadsheet.RecordEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.TableEntry;
import com.google.gdata.data.spreadsheet.TableFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;

//@Service
//@Scope("singleton")
public class GoogleCloudService {

	
	private HttpTransport httpTransport;
	private JacksonFactory jsonFactory;
	private GoogleCredential credential;
	private Drive drive;
	private SpreadsheetService sheetService ;
	private FeedURLFactory factory;
	URL tablesFeedUrl;
	Sheets sheet;
	
	
	public GoogleCloudService() throws Exception{
		 httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		 jsonFactory = new JacksonFactory();
		 credential= getCredential();
		 drive =  getDriveService();
		 sheetService  = getSpredSheetService();
		 factory = FeedURLFactory.getDefault();
		 
		
		 
		 //createDomain("axis.com");
		 //getCampainConstructs();
		// insertCellEntry("campaign_constructs","campaign_constructs","shiju");
		 test();
		 
	}
	
	
	void test() throws Exception{
		SpreadsheetEntry  spreadsheet = getSpreadsheet("campaign_constructs");
//		write(spreadsheet.getKey());
		WorksheetEntry worksheet  = getWorksheet("campaign_constructs","campaign_constructs");
		getColumnHeaders(worksheet); 
		
		/*
		WorksheetEntry worksheet  = getWorksheet("campaign_constructs","campaign_constructs");
		List <String> headers =  getColumnHeaders(worksheet);
		
	
		
       
        for (String header : headers) {
        }*/
		
		
		
        URL spreadsheetUrl = new java.net.URL(
                spreadsheet.getSpreadsheetLink().getHref());
            String baseUrl = new java.net.URL(spreadsheetUrl.getProtocol() + "://"
                + spreadsheetUrl.getHost()).toString();
//            tablesFeedUrl = new java.net.URL("https://www.googleapis.com/auth/spreadsheets"+ "/feeds/" + spreadsheet.getKey()
//                + "/tables");
            
            tablesFeedUrl =  factory.getTableFeedUrl(spreadsheet.getKey());
//            tablesFeedUrl  = new URL("https://spreadsheets.google.com/feeds/"+spreadsheet.getKey()+ "/tables");
            listAllTableEntries();
            
           
          
	}
	
	
	void write(String sheetId) throws IOException{
		
		ValueRange body = new ValueRange()
			      .setValues(Arrays.asList(
			        Arrays.asList("Expenses January"), 
			        Arrays.asList("books", "30"), 
			        Arrays.asList("pens", "10"),
			        Arrays.asList("Expenses February"), 
			        Arrays.asList("clothes", "20"),
			        Arrays.asList("shoes", "5")));
			    AppendValuesResponse result = sheet.spreadsheets().values()
			      .append(sheetId, "A1",  body)
			      .setValueInputOption("RAW")
			      .execute();
		
	}
	
	 public void listAllTableEntries() throws IOException, ServiceException {
		    TableFeed feed = sheetService.getFeed(tablesFeedUrl, TableFeed.class);

		    for (TableEntry entry : feed.getEntries()) {
		    	
//		    	  String id = entry.getId().substring(entry.getId().lastIndexOf('/') + 1);
		    
//		      printAndCacheEntry(entry);
		    }
		    if (feed.getEntries().size() == 0) {
		    }
		  }
	 
	


	private GoogleCredential getCredential() throws GeneralSecurityException, IOException, URISyntaxException{			    
	    return new GoogleCredential.Builder()
	     .setTransport(httpTransport)
	     .setJsonFactory(jsonFactory) 
	     .setServiceAccountId("neon-service@testapp-232908.iam.gserviceaccount.com")
	     .setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE))
	     .setServiceAccountPrivateKeyFromP12File(new java.io.File
	    		 (GoogleCloudService.class.getClassLoader().getResource("testapp-232908-9650364d79d7.p12").toURI()))
	     .build();
		
	}
	
	private Drive getDriveService() throws GeneralSecurityException, IOException, URISyntaxException{    
		return  new Drive.Builder(httpTransport, jsonFactory,credential)
         .setApplicationName("neon_cloud").build();
	}
	
	private SpreadsheetService getSpredSheetService() {
		SpreadsheetService service = new SpreadsheetService("neon_cloud");
		service.setOAuth2Credentials(credential);	
		sheet =  new Sheets.Builder(httpTransport, jsonFactory, credential)
		        .setApplicationName("Google-SheetsSample/0.1")
		        .build();
		return service;		
	}
	
	
	public SpreadsheetEntry getSpreadsheet(String spreadsheet) throws Exception {

		SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(factory.getSpreadsheetsFeedUrl());
		spreadsheetQuery.setTitleQuery(spreadsheet);
		SpreadsheetFeed spreadsheetFeed = sheetService.query(spreadsheetQuery, SpreadsheetFeed.class);
		List<SpreadsheetEntry> spreadsheets = spreadsheetFeed.getEntries();
		if (spreadsheets.isEmpty()) {
			throw new Exception("No spreadsheets with that name");
		}

		return spreadsheets.get(0);
	}
	 
	 
	/**
	 * 
	 * @param spreadsheet
	 * @param worksheet
	 * @return
	 * @throws Exception
	 */
	public WorksheetEntry getWorksheet(String spreadsheet, String worksheet) throws Exception {

		SpreadsheetEntry spreadsheetEntry = getSpreadsheet(spreadsheet);

		WorksheetQuery worksheetQuery = new WorksheetQuery(spreadsheetEntry.getWorksheetFeedUrl());

		worksheetQuery.setTitleQuery(worksheet);
		WorksheetFeed worksheetFeed = sheetService.query(worksheetQuery, WorksheetFeed.class);
		List<WorksheetEntry> worksheets = worksheetFeed.getEntries();
		if (worksheets.isEmpty()) {
			throw new Exception(
					"No worksheets with that name in spreadhsheet " + spreadsheetEntry.getTitle().getPlainText());
		}

		return worksheets.get(0);
	}
	 
	 
	public void insertCellEntry(String spreadsheet, String worksheet, int row, int col, String input) throws Exception {

		WorksheetEntry worksheetEntry = getWorksheet(spreadsheet, worksheet);
		URL cellFeedUrl = worksheetEntry.getCellFeedUrl();
		CellEntry newEntry = new CellEntry(row, col, input);
		sheetService.insert(cellFeedUrl, newEntry);


	}
	 
	 /**
	   * Retrieves the columns headers from the cell feed of the worksheet
	   * entry.
	   *
	   * @param worksheet worksheet entry containing the cell feed in question
	   * @return a list of column headers
	   * @throws Exception if error in retrieving the spreadsheet information
	   */
	public List<String> getColumnHeaders(WorksheetEntry worksheet) throws Exception {

		List<String> headers = new ArrayList<String>();

		// Get the appropriate URL for a cell feed
		URL cellFeedUrl = worksheet.getCellFeedUrl();
		
		URL listfeed = worksheet.getListFeedUrl();
		
//		ListQuery listQuery = new ListQuery(feedUrl)
		
//		sheetService.query(Query, ListFeed.class);
		

		// Create a query for the top row of cells only (1-based)
		CellQuery cellQuery = new CellQuery(cellFeedUrl);
		cellQuery.setMaximumRow(2);	
		cellQuery.setIntegerCustomParameter("description", 20);
			
		cellQuery.addCustomParameter(new CustomParameter("description", "20"));
		cellQuery.equals(true);
		//cellQuery.setFullTextQuery(query);
		
		

		// Get the cell feed matching the query
		CellFeed topRowCellFeed = sheetService.query(cellQuery, CellFeed.class);

		// Get the cell entries fromt he feed
		List<CellEntry> cellEntries = topRowCellFeed.getEntries();
		for (CellEntry entry : cellEntries) {

			// Get the cell element from the entry
			Cell cell = entry.getCell();
			headers.add(cell.getValue());
		}

		return headers;
	}


	
	
	
	/**
	 * 
	 * @param domainName
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public void  createDomain(String domainName) throws IOException, URISyntaxException{		
	String dirId = getDirectoryId(domainName);
	  if (null==dirId) {			  
		  com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		  fileMetadata.setName(domainName);
		  fileMetadata.setMimeType("application/vnd.google-apps.folder");
		  
		  com.google.api.services.drive.model.File file = drive.files().create(fileMetadata)
		    .setFields("id")
		    .execute();	
		  dirId = file.getId();
	  }
	  uploadTemplates(domainName,dirId);
		  
	}
	
	/**
	 * 
	 * @param domainName
	 * @return
	 */
	private String getDirectoryId(String domainName) {
		try {
			boolean exist=false;
			Files.List request = drive.files().list().setQ(
				       "mimeType='application/vnd.google-apps.folder' and trashed=false and name='"+domainName+"'");
			File domainDir = null;
			  do {
				  	FileList files = request.execute();
				  	for (File directory : files.getFiles()) {
				  		if(domainName.equals(directory.getName()) || domainName.equals(directory.getOriginalFilename())) {
				  			domainDir = directory;
				  			exist=true;
				  			break;
				  		}			  		
				  	}		      
			        request.setPageToken(files.getNextPageToken());
			    } while (request.getPageToken() != null &&
			             request.getPageToken().length() > 0 && !exist);
			
			
			return null!=domainDir ? domainDir.getId() : null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws IOException
	 */
	private  List<File> filesBasedOnCriteria(String criteria) throws IOException{
		 List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
		 Files.List request = drive.files().list();
		 if (criteria!=null) {
			 request = request.setQ(criteria);
		 }	
	    do {
	        FileList files = request.execute();
	        result.addAll(files.getFiles());
	        request.setPageToken(files.getNextPageToken());
	    } while (request.getPageToken() != null &&
	             request.getPageToken().length() > 0);
	     return result;
	}
	
	
	/**
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void uploadTemplates(String domainName,String parent) throws IOException, URISyntaxException {
		
		File fileMetadata = new File();
		fileMetadata.setName("campaign_constructs");
		fileMetadata.setOriginalFilename("campaign_constructs");
		fileMetadata.setParents(Collections.singletonList(parent));
		fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");
		
		java.io.File filePath = new java.io.File(this.getClass().getClassLoader().getResource("templates/campaign_constructs.csv").toURI());
		FileContent mediaContent = new FileContent("text/csv", filePath);
		

		
		File file = drive.files().create(fileMetadata, mediaContent)
		    .setFields("id")
		    .execute();
		createPermissionForEmail(file.getId(), "shiju.john@flytxt.com");
	}
	

	private Permission createPermissionForEmail(String googleFileId, String googleEmail) throws IOException {
		// All values: user - group - domain - anyone
		String permissionType = "user"; // Valid: user, group
		// organizer - owner - writer - commenter - reader
		String permissionRole = "writer";

		Permission newPermission = new Permission();
		newPermission.setType(permissionType);
		newPermission.setRole(permissionRole);
		newPermission.setEmailAddress(googleEmail);
		return drive.permissions().create(googleFileId, newPermission).execute();
	}
	
	public void getCampainConstructs(){
		URL SPREADSHEET_FEED_URL;
		try {
			SPREADSHEET_FEED_URL = new URL(
			        "https://spreadsheets.google.com/feeds/spreadsheets/private/full");
			
			 SpreadsheetFeed feed = sheetService.getFeed(SPREADSHEET_FEED_URL,
				        SpreadsheetFeed.class);
			 List<SpreadsheetEntry> spreadsheets = feed.getEntries();
			 for(SpreadsheetEntry entry : spreadsheets) {
				
				 
				 //sheetService.inser
				 
				 WorksheetFeed worksheetFeed = sheetService.getFeed(new URL(entry.getId()),WorksheetFeed.class);
				for(WorksheetEntry worksheet : worksheetFeed.getEntries()){
					
					 int rowCount = worksheet.getRowCount();
				      int colCount = worksheet.getColCount();
				    
					
				}
//				 sheetService.delete(new URL(entry.getId()));
				 
				// drive.files().delete(entry.getKey()).execute();
			 }
		} catch ( IOException | ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	public void insert() throws MalformedURLException, IOException, ServiceException {
		
		RecordEntry newEntry = new RecordEntry();
		newEntry.addField(new Field(null,"campaignName","TEST"));

		//
		
		sheetService.insert(new URL("https://spreadsheets.google.com/feeds/spreadsheets/12JYd8c3knlWjtDo-R5nGuXVfljag2orEaKFRqj4a1u4"), 
				newEntry);
	}
	
	 
	
/*	public void getSheetService() throws GeneralSecurityException, IOException, URISyntaxException, ServiceException {	
		
		
		
		URL SPREADSHEET_FEED_URL = new URL(
		        "https://spreadsheets.google.com/feeds/spreadsheets/private/full");

		    // Make a request to the API and get all spreadsheets.
		    SpreadsheetFeed feed = service.getFeed(SPREADSHEET_FEED_URL,
		        SpreadsheetFeed.class);
		    List<SpreadsheetEntry> spreadsheets = feed.getEntries();
		    
		    SpreadsheetEntry spreadsheet = spreadsheets.get(0);
		  
		    
		    //service.set
		    
		    
		
		
	}
	*/

	public  void main() throws IOException, GeneralSecurityException, URISyntaxException, ServiceException{
//        Drive service = getDriveService();
//        
//        String id = createSpreadSheet("icici_campaign_constaints", "icici_Campaign_Constaints.csv", "icicibank.com", service);
//        createPermissionForEmail(id,"shiju.john@flytxt.com");
//        File file = getFileById(service,id);
//        
//        file = getFileParentsDetails(service,id);
//        
//        List<File> files = listFilesBasedOnCriteria(service,"name='icici_campaign_constaints'");
//        
//        
        
        
//		getSheetService(); 

        
	}
	
	
	
	 
	/*static void writeFile() throws GeneralSecurityException, IOException, URISyntaxException{
		
		 Drive drive = getDriveService();
//		  com.google.api.services.drive.model.File  file = new com.google.api.services.drive.model.File();
//          file.setName("axis_Campaign_shiju");
//          file.setMimeType("application/vnd.google-apps.spreadsheet");
//          file.setParents(Collections.singletonList(createFolder("axisbank.com", drive)));
//          
//          file.setOriginalFilename("axis_Camapin_shiju.csv");
		 

	
          
       
          try {
//        	  com.google.api.services.drive.model.File  file = new com.google.api.services.drive.model.File()
        			  
//        	  Create create = drive.files().create(file).setFields("id, parents");
//              file = create.execute();
              //List<com.google.api.services.drive.model.File> result  = retrieveAllFiles(drive);
              //1wj5MrNUZ5aBKOt7UIyaQevEDdw_hGPApCOMVEHxse8U
              
              com.google.api.services.drive.model.File uploadedDoc = drive.files().get("1wj5MrNUZ5aBKOt7UIyaQevEDdw_hGPApCOMVEHxse8U").setFields("parents").setFields("id").execute();
              createPermissionForEmail(uploadedDoc.getId(),"shiju.john@flytxt.com");


          } catch (Exception e) {
              e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
          }

          return;       
	}*/
	
	
	private static File getFileById(Drive driveService,String fileId) throws IOException{		
		 com.google.api.services.drive.model.File uploadedDoc = driveService.files().get(fileId).execute();
		 return uploadedDoc;
	}
	
	private static File getFileParentsDetails(Drive driveService,String fileId) throws IOException {
		 com.google.api.services.drive.model.File uploadedDoc = driveService.files().get(fileId).setFields("parents").setFields("id").execute();
		 return uploadedDoc;		
	}
	
	private static String  createSpreadSheet(String fileName,String originalFileName,String parentDir,Drive drive) throws IOException, GeneralSecurityException, URISyntaxException{
		
//		Drive drive = getDriveService();
		com.google.api.services.drive.model.File  file = new com.google.api.services.drive.model.File();
        file.setName(fileName);
        file.setMimeType("application/vnd.google-apps.spreadsheet");
      //  file = parentDir!=null ? file.setParents(Collections.singletonList(createFolder(parentDir, drive))):file;         
        file.setOriginalFilename(originalFileName);
        Create create = drive.files().create(file).setFields("id, parents");
        file = create.execute();
        return file.getId();
		
	}
	
	

	
	private static List<com.google.api.services.drive.model.File> retrieveAllFiles(Drive service) throws IOException {
	    List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
	    Files.List request = service.files().list().setQ("name='axis_Campaign_shiju'");

	    do {
	        FileList files = request.execute();

	        result.addAll(files.getFiles());
	        request.setPageToken(files.getNextPageToken());
	    } while (request.getPageToken() != null &&
	             request.getPageToken().length() > 0);
	     return result;
	  }	
	
}
