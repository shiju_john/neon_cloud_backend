package com.flytxt.vision.configuration.entity;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author shiju.john
 *
 */
public class FormDataWithFile  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String fileMeta;
	private String fileType;	
    private MultipartFile file;
	
    
    /**
	 * @return the fileMeta
	 */
	public String getFileMeta() {
		return fileMeta;
	}
	/**
	 * @param fileMeta the fileMeta to set
	 */
	public void setFileMeta(String fileMeta) {
		this.fileMeta = fileMeta;
	}
	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
