package com.flytxt.vision.processflow;

import java.util.HashMap;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.flytxt.vision.aws.config.DataStoreBeanImpl;
import com.flytxt.vision.aws.config.DynamoDBConfig;
import com.flytxt.vision.aws.dao.ProcessDao;
import com.flytxt.vision.aws.repository.ProcessRepository;
import com.flytxt.vision.processflow.config.ProcessFlowBeanConfig;
import com.flytxt.vision.processflow.services.ProcessServices;
import com.flytxt.vision.processruntime.BeanConfiguration;
import com.flytxt.vision.processruntime.exception.ProcessException;
import com.flytxt.vision.processruntime.runtime.impl.ProcessRuntimeImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes= {DataStoreBeanImpl.class,BeanConfiguration.class,ProcessRepository.class,DynamoDBConfig.class,
//		ProcessFlowBeanConfig.class,ProcessRuntimeImpl.class,ProcessDao.class,ProcessServices.class})
//@ComponentScan(basePackages = "com.flytxt.vision")
//@ActiveProfiles("aws")
public class AppTest extends TestCase
{
	
//	@Autowired
//	ProcessFlow processFlow;
//  
    
    public AppTest(  ) {
       
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    @org.junit.Test
    public void testApp()
    {
//    	
//    	try {
////			processFlow.startProcess("2", "2", new HashMap<String, Object>());
//		} catch (ProcessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        assertTrue( true );
    }
}
