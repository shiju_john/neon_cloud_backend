package com.flytxt.vision.processflow.services;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class ProcessServices extends AbstractService<ProcessFlowEntity, Filter>{

	@Autowired
	public ProcessServices(@Qualifier("ProcessFlowEntity")  VisionDao<ProcessFlowEntity> dao) {
		super(dao);		
	}

	@Override
	public Pages<ProcessFlowEntity> search(Filter criteria, int pageNo, int pageSize, String sortField,
			String sortOrder) throws VisionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<ProcessFlowEntity> search(Filter criteria) throws VisionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProcessFlowEntity deleteById(Serializable id) throws VisionException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ProcessFlowEntity findByProcessIdAndProcessVersionAndStatus(ProcessFlowEntity config) throws VisionException{
		Iterable< ProcessFlowEntity> result = dao.findBy(config, null, new String[0]);		
		if(null==config.getProcessVersion() || config.getProcessVersion().length()<1) {
			double version = -1;
			ProcessFlowEntity resultEntity = null;
			for(Iterator<ProcessFlowEntity> iterator = result.iterator();iterator.hasNext();) {
				ProcessFlowEntity flowEntity = iterator.next();
				double processVersion = Double.valueOf(flowEntity.getProcessVersion());
				if(processVersion>version) {
					resultEntity = flowEntity;
					version=processVersion; 
				}
			}
			return resultEntity;
		}
		if (null!=result && result.iterator().hasNext()) {
			return result.iterator().next();
		}
		return null;				
	}
	
	/**
	 * Method will return all the latest Process definition with the given status 
	 * @param config
	 * @return
	 * @throws VisionException 
	 */
	public Map<String,ProcessFlowEntity> findByStatus(ProcessFlowEntity config) throws VisionException{
		
		Iterable< ProcessFlowEntity> result = dao.findBy(config, null,"status");
		Map<String,ProcessFlowEntity>  processMap = new HashMap<>();
		if (null!=result ) {
			Iterator<ProcessFlowEntity> processIterator = result.iterator();
			while(processIterator.hasNext()) {
				
				ProcessFlowEntity entity = processIterator.next();
				String processId = entity.getProcessId().intern();
				if (processMap.containsKey(processId)) {
					if (Double.valueOf(processMap.get(processId).getProcessVersion()) > Double
							.valueOf(entity.getProcessVersion())) {
						 processMap.put(processId, entity);
					}
				}else {
				  processMap.put(processId, entity);
				}

			}		
			
		}
		return null;				
	}
	
	
}
