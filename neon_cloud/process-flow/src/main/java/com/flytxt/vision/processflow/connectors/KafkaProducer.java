package com.flytxt.vision.processflow.connectors;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.processflow.services.SettingsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("singleton")
public class KafkaProducer {
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

	private Map<String, Object> kafkaConfig;
	private Producer<Long, String> producer;
	private static String TOPIC_NAME = "";

	@Autowired
	public KafkaProducer(SettingsService service) throws VisionException {
		kafkaConfig = this.getConfiguration(service);
		producer = createProducer();
		TOPIC_NAME = (String) kafkaConfig.get("TOPIC_NAME");

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Producer<Long, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.get("KAFKA_BROKERS"));
		props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaConfig.get("CLIENT_ID"));
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return new org.apache.kafka.clients.producer.KafkaProducer(props);
	}

	/**
	 * 
	 * @param service
	 * @return
	 * @throws VisionException 
	 */
	private Map<String, Object> getConfiguration(SettingsService service) throws VisionException {
		Settings config = new Settings();
		config.setKey("KAFKA_SETTINGS");
		Iterable<Settings> result = service.findByKey(config, null);
		if (result.iterator() != null && result.iterator().hasNext()) {
			Gson gson = new Gson();
			return gson.fromJson(result.iterator().next().getValue(), new TypeToken<Map<String, Object>>() {
			}.getType());
		}
		return new HashMap<>();
	}

	/**
	 * 
	 * @param message
	 */
	public void send(String message) {
		ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(TOPIC_NAME, message);
		try {
			RecordMetadata metaData= producer.send(record).get();
			logger.debug(message+" message produce to the partition "+metaData.partition() +" with offeset "+metaData.offset());
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage(),e);
		}
	}

}
