package com.flytxt.vision.processflow.connectors;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.exception.TaskException;
import com.flytxt.vision.processruntime.runtime.TaskExecutor;

/**
 * 
 * @author shiju.john
 *
 */

@Component
public class IncidentTrackerExecutor implements TaskExecutor {

	WebserviceTemplate template =  new WebserviceTemplate();
	@Override
	public Map<String, Object> execute(TaskInstance instance, Map<String, Object> processVariables)
			throws TaskException {
		return template.execute(instance,processVariables);
	}

}
