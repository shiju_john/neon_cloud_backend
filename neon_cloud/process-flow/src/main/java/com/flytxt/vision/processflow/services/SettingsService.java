package com.flytxt.vision.processflow.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class SettingsService extends AbstractService<Settings, Filter> {

	@Autowired
	public SettingsService(@Qualifier("Settings") VisionDao<Settings> dao) {
		super(dao);		
	}

	@Override
	public Pages<Settings> search(Filter criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException {
		return null;
	}

	@Override
	public Iterable<Settings> search(Filter criteria) throws VisionException {
		return null;
	}

	@Override
	public Settings deleteById(Serializable id) throws VisionException {
		return null;
	}
	
	public Iterable<Settings> findByKey(Settings config,Pages<Settings> pages) throws VisionException {
		return dao.findBy(config, null, new String[0]);				
	}


}
