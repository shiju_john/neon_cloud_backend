package com.flytxt.vision.processflow.connectors;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.filter.entity.FilterRules;
import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.runtime.TaskExecutor;
/**
 * 
 * @author shiju.john
 *
 */
@Component
public class OutConnectorExecutor implements TaskExecutor {
	
	ScriptEngineManager factory = new ScriptEngineManager();
	ScriptEngine engine = factory.getEngineByName("JavaScript");

	@Autowired
	KafkaProducer producer;

	@Override
	public Map<String, Object> execute(TaskInstance instance, Map<String, Object> processVariables) {
		Map<String, Object> result = new HashMap<>();
		processVariables.put("sentiment","positive");
		processVariables.put("context","call drop");
		processVariables.put("sentimentScore",10.5);
		
		ObjectMapper mapper = new ObjectMapper();
		byte json[];
		
		
		try {
			json = mapper.writeValueAsBytes(instance.getTaskProperty("filter"));
			Filter filter = mapper.readValue(json, Filter.class);
			applyFilter(filter,processVariables);
			
			
		} catch ( IOException |ScriptException e) {
			e.printStackTrace();
		}
		
		
		result.put("value", "OutConnector");
		return result;
	}

	boolean applyFilter(Filter filter, Map<String, Object> processVariables) throws ScriptException {

		
		String condition = filter.getCondition();
		Boolean result = null;
		setVariables(processVariables);
		for (FilterRules rule : filter.getRules()) { 	
			result = filter(condition,getOperator(rule.getKey() , rule.getOperator() ,rule.getValue()),result);
			if("OR".equalsIgnoreCase(condition) && result) {
				break;
			}
			
		}

		for (Filter filterGroup : filter.getInnergroups()) {
			result = "AND".equalsIgnoreCase(condition) ? result && applyFilter(filterGroup, processVariables)
					: result || applyFilter(filterGroup, processVariables);
			if("OR".equalsIgnoreCase(condition) && result) {
				break;
			}
		}
		return result;

	}
	
	private boolean filter(String condition,String expression,Boolean previousResult) throws ScriptException {
		boolean result = previousResult == null?"AND".equalsIgnoreCase(condition) ? true:false:previousResult;
		return  "AND".equalsIgnoreCase(condition) ?result && (boolean) engine.eval(expression) :
			result || (boolean) engine.eval(expression);		
	}

	private void setVariables(Map<String, Object> processVariables) throws ScriptException {
		for(Entry<String, Object> entry : processVariables.entrySet()) {			
			engine.eval(entry.getKey() +" = "+getValue(entry.getValue()));
		}
		
	}
	
	public String getValue(Object value ){
		return null!=value ? value instanceof Number ? ""+value: "\""+value+"\"":null;		
	}

	/**
	 * 
	 * @param operator
	 * @param value
	 * @param field
	 * @return
	 */
	private String getOperator(String field, String operator, Object value) {
		switch (operator) {
		case "Between":
			
			String args[] = String.valueOf(value).split(",");
			String splitedValue = args.length>0?args[1]:args[0];
			return field + "<=" + args[0] + " && " + field + " >=" + splitedValue;
		case "Contains":
			return field + ".search(\"" + value + "\") >=0";			
		case "=":
			return  field + "==" + "\""+value+"\"";
		default:
			return field + operator + getValue(value);
		}

	}

}
