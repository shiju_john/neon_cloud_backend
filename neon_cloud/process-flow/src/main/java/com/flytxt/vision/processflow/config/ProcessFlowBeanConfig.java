package com.flytxt.vision.processflow.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.flytxt.vision.processflow.ProcessFlow;
import com.flytxt.vision.processflow.connectors.FilterEvaluator;
import com.flytxt.vision.processflow.connectors.IncidentTrackerExecutor;
import com.flytxt.vision.processflow.connectors.OutConnectorExecutor;
import com.flytxt.vision.processflow.connectors.PersonalizationAgentExecutor;
import com.flytxt.vision.processflow.connectors.SentimentSystemExeccutor;
import com.flytxt.vision.processruntime.KnowledgeBase;
import com.flytxt.vision.processruntime.runtime.KnowledgeRuntime;
/**
 * 
 * @author shiju.john
 *
 */
@Configuration
public class ProcessFlowBeanConfig {
	
	@Autowired
	KnowledgeRuntime knowledgeRuntime;
	
	@Autowired
	KnowledgeBase knowledgeBase;
	
	
	
	@Autowired
	OutConnectorExecutor outConnector;
	
	@Autowired
	PersonalizationAgentExecutor pAgent;
	
	@Autowired
	SentimentSystemExeccutor sentimentAgent;
	
	@Autowired
	IncidentTrackerExecutor incidentAgent;
	
	@Autowired
	FilterEvaluator ruleEvalator;
	
	@Bean
	@Scope("singleton")
    public ProcessFlow processFlowBean() {		
		ProcessFlow flow =  new ProcessFlow();		
		register();
		return flow;
    }
	
	private void register() {
	
		knowledgeRuntime.addTaskExcecutor("personalization", pAgent);
		knowledgeRuntime.addTaskExcecutor("sentimentAnalyser", sentimentAgent);
		knowledgeRuntime.addTaskExcecutor("outConnector", outConnector);		
		knowledgeRuntime.addTaskExcecutor("incidentTracker",incidentAgent );		
		knowledgeRuntime.addRuleEvaluator("filter",ruleEvalator );
	}

}
