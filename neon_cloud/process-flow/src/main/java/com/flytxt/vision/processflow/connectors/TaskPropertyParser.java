package com.flytxt.vision.processflow.connectors;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.utils.HelperUtils;

/**
 * 
 * @author shiju.john
 *
 */
public class TaskPropertyParser {

	public static String getServiceUrl(TaskInstance instance) {
		return (String) instance.getTaskProperty("serviceUrl");
	}

	public static String getMethodType(TaskInstance instance) {
		return (String) instance.getTaskProperty("requestMethod");
	}

	@SuppressWarnings({ "rawtypes" })

	public static HttpHeaders getHeader(TaskInstance instance, Map processVariables) {

		HttpHeaders headers = new HttpHeaders();
		String contentType = (String) instance.getTaskProperty("contentType");
		headers.setContentType(
				"application/json".equalsIgnoreCase(contentType) ? MediaType.APPLICATION_JSON :MediaType.TEXT_PLAIN );

		Map header = new HashMap<>();
		Object headerObj = instance.getTaskProperty("header");
		if (headerObj instanceof Map) {
			header = (Map) headerObj;

		}
		String authorizationType = (String) header.get("authorization");
		if (HelperUtils.isNotNull(authorizationType)) {
			headers.add("Authorization", getEncodedOauth(authorizationType, (String) header.get("userName"),
					(String) header.get("password"), (String) header.get("token")));
		}
		
		getParameterValue(header.get("parameters"), processVariables).forEach((key,value)->{
			headers.add(key, value);
		});
		
		/*List headerParameters = null;
		Object headerParam = header.get("parameters");
		if (headerParam instanceof List) {
			headerParameters = (List) headerParam;

		}
		if (null != headerParameters && !headerParameters.isEmpty()) {
			for (Object paramMap : headerParameters) {
				String key = "" + ((Map) paramMap).get("id");
				String valueType = (String) ((Map) paramMap).get("valueType");
				String value = (String) ((Map) paramMap).get("value");
				headers.add(key, "PV".equalsIgnoreCase(valueType) ? "" + processVariables.get(value) : value);

			}
		}*/
		return headers;

	}

	@SuppressWarnings("rawtypes")
	public static Map getPathParms(TaskInstance instance, Map processVariables) {

		Map propertyMap = new HashMap<>(2);
		Object headerObj = instance.getTaskProperty("pathParams");
		if (headerObj instanceof Map) {
			propertyMap = (Map) headerObj;
		}

		return getParameterValue(propertyMap.get("parameters"), processVariables);		

	}

	@SuppressWarnings("rawtypes")
	private static Map<String, String> getParameterValue(Object parameters, Map processVariables) {

		Map<String, String> parameterMap = new HashMap<>();

		List parameterList = null;
		if (parameters instanceof List) {
			parameterList = (List) parameters;
		}

		if (null != parameterList && !parameterList.isEmpty()) {
			for (Object paramMap : parameterList) {
				String key = "" + ((Map) paramMap).get("id");
				String valueType = (String) ((Map) paramMap).get("valueType");
				String value = (String) ((Map) paramMap).get("value");
				parameterMap.put(key, "PV".equalsIgnoreCase(valueType) ? "" + processVariables.get(value) : value);

			}
		}
		return parameterMap;

	}

	@SuppressWarnings("rawtypes")
	public static Map getQueryParms(TaskInstance instance, Map processVariables) {
		
		Map propertyMap = new HashMap<>(2);
		if (instance.getTaskProperty("queryParams") instanceof Map) {
			propertyMap = (Map) instance.getTaskProperty("queryParams");
		}
		return getParameterValue(propertyMap.get("parameters"), processVariables);	
		

	}

	private static String getEncodedOauth(String authorizationType, String user, String secrate, String token) {
		switch (authorizationType) {
		case "Basic":
			return "Basic " + Base64.getEncoder().encodeToString((user + ":" + secrate).getBytes());
		case "Bearer":
			return "Bearer " + token;
		default:
			return null;
		}
	}

}
