/**
 * 
 */
package com.flytxt.vision.processflow.connectors;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.flytxt.vision.processflow.entity.AuthenticationResponse;

/**
 * @author athul
 *
 */
public class SalesforceOutconnector {
	
    public String createRecord(Map<String, String> payload, Map<String, String> mapping, String object, AuthenticationResponse authResponse) {
        // Create a Record
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + authResponse.getAccess_token());

        Map<String, String> record = matchKeys(mapping, payload);
        
        final HttpEntity<Object> requestEntity = new HttpEntity<>(record, headers);
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> responseEntity = restTemplate.exchange(authResponse.getInstance_url() + "/services/data/v44.0/sobjects/" + object + "/", HttpMethod.POST, requestEntity,
                String.class);
        return responseEntity.getBody();
    }
    
    private Map<String, String> matchKeys(Map<String, String> keySet, Map<String, String> valueSet) {
    	Map<String, String> record = new HashMap<String, String>();
        for(String fbKey: keySet.keySet()) {
        	record.put(keySet.get(fbKey), valueSet.getOrDefault(fbKey, ""));
        }
        return record;
    }
    
    public static void main(String[] args) {
		SalesforceOutconnector connector = new SalesforceOutconnector();
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("ID", "432432424324");
		payload.put("FB Name", "Dummy FB User");
		payload.put("Context", "call drop");
		payload.put("Sentiment Score", "-3");
		payload.put("Timestamp", Long.toString(System.currentTimeMillis()));
		
		Map<String, String> keySet = new HashMap<String, String>();
		keySet.put("ID", "AccountID");
		keySet.put("FB Name", "Name");
		keySet.put("Context", "CreatedByID");
		keySet.put("Sentiment Score", "Deleted");
		keySet.put("Timestamp", "CreatedDate");


	    AuthenticationResponse authResponse = new AuthenticationResponse();
	    
		authResponse.setAccess_token("00D0o000001C57z!AQkAQGki._Nkwl.W.bbDIbf22O4jwrKfI8p6LpCD6IXifls09nUKzy5Vd6tagL5cveYhZwwdLif3u7tX36LdJH76jiyMV.mv");
		authResponse.setInstance_url("https://ap8.salesforce.com");
		connector.createRecord(payload, keySet, "Account", authResponse);
	}
}
