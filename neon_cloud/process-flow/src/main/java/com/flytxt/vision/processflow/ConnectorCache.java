package com.flytxt.vision.processflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.Settings;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.processflow.connectors.KafkaProducer;
import com.flytxt.vision.processflow.services.PageMappingService;
import com.flytxt.vision.processflow.services.SettingsService;
/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope("singleton")
public class ConnectorCache {
		
	
	
	
	private SettingsService initService;
	
	private static String SENTIMENT_URL=null;
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
	
	/**
	 * 
	 */
	@Autowired
	public ConnectorCache(PageMappingService pageMappingService,SettingsService initService){	
		this.initService = initService;
		SENTIMENT_URL= getUrl();
		
	}

	private String getUrl() {
		Settings  config = new Settings();
		config.setKey("SENTIMENT_URL");
		Iterable<Settings> rows;
		try {
			rows = initService.findByKey(config, null);
			if( null!= rows && rows.iterator().hasNext()) {
				return rows.iterator().next().getValue();
			}
		} catch (VisionException e) {
			logger.error("Sentiment System URL is not configured",e);
		}
		
		logger.error("Sentiment System URL is not configured");
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String getSentimentUrl() {
		return SENTIMENT_URL;
	}
	
}
