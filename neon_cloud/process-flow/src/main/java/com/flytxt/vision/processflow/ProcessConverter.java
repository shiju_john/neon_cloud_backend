package com.flytxt.vision.processflow;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.processruntime.entity.ProcessDefinition;
import com.google.gson.Gson;

/**
 * 
 * @author shiju.john
 *
 */
public class ProcessConverter {
	
	static final Gson gson = new Gson();
	
	public static ProcessDefinition convertToProcess(ProcessFlowEntity entity) {
		
		ProcessDefinition processDefinition = gson.fromJson(entity.getProcessConfig(),ProcessDefinition.class);	
		processDefinition.setProcessId(entity.getProcessId());
		processDefinition.setProcessName(entity.getProcessName());
		processDefinition.setProcessVersion(entity.getProcessVersion());
		processDefinition.setTenantId(entity.getTenantId());
		return processDefinition;
	}
	
	
	public static void main(String[] args) {
		ProcessFlowEntity entity =  new ProcessFlowEntity();
				entity.setProcessConfig(
						"{\"displayProperties\":{\"centerX\":1024,\"centerY\":80,\"scale\":1,\"elements\":"
						+ "[{\"id\":2,\"x\":-1000,\"y\":15},{\"id\":6,\"x\":-269,\"y\":25},{\"id\":7,\"x\":-641,\"y\":30}]},"
						+ "\"processVariable\":[],\"tasks\":[{\"id\":2,\"taskType\":\"startEvent\","
						+ "\"taskName\":\"Start Event\",\"taskLabel\":\"Start\",\"startEvent\":true,\""
						+ "onEntry\":\"\",\"onExit\":\"\",\"dataInput\":[],\"dataOutput\":[],\"taskProperties\""
						+ ":{\"category\":\"Event\"}},{\"id\":6,\"startEvent\":false,\"taskName\":\"End\",\"taskLabel\""
						+ ":\"End\",\"taskType\":\"stopEvent\",\"onEntry\":\"\",\"onExit\":\"\",\"dataInput\":"
						+ "[],\"dataOutput\":[],\"taskProperties\":{\"category\":\"Event\",\"outputasks\":[{\"context\":"
						+ "\"\",\"sentiment\":\"\",\"action\":null}],\"action\":null,\"pathParams\":{\"parameters\":[]},"
						+ "\"queryParams\":{\"parameters\":[]},\"header\":{\"parameters\":[]}}},{\"id\":7,\"startEvent\""
						+ ":false,\"taskName\":\"Exclusive OR\",\"taskLabel\":\"Exclusive OR\",\"taskType\":"
						+ "\"exclusiveOr\",\"onEntry\":\"\",\"onExit\":\"\",\"dataInput\":[],\"dataOutput\""
						+ ":[],\"taskProperties\":{\"category\":\"GateWay\",\"outputasks\":[{\"context\":"
						+ "\"\",\"sentiment\":\"\",\"action\":null}],\"action\":null,\"pathParams\":"
						+ "{\"parameters\":[]},\"queryParams\":{\"parameters\":[]},\"header\":"
						+ "{\"parameters\":[]}}}],\"sequenceFlows\":[{\"id\":1,\"fromPosition\":"
						+ "\"right\",\"from\":2,\"to\":7,\"toPosition\":\"left\",\"filter\":{}},"
						+ "{\"id\":2,\"fromPosition\":\"right\",\"from\":7,\"to\":6,\"toPosition\":"
						+ "\"left\",\"filter\":{}}]}"
						);
				convertToProcess(entity);
	}

}
