package com.flytxt.vision.processflow.services;

import java.io.Serializable;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class ContextCloudFunctionService extends AbstractService<ContextEntity, Filter>{

	@Autowired
	public ContextCloudFunctionService(@Qualifier("ContextEntity") VisionDao<ContextEntity> dao) {
		super(dao);
	}

	@Override
	public Pages<ContextEntity> search(Filter criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException {
		return null;
	}

	@Override
	public Iterable<ContextEntity> search(Filter criteria) throws VisionException {
		return null;
	}

	@Override
	public ContextEntity deleteById(Serializable id) throws VisionException {
		return null;
	}
	
	/**
	 * 
	 * @param name
	 * @param ten
	 * @return
	 * @throws VisionException
	 */
	public ContextEntity findByContextName(String name,String tenantId) throws VisionException {
		ContextEntity entity = new ContextEntity();
		entity.setName(name);
		entity.setTenantId(tenantId);		
		Iterable<ContextEntity> contextEntity = dao.findBy(entity, null,  "byName");
		ContextEntity defaultContext =null;
		if (null!= contextEntity && contextEntity.iterator().hasNext()) {
			for(Iterator<ContextEntity > entities = contextEntity.iterator();entities.hasNext();) {
				ContextEntity  cEntity = entities.next();
				if (cEntity.getTenantId() ==null || "null".equalsIgnoreCase(cEntity.getTenantId())) {
					defaultContext = cEntity;
				}else if(cEntity.getTenantId().equalsIgnoreCase(tenantId)) {
					return cEntity;
				}
			}
			
		}
		return  defaultContext;
	}
	

}
