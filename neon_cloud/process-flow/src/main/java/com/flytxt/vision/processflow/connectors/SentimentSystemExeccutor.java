package com.flytxt.vision.processflow.connectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.flytxt.vision.aws.entity.ContextEntity;
import com.flytxt.vision.processflow.ConnectorCache;
import com.flytxt.vision.processflow.services.ContextCloudFunctionService;
import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.exception.TaskException;
import com.flytxt.vision.processruntime.runtime.TaskExecutor;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
/**
 * 
 * @author shiju.john
 *
 */
@Component
public class SentimentSystemExeccutor implements TaskExecutor {

	WebserviceTemplate template =  new WebserviceTemplate();
	Gson gson = new Gson();
	
	@Autowired
	ConnectorCache cache;
	
	@Autowired
	ContextCloudFunctionService service;
	
	@Override
	public Map<String,Object> execute(TaskInstance instance, Map<String, Object> processVariables) throws TaskException {
		
		
		HashMap<String,Object>	result = 	new HashMap<>()	;
		JsonObject body =  new JsonObject();			
		body.addProperty("sentence", (String)processVariables.get("tweet"));
		String httpBody = null!=body ? body.toString():null;
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType( MediaType.APPLICATION_JSON );
		
		Object results;
		try {
			results = template.consumePostRequest(cache.getSentimentUrl(),httpBody,headers,new HashMap<>());		
			result.put("result", results);
			
					
			LinkedHashMap contextMap = (LinkedHashMap<String,Object>)results;
			ArrayList<LinkedHashMap<String, Object>> contextList = (ArrayList)contextMap.get("context");
			
			for(LinkedHashMap<String,Object> context: contextList) {
				String contextName = (String)context.get("context");
				String sentiment = (String)context.get("sentiment");
				ContextEntity contextEntity = service.findByContextName(contextName, (String)processVariables.get("tenant_id"));
				
				if (null!=contextEntity) {
					contextEntity.setCount(contextEntity.getCount()+1);
					Map<String, Object> detailsMap = gson.fromJson(contextEntity.getCountDetails(), new TypeToken<Map<String, Object>>() {
					}.getType());
					
					JsonObject jsonObject = new JsonObject();					
					if (null!=sentiment) {
						Double count =(Double) detailsMap.get(sentiment);
						detailsMap.put(sentiment, count+1);						
						contextEntity.setCountDetails(gson.toJson(detailsMap, Map.class));
					}
				}else {
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty(sentiment, 1L);
					contextEntity =  new ContextEntity();
					contextEntity.setName(contextName);
					contextEntity.setCount(1L);
					contextEntity.setTenantId((String)processVariables.get("tenant_id"));
					contextEntity.setCountDetails(jsonObject.toString());
					
				}
				service.save(contextEntity);				
			}
		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}

}
