package com.flytxt.vision.processflow.connectors;

import java.util.Map;
import java.util.Map.Entry;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.stereotype.Component;

import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.filter.entity.FilterRules;
import com.flytxt.vision.processruntime.exception.RuleException;
import com.flytxt.vision.processruntime.runtime.RuleEvalator;

/**
 * 
 * @author shiju.john
 *
 */

@Component
public class FilterEvaluator implements RuleEvalator{
	
	private ScriptEngineManager factory = new ScriptEngineManager();
	private ScriptEngine engine = factory.getEngineByName("JavaScript");

	
	private boolean filter(String condition,String expression,Boolean previousResult) throws ScriptException  {
		boolean result = previousResult == null?"AND".equalsIgnoreCase(condition) ? true:false:previousResult;		
		return  "AND".equalsIgnoreCase(condition) ?result && (boolean) engine.eval(expression) :
				result || (boolean) engine.eval(expression);	
			
		
			
	}

	private void setVariables(Map<String, Object> processVariables) throws ScriptException {
		for(Entry<String, Object> entry : processVariables.entrySet()) {			
			engine.eval(entry.getKey() +" = "+getValue(entry.getValue()));
		}
		
	}
	
	private String getValue(Object value ){
		return null!=value ? value instanceof Number ? ""+value: "\""+value+"\"":null;		
	}

	/**
	 * 
	 * @param operator
	 * @param value
	 * @param field
	 * @return
	 */
	private String getOperator(String field, String operator, Object value) {
		switch (operator) {
		case "Between":			
			String args[] = String.valueOf(value).split(",");
			String splitedValue = args.length>0?args[1]:args[0];
			return field + "<=" + args[0] + " && " + field + " >=" + splitedValue;
		case "Contains":
			return field + ".search(\"" + value + "\") >=0";			
		case "=":
			return  field + "==" + "\""+value+"\"";
		default:
			return field + operator + getValue(value);
		}
	}


	@Override
	public boolean evaluateRule(Object ruleobject, Map<String, Object> processVariables) throws RuleException {
		Filter filter = (Filter)ruleobject;
		String condition = filter.getCondition();
		Boolean result = null;
		try {
			setVariables(processVariables);
			for (FilterRules rule : filter.getRules()) { 	
				result = filter(condition,getOperator(rule.getKey() , rule.getOperator() ,rule.getValue()),result);
				if("OR".equalsIgnoreCase(condition) && result) {
					break;
				}			
			}
			for (Filter filterGroup : filter.getInnergroups()) {
				result = "AND".equalsIgnoreCase(condition) ? result && evaluateRule(filterGroup, processVariables)
						: result || evaluateRule(filterGroup, processVariables);
				if("OR".equalsIgnoreCase(condition) && result) {
					break;
				}
			}
			return result;
		}catch (ScriptException e) {
			throw new RuleException(e.getLocalizedMessage(),e);
		}
		
	}


}
