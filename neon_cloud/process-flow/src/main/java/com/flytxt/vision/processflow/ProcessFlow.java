package com.flytxt.vision.processflow;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.flytxt.vision.aws.entity.ProcessFlowEntity;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.processflow.services.ProcessServices;
import com.flytxt.vision.processruntime.entity.ProcessDefinition;
import com.flytxt.vision.processruntime.exception.ProcessCacheException;
import com.flytxt.vision.processruntime.exception.ProcessException;
import com.flytxt.vision.processruntime.runtime.ProcessRuntime;

/**
 * 
 * @author shiju.john
 *
 */
public class ProcessFlow {
	
	private static final String PROCESS_STATUS_ACTIVE ="Active";
	
	@Autowired
	private ProcessRuntime processRuntime;
	
	@Autowired
	private ProcessServices processServices;
	

		
	public ProcessFlow(){		
		
	}
	
	/**
	 * 
	 * @param processId
	 * @param version
	 * @param globalVariables
	 * @throws ProcessException
	 */
	public void startProcess(String processId,String version,Map<String,Object> globalVariables) throws ProcessException {
		if(!processRuntime.hasProcessLoaded(processId, version)) {
			loadProcess(processId, version);
		}
		processRuntime.startProcess(processId, version, globalVariables);
	}

	/**
	 * 
	 * @param processId
	 * @param version
	 * @throws ProcessCacheException
	 */
	private void loadProcess(String processId, String version) throws ProcessCacheException {
		ProcessFlowEntity entity = new ProcessFlowEntity();
		entity.setProcessId(processId);
		entity.setProcessVersion(version);
		entity.setStatus(PROCESS_STATUS_ACTIVE);
		try {
			entity = processServices.findByProcessIdAndProcessVersionAndStatus(entity);
		} catch (VisionException e) {
			throw new ProcessCacheException("Unable to load Process with Id "+processId);
		}
		ProcessDefinition process = ProcessConverter.convertToProcess(entity);		
		processRuntime.addProcess(process);			
	}
	
	
	public void registerTaskHandler() {
	
	}	

}
