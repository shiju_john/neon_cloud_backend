package com.flytxt.vision.processflow.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.flytxt.vision.aws.entity.PageMapping;
import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.Pages;
import com.flytxt.vision.core.exception.VisionException;
import com.flytxt.vision.core.filter.entity.Filter;
import com.flytxt.vision.core.service.AbstractService;
/**
 * 
 * @author shiju.john
 *
 */
@Service
public class PageMappingService extends AbstractService<PageMapping, Filter>{
	
//	private Gson gson = new Gson();
	
	@Autowired
	public PageMappingService(@Qualifier("PageMapping") VisionDao<PageMapping> dao) {
		super(dao);
	}

	@Override
	public Pages<PageMapping> search(Filter criteria, int pageNo, int pageSize, String sortField, String sortOrder)
			throws VisionException {
		return null;
	}
	
	@Override
	public Iterable<PageMapping> search(Filter criteria) throws VisionException {
		return null;
	}
	
	@Override
	public PageMapping deleteById(Serializable id) throws VisionException {
		return null;
	}

	

	/**
	 * 
	 * @param pageId
	 * @return 
	 * @throws VisionException 
	 */
	public Iterable<PageMapping> findByPageId(String pageId) throws VisionException {
		PageMapping pageMapping = new PageMapping();
		pageMapping.setPageId(pageId);
		return dao.findBy(pageMapping, null, "byPageId");
		
	}
}
