package com.flytxt.vision.processflow.connectors;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.flytxt.vision.processruntime.entity.TaskInstance;
import com.flytxt.vision.processruntime.exception.TaskException;
import com.google.gson.JsonObject;
/**
 * 
 * @author shiju.john
 *
 */
@Component
@Scope(value="singleton")
public class WebserviceTemplate {
	
	private RestTemplate template;
	
	
	WebserviceTemplate(){
		template  = new RestTemplate() ;
		template.setErrorHandler(new DefaultResponseErrorHandler());
	}
	
	/**
	 * 
	 * @param url
	 * @param body
	 * @param headers
	 * @param pathParams
	 * @return
	 * @throws ResourceAccessException
	 * @throws Exception
	 */
	public Object consumeGetRequest(String url,Object body,HttpHeaders headers,
			Map<String,String> pathParams) 	 {
		HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);		
		ResponseEntity response = headers.getContentType().equals(MediaType.TEXT_PLAIN)
				? response = template.exchange(url, HttpMethod.GET, entity, String.class, pathParams)
				: template.exchange(url, HttpMethod.GET, entity, Object.class, pathParams);

		return response.getBody();
	}
	
	/**
	 * 
	 * @param url
	 * @param body
	 * @param headers
	 * @param pathParams
	 * @return
	 * @throws ResourceAccessException
	 * @throws Exception
	 */
	public Object consumePostRequest(String url,Object body,HttpHeaders headers,
			Map<String,String> pathParams)
			throws ResourceAccessException, Exception {		
		HttpEntity<Object> entity = new HttpEntity<Object>(body, headers); 
		ResponseEntity response = headers.getContentType().equals(MediaType.TEXT_PLAIN)
				? response = template.exchange(url, HttpMethod.POST, entity, String.class, pathParams)
				: template.exchange(url, HttpMethod.POST, entity, Object.class, pathParams);
		return response.getBody();
		
	}
	
	/**
	 * 
	 * @param url
	 * @param body
	 * @param headers
	 * @param pathParams
	 * @return
	 * @throws ResourceAccessException
	 * @throws Exception
	 */
	public Object consumePutRequest(String url,Object body,HttpHeaders headers,
			Map<String,String> pathParams)
			throws ResourceAccessException, Exception {		
		HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);
		ResponseEntity response = headers.getContentType().equals(MediaType.TEXT_PLAIN)
				? response = template.exchange(url, HttpMethod.PUT, entity, String.class, pathParams)
				: template.exchange(url, HttpMethod.PUT, entity, Object.class, pathParams);
		

		return response.getBody();
		
	}

	public Map<String, Object> execute(TaskInstance instance, Map<String, Object> processVariables) throws TaskException {
		try{
			String serviceUrl = TaskPropertyParser.getServiceUrl(instance);  
			String methodType = TaskPropertyParser.getMethodType(instance); 
			
			Map<String,String> pathParams = TaskPropertyParser.getPathParms(instance, processVariables);
			Map<String,String> queryParams = TaskPropertyParser.getQueryParms(instance, processVariables);
			
			JsonObject body = null;			
			Set<String> inputProperties = instance.getTaskInputKeys();
			if (null!=inputProperties && !inputProperties.isEmpty()) {
				body =  new JsonObject();
				for(String key : inputProperties) {
					body.addProperty(key, ""+instance.getTaskInputValue(key));
				} 
			}
			
			if(queryParams!=null && !queryParams.isEmpty() ) {			
				serviceUrl = getQueryString(serviceUrl, queryParams, pathParams);			
			}
				
			String jsonBody = null!=body ? body.toString():null;
			HttpHeaders headers = TaskPropertyParser.getHeader(instance,processVariables);			
			Object results = "GET".equalsIgnoreCase(methodType)?consumeGetRequest(serviceUrl, jsonBody, headers, pathParams):
				"POST".equalsIgnoreCase(methodType)?consumePostRequest(serviceUrl, jsonBody, headers, pathParams):
					"PUT".equalsIgnoreCase(methodType)?consumePutRequest(serviceUrl, jsonBody, headers, pathParams):null;
			HashMap<String,Object>	result = 	new HashMap<>()	;
			result.put("result", results);
			return result;
		}catch (Exception e) {
			throw new TaskException("Error while executing the Task "+instance.getTaskId(),e);
		}
		
		
	}
	
	/**
	 * 
	 * @param serviceUrl
	 * @param queryParams
	 * @param pathParams
	 * @return
	 */
	private String getQueryString(String serviceUrl,Map<String,String> queryParams,Map<String,String> pathParams) {
		try { 
			String url [] = serviceUrl.split("\\?");			
			String queryString = url.length > 1 ? url [1] : null;
			if(null!=queryString) {
				Map<String,String> params = splitQuery(queryString);
				
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url[0]);
				for (Entry<String, String> entry: params.entrySet()) {					
					 String value = "$value".equalsIgnoreCase(entry.getValue()) ? 
							 queryParams.get(entry.getKey()): entry.getValue();
					builder.queryParam(entry.getKey(), value);
				}
				return  builder.buildAndExpand(pathParams).toUri().toString();
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return serviceUrl;
		
		
	}

	public static Map<String,String> splitQuery(String url) throws UnsupportedEncodingException  {
		  final Map<String,String> query_pairs = new LinkedHashMap<String,String>();
		  final String[] pairs = url.split("&");
		  for (String pair : pairs) {
		    final int idx = pair.indexOf("=");
		    final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
		    final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
			
		    if (!query_pairs.containsKey(key)) {
		      query_pairs.put(key, value);
		    }
		  }
		  return query_pairs;
		}
	
	
	public static void main(String[] args) {
		try {
			 String user="";
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
//			headers.add("Authorization", encodedAuth);
			/*JsonObject jsonObject = new JsonObject();			
			jsonObject.addProperty("processNamae", "Test Service");
			jsonObject.addProperty("processVersion", "1");*/
			 Map<String,String> pathParm = new HashMap<>();
//			 pathParm.put("phno", "123");
			 Object obj = new WebserviceTemplate().consumeGetRequest("http://localhost:9091/vision/dummy/sentimentAnalyser?text=test",null,headers,pathParm);
		} catch (ResourceAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
