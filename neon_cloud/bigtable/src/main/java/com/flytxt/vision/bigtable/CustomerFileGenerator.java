package com.flytxt.vision.bigtable;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomerFileGenerator {
	
	void writeFile(){
		try {
			FileWriter csvWriter = new FileWriter("/home/fly/Desktop/Keys/input/customer/customerSeqment.csv");
			csvWriter.append("CustomerId");  
			csvWriter.append(",");  
//			csvWriter.append("CustomerName");  
//			csvWriter.append(",");  
//			csvWriter.append("Category");  
//			csvWriter.append(",");  
//			csvWriter.append("Location");  
//			csvWriter.append(",");  
//			csvWriter.append("Pincode");  
//			csvWriter.append(",");  
//			csvWriter.append("Email");  
//			csvWriter.append(",");  
//			csvWriter.append("FaceBookId");  
//			csvWriter.append(",");  
			csvWriter.append("MobileNo");  
			csvWriter.append("\n");
			
			for (int i=100000;i<1110000;i++) {  
				List<String> data = new ArrayList<>();
				data.add(""+i);
//				data.add("CustomerName"+i);
//				data.add("Category"+i);				
//				data.add("Location"+i);
//				data.add("686661");
//				data.add("abc"+i+"@mail.com");
				data.add("98"+i);
//				data.add("12345678910");
				
			    csvWriter.append(String.join(",", data));
			    csvWriter.append("\n");
			}

			csvWriter.flush();  
			csvWriter.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
	}
	public static void main(String[] args) {
		new CustomerFileGenerator().writeControlGroupFile();
		new CustomerFileGenerator().writeFile();
	}
	private void writeControlGroupFile() {
		try {
			FileWriter csvWriter = new FileWriter("/home/fly/Desktop/Keys/input/customer/customerControlGroup.csv");
			csvWriter.append("CustomerId");  
			csvWriter.append(",");  
			csvWriter.append("MobileNo");  
			csvWriter.append("\n");
			
			for (int i=100000;i<1110000;i=i+10) {  
				List<String> data = new ArrayList<>();
				data.add(""+i);		
				data.add("98"+i);				
			    csvWriter.append(String.join(",", data));
			    csvWriter.append("\n");
			}

			csvWriter.flush();  
			csvWriter.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
	}
	
	private void writeProductFile() {
		try {
			FileWriter csvWriter = new FileWriter("/home/fly/Desktop/Keys/input/product/product101_10000.csv");
			csvWriter.append("ProductId");  
			csvWriter.append(",");  
			csvWriter.append("ProductName");  
			csvWriter.append(",");  
			csvWriter.append("ProductDescription");  
			  csvWriter.append("\n");
			
			for (int i=101;i<10000;i++) {  
				List<String> data = new ArrayList<>();
				data.add(""+i);		
				data.add("Name "+i);
				data.add("Description "+i);				
				
			    csvWriter.append(String.join(",", data));
			    csvWriter.append("\n");
			}

			csvWriter.flush();  
			csvWriter.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
	}


}
