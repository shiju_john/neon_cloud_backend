package com.flytxt.vision.bigtable.entity;

import java.util.List;

import com.flytxt.vision.core.entity.VisionEntity;
/**
 * 
 * @author shiju.john
 *
 */
public abstract class BigTableEntity implements VisionEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String rowId;
	
	private List<ColumnFamily> datas;

	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}

	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return the datas
	 */
	public List<ColumnFamily> getDatas() {
		return datas;
	}

	/**
	 * @param datas the datas to set
	 */
	public void setDatas(List<ColumnFamily> datas) {
		this.datas = datas;
	}

	

}
