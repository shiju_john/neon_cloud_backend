package com.flytxt.vision.bigtable;

import java.io.IOException;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
/**
 * 
 * @author shiju.john
 *
 */
public class BigTableConnection {
	
	private static  Connection connection ;
	private static volatile BigTableConnection bigTableConnection = null;
	
	private BigTableConnection() throws IOException {
		connection = ConnectionFactory.createConnection(HBaseConfiguration.create());		
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */	
	public static BigTableConnection getInstance() throws IOException {
		
		if(null==bigTableConnection) {
			synchronized (BigTableConnection.class) {
				if(null==bigTableConnection) {
					bigTableConnection= new BigTableConnection();
				}				
			}			
		}
		return bigTableConnection;		
		
	}
	
	public void close() {
		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public Admin getAdmin() throws IOException {
		return connection.getAdmin();
	}
	
	/**
	 * 
	 * @param tableName
	 * @return
	 * @throws IOException
	 */
	public Table getTable(String tableName) throws IOException {
		return connection.getTable(TableName.valueOf(tableName));
	}

}
