package com.flytxt.vision.bigtable;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;

/**
 * 
 * @author shiju.john
 *
 */
public class BigTableDdlService {
	
	private BigTableConnection bigTableConnection ;
	
	public BigTableDdlService() throws IOException{
		bigTableConnection = BigTableConnection.getInstance();
	}
	
	
	/**
	 * 
	 * @param tableName
	 * @param columnFamilies
	 * @throws IOException
	 */
	public void createTable( String tableName,List<String> columnFamilies) throws IOException {
	  Admin admin = bigTableConnection.getAdmin();
      HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
      for (String colFamily : columnFamilies) {
          tableDescriptor.addFamily(new HColumnDescriptor(colFamily));
      }
      admin.createTable(tableDescriptor);		
	}
	
	/**
	 * 
	 * @param tableId
	 * @throws IOException
	 */
	public void deleteTable(String tableId) throws IOException {		
		Admin admin = bigTableConnection.getAdmin();
        admin.deleteTable(TableName.valueOf(tableId));
		
	}
	
	

}
