package com.flytxt.vision.bigtable;

import java.io.IOException;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.springframework.data.repository.CrudRepository;

import com.flytxt.vision.core.dao.VisionDao;
import com.flytxt.vision.core.entity.VisionEntity;

public class BigTableService extends VisionDao<VisionEntity> {

	public BigTableService(CrudRepository crudRepository) {
		super(crudRepository);
		
	}
	
	public void scan(String tableName) {
		 


        Table table;
		try {
			table = BigTableConnection.getInstance().getTable(tableName);
			Scan scan = new Scan();
			 ResultScanner resultScanner = table.getScanner(scan);
	           for (Result result : resultScanner) {
	               for (Cell cell : result.listCells()) {
	                   String row = new String(CellUtil.cloneRow(cell));
	                   String family = new String(CellUtil.cloneFamily(cell));
	                   String column = new String(CellUtil.cloneQualifier(cell));
	                   String value = new String(CellUtil.cloneValue(cell));
	                   long timestamp = cell.getTimestamp();
	               }
	           }
		} catch (IOException e) {
			e.printStackTrace();
		}

           // Create a new Scan instance.
          

           // This command supports using a columnvalue filter.
           // The filter takes the form of <columnfamily>:<column><operator><value>
           // An example would be cf:col>=10
//           if (filterVal != null) {
//               String splitVal = "=";
//               CompareFilter.CompareOp op = CompareFilter.CompareOp.EQUAL;
//
//               if (filterVal.contains(">=")) {
//                    op = CompareFilter.CompareOp.GREATER_OR_EQUAL;
//                    splitVal = ">=";
//               } else if (filterVal.contains("<=")) {
//                    op = CompareFilter.CompareOp.LESS_OR_EQUAL;
//                    splitVal = "<=";
//               } else if (filterVal.contains(">")) {
//                    op = CompareFilter.CompareOp.GREATER;
//                    splitVal = ">";
//               } else if (filterVal.contains("<")) {
//                    op = CompareFilter.CompareOp.LESS;
//                    splitVal = "<";
//               }
//               String[] filter = filterVal.split(splitVal);
//               String[] filterCol = filter[0].split(":");
//               scan.setFilter(new SingleColumnValueFilter(filterCol[0].getBytes(), filterCol[1].getBytes(), op, filter[1].getBytes()));
//           }
          
	}

}
