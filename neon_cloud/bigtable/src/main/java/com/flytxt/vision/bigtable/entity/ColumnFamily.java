package com.flytxt.vision.bigtable.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ColumnFamily  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String columnFamilyName;
	
	private Map<String,Object> columns;
	
	public ColumnFamily(){
		this.columns = new HashMap<>();
	}
	
	

	/**
	 * @return the columnFamilyName
	 */
	public String getColumnFamilyName() {
		return columnFamilyName;
	}

	/**
	 * @param columnFamilyName the columnFamilyName to set
	 */
	public void setColumnFamilyName(String columnFamilyName) {
		this.columnFamilyName = columnFamilyName;
	}

	/**
	 * @return the columns
	 */
	public Map<String,Object> getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(Map<String,Object> columns) {
		this.columns = columns;
	}
	

}
